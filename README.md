# **Integrated Urban Water Model (IUWM)**

A Highly-Automated Municipal Water Demand Planning Tool

The Integrated Urban Water Model (IUWM) forecasts water demand given changes to population, land use, and climate. It also evaluates various demand management strategies including more efficient home appliances, behavior and landscape changes to outdoor irrigation, and use of alternative water sources such as reclaimed wastewater, graywater, stormwater, and roof runoff. IUWM has been used to assess land development impacts on water use, water requirements, alternative water sources and end-uses. The model also forecasts water use and costs based on user-provided building patterns, efficiency gains through technology or behavioral changes, and water recycling. It contains multiple submodels for representing both indoor and outdoor uses, both CII and residential uses, and handles input changes to land uses. IUWM is packaged with automated calibration procedures, parameter sensitivity analyses, and custom coding capabilities for user-specified submodels. Output includes forecasted water use, potable water demand, indoor and outdoor water use, CII and residential water use, local water sources (generation and use of graywater, wastewater, stormwater, and roof runoff), leaks, and outflow to wastewater treatment plants. Simulations are at a daily timestep, and outputs are automatically aggregated to monthly and annual timesteps.

## Installation

1. Download and install [Anaconda (with Python version 2.7)](https://www.continuum.io/downloads). For manual installation of packages, get Python 2.7 with packages: [pandas](https://pypi.python.org/pypi/pandas/), [numpy]https://pypi.python.org/pypi/numpy), and [SALib](https://pypi.python.org/pypi/SALib/1.0.2). To install SALib with Anaconda, run this command within a command prompt: `conda install -c conda-forge salib`. 
  - It is helpful to add the install folder and the "Scripts" subfolder to your path environment variable.
2. Download [csip-weather-client](https://bitbucket.org/adozier/csip-weather-client/downloads/?tab=tags) and unzip into the same directory that IUWM resides
3. Download [IUWM source code](https://bitbucket.org/iuwm/iuwm/downloads/?tab=tags) and unzip into the same directory (e.g., C:\Projects\ModelMyCity)

## Help

[`python iuwm.py -h`](https://bitbucket.org/iuwm/iuwm/wiki/Help)

## Quick Start

You can run the model from command line or build a scripting file (*.bat on Windows, *.sh on Linux) that will run your model by double-clicking. Either way, you can test IUWM with a sample model by running the following command (assuming IUWM code is in a subfolder called `iuwm`): 

```
python iuwm/iuwm.py iuwm/tests/co.csv --output_files out_yearly.csv out_monthly.csv out_daily.csv --output_timesteps yearly monthly daily
```

A custom weather file can be supplied via the `--weather_file` option (using the same format and headers as in `iuwm/tests/weather_fort_collins.csv`). Starting and ending date can be supplied via command line options `--start_date` and `--end_date`, respectively. For more verbosity, use the switch `--verbose`. 

## Advanced topics 

* [Storing output](https://bitbucket.org/iuwm/iuwm/wiki/output)
* [Batch model runs](https://bitbucket.org/iuwm/iuwm/wiki/batch)
* [Sensitivity analysis](https://bitbucket.org/iuwm/iuwm/wiki/sensitivity)
* [Automatic calibration](https://bitbucket.org/iuwm/iuwm/wiki/calibration)

## Citation

Sharvelle, S., Dozier, A. Q., Arabi, M., and Reichel, B. I. (2017). “A geospatially-enabled web tool for urban water demand forecasting and assessment of alternative urban water management strategies”, *Environmental Modelling & Software*, 97, 213-228. [https://doi.org/10.1016/j.envsoft.2017.08.009](https://doi.org/10.1016/j.envsoft.2017.08.009){:target="_blank"}.

## Links 

 - [Similar Software Page](https://bitbucket.org/iuwm/iuwm/wiki/similar_software)