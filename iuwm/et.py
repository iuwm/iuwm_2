# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from components import IUWMObject
import convert
import json
from multiprocessing import Pool
import numpy as np
import os
import requests
import warnings

DEFAULT_KT = 0.17
SOLAR_CONSTANT = 0.082  # MJ/m^2/min
STEFAN_BOLTZMANN = 4.903E-9  # MJ/K^4/m^2/day
ALBEDO_GRASS = 0.23
A_COEFF = 0.25
B_COEFF = 0.5
DEFAULT_WIND_SPEED = 2  # m/s, average over 2000 weather stations around the globe


def solar_parameters(date, latitudes):
    # parse parts of the date
    yday = date.timetuple().tm_yday
    year = date.year

    # number of days in a year
    n_days = 366.0 if year % 4 == 0 else 365.0

    # angle of day
    a = 2 * np.pi * yday / n_days

    # inverse relative distance
    d_r = 1 + 0.033 * np.cos(a)

    # solar declination angle
    s = 0.409 * np.sin(a - 1.39)

    # latitudes in radians
    lat = latitudes * np.pi / 180.0

    # sunset hour angle
    w = np.arccos(-np.tan(lat) * np.tan(s))
    return lat, d_r, s, w


def extraterrestrial_radiation(date, latitudes):
    """
    Calculates extra terrestrial radiation (MJ/m^2/day) from a date and latitude

    :param date: The date of interest [datetime]
    :param latitudes: Array of latitudes in decimal degrees for each location
    :return: Returns the estimated extra terrestrial radiation [MJ/m^2/day]
    """

    # extraterrestrial radiation (MJ/m^2/day)
    lat, d_r, s, w = solar_parameters(date, latitudes)
    r_a = 1440.0 / np.pi * SOLAR_CONSTANT * d_r * (w * np.sin(lat) * np.sin(s) + np.cos(lat) * np.cos(s) * np.sin(w))
    return r_a


def daylight_hours(date, latitudes):
    lat, d_r, s, w = solar_parameters(date, latitudes)
    w = np.arccos(-np.tan(lat) * np.tan(s))
    return 24 / np.pi * w


def solar_radiation_hargreaves(date, latitudes, temp_min, temp_max, k_t=DEFAULT_KT):
    """
    Estimates solar radiation based on latitudes and date and daily temperatures

    Equations used directly from here:
        http://www.zohrabsamani.com/research_material/files/Hargreaves-samani.pdf

    :param date: The date for which to estimate solar radiation
    :param latitudes: The latitudes of each various spatial subunit for which to estimate solar radiation [decimal deg]
    :param temp_min: The minimum daily temperature [deg. C.]
    :param temp_max: The maximum daily temperature [deg. C.]
    :param k_t: Empirical coefficient. Interior regions: 0.162. Coastal regions: 0.19.
    :return: An estimated downwelling shortwave solar radiation [MJ/m^2/day]
    """

    # run checks
    if np.any(temp_max < temp_min):
        warnings.warn('WARNING: max temp is smaller than min temp on {0}!'.format(date.strftime('%Y-%m-%d')))
        min_t = np.minimum(temp_min, temp_max)
        temp_max = np.maximum(temp_min, temp_max)
        temp_min = min_t

    # calculate estimated land surface downwelling shortwave solar radiation
    r_a = extraterrestrial_radiation(date, latitudes)
    solar_rad = k_t * r_a * np.sqrt(temp_max - temp_min)
    return solar_rad


def solar_radiation_penman_monteith(date, latitudes, n_sunshine, a=A_COEFF, b=B_COEFF, r_a=None):
    """
    Estimates solar radiation based on latitudes and date and daily temperatures (used with Penman-Monteith)

    Equations used directly from here:
        http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param date: The date for which to estimate solar radiation
    :param latitudes: The latitudes of each various spatial subunit for which to estimate solar radiation [decimal deg]
    :param n_sunshine: Actual duration of sunshine [hour]
    :param a: Regression coefficient, fraction of extraterrestrial radiation reaching the earth on overcast days
    :param b: Regression coefficient
    :param r_a: Extraterrestrial radiation, calculated from date and latitudes if not provided [MJ/m^2/day]
    :return: Returns estimated downwelling solar/shortwave radiation [MJ/m^2/day]
    """
    if r_a is None:
        r_a = extraterrestrial_radiation(date, latitudes)
    n_total = daylight_hours(date, latitudes)
    return (a + b * n_sunshine / n_total) * r_a


class Elevation:
    meters = 'Meters'
    feet = 'Feet'

    def __init__(self, i, lon, lat, units=meters):
        self.i = i
        self.lon = lon
        self.lat = lat
        self.units = units
        self.elevation = 0.0

    def get(self):
        url = r'https://nationalmap.gov/epqs/pqs.php?x={0:f}&y={1:f}&units={2}&output=json'
        resp = requests.get(url.format(self.lon, self.lat, self.units))
        j = json.loads(resp.content)
        self.elevation = float(j['USGS_Elevation_Point_Query_Service']['Elevation_Query']['Elevation'])
        return self


def cache_directory(sub_folder):
    cache_dir = os.path.expanduser('~/.iuwm_cache/{0}'.format(sub_folder))
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    return cache_dir


def cache_file_path(sub_folder, file_name):
    return os.path.join(cache_directory(sub_folder), file_name)


def get_elevation_single(elevation):
    return elevation.get()


def get_elevation(longitudes, latitudes, units=Elevation.meters, n_proc=20):

    # make sure longitudes and latitudes are in correct format
    if hasattr(longitudes, '__len__'):
        n = len(longitudes)
    else:
        n = 1
        longitudes = np.float64([longitudes], ndmin=1)
        latitudes = np.float64([latitudes], ndmin=1)

    # check the cache directory and file before continuing
    h_lon = hash(longitudes.tostring())
    h_lat = hash(latitudes.tostring())
    cache_file = cache_file_path('elevation', 'elev_{0}_{1}.npy'.format(h_lon, h_lat))
    if os.path.exists(cache_file):
        return np.load(cache_file)

    # acquiring elevation data
    elevation = [Elevation(i, lon, lat, units=units) for i, lon, lat in zip(range(n), longitudes, latitudes)]
    p = Pool(n_proc)
    data = p.map(get_elevation_single, elevation)
    e = np.zeros(n)
    for i, d in enumerate(data):
        # print('{0} =? {1}'.format(i, d.i))
        e[d.i] = d.elevation

    ei = e == -1000000.0
    if np.any(ei):
        lat = latitudes[ei]
        lon = longitudes[ei]
        raise ValueError('An elevation was retrieved outside of the U.S. that is not available!\n  {0}'.format(
            '\n  '.join('{0:6.2f}, {1:6.2f}'.format(lo, la) for la, lo in zip(lat, lon))
        ))

    # save to cache
    np.save(cache_file, e)
    return e


def atmospheric_pressure(longitudes, latitudes, elevations=None):
    """
    Calculates atmospheric pressure exerted by the weight of the earth's atmosphere
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param longitudes: Longitudinal coordinates
    :param latitudes: Latitudinal coordinates
    :param elevations: Elevation at locations of interest
    :return: Returns the atmospheric pressure [kPa] at each location
    """

    if elevations is None:
        elevations = get_elevation(longitudes, latitudes)
    return 101.3 * np.power((293 - 0.0065 * elevations) / 293, 5.26)


def psychrometric_constant(atm_pressure):
    """
    The psychrometric constant, the specific heat of air at constant pressure
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param atm_pressure: Atmospheric pressure [kPa] of the location
    :return: Returns the psychrometric constant as a function of average atmospheric pressure
    """
    return 0.665e-3 * atm_pressure


def average_temperature(temp_min, temp_max):
    """
    Estimates the average temperature from the min and max daily temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_min: Minimum daily air temperature [deg. C]
    :param temp_max: Maximum daily air temperature [deg. C]
    :return: Returns the average of the min and max daily air temperature [deg. C]
    """
    return (temp_max + temp_min) / 2.0


def saturation_vapour_pressure(temp):
    """
    Estimates saturation vapour pressure [kPa] related to air temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp: Air temperature [deg. C]
    :return: Returns the estimated saturation vapour pressure [kPa]
    """
    return 0.6108 * np.exp((17.27 * temp) / (temp + 237.3))


def mean_saturation_vapour_pressure(temp_min, temp_max):
    """
    Estimates the mean saturation vapour pressure [kPa] related to air temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_min: Minimum daily air temperature [deg. C]
    :param temp_max: Maximum daily air temperature [deg. C]
    :return: Returns the estimated mean daily saturation vapour pressure [kPa]
    """
    return (saturation_vapour_pressure(temp_min) + saturation_vapour_pressure(temp_max)) / 2.0


def slope_of_saturation_vapour_pressure_curve(temp_avg):
    """
    Estimates the slope of the saturation vapour pressure curve using average temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_avg: Average daily air temperature [deg. C]
    :return: Returns the slope of the saturation vapour pressure curve [kPa/deg. C]
    """
    return 4098.0 * saturation_vapour_pressure(temp_avg) / (temp_avg + 237.3) ** 2


def actual_vapour_pressure(temp_min, temp_max, rel_hum_min, rel_hum_max):
    """
    Estimates the actual vapour pressure from relative humidity and min/max temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_min: Minimum daily air temperature [deg. C]
    :param temp_max: Maximum daily air temperature [deg. C]
    :param rel_hum_min: Minimum daily relative humidity [%]
    :param rel_hum_max: Maximum daily relative humidity [%]
    :return: Returns the estimated actual vapour pressure [kPa]
    """
    return (saturation_vapour_pressure(temp_min) * rel_hum_max / 100.0
            + saturation_vapour_pressure(temp_max) * rel_hum_min / 100.0) / 2.0


def actual_vapour_pressure_temp_dew_avg(temp_dew_avg):
    """
    Estimates the actual vapour pressure from dew point temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_dew_avg: Mean daily dew point temperature [deg. C]
    :return: Returns the estimated actual vapour pressure [kPa]
    """
    return 0.6108 * np.exp(17.27 * temp_dew_avg / (temp_dew_avg + 237.3))


def actual_vapour_pressure_rel_hum_max(temp_min, rel_hum_max):
    """
    Estimates the actual vapour pressure from relative humidity and min/max temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_min: Minimum daily air temperature [deg. C]
    :param rel_hum_max: Maximum daily relative humidity [%]
    :return: Returns the estimated actual vapour pressure [kPa]
    """
    return saturation_vapour_pressure(temp_min) * rel_hum_max / 100.0


def actual_vapour_pressure_rel_hum_avg(temp_min, temp_max, rel_hum_avg):
    """
    Estimates the actual vapour pressure from relative humidity and min/max temperature
    http://www.fao.org/docrep/X0490E/x0490e07.htm

    :param temp_min: Minimum daily air temperature [deg. C]
    :param temp_max: Maximum daily air temperature [deg. C]
    :param rel_hum_avg: Average daily relative humidity [%]
    :return: Returns the estimated actual vapour pressure [kPa]
    """
    return rel_hum_avg / 100.0 * mean_saturation_vapour_pressure(temp_min, temp_max)


def actual_vapour_pressure_temp_min(temp_min):
    """
    Estimates the actual vapour pressure from min temperature

    :param temp_min: Minimum daily air temperature [deg. C]
    :return: Returns the estimated actual vapour pressure [kpa]
    """
    return 0.611 * np.exp(17.27 * temp_min / (temp_min + 237.3))


def vapour_pressure_deficit(sat_vapour_pressure, act_vapour_pressure):
    """
    Estimates the vapour pressure deficit [kPa]

    :param sat_vapour_pressure: Saturation vapour pressure [kPa]
    :param act_vapour_pressure: Actual vapour pressure [kPa]
    :return: Returns the estimated vapour pressure deficit [kPa]
    """
    return sat_vapour_pressure - act_vapour_pressure


def clear_sky_solar_radiation(elevation, r_a):
    """
    Estimates clear sky solar radiation [MJ/m^2/day]

    :param elevation: Elevation [meters] of location
    :param r_a: Extraterrestrial radiation [MJ/m^2/day]
    :return: Returns the estimated clear sky solar radiation [MJ/m^2/day]
    """
    return (0.75 + 2e-5 * elevation) * r_a


def net_solar_radiation(solar_rad, albedo=ALBEDO_GRASS):
    """
    Estimates net solar radiation [MJ/m^2/day]

    :param solar_rad: Downwelling solar/shortwave radiation [MJ/m^2/day]
    :param albedo: Albedo or canopy reflection coefficient, which is usually 0.23 for hypothetical grass reference crop
    :return: Returns the estimated net solar radiation [MJ/m^2/day]
    """
    return (1 - albedo) * solar_rad


def net_longwave_radiation(temp_min, temp_max, act_vapour_pressure, solar_rad, clear_sky_solar_rad):
    """
    Estimates net longwave radiation [MJ/m^2/day]

    :param temp_min: Minimum daily air temperature [deg. C]
    :param temp_max: Maximum daily air temperature [deg. C]
    :param act_vapour_pressure: Actual vapour pressure [kPa]
    :param solar_rad: Solar radiation [MJ/m^2/day]
    :param clear_sky_solar_rad: Clear-sky solar radiation [MJ/m^2/day]
    :return: Returns the estimated net longwave radiation [MJ/m^2/day]
    """
    min_t = (temp_min + 273.15) ** 4
    max_t = (temp_max + 273.15) ** 4
    r_nl_1 = STEFAN_BOLTZMANN * (min_t + max_t) / 2.0
    r_nl_2 = (0.34 - 0.14 * np.sqrt(act_vapour_pressure))
    r_nl_3 = (1.35 * solar_rad / clear_sky_solar_rad - 0.35)
    r_nl = r_nl_1 * r_nl_2 * r_nl_3
    return r_nl


def net_radiation(net_solar_rad, net_longwave_rad):
    """
    Calculations net radiation [MJ/m^2/day]

    :param net_solar_rad: Net solar/shortwave radiation [MJ/m^2/day]
    :param net_longwave_rad: Net longwave radiation [MJ/m^2/day]
    :return: Returns the estimated net radiation [MJ/m^2/day]
    """
    return net_solar_rad - net_longwave_rad


def wind_speed_2m(u_z, z):
    return u_z * 4.87 / np.log(67.8 * z - 5.42)


def wind_speed_magnitude(east, north):
    return np.sqrt(east ** 2 + north ** 2)


def weather_defaults(date, longitudes, latitudes, temp_min, temp_max,
                     rel_hum_min=None, rel_hum_max=None, rel_hum_avg=None,
                     wind_speed=None, solar_rad=None, temp_avg=None, temp_dew_avg=None,
                     east_wind=None, north_wind=None, wind_height=2,
                     elevation=None, n_sunshine=None, solar_rad_reg_a=A_COEFF, solar_rad_reg_b=B_COEFF,
                     r_a=None, k_t=DEFAULT_KT):
    """
    Estimates weather defaults when data is not available to use the Penman-Monteith equation
    http://www.fao.org/docrep/X0490E/x0490e06.htm
    http://www.fao.org/docrep/X0490E/x0490e07.htm
    http://www.fao.org/docrep/X0490E/x0490e08.htm
    http://edis.ifas.ufl.edu/pdffiles/ae/ae45900.pdf

    :param date: Date of interest
    :param longitudes: Longitudinal coordinates [decimal deg], array of size N
    :param latitudes: Latitudinal coordinates [decimal deg], array of size N
    :param temp_min: Minimum daily temperature values [deg. C], array of size N
    :param temp_max: Maximum daily temperature values [deg. C], array of size N
    :param solar_rad: Downwelling solar or shortwave radiation [deg. C], array of size N
    :param rel_hum_min: Minimum relative humidity [%]
    :param rel_hum_max: Maximum relative humidity [%]
    :param rel_hum_avg: Average relative humidity [%]
    :param wind_speed: Wind speed magnitude at `wind_height` above surface [m/s], def to 2m/s if not provided
    :param east_wind: Eastward wind speed at `wind_height` above surface [m/s]
    :param north_wind: Northward wind speed at `wind_height` above surface [m/s]
    :param wind_height: Height at which wind speed is measured, default is 2m [m]
    :param temp_avg: Average temperature [deg. C], calculated from min/max temp if not provided, array of size N
    :param temp_dew_avg: Mean daily dew point temperature [deg. C]
    :param elevation: Elevation [meters], retrieved from USGS with longitudes/latitudes if not provided, array of size N
    :param n_sunshine: Actual duration of sunshine [hour], used if `solar_rad` not provided
    :param solar_rad_reg_a: Regression coefficient for solar radiation, used if `solar_rad` not provided
    :param solar_rad_reg_b: Regression coefficient for solar radiation, used if `solar_rad` not provided
    :param r_a: Extraterrestrial radiation, calculated from date and latitudes if not provided [MJ/m^2/day]
    :param k_t: Regression coefficient for estimating solar radiation via Hargreaves,
        used if both `solar_rad` and `n_sunshine` are not provided
    :return: Returns estimated reference evapotranspiration [mm/day]
    """

    if elevation is None:
        elevation = get_elevation(longitudes, latitudes)
    if temp_avg is None:
        temp_avg = average_temperature(temp_min, temp_max)
    if r_a is None:
        r_a = extraterrestrial_radiation(date, latitudes)
    if solar_rad is None:
        if n_sunshine is None:
            solar_rad = solar_radiation_hargreaves(date, latitudes, temp_min, temp_max, k_t=k_t)
        else:
            solar_rad = solar_radiation_penman_monteith(date, latitudes, n_sunshine,
                                                        a=solar_rad_reg_a, b=solar_rad_reg_b, r_a=r_a)
    if wind_speed is None:
        if east_wind is not None and north_wind is not None:
            if wind_height != 2:
                east_wind = wind_speed_2m(east_wind, wind_height)
                north_wind = wind_speed_2m(north_wind, wind_height)
            wind_speed = wind_speed_magnitude(east_wind, north_wind)
        else:
            wind_speed = DEFAULT_WIND_SPEED
    elif wind_height != 2:
        wind_speed = wind_speed_2m(wind_speed, wind_height)
    wind_speed = np.abs(wind_speed)

    # vapour pressure [kPa]
    if rel_hum_min is None or rel_hum_max is None:
        if rel_hum_max is None:
            if rel_hum_avg is None:
                if temp_dew_avg is None:
                    vp_act = actual_vapour_pressure_temp_min(temp_min)
                else:
                    vp_act = actual_vapour_pressure_temp_dew_avg(temp_dew_avg)
            else:
                vp_act = actual_vapour_pressure_rel_hum_avg(temp_min, temp_max, rel_hum_avg)
        else:
            vp_act = actual_vapour_pressure_rel_hum_max(temp_min, rel_hum_max)
    else:
        vp_act = actual_vapour_pressure(temp_min, temp_max, rel_hum_min, rel_hum_max)

    return elevation, temp_avg, r_a, solar_rad, wind_speed, vp_act


def standard_array(a, n=1, dtype=np.float_):
    if a is None:
        return None
    if not isinstance(a, np.ndarray):
        a = np.array(a, dtype=dtype, ndmin=1)
    if len(a) != n:
        a = np.full(n, a)
    return a


def penman_monteith(date, longitudes, latitudes, temp_min, temp_max,
                    rel_hum_min=None, rel_hum_max=None, rel_hum_avg=None,
                    wind_speed=None, solar_rad=None, temp_avg=None,
                    east_wind=None, north_wind=None, wind_height=2,
                    elevation=None, albedo=ALBEDO_GRASS, n_sunshine=None, solar_rad_reg_a=A_COEFF, solar_rad_reg_b=B_COEFF, r_a=None, k_t=DEFAULT_KT):
    """
    Estimates reference evapotranspiration [mm/day] using the Penman-Monteith equation
    http://www.fao.org/docrep/X0490E/x0490e06.htm
    http://www.fao.org/docrep/X0490E/x0490e07.htm
    http://www.fao.org/docrep/X0490E/x0490e08.htm
    http://edis.ifas.ufl.edu/pdffiles/ae/ae45900.pdf

    :param date: Date of interest
    :param longitudes: Longitudinal coordinates [decimal deg], array of size N
    :param latitudes: Latitudinal coordinates [decimal deg], array of size N
    :param temp_min: Minimum daily temperature values [deg. C], array of size N
    :param temp_max: Maximum daily temperature values [deg. C], array of size N
    :param solar_rad: Downwelling solar or shortwave radiation [deg. C], array of size N
    :param rel_hum_min: Minimum relative humidity [%]
    :param rel_hum_max: Maximum relative humidity [%]
    :param rel_hum_avg: Average relative humidity [%]
    :param wind_speed: Wind speed magnitude at `wind_height` above surface [m/s], def to 2m/s if not provided
    :param east_wind: Eastward wind speed at `wind_height` above surface [m/s]
    :param north_wind: Northward wind speed at `wind_height` above surface [m/s]
    :param wind_height: Height at which wind speed is measured, default is 2m [m]
    :param temp_avg: Average temperature [deg. C], calculated from min/max temp if not provided, array of size N
    :param elevation: Elevation [meters], retrieved from USGS with longitudes/latitudes if not provided, array of size N
    :param albedo: Albedo [dimensionless], 0.23 if not provided
    :param n_sunshine: Actual duration of sunshine [hour], used if `solar_rad` not provided
    :param solar_rad_reg_a: Regression coefficient for solar radiation, used if `solar_rad` not provided
    :param solar_rad_reg_b: Regression coefficient for solar radiation, used if `solar_rad` not provided
    :param r_a: Extraterrestrial radiation, calculated from date and latitudes if not provided [MJ/m^2/day]
    :param k_t: Regression coefficient for estimating solar radiation via Hargreaves,
        used if both `solar_rad` and `n_sunshine` are not provided
    :return: Returns estimated reference evapotranspiration [mm/day]
    """

    # collect default information if values are not provided
    elevation, temp_avg, r_a, solar_rad, wind_speed, vp_act = weather_defaults(
        date, longitudes, latitudes, temp_min, temp_max,
        rel_hum_min=rel_hum_min, rel_hum_max=rel_hum_max, rel_hum_avg=rel_hum_avg,
        wind_speed=wind_speed, solar_rad=solar_rad, temp_avg=temp_avg,
        east_wind=east_wind, north_wind=north_wind, wind_height=wind_height,
        elevation=elevation, n_sunshine=n_sunshine, solar_rad_reg_a=solar_rad_reg_a, solar_rad_reg_b=solar_rad_reg_b, r_a=r_a, k_t=k_t,
    )

    # vapour pressure
    vp_sat = mean_saturation_vapour_pressure(temp_min, temp_max)
    vp_def = vp_sat - vp_act

    # radiation [MJ/m^2/day]
    net_solar_rad = net_solar_radiation(solar_rad, albedo=albedo)
    clear_sky_solar_rad = clear_sky_solar_radiation(elevation, r_a)
    net_longwave_rad = net_longwave_radiation(temp_min, temp_max, vp_act, solar_rad, clear_sky_solar_rad)

    # net radiation at the crop surface (MJ/m^2/day)
    net_rad = net_radiation(net_solar_rad, net_longwave_rad)

    # soil heat flux density (MJ/m^2/day)
    soil_heat_flux = 0.0  # within the day, this flux is small and can be ignored

    # mean daily air temperature at 2m height (converting deg. C to K)
    t = convert.c2k(temp_avg)

    # slope vapour pressure curve (kPa/degC)
    vp_slope = slope_of_saturation_vapour_pressure_curve(temp_avg)

    # psychrometric constant (kPa/degC)
    atm_pressure = atmospheric_pressure(longitudes, latitudes, elevations=elevation)
    cp = psychrometric_constant(atm_pressure)

    # reference evapotranspiration (mm/day)
    pet = (convert.mjm2mm(vp_slope * (net_rad - soil_heat_flux)) + cp * 900.0 / t * wind_speed * vp_def) \
        / (vp_slope + cp * (1 + 0.34 * wind_speed))

    n_size = len(latitudes) if hasattr(latitudes, '__len__') else 1
    return standard_array(pet, n_size)


def hargreaves(date, latitudes, temp_min, temp_max, temp_avg=None, solar_rad=None, k_t=DEFAULT_KT):
    """
    Estimates evapotranspiration according to Hargreaves-Samani (2002, 2005) [mm/day]
    http://www.zohrabsamani.com/research_material/files/Hargreaves-samani.pdf

    :param date: Date of interest
    :param latitudes: Array of latitudes in decimal degrees for each location
    :param temp_min: Minimum daily temperature  [deg. C]
    :param temp_max: Maximum daily temperature  [deg. C]
    :param temp_avg: Average daily temperature  [deg. C]
    :param solar_rad: Surface downwelling shortwave radiation  [MJ/m^2/day]
    :param k_t: Empirical coefficient. Interior regions: 0.162. Coastal regions: 0.19.
    :return: Returns the estimated potential evapotranspiration [mm/day] using Hargreaves-Samani (2005)
    """

    if solar_rad is None:
        solar_rad = solar_radiation_hargreaves(date, latitudes, temp_min, temp_max, k_t=k_t)
    if temp_avg is None:
        temp_avg = average_temperature(temp_min, temp_max)

    v = convert.mjm2mm(0.0135 * solar_rad * (temp_avg + 17.8))
    n_size = len(latitudes) if hasattr(latitudes, '__len__') else 1
    return standard_array(v, n_size)


class ETMethod:
    penman_monteith = 1
    hargreaves = 2


class Weather(IUWMObject):

    def __init__(self, name, date, et_method, longitude, latitude, temp_min, temp_max,
                 rel_hum_min=None, rel_hum_max=None, rel_hum_avg=None,
                 wind_speed=None, east_wind=None, north_wind=None, wind_height=2,
                 solar_rad=None, temp_avg=None, precipitation=None,
                 elevation=None, albedo=ALBEDO_GRASS, n_sunshine=None,
                 solar_rad_reg_a=A_COEFF, solar_rad_reg_b=B_COEFF, r_a=None, k_t=DEFAULT_KT):
        """
        Builds the Evapotranspiration Calculator that estimates reference ET and actual ET with multiple methods

        :param date: Date of interest
        :param et_method: Methods to use (Penman-Monteith=1, Hargreaves-Samani=2)
        :param longitude: Longitudinal coordinates [decimal deg], array of size N
        :param latitude: Latitudinal coordinates [decimal deg], array of size N
        :param temp_min: Minimum daily temperature values [deg. C], array of size N
        :param temp_max: Maximum daily temperature values [deg. C], array of size N
        :param solar_rad: Downwelling solar or shortwave radiation [deg. C], array of size N
        :param rel_hum_min: Minimum relative humidity [%]
        :param rel_hum_max: Maximum relative humidity [%]
        :param rel_hum_avg: Average relative humidity [%]
        :param wind_speed: Wind speed magnitude at `wind_height` above surface [m/s], def to 2m/s if not provided
        :param east_wind: Eastward wind speed at `wind_height` above surface [m/s]
        :param north_wind: Northward wind speed at `wind_height` above surface [m/s]
        :param wind_height: Height at which wind speed is measured, default is 2m [m]
        :param temp_avg: Average temperature [deg. C], calculated from min/max temp if not provided, array of size N
        :param precipitation: Precipitation [mm]
        :param elevation: Elevation [meters], retrieved from USGS with longitudes/latitude if not provided, array of size N
        :param albedo: Albedo [dimensionless], 0.23 if not provided
        :param n_sunshine: Actual duration of sunshine [hour], used if `solar_rad` not provided
        :param solar_rad_reg_a: Regression coefficient for solar radiation, 0.25 used if `solar_rad` not provided
        :param solar_rad_reg_b: Regression coefficient for solar radiation, 0.5 used if `solar_rad` not provided
        :param r_a: Extraterrestrial radiation, calculated from date and latitude if not provided [MJ/m^2/day]
        :param k_t: Regression coefficient for estimating solar radiation via Hargreaves,
            used if both `solar_rad` and `n_sunshine` are not provided
        """

        IUWMObject.__init__(self, name)

        # collect defaults
        elevation, temp_avg, r_a, solar_rad, wind_speed, vp_act = weather_defaults(
            date, longitude, latitude, temp_min, temp_max,
            rel_hum_min=rel_hum_min, rel_hum_max=rel_hum_max, rel_hum_avg=rel_hum_avg,
            wind_speed=wind_speed, solar_rad=solar_rad, temp_avg=temp_avg,
            east_wind=east_wind, north_wind=north_wind, wind_height=wind_height,
            elevation=elevation, n_sunshine=n_sunshine,
            solar_rad_reg_a=solar_rad_reg_a, solar_rad_reg_b=solar_rad_reg_b, r_a=r_a, k_t=k_t,
        )

        # hold instance data
        self.date = date
        self.longitudes = longitude
        self.latitudes = latitude
        n_size = len(latitude) if hasattr(latitude, '__len__') else 1
        self.temp_min = standard_array(temp_min, n_size)
        self.temp_max = standard_array(temp_max, n_size)
        self.rel_hum_min = standard_array(rel_hum_min, n_size)
        self.rel_hum_max = standard_array(rel_hum_max, n_size)
        self.rel_hum_avg = standard_array(rel_hum_avg, n_size)
        self.wind_speed = standard_array(wind_speed, n_size)
        self.solar_rad = standard_array(solar_rad, n_size)
        self.temp_avg = standard_array(temp_avg, n_size)
        self.precipitation_mm = standard_array(precipitation, n_size)
        self.elevation = standard_array(elevation, n_size)
        self.albedo = standard_array(albedo, n_size)
        self.n_sunshine = standard_array(n_sunshine, n_size)
        self.solar_rad_reg_a = standard_array(solar_rad_reg_a, n_size)
        self.solar_rad_reg_b = standard_array(solar_rad_reg_b, n_size)
        self.r_a = standard_array(r_a, n_size)
        self.k_t = standard_array(k_t, n_size)

        # calculate PET using the various et_method across space
        h_i = et_method == ETMethod.hargreaves
        pm_i = et_method == ETMethod.penman_monteith

        if np.all(h_i):
            self.pet_mm = self.hargreaves()
            """Potential evapotranspiration in mm/day"""
        elif np.all(pm_i):
            self.pet_mm = self.penman_monteith()
        else:
            self.pet_mm = np.zeros(latitude.shape)
            if (hasattr(latitude, '__len__') and np.sum(h_i + pm_i) == len(latitude)) or np.sum(h_i + pm_i) == 1:
                raise ValueError('Must specify an ET method for all spatial subunits!')
            self.pet_mm[h_i] = self.hargreaves()[h_i]
            self.pet_mm[pm_i] = self.penman_monteith()[pm_i]

        self.pet_in = convert.mm2in(self.pet_mm)
        self.precipitation_in = convert.mm2in(self.precipitation_mm)

    def hargreaves(self):

        return hargreaves(
            self.date,
            self.latitudes,
            self.temp_min,
            self.temp_max,
            temp_avg=self.temp_avg,
            solar_rad=self.solar_rad,
            k_t=self.k_t,
        )

    def penman_monteith(self):

        return penman_monteith(
            self.date,
            self.longitudes,
            self.latitudes,
            self.temp_min,
            self.temp_max,
            rel_hum_min=self.rel_hum_min,
            rel_hum_max=self.rel_hum_max,
            rel_hum_avg=self.rel_hum_avg,
            wind_speed=self.wind_speed,
            solar_rad=self.solar_rad,
            temp_avg=self.temp_avg,
            elevation=self.elevation,
            albedo=self.albedo,
            n_sunshine=self.n_sunshine,
            solar_rad_reg_a=self.solar_rad_reg_a,
            solar_rad_reg_b=self.solar_rad_reg_b,
            r_a=self.r_a,
            k_t=self.k_t,
        )

