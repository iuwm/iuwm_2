# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import convert
import csv
import numpy as np
import os
import re

OLD = "2.0.0"
__version__ = "3.1.3"

VERSION_UPDATES_FILE = constants.VERSION_UPDATES_FILE


def to_num(v):
    vl = v.split(".")
    assert len(vl) == 3, "Version number must be 3 numbers separated by 2 dots!"
    return sum(int(vi) * f for vi, f in zip(vl, [1e6, 1e3, 1]))


def to_str(v):
    n = []
    for f in [1e6, 1e3, 1]:
        vi = int(v / f)
        n.append(str(vi))
        v -= vi * f
    assert v == 0, "Version number should be a whole number!"
    return ".".join(n)


def compare(v1, v2):
    """
    Compares two version strings. Return values are as follows:
      if v1 > v2: return 1
      if v1 == v2: return 0
      if v1 < v2: return -1

    :param v1: First version string to compare
    :param v2: Second version string to compare
    :return: Returns 1 when v1 > v2, 0 when equal, and -1 when v1 < v2.
    """
    v1 = to_num(v1)
    v2 = to_num(v2)
    if v1 > v2:
        return 1
    elif v1 == v2:
        return 0
    else:  # v1 < v2
        return -1


def detect(inputs):
    if isinstance(inputs, dict):
        if "version" in inputs:
            if isinstance(inputs["version"], np.ndarray):
                v = np.unique(inputs["version"])
                msg = "IUWM versions must not be different across subunits. Got the following:\n  {0}"
                assert len(v) == 1, msg.format("\n  ".join(v))
                return v[0]
            else:
                return inputs["version"]
        else:
            return OLD
    else:
        with open(inputs, "r") as f:
            cr = csv.DictReader(f)
            first = next(cr)
        return detect(first)


def needs_update(v):
    v = to_num(v)
    curr_v = to_num(__version__)
    return v < curr_v


def file_needs_update(input_file):
    version = detect(input_file)
    return needs_update(version)


def convert_row(r):
    r["var_names"] = convert.to_list(r["var_names"])
    r["new_names"] = convert.to_list(r["new_names"])
    r["values"] = convert.to_text_list(r["values"])
    r["columns"] = convert.to_int_list(r["columns"])
    return r


def get_all_updates(updates_file=VERSION_UPDATES_FILE):
    with open(updates_file, "r") as f:
        cr = csv.DictReader(f)
        operations = [convert_row(r) for r in cr]
    return operations


UPDATE_OPERATIONS = get_all_updates()


def required_updates(v, update_current_version=False):
    v = to_num(v)
    curr_v = to_num(__version__)
    v_update = {
        "version": __version__,
        "operation": "update",
        "var_names": ["version"],
        "values": [__version__],
        "columns": [0],
    }
    if update_current_version:
        return [v_update] + [
            op for op in UPDATE_OPERATIONS if v <= to_num(op["version"]) <= curr_v
        ]
    else:
        return [v_update] + [
            op for op in UPDATE_OPERATIONS if v < to_num(op["version"]) <= curr_v
        ]


def rename_file(input_file):
    exp = r"(\.v\d+\.\d+\.\d+)?(\.[^\.]*)$"
    if re.search(exp, input_file):
        return re.sub(exp, r".v{0}\2".format(__version__), input_file)
    else:
        return "{1}.v{0}{2}".format(__version__, *os.path.splitext(input_file))
