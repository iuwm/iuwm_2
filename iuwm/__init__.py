import version

__version__ = version.__version__

if __name__ == '__main__':
    import console
    console.main()
