# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import os
import csv
import numpy as np
import matplotlib

if constants.SILENT_PLOTTING:
    matplotlib.use("Agg")  # Force matplotlib to not use any Xwindows backend.
import matplotlib.pyplot as plt

DEFAULT_TIME_COL = "time"


def read_csv(
    csv_file, time_column=DEFAULT_TIME_COL, data_columns=None, filter_column=None
):

    with open(csv_file, "r") as f:
        cr = csv.reader(f)
        headers = next(row for row in cr)
        time_col = headers.index(time_column)
        if data_columns is not None:
            data_cols = [headers.index(c) for c in data_columns]
        else:
            data_cols = [
                i
                for i, c in enumerate(headers)
                if c != time_column and (filter_column is None or c != filter_column)
            ]
        all_data = np.array([row for row in cr], dtype=np.object)

    if filter_column is not None:
        filter_col = headers.index(filter_column)
        filter_vals = np.array([row[filter_col] for row in all_data])
        filter_unique = sorted(list(set(filter_vals)))
        data = {}
        for unique_filter_val in filter_unique:
            i = filter_vals == unique_filter_val
            t = np.array([row[time_col] for row in all_data[i]])
            v = np.array(
                [[row[c] for c in data_cols] for row in all_data[i]],
                dtype=np.float32,
                ndmin=2,
            )
            data[str(unique_filter_val)] = (t, v)
    else:
        t = np.array([row[time_col] for row in all_data])
        v = np.array(
            [[row[c] for c in data_cols] for row in all_data], dtype=np.float32
        )
        data = {
            "all": (t, v),
        }

    return [headers[c] for c in data_cols], data


def plot(
    input_csv,
    output_dir,
    time_column=DEFAULT_TIME_COL,
    data_columns=None,
    filter_column=None,
):

    data_cols, data = read_csv(
        input_csv,
        time_column=time_column,
        data_columns=data_columns,
        filter_column=filter_column,
    )

    for k, (t, v) in data.items():
        for c in range(v.shape[1]):
            out_name = os.path.join(data_cols[c], k + ".png")
            out_file = os.path.join(output_dir, out_name)
            print("Working on {0}...".format(out_name))

            if not os.path.exists(os.path.dirname(out_file)):
                os.makedirs(os.path.dirname(out_file))

            plt.figure()
            plt.plot(t, v[:, c], "k-")
            plt.xlabel("Time")
            plt.ylabel(data_cols[c].title())
            plt.savefig(out_file)
            plt.close("all")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Plot timeseries data.")
    parser.add_argument(
        "input_csv",
        type=str,
        help="CSV input file with a time column and data columns.",
    )
    parser.add_argument(
        "output_dir", type=str, help="Output directory for timeseries plots."
    )
    parser.add_argument(
        "--time_column",
        type=str,
        default=DEFAULT_TIME_COL,
        help='Name of time column. Default: "time".',
    )
    parser.add_argument(
        "--data_columns",
        type=str,
        nargs="+",
        default=None,
        help="Columns containing data to plot. Default: all non-time and non-filter columns.",
    )
    parser.add_argument(
        "--filter_column",
        type=str,
        default=None,
        help="Column by which to filter results. Default: None.",
    )
    args = parser.parse_args()

    plot(
        args.input_csv,
        args.output_dir,
        time_column=args.time_column,
        data_columns=args.data_columns,
        filter_column=args.filter_column,
    )