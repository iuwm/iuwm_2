# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import convert
import csv
import fnmatch
import itertools
import os
import re
import shutil
import sys
import version
import utils

THIS_DIR = os.path.abspath(os.path.dirname(__file__))
TEST_DIR = os.path.abspath(os.path.join(THIS_DIR, "tests"))
DATA_DIR = os.path.abspath(os.path.join(THIS_DIR, "core", "data"))

BAK = ".bak"
TMP = ".tmp"


def core_model_files(pattern="*.csv", skip=None):
    f_names = ["default_outputs.txt", "possible_outputs.txt", "input_variables.csv"]
    f_paths = [os.path.join(DATA_DIR, fn) for fn in f_names]
    return itertools.chain(
        utils.search_files(TEST_DIR, pattern, skip=skip),
        (f for f in f_paths if os.path.exists(f)),
    )


def get_files(the_dir, pattern="*.csv", skip=None):
    if the_dir is None or utils.same_dir(the_dir, THIS_DIR):
        return core_model_files()
    else:
        return utils.search_files(the_dir, pattern, skip=skip)


def sub_file_path(file_path, sub_dir):
    tmp_dir = os.path.join(os.path.dirname(file_path), sub_dir)
    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)
    return os.path.join(tmp_dir, os.path.basename(file_path))


def tmp_file_path(file_path):
    return file_path + TMP


def backup_file_path(file_path):
    return sub_file_path(file_path, BAK) + BAK


def move(src, dest, save_backup=False):
    if save_backup:
        backup(dest)
    if os.path.exists(dest):
        os.remove(dest)
    os.rename(src, dest)


def backup(orig_file):
    backup_file = backup_file_path(orig_file)
    move(orig_file, backup_file)


def restore_backup(orig_file):
    backup_file = backup_file_path(orig_file)
    if os.path.exists(backup_file):
        move(backup_file, orig_file)
    else:
        print("WARNING: Could not find backup file for {0}".format(orig_file))


class ModelData:
    def __init__(self, file_path, headers=None, data=None, changes=0):
        self.file_path = file_path
        self.changes = changes

        if headers is not None and data is not None:
            self.headers = headers
            self.data = data

        else:

            if headers is not None or data is not None:
                print(
                    'Reading file {0} because both "headers" and "data" were not provided.'.format(
                        file_path
                    )
                )

            with open(file_path, "r") as fh:
                cr = csv.DictReader(fh)
                self.headers = cr.fieldnames
                self.data = [row for row in cr]

        self.is_model_input = all(
            v in self.headers
            for v in (
                "geoid",
                "population",
                "households",
                "shape_area_acres",
                "weather_source",
            )
        )

    def remove(self, var_name):
        new_headers = [h for h in self.headers if h != var_name]
        self.changes += len(self.headers) - len(new_headers)
        return ModelData(
            self.file_path,
            headers=new_headers,
            data=[{k: v for k, v in row.items() if k != var_name} for row in self.data],
            changes=self.changes,
        )

    def remove_multiple(self, var_names):
        var_names = set(var_names)
        new_headers = [h for h in self.headers if h not in var_names]
        self.changes += len(self.headers) - len(new_headers)
        return ModelData(
            self.file_path,
            headers=new_headers,
            data=[
                {k: v for k, v in row.items() if k not in var_names}
                for row in self.data
            ],
            changes=self.changes,
        )

    def update(self, var_name, value, skip_existing=False):
        found = var_name in self.headers
        new_headers = self.headers if found else self.headers + [var_name]

        dict_values = {} if found and skip_existing else {var_name: value}
        if len(dict_values) != 1:
            print("  Skipping {0} in {1}".format(var_name, self.file_path))
        self.changes += 1
        return ModelData(
            self.file_path,
            headers=new_headers,
            data=[utils.merge_dicts(row, dict_values) for row in self.data],
        )

    def update_multiple(self, var_names, values, skip_existing=False, columns=None):

        if len(values) != len(var_names):
            msg = "Number of values for updated variables must be the same as the number of variable names!"
            raise ValueError(msg)

        dict_values = {
            k: v
            for k, v in zip(var_names, values)
            if not (k in self.headers and skip_existing)
        }
        if len(dict_values) != len(var_names):
            skip_vars = ", ".join(v for v in var_names if v not in dict_values)
            print("  Skipping {0} in {1}".format(skip_vars, self.file_path))

        new_headers = self.headers + [v for v in var_names if v not in self.headers]
        if columns is not None:
            assert len(columns) == len(
                var_names
            ), "Must provide the same number of columns as variable names."
            for c, v in sorted(zip(columns, var_names), key=lambda r: r[0]):
                if c == -999999:
                    continue
                new_headers.insert(c, new_headers.pop(new_headers.index(v)))
        self.changes += len(var_names)

        return ModelData(
            self.file_path,
            headers=new_headers,
            data=[utils.merge_dicts(row, dict_values) for row in self.data],
        )

    def list_values(self, var_name, intro=True, print_always=False):

        found = var_name in self.headers

        msg = "  {0}:\n".format(var_name)
        if found:
            msg += "    {0}\n".format(
                ", ".join(set([row[var_name] for row in self.data]))
            )
        else:
            msg += "    Not found.\n".format(var_name)

        if print_always or found:
            if intro:
                sys.stdout.write("{0}\n".format(self.file_path))
            sys.stdout.write(msg)

        return found

    def list_values_multiple(self, var_names, print_always=False):
        intro = True
        any_found = False
        for var_name in var_names:
            found = self.list_values(var_name, intro=intro, print_always=print_always)
            if found:
                intro = False
                any_found = True
        return any_found

    def save(self, file_path=None):

        # set file path
        if file_path is None:
            file_path = self.file_path

        # write model data to temporary file
        tmp_path = tmp_file_path(file_path)
        with open(tmp_path, "wb") as fh:
            cw = csv.DictWriter(fh, fieldnames=self.headers)
            cw.writeheader()
            for row in self.data:
                cw.writerow(row)

        # back up original file
        backup(file_path)

        # rename temporary file to final output file
        os.rename(tmp_path, file_path)

        return self


def substitute_multiple(old_names, new_names, f_str):
    assert len(old_names) == len(
        new_names
    ), "Lists of old and new variable names need to be the same length!"
    for o, n in zip(old_names, new_names):
        f_str = re.sub(o, n, f_str)
    return f_str


def change_name(file_path, var_names, new_names):
    if var_names is None:
        raise ValueError("Must provide var_names when changing names!")
    if new_names is None:
        raise ValueError("Must provide new_names when changing names!")

    # list all the possible replacements
    with open(file_path, "r") as f:
        f_str = f.read()

    # actually change the variable names in the files
    tmp_file = tmp_file_path(file_path)
    with open(tmp_file, "w") as f:
        f.write(substitute_multiple(var_names, new_names, f_str))

    # save over original file path
    move(tmp_file, file_path, save_backup=True)


def change_name_in_dir(
    the_dir, var_names, new_names, pattern="*.csv", skip=None, **kwargs
):
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("Changing names: {0:<60s}\n".format(file_path[-79:]))
        change_name(file_path, var_names, new_names)


def recover(file_path):
    backup_file = backup_file_path(file_path)
    if os.path.exists(backup_file):
        print("Recovering    : {0}...".format(file_path))
        move(backup_file, file_path)


def recover_in_dir(the_dir, pattern="*.csv", skip=None, **kwargs):
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("Recovering    : {0:<60s}\n".format(file_path[-79:]))
        recover(file_path)


def remove(file_path, var_names, only_in_input=False):
    if var_names is None:
        raise ValueError("Must provide var_names when removing!")
    md = ModelData(file_path)
    if not only_in_input or md.is_model_input:
        md.remove_multiple(var_names).save()


def remove_in_dir(
    the_dir, var_names, only_in_input=False, pattern="*.csv", skip=None, **kwargs
):
    if var_names is None:
        raise ValueError("Must provide var_names when removing!")
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("Removing var  : {0:<60s}\n".format(file_path[-79:]))
        remove(file_path, var_names, only_in_input=only_in_input)


def update(
    file_path, var_names, values, skip_existing=False, only_in_input=False, columns=None
):
    if var_names is None:
        raise ValueError("Must provide var_names when updating!")
    if values is None:
        raise ValueError("Must provide values when updating!")
    md = ModelData(file_path)
    if not only_in_input or md.is_model_input:
        md.update_multiple(
            var_names, values=values, skip_existing=skip_existing, columns=columns
        ).save()


def update_in_dir(
    the_dir,
    var_names,
    values,
    skip_existing=False,
    only_in_input=False,
    columns=None,
    pattern="*.csv",
    skip=None,
    **kwargs
):
    if var_names is None:
        raise ValueError("Must provide var_names when updating!")
    if values is None:
        raise ValueError("Must provide values when updating!")
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("Update var    : {0:<60s}\n".format(file_path[-79:]))
        update(
            file_path,
            var_names,
            values=values,
            skip_existing=skip_existing,
            only_in_input=only_in_input,
            columns=columns,
        )


def list_values(file_path, var_names):
    if var_names is None:
        raise ValueError("Must provide var_names when listing values!")
    ModelData(file_path).list_values_multiple(var_names)


def list_values_in_dir(the_dir, var_names, pattern="*.csv", skip=None, **kwargs):
    if var_names is None:
        raise ValueError("Must provide var_names when listing values!")
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("List value    : {0:<60s}\n".format(file_path[-79:]))
        list_values(file_path, var_names)


def remove_backup_in_dir(the_dir, pattern=".bak", skip=None, **kwargs):
    for dir_name in search_directories(the_dir, pattern):
        shutil.rmtree(dir_name)


def update_version(input_file, in_place=False, force=False, out_file=None):

    v = version.detect(input_file)
    if version.needs_update(v) or force:

        # get output file location
        if out_file is None:
            out_file = version.rename_file(input_file)
        if in_place:
            out_file = input_file
        if not utils.same_file(out_file, input_file):
            if os.path.exists(out_file):
                backup(out_file)
            shutil.copy2(input_file, out_file)

        # loop through all necessary updates on the file... n
        for u in version.required_updates(v, update_current_version=force):
            op = u.pop("operation")
            u["pattern"] = os.path.basename(out_file)
            the_dir = os.path.dirname(out_file)
            if the_dir == "":
                the_dir = "."
            u["the_dir"] = the_dir
            manipulate(op, **u)
            u["operation"] = op
        return out_file

    return input_file


def update_version_in_dir(
    the_dir, pattern="*.csv", in_place=False, skip=None, force=False, **kwargs
):
    for file_path in get_files(the_dir, pattern=pattern, skip=skip):
        sys.stdout.write("Update version: {0:<60s}\n".format(file_path[-79:]))
        update_version(file_path, in_place=in_place, force=force)


OPERATIONS = {
    "recover": recover_in_dir,
    "list_values": list_values_in_dir,
    "remove": remove_in_dir,
    "update": update_in_dir,
    "change_name": change_name_in_dir,
    "remove_backup": remove_backup_in_dir,
    "update_version": update_version_in_dir,
}


def manipulate(op, **kwargs):

    OPERATIONS[op](**kwargs)


def main(args=None):
    # build program argument processes
    import argparse

    parser = argparse.ArgumentParser(description="Quickly manipulate IUWM input files.")
    parser.add_argument(
        "operation",
        type=str,
        choices=OPERATIONS.keys(),
        help="The operation to perform.",
    )
    parser.add_argument(
        "the_dir", type=str, help="The directory containing files to manipulate."
    )

    # support variables
    parser.add_argument(
        "--var_names", type=str, nargs="+", help="Variable name(s) for which to search."
    )
    parser.add_argument(
        "--new_names", type=str, nargs="+", help="New name(s) for variables."
    )
    parser.add_argument(
        "--values",
        type=str,
        nargs="+",
        help="Values to update/add corresponding to --var_names.",
    )
    parser.add_argument(
        "--columns",
        type=int,
        nargs="+",
        default=None,
        help="Column numbers associated with --var_names when updating/adding. Use large number to "
        "specify the last column, 0 to specify beginning, negative numbers to count back from the "
        "last column, and -999999 to specify original column (if the variable already exists in "
        "the input file).",
    )
    parser.add_argument(
        "--pattern", type=str, default="*.csv", help="Search pattern for files."
    )
    parser.add_argument(
        "--skip", type=str, nargs="+", default=[], help="File names to skip."
    )
    parser.add_argument(
        "--from_version",
        type=str,
        default="2.0.0",
        help="Version from which to convert.",
    )
    parser.add_argument(
        "--to_version",
        type=str,
        default=version.__version__,
        help="Version from which to convert.",
    )

    # switches
    parser.add_argument(
        "--skip_existing",
        action="store_true",
        help="Skip variable update/add when existing.",
    )
    parser.add_argument(
        "--only_in_input",
        action="store_true",
        help="Only remove/update in model input files.",
    )
    parser.add_argument(
        "--in_place",
        action="store_true",
        help="Update versions of files in place (overwrite files).",
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="Force update of version regardless of version.",
    )

    args = vars(parser.parse_args(args=args))
    op = args.pop("operation")
    manipulate(op, **args)
    sys.stdout.write("{0:<60s}\n".format("Done!"))


if __name__ == "__main__":
    main()
