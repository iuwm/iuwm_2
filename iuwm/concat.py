# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import os
import csv


class UpdateOption:
    do_nothing = 0
    add_prefix = 1
    replace_geoid = 2

    def __init__(self, option_num, csv_file):
        self.csv_file = csv_file
        self.loc = os.path.basename(os.path.dirname(csv_file))

        if option_num == self.do_nothing:
            self.new_geoid = lambda old_geoid: old_geoid

        elif option_num == self.add_prefix:
            self.new_geoid = lambda old_geoid: "{0}{1}".format(self.loc, old_geoid)

        elif option_num == self.replace_geoid:
            self.new_geoid = lambda old_geoid: self.loc

        else:
            raise ValueError("Option number {0} unrecognized!".format(option_num))

    def update(self, row):
        row["geoid"] = self.new_geoid(row["geoid"])
        return row


def concatenate_regions(
    output_file, service_area_files, update_option=UpdateOption.do_nothing
):

    headers = None
    data = []
    for csv_file in service_area_files:
        with open(csv_file, "r") as f:
            cr = csv.DictReader(f)
            curr_headers = cr.fieldnames
            if headers is None:
                headers = curr_headers

            if curr_headers != headers:
                raise ValueError("Headers in files to concatenate must match!")

            u = UpdateOption(update_option, csv_file)
            data.extend(u.update(row) for row in cr)

    with open(output_file, "wb") as f:
        cw = csv.DictWriter(f, headers)
        cw.writeheader()
        for row in data:
            cw.writerow(row)


if __name__ == "__main__":

    # arguments for simulating
    import argparse

    parser = argparse.ArgumentParser(description="Concatenate IUWM models.")
    parser.add_argument(
        "output_file", type=str, help="Path to output IUWM service area file."
    )
    parser.add_argument(
        "service_areas", type=str, nargs="+", help="Paths to IUWM service area files."
    )
    parser.add_argument(
        "--update_option",
        type=int,
        default=UpdateOption.do_nothing,
        help="Option for updating geoid: "
        "0 - do nothing, "
        "1 - add sub-directory as prefix to geoid, "
        "2 - replace geoid with sub-directory.",
    )
    args = parser.parse_args()

    concatenate_regions(
        args.output_file, args.service_areas, update_option=args.update_option
    )
