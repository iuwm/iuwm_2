# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import csv
import os
import re


# directory locations (remain constant)
try:
    THIS_DIR = os.path.abspath(os.path.dirname(__file__))
    IUWM_FILE = os.path.join(THIS_DIR, "console.py")
except NameError:  # we are the main py2exe script, not a module
    import sys

    IUWM_FILE = os.path.abspath(sys.argv[0])
    THIS_DIR = os.path.dirname(IUWM_FILE)

DATA_DIR = os.path.abspath(os.path.join(THIS_DIR, "data"))
POSSIBLE_OUTPUTS_FILE = os.path.join(DATA_DIR, "possible_outputs.txt")
VERSION_UPDATES_FILE = os.path.join(DATA_DIR, "version_updates.csv")
SILENT_PLOTTING = True
TOLERANCE = 0.00001

# reuse containers... these must be the same list of containers that StorageContainerReuse expects
RESIDENTIAL_REUSE_CONTAINERS = ["combfi", "combpi", "flushing", "irrigation", "potable"]
CII_REUSE_CONTAINERS = ["cii"]
REUSE_CONTAINERS = [C for C in RESIDENTIAL_REUSE_CONTAINERS + CII_REUSE_CONTAINERS]
SOURCE_TYPES = {
    "roof": "roofwater",
    "storm": "stormwater",
    "gray": "graywater",
    "waste": "wastewater",
    "fixed_supply": "fixed_supply",
}

NLCD_CLASSES = [
    "open",
    "low",
    "med",
    "high",
]

# conservation scenarios
REASONABLE_SAVINGS = {
    "toilet": 0.3,
}


# read indoor use profiles
def indoor_profiles():
    with open(os.path.join(DATA_DIR, "demand_profiles.csv"), "r") as f:
        cr = csv.DictReader(f)
        profiles = {}
        print(cr.fieldnames)
        attributes = sorted(cr.fieldnames)
        end_uses = sorted(k for k in attributes if k not in {"name", "a", "b"})
        for row in cr:
            name = row.pop("name")
            profiles[name] = {k: float(v) for k, v in row.items()}
    return profiles, attributes, end_uses


DEMAND_PROFILES, DEMAND_PROFILE_ATTRIBUTES, INDOOR_END_USES = indoor_profiles()


# default output variables
def default_outputs():
    with open(os.path.join(DATA_DIR, "default_outputs.txt"), "r") as f:
        return [s.strip() for s in f.readlines() if s.strip()]


DEFAULT_OUTPUT_VARIABLES = default_outputs()

# parallel parameters
DATA_TAG = 0
MSG_TAG = 1


# events
def list_events():

    # read model file as string
    m_file = os.path.join(THIS_DIR, "model.py")
    with open(m_file, "r") as f:
        f_str = f.read()

    # all events as regex
    events = re.findall(r"""self\.fire\(['"](\w+)['"]""", f_str, flags=re.MULTILINE)

    # alternative sources - special handling
    source_events = re.findall(
        r"""self\.fire\(['"](\w+{source}\w+)['"]""", f_str, flags=re.MULTILINE
    )
    for e in source_events:
        for source in SOURCE_TYPES:
            events.append(e.format(source=SOURCE_TYPES[source]))

    # sort and return
    return sorted(events)


EVENTS = list_events()

DEFAULT_CALIBRATION_METRIC = "log_likelihood_ar1"

IUWM_PARAM_TAG = "__parameters__"
