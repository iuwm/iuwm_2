# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from constants import IUWM_FILE
import subprocess

POSITIONAL_INPUTS = [
    'service_area',
    'param_bounds_file',
    'param_file',
    'results_file',
]


def prepare_command_line(key, value):

    if value is None:
        return []

    if key == '__raw__':
        if not isinstance(value, (list, tuple)):
            msg = 'Command line parameter "__raw__" must be a list! Found type {0}: "{1}"'
            raise ValueError(msg.format(type(value), value))
        return [str(v) for v in value]

    if value:
        if isinstance(value, bool):
            if value:
                return ['--{0}'.format(key)]
            else:
                return []
        if isinstance(value, list):
            return ['--{0}'.format(key)] + [str(v) for v in value]
        else:
            return ['--{0}'.format(key), str(value)]
    else:
        return []


def iuwm_console(command, just_print=False, **kwargs):

    args = ['python', IUWM_FILE, command]
    for p in POSITIONAL_INPUTS:
        if p in kwargs:
            v = kwargs.pop(p)
            if v:
                args.append(v)

    for k, v in kwargs.items():
        args.extend(prepare_command_line(k, v))

    if just_print:
        print(args)
        return None

    verbose = kwargs.get('verbose', False)
    p = subprocess.Popen(args, stdout=None if verbose else subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    return stderr


