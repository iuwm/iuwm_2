# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from collections import OrderedDict
from constants import IUWM_PARAM_TAG
import csv
from datetime import datetime, timedelta
import inputs
import itertools
import manipulator
import numpy as np
import os
import performance
import results
from tseries import (
    truncate_date,
    DataAggs,
    matching_aggregation,
    CategorizedTimeseries,
    Timestep,
)
import utils
import warnings


class IUWMOutputAssertionError(AssertionError):
    pass


class IUWMOutputValueError(ValueError):
    pass


TIMESTEP_NAMES = {
    "daily": {"day", "daily"},
    "monthly": {"month", "monthly"},
    "yearly": {"year", "yearly", "annual"},
    "annual_average": {
        s
        for y in {"year", "yearly", "annual"}
        for s in ["average_" + y, y + "_average", "avg_" + y, y + "_avg"]
    },
}

DATE_COLS = {
    "daily": ["year", "month", "day"],
    "monthly": ["year", "month"],
    "yearly": ["year"],
    "annual_average": ["year"],
}


def standard_timestep(timestep):
    for k, names in TIMESTEP_NAMES.items():
        if timestep in names:
            return k
    raise IUWMOutputValueError('Unrecognized timestep "{0}"!'.format(timestep))


def date_grabber_daily(date):
    return datetime(date.year, date.month, date.day)


def date_grabber_monthly(date):
    return datetime(date.year, date.month, 1)


def date_grabber_yearly(date):
    return datetime(date.year, 1, 1)


DATE_GRABBERS = {
    "daily": date_grabber_daily,
    "monthly": date_grabber_monthly,
    "yearly": date_grabber_yearly,
    "annual_average": date_grabber_yearly,
}


def get_date_cols(timestep):
    return DATE_COLS[standard_timestep(timestep)]


def get_date(timestep, date):
    return DATE_GRABBERS[standard_timestep(timestep)](date)


def new_directory(file_path, new_dir):
    is_abs = utils.same_file(os.path.abspath(file_path), file_path)
    if is_abs:
        out_file = os.path.join(new_dir, os.path.basename(file_path))
    else:
        out_file = os.path.join(new_dir, file_path)
    return out_file


class OutputCsvFile:
    def __init__(self, file_path, in_memory=False, include_parameters=True):
        self.file_path = file_path
        self.directory = os.path.dirname(file_path)
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        utils.wait_until_file_is_closed(self.file_path)
        self.file = None
        self.csv = None
        self.headers = []
        self.params = []
        self.include_parameters = include_parameters
        self.in_memory = in_memory
        self.results = OrderedDict()

    def writerow(self, row):

        # build headers and params from the first result
        named_parameters = row.pop(IUWM_PARAM_TAG)
        if not self.headers:
            self.headers = [k for k in row.keys() if k != IUWM_PARAM_TAG]
        if not self.params:
            self.params = named_parameters.keys() if self.include_parameters else []

        # startup file
        if self.file is None:
            self.file = open(self.file_path, "wb")
            self.csv = csv.DictWriter(self.file, self.headers + self.params)
            self.csv.writeheader()

            if self.in_memory:
                for h in self.headers:
                    self.results[h] = []
                for p in self.params:
                    self.results[p] = []

        # write data to the row
        if self.include_parameters:
            # update output with input parameters without overwriting
            row.update({n: p for n, p in named_parameters.items() if n not in row})
        self.csv.writerow(row)
        self.file.flush()

        # store results in memory
        if self.in_memory:
            for h in self.headers:
                self.results[h].append(row[h])
            for p in self.params:
                self.results[p].append(named_parameters[p])

    def get_results(self):
        if not self.in_memory:
            raise IUWMOutputAssertionError(
                "Cannot get results unless results are stored in memory."
            )
        return self.results

    def close(self):
        self.file.flush()
        self.file.close()
        self.file = None


class OutputDefinition:

    metrics = performance.METRIC_NAMES

    def __init__(
        self,
        timestep,
        file=None,
        by_geoid=False,
        flatten=False,
        in_memory=False,
        obs_file=None,
        obs_vars=None,
        iuwm_vars=None,
        metrics=None,
        compare_file=None,
    ):

        self.file = file
        self.timestep = standard_timestep(timestep)
        self.orig_timestep = self.timestep
        self.annual_average = timestep in TIMESTEP_NAMES["annual_average"]
        if self.annual_average:
            self.timestep = standard_timestep("annual")
        self.by_geoid = by_geoid
        self.flatten = flatten
        self.in_memory = in_memory

        if obs_file or obs_vars or iuwm_vars:
            if not obs_file or not obs_vars or not iuwm_vars:
                msg = 'When specifying "obs_file", "obs_vars", or "iuwm_vars", must specify all three.'
                raise IUWMOutputValueError(msg)
            self.has_observed = True
        else:
            self.has_observed = False

        self.obs_file = obs_file
        self.obs_vars = obs_vars
        self.iuwm_vars = iuwm_vars
        if metrics is not None:
            self.metrics = metrics
        self.metrics = performance.get_metric_functions(self.metrics)
        self.compare_file = compare_file

        if self.flatten:
            self.by_geoid = True

        if self.has_observed:
            self.in_memory = True

        self.date_cols = get_date_cols(self.timestep)
        self.categories = ["geoid"] if self.by_geoid else []

        if self.file is None:
            if compare_file is None:
                if obs_file is None:
                    self.directory = "."
                else:
                    self.directory = os.path.dirname(obs_file)
            else:
                self.directory = os.path.dirname(compare_file)
            self.file = os.path.join(self.directory, "{0}.csv".format(timestep))
        else:
            self.directory = os.path.dirname(self.file)

        if not self.directory:
            self.directory = "."

    def suffix(self):
        return "{0}{1}{2}".format(
            self.orig_timestep,
            "_by_geoid" if self.by_geoid else "",
            "_flat" if self.flatten else "",
        )

    def results_file_name(self):
        return "results_{0}.csv".format(self.suffix())

    def results_file(self, the_dir=None):
        if the_dir is None:
            the_dir = self.directory
        return os.path.join(the_dir, self.results_file_name())

    def copy(self, **kwargs):
        return OutputDefinition(
            self.orig_timestep,
            file=kwargs.get("file", self.file),
            by_geoid=kwargs.get("by_geoid", self.by_geoid),
            flatten=kwargs.get("flatten", self.flatten),
            in_memory=kwargs.get("in_memory", self.in_memory),
            obs_file=kwargs.get("obs_file", self.obs_file),
            obs_vars=kwargs.get("obs_vars", self.obs_vars),
            iuwm_vars=kwargs.get("iuwm_vars", self.iuwm_vars),
            metrics=kwargs.get("metrics", self.metrics),
            compare_file=kwargs.get("compare_file", self.compare_file),
        )

    def get_obs(self, start_date=None, end_date=None):
        if self.has_observed:
            return CategorizedTimeseries.from_file(
                self.obs_file,
                categories=self.categories,
                variables=self.obs_vars,
                date_cols=self.date_cols,
                timestep=self.orig_timestep,
                start_date=start_date,
                end_date=end_date,
            )
        else:
            return None

    def get_timeseries(
        self,
        data,
        timestep=None,
        variables=None,
        fxn="sum",
        start_date=None,
        end_date=None,
    ):

        if timestep is None:
            timestep = self.timestep

        if variables is None:
            variables = self.iuwm_vars

        ct = CategorizedTimeseries(
            categories=self.categories,
            variables=variables,
            timeseries_fxn=fxn,
            date_cols=self.date_cols,
            timestep=timestep,
            start_date=start_date,
            end_date=end_date,
        )
        return ct.read_data(data)

    def is_compatible(self, obs_timestep):
        """
        Returns whether or not this output aggregator is compatible with observed data timestep. If model output is
        at a smaller timestep than included in the observed data, this aggregator is not compatible with observed.
        If model output is at the same or larger timestep than is included in the observed data, observed data will be
        aggregated up to the model output timestep.

        :param obs_timestep: Timestep of observed data
        :return: Returns whether or not this output aggregator is compatible with observed data timestep
        """
        # todo: allow observed data to be aggregated (e.g., from daily to monthly or monthly to annual) for comparison
        # return Timestep(self.timestep).enum_val >= Timestep(obs_timestep).enum_val
        return Timestep(self.timestep).enum_val == Timestep(obs_timestep).enum_val

    def add_observed(
        self, obs_timestep, obs_file, obs_vars, iuwm_vars, metrics, compare_file
    ):

        if not self.is_compatible(obs_timestep):
            msg = "Observed data at the {0} timestep cannot be compared to modeled data at the {1} timestep!"
            raise IUWMOutputValueError(msg.format(obs_timestep, self.timestep))

        return OutputDefinition(
            self.orig_timestep,
            file=self.file,
            by_geoid=self.by_geoid,
            flatten=self.flatten,
            in_memory=self.in_memory,
            obs_file=obs_file,
            obs_vars=obs_vars,
            iuwm_vars=iuwm_vars,
            metrics=metrics,
            compare_file=compare_file,
        )

    def new_directory(self, new_dir):

        out_file = new_directory(self.file, new_dir)
        compare_file = (
            new_directory(self.compare_file, new_dir) if self.compare_file else None
        )

        return OutputDefinition(
            self.orig_timestep,
            file=out_file,
            by_geoid=self.by_geoid,
            flatten=self.flatten,
            in_memory=self.in_memory,
            obs_file=self.obs_file,
            obs_vars=self.obs_vars,
            iuwm_vars=self.iuwm_vars,
            metrics=self.metrics,
            compare_file=compare_file,
        )

    def initiate(self):
        out_dir = os.path.dirname(self.file)
        if out_dir and not os.path.exists(out_dir):
            os.makedirs(out_dir)

        if self.compare_file:
            out_dir = os.path.dirname(self.compare_file)
            if out_dir and not os.path.exists(out_dir):
                os.makedirs(out_dir)

    def __contains__(self, item):
        return item in self.__dict__

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, item, value):
        setattr(self, item, value)

    def get_date(self, date):
        return get_date(self.timestep, date)

    def vars(self):
        if self.iuwm_vars and self.obs_vars:
            return [(i, o) for i, o in zip(self.iuwm_vars, self.obs_vars)]
        else:
            return []

    def dict(self):
        return self.__dict__.copy()

    @classmethod
    def from_dict(cls, d):
        params = (
            "timestep",
            "file",
            "by_geoid",
            "flatten",
            "in_memory",
            "obs_file",
            "obs_vars",
            "iuwm_vars",
            "metrics",
            "compare_file",
        )
        return OutputDefinition(**{k: v for k, v in d.items() if k in params})

    @classmethod
    def from_definition(cls, d):
        assert isinstance(
            d, OutputDefinition
        ), "To create an output definition, must be OutputDefinition"
        return d.copy()


class OutputDefinitionList(list):
    def __init__(self, definitions=(), directory=None):

        definitions = [
            OutputDefinition(**d) if not isinstance(d, OutputDefinition) else d
            for d in definitions
        ]
        if directory is not None:
            definitions = (o.new_directory(directory) for o in definitions)

        list.__init__(self, definitions)

        self.directory = directory
        if self.directory is None:
            self.directory = self.common_directory()

    def files(self):
        return [defn.file for defn in self]

    def results_file_names(self):
        return [defn.results_file_name() for defn in self]

    def results_files(self, the_dir=None):
        return [defn.results_file(the_dir=the_dir) for defn in self]

    def common_directory(self):
        if len(self) == 0:
            return None
        dirs = [o.directory for o in self]
        s = sorted([d for d in set(dirs)], key=lambda d: dirs.count(d))
        return s[-1]

    def compare_observed(
        self, obs_timestep, obs_file, obs_vars, iuwm_vars, metrics, compare_file
    ):

        # update existing output files that have the same timesteps defined
        updated = False
        ret_val = []
        for i, defn in enumerate(self):
            if defn.is_compatible(obs_timestep):
                defn = defn.add_observed(
                    obs_timestep, obs_file, obs_vars, iuwm_vars, metrics, compare_file
                )
                ret_val.append(defn)
                updated = True
            else:
                ret_val.append(defn)

                # if no existing output file, save to
        if not updated:
            msg = (
                "No output timestep matching observed timestep ({0}) provided!".format(
                    obs_timestep
                )
            )
            raise IUWMOutputValueError(msg)

        return OutputDefinitionList(ret_val)

    def new_directory(self, new_dir):
        return OutputDefinitionList(
            [o.new_directory(new_dir) for o in self], directory=new_dir
        )

    def copy(self, **kwargs):
        return OutputDefinitionList(
            [o.copy(**kwargs) for o in self], directory=self.directory
        )


class BaseAggregator:
    """
    The base output aggregator, used for daily output by default
    """

    def __init__(self, definition, geoids, output_variables, start_date, end_date):

        if isinstance(definition, dict):
            definition = OutputDefinition(**definition)

        self.defn = definition
        self.geoids = geoids
        self.geoid_indices = {g: i for i, g in enumerate(self.geoids)}
        self.output_variables = output_variables
        self.output_variables_col = {v: i for i, v in enumerate(self.output_variables)}
        self.start_date = start_date
        self.end_date = end_date

        self.obs = self.defn.get_obs(self.start_date, self.end_date)

        self.n_sites = len(geoids)
        self.n_vars = len(output_variables)

        self.all_dates = []
        self.all_data = []
        self.date = None

        # add the rest as those variables that should be summed
        self.aggregations = DataAggs(self.output_variables)

        # cache headers
        self.headers = self._headers()
        self.header_cols = {h: i for i, h in enumerate(self.headers)}

    def col_names(self, col_type):
        gs = "_by_geoid" if self.defn.flatten else ""
        default_vars = {
            "var": self.defn.iuwm_vars,
            "observed": self.defn.obs_vars,
        }[col_type]
        vs = zip(itertools.cycle([col_type + gs]), default_vars)
        geoids = self.geoids if self.defn.by_geoid else [None]
        return [results.header(t, geoid=g, var=v) for g in geoids for t, v in vs]

    def norm_geoid(self, geoid):
        if geoid is None:
            return "sum_total"
        else:
            return geoid

    def norm_col_type(self, col_type):
        if self.defn.flatten:
            gs = "_by_geoid"
            if not col_type.endswith(gs):
                col_type += gs
        return col_type

    def norm_geoids(self):
        return (
            list(self.geoids) if self.defn.by_geoid and self.defn.flatten else []
        ) + ["sum_total"]

    def out_col_name(self, col_type, var, geoid=None):
        geoid = self.norm_geoid(geoid)
        col_type = self.norm_col_type(col_type)
        return results.header(col_type, geoid=geoid, var=var)

    def out_col_i(self, col_type, var, geoid=None):
        c = self.out_col_name(col_type, var, geoid=geoid)
        return self.header_cols[c]

    def _headers(self):

        gs = "_by_geoid" if self.defn.flatten else ""
        vs = zip(itertools.cycle(["var" + gs]), self.output_variables)
        geoids = self.norm_geoids()

        if self.obs:
            vs += zip(itertools.cycle(["observed" + gs]), self.defn.obs_vars)
            vs += zip(itertools.cycle(["error" + gs]), self.defn.obs_vars)

        return (
            self.defn.date_cols
            + (["geoid"] if self.defn.by_geoid and not self.defn.flatten else [])
            + [self.out_col_name(t, v, geoid=g) for g in geoids for t, v in vs]
        )

    def current_comparison_data(self, data):

        for iuwm_var, obs_var in self.defn.vars():
            if iuwm_var not in self.output_variables_col:
                raise IUWMOutputValueError(
                    "IUWM variable {0} is not in IUWM output!".format(iuwm_var)
                )

            def get_obs(geoid):
                if utils.buried_dict_value_exists(self.obs, geoid + [obs_var]):
                    ts_obs = utils.get_buried_dict_value(self.obs, geoid + [obs_var])
                    i = np.where(
                        ts_obs.dates == truncate_date(self.date, self.defn.timestep)
                    )[0]
                    if np.any(i):
                        if len(i) > 1:
                            msg = (
                                "More than one of the same dates is provided in the observed "
                                'file for variable "{0}", goeid "{1}", and date "{2}".'
                            )
                            raise IUWMOutputValueError(
                                msg.format(
                                    obs_var, geoid, self.date.strftime("%Y-%m-%d")
                                )
                            )
                        return ts_obs.data[i[0]]
                    else:
                        return np.nan
                else:
                    return np.nan

            geoids = [[geoid] for geoid in self.geoids] if self.defn.by_geoid else [[]]
            observed = np.array([get_obs(geoid) for geoid in geoids])

            assert len(self.output_variables_col) == data.shape[1]
            modeled = data[:, self.output_variables_col[iuwm_var]]

            yield modeled, observed

    def results(self, starting_dict=None):
        res = starting_dict.copy() if starting_dict else OrderedDict()
        res = self.annual_average(res)
        if self.defn.has_observed:
            res = self.compare_data(res)
        return res

    def save_results(self, results_csv=None, results_dict=None):

        if results_csv is None:
            results_csv = self.defn.compare_file
            if results_csv is None:
                results_csv = os.path.join(
                    self.defn.directory, self.defn.results_file_name()
                )

        if results_dict is None:
            results_dict = self.results()

        was_open = False

        if isinstance(results_csv, (str, unicode)):

            results_csv = open(results_csv, "wb")

        elif isinstance(results_csv, file):

            if results_csv.closed:
                results_csv = open(results_csv.name, "wb")
            else:
                was_open = True

        else:
            raise IUWMOutputValueError(
                'Unrecognized compare_file provided of type "{0}"'.format(
                    type(results_csv)
                )
            )

        keys = results_dict.keys()
        cw = csv.DictWriter(results_csv, keys)
        cw.writeheader()
        cw.writerow({k: results_dict[k] for k in keys})

        if not was_open:
            results_csv.close()

    def annual_average(self, res=None):

        if res is None:
            res = OrderedDict()

        fxn = [
            (utils.get_fxn(v)[1] or inputs.get_aggregation(v) or "sum")
            for v in self.output_variables
        ]
        output = self.timeseries(
            output_timestep="annual", variables=self.output_variables, fxn=fxn
        )
        col_type = "var" + ("_by_geoid" if self.defn.by_geoid else "")

        if self.defn.by_geoid:

            geoids = output.keys()
            for geoid in geoids:
                ts = output[geoid]
                for var in ts:
                    nm = results.header(col_type, geoid=geoid, var=var)
                    res[nm] = ts[var].data.mean()

            for var, f in zip(self.output_variables, fxn):
                nm = results.header(col_type, geoid="sum_total", var=var)
                d = np.array([output[geoid][var].data for geoid in geoids])
                agg = matching_aggregation(f)
                res[nm] = agg.spatial_reduce(d).mean()

        else:

            for var in output:
                nm = results.header(col_type, geoid="sum_total", var=var)
                res[nm] = output[var].data.mean()

        return res

    def compare_data(self, res=None):

        if res is None:
            res = OrderedDict()

        output = self.timeseries()

        # collect modeled data and calculating metrics
        col_type = "metric" + ("_by_geoid" if self.defn.by_geoid else "")
        all_observed = {v: [] for v in self.defn.iuwm_vars}
        all_modeled = {v: [] for v in self.defn.iuwm_vars}
        for iuwm_var, obs_var in self.defn.vars():

            geoids = (
                [[geoid] for geoid in output.keys()] if self.defn.by_geoid else [[]]
            )
            for geoid in geoids:
                if utils.buried_dict_value_exists(self.obs, geoid + [obs_var]):

                    # timeseries data
                    ts_iuwm = utils.get_buried_dict_value(output, geoid + [iuwm_var])
                    ts_obs = utils.get_buried_dict_value(self.obs, geoid + [obs_var])

                    # take annual average
                    if self.defn.annual_average:
                        modeled = np.array(np.nanmean(ts_iuwm.data), ndmin=1)
                        observed = np.array(np.nanmean(ts_obs.data), ndmin=1)
                        annual_modeled = modeled
                        annual_observed = observed

                    else:

                        # match timeseries at dates
                        mi, oi = ts_iuwm.indices_at_matching_dates(ts_obs)
                        v_iuwm, v_obs = ts_iuwm.data[mi], ts_obs.data[oi]

                        # summarize to annual
                        ts_iuwm_annual = ts_iuwm.copy()
                        ts_iuwm_annual.aggregate(Timestep.annual)
                        ts_obs_annual = ts_obs.copy()
                        ts_obs_annual.aggregate(Timestep.annual)

                        # save all data
                        annual_modeled = ts_iuwm_annual.data
                        annual_observed = ts_obs_annual.data
                        modeled = v_iuwm
                        observed = v_obs

                        # use only non-nan values
                        i = ~np.isnan(observed)
                        observed = observed[i]
                        modeled = modeled[i]

                        # append to totalled data
                        if len(observed) > 0:
                            all_observed[iuwm_var].append(ts_obs)
                            all_modeled[iuwm_var].append(ts_iuwm)

                    # skip metric calculation for this geoid if no observed data exists
                    if len(observed) == 0:
                        continue

                    # build name
                    def nm(metric):
                        gs = geoid[0] if geoid else "sum_total"
                        return results.header(
                            col_type, geoid=gs, var=iuwm_var, metric=metric
                        )

                    for calc_metric in self.defn.metrics:
                        p = calc_metric(observed, modeled)
                        res[nm(calc_metric.__name__)] = p

                    res[nm("iuwm_monthly")] = modeled.mean()
                    res[nm("obs_monthly")] = observed.mean()

                    # mean annual res for the modeled variable
                    res[nm("iuwm_annual")] = annual_modeled.mean()
                    res[nm("obs_annual")] = annual_observed.mean()

        # calculate summed totals and performance of model at that scale too
        if self.defn.by_geoid:
            for iuwm_var in self.defn.iuwm_vars:

                # current var
                all_o = all_observed[iuwm_var]
                all_m = all_modeled[iuwm_var]

                if all_o and all_m:

                    assert len(all_o) == len(
                        all_m
                    ), "Observed and modeled data should have equal number of locations!"

                    # find start and end dates
                    start_date = max(min(min(o.dates) for o in all_o), self.start_date)
                    end_date = min(
                        max(max(o.dates) for o in all_o) + timedelta(days=1),
                        self.end_date,
                    )

                    # fill timeseries for all dates
                    all_m_annual = []
                    all_o_annual = []
                    for o in all_o:
                        o.aggregate(
                            self.defn.timestep, start_date=start_date, end_date=end_date
                        )

                        o_annual = o.copy()
                        o_annual.aggregate(Timestep.annual)
                        all_o_annual.append(o_annual)

                    for m in all_m:
                        m.aggregate(
                            self.defn.timestep, start_date=start_date, end_date=end_date
                        )

                        m_annual = m.copy()
                        m_annual.aggregate(Timestep.annual)
                        all_m_annual.append(m_annual)

                    # sum the data across space only for times when there are observations
                    nonnan_locs = [~np.isnan(o.data) for o in all_o]
                    observed = np.nansum(
                        [o.data * i for o, i in zip(all_o, nonnan_locs)], axis=0
                    )
                    modeled = np.nansum(
                        [m.data * i for m, i in zip(all_m, nonnan_locs)], axis=0
                    )

                    nonnan_locs_annual = [~np.isnan(o.data) for o in all_o_annual]
                    annual_observed = np.nansum(
                        [o.data * i for o, i in zip(all_o_annual, nonnan_locs_annual)],
                        axis=0,
                    )
                    annual_modeled = np.nansum(
                        [m.data * i for m, i in zip(all_m_annual, nonnan_locs_annual)],
                        axis=0,
                    )

                    # build name
                    def nm(metric):
                        return results.header(
                            col_type, geoid="sum_total", var=iuwm_var, metric=metric
                        )

                    # calculate metrics for the summed totals
                    for calc_metric in self.defn.metrics:
                        p = calc_metric(observed, modeled)
                        res[nm(calc_metric.__name__)] = p

                    res[nm("iuwm_monthly")] = modeled.mean()
                    res[nm("obs_monthly")] = observed.mean()

                    # averaged totals
                    res[nm("iuwm_annual")] = annual_modeled.mean()
                    res[nm("obs_annual")] = annual_observed.mean()

                else:
                    warnings.warn(
                        "No matching geoids found in the observed data! Make sure geoids match exactly in the model "
                        'as in the observed data for "{0}" (even trailing zeros and spaces matter)!'.format(
                            iuwm_var
                        )
                    )

        return res

    def current_date_comparison(self, data):
        obs_cols = []
        err_cols = []
        for modeled, observed in self.current_comparison_data(data):
            obs_cols.append(observed)
            err_cols.append(performance.residuals(observed, modeled))
        return obs_cols + err_cols

    def write_headers(self):
        # headers
        if self.defn.file is not None:
            if isinstance(self.defn.file, str):
                the_dir = os.path.dirname(self.defn.file)
                if the_dir and not os.path.exists(the_dir):
                    os.makedirs(the_dir)
                self.defn.file = open(self.defn.file, "w")
            cw = csv.writer(self.defn.file)
            cw.writerow(self.headers)

    def add_cols(self, data, new_cols_data, right=False):

        new_cols_array = np.empty((data.shape[0], len(new_cols_data)), dtype=np.object)
        for i, v in enumerate(new_cols_data):
            new_cols_array[:, i] = v

        if right:
            return np.hstack((data, new_cols_array))
        else:
            return np.hstack((new_cols_array, data))

    def output_date(self):
        return self.defn.get_date(self.date)

    def add_date_cols(self, data):
        # columns to the left
        left_cols = [
            getattr(self.date, period)
            for period in DATE_COLS["daily"]
            if period in self.defn.date_cols
        ]
        if self.defn.by_geoid and not self.defn.flatten:
            left_cols.append(self.geoids)
        data = self.add_cols(data, left_cols, right=False)
        return data

    def add_obs_cols(self, data):
        if self.obs is not None:
            right_cols = self.current_date_comparison(data)
            data = self.add_cols(data, right_cols, right=True)
        return data

    def should_write(self):
        return True

    def initiate(self):
        self.all_dates = []
        self.all_data = []

    def aggregate(self, date, data):
        self.date = date

        if not self.defn.by_geoid:
            # aggregate spatially if not "by_geoid"
            data = self.aggregations.spatial_reduce(data)

        # aggregate temporally
        self.aggregations.advance(data)

        if self.should_write():

            # reduce values across time appropriately
            d = self.aggregations.temporal_reduce()

            # write output and start data over
            self.write(d)

    def write(self, data):

        # return when not writing anything
        if self.defn.file is None and not self.defn.in_memory:
            return

        # add observed data columns
        data = self.add_obs_cols(data)

        # flatten output data
        if self.defn.flatten:
            data = data.flatten().reshape((1, -1))

        # add date columns
        data = self.add_date_cols(data)

        # write output to file
        if self.defn.file:
            cw = csv.writer(self.defn.file)
            for row in data:
                cw.writerow(row)

        # save output to memory
        if self.defn.in_memory:
            self.all_dates.append(self.date)
            self.all_data.append(data)

    def read(self):

        if self.defn.file is None:
            return
        if self.defn.flatten:
            raise NotImplementedError(
                "Reading output that is flattened is not implemented yet"
            )

        if isinstance(self.defn.file, str):
            self.defn.file = open(self.defn.file, "r")

        elif isinstance(self.defn.file, file):
            if not self.defn.file.closed:
                self.defn.file.flush()
                self.defn.file.close()
            self.defn.file = open(self.defn.file.name, "r")

        file_name = self.defn.file.name

        cr = csv.reader(self.defn.file)
        self.all_dates = []
        self.all_data = []
        headers = next(row for row in cr)

        def get_col(row, col, default=None, dtype=str):
            if col not in headers:
                if default is None:
                    assert col in headers, "Column {0} must exist in {1}".format(
                        col, file_name
                    )
                else:
                    return default
            return dtype(row[headers.index(col)])

        for row in cr:

            year = get_col(row, "year", dtype=int)
            month = get_col(
                row, "month", None if "month" in self.defn.date_cols else 1, dtype=int
            )
            day = get_col(
                row, "day", None if "day" in self.defn.date_cols else 1, dtype=int
            )
            if self.defn.by_geoid:
                get_col(row, "geoid")
            date = datetime(year, month, day)
            self.all_dates.append(date)
            self.all_data.append(np.array(row, ndmin=2))

    def close(self):
        if (
            self.defn.file is not None
            and not self.defn.file.closed
        ):
            file_path = self.defn.file.name
            self.defn.file.flush()
            self.defn.file.close()
            self.defn.file = file_path

        if self.defn.compare_file is not None:
            self.save_results(self.defn.compare_file)

    def data_in_memory(self, rows_as_dicts=False):
        headers = self.headers
        data = np.concatenate(self.all_data)
        if rows_as_dicts:
            if len(headers) != data.shape[1]:
                raise IUWMOutputAssertionError(
                    "Headers must have the same length as 2nd dim of data!"
                )
            return headers, ({h: row[i] for i, h in enumerate(headers)} for row in data)
        return headers, data

    def timeseries(self, output_timestep=None, variables=None, fxn="sum"):
        headers, data = self.data_in_memory(rows_as_dicts=True)
        return self.defn.get_timeseries(
            data,
            timestep=output_timestep,
            variables=variables,
            fxn=fxn,
            start_date=self.start_date,
            end_date=self.end_date,
        )


class MonthlyAggregator(BaseAggregator):

    date_cols = ["year", "month"]

    def should_write(self):
        next_date = self.date + timedelta(days=1)
        return next_date.day == 1 or self.date == self.end_date


class YearlyAggregator(BaseAggregator):

    date_cols = ["year"]

    def should_write(self):
        next_date = self.date + timedelta(days=1)
        return next_date.timetuple().tm_yday == 1 or self.date == self.end_date


class AnnualAverageAggregator(YearlyAggregator):
    def __init__(self, defn, geoids, output_variables, start_date, end_date):

        self.moved_yearly = False
        if defn.file and os.path.exists(defn.file):
            os.remove(defn.file)
        defn.in_memory = True
        YearlyAggregator.__init__(
            self, defn, geoids, output_variables, start_date, end_date
        )

    def data_in_memory(self, rows_as_dicts=False):
        headers, data = YearlyAggregator.data_in_memory(self)
        if self.defn.by_geoid:
            if self.defn.flatten:
                data = data.mean(axis=0, keepdims=True)
                year_col = headers.index("year")
                data = np.delete(data, year_col, axis=1)
                headers.pop(year_col)
            else:
                # rearrange headers
                geoid_col = headers.index("geoid")
                year_col = headers.index("year")
                headers.remove("geoid")
                headers.remove("year")
                headers.insert(0, "geoid")

                # rearrange data
                geoids = data[:, geoid_col]
                data = np.delete(data, [geoid_col, year_col], axis=1)
                geoid_data = []
                for geoid in self.geoids:
                    i = geoids == geoid
                    curr_data = data[i].mean(axis=0, keepdims=True)
                    geoid_data.append(np.hstack(([[geoid]], curr_data)))
                data = np.concatenate(geoid_data, axis=0)

        else:
            year_col = headers.index("year")
            headers.remove("year")
            data = np.delete(data, year_col, axis=1)
            data = data.mean(axis=0, keepdims=True)

        if rows_as_dicts:
            ind = zip(headers, range(data.shape[1]))
            data = ({h: row[i] for h, i in ind} for row in data)

        return headers, data

    def write_avg_annual(self):

        # write the new file
        headers, data = self.data_in_memory()
        with open(self.defn.file, "wb") as fh:
            cw = csv.writer(fh)
            cw.writerow(headers)
            for row in data:
                cw.writerow(row)

    def get_yearly_file(self):
        return "{0}_yearly{1}".format(*os.path.splitext(self.defn.file))

    def close(self):

        # close the file
        YearlyAggregator.close(self)

        # move the yearly file
        if os.path.exists(self.defn.file) and not self.moved_yearly:

            yearly_file = self.get_yearly_file()
            manipulator.move(self.defn.file, yearly_file)
            self.moved_yearly = True

        # write average annual data to output file
        if self.all_data:
            self.write_avg_annual()


OUTPUT_AGGREGATORS = {
    "daily": BaseAggregator,
    "monthly": MonthlyAggregator,
    "yearly": YearlyAggregator,
    "annual_average": AnnualAverageAggregator,
}


class OutputAggregator:

    classes = OUTPUT_AGGREGATORS

    def __init__(
        self,
        definitions=(),
        geoids=None,
        variables=None,
        start_date=None,
        end_date=None,
    ):

        self._def = OutputDefinitionList()
        self._agg = []

        for defn in definitions:
            if isinstance(defn, dict):
                defn = OutputDefinition.from_dict(defn)
            elif isinstance(defn, OutputDefinition):
                defn = OutputDefinition.from_definition(defn)
            else:
                raise IUWMOutputValueError(
                    "Definitions for aggregators must either be type dict or OutputDefinition"
                )
            self._def.append(defn)
        self.directory = self._def.directory

        self.geoids = geoids
        self.variables = variables
        self.start_date = start_date
        self.end_date = end_date

        if geoids is not None or variables is not None or start_date or end_date:
            if not (
                geoids is not None and variables is not None and start_date and end_date
            ):
                raise IUWMOutputAssertionError(
                    "Must provide all geoids, variables, start and end date!"
                )
            self._agg = [
                self.classes[d.timestep](d, geoids, variables, start_date, end_date)
                for d in self._def
            ]

    def add(self, defn):
        if isinstance(defn, dict):
            defn = OutputDefinition(**defn)
        return OutputAggregator(
            definitions=self._def + [defn],
            geoids=self.geoids,
            variables=self.variables,
            start_date=self.start_date,
            end_date=self.end_date,
        )

    def copy(self, **kwargs):
        return OutputAggregator(
            definitions=OutputDefinitionList(self._def).copy(**kwargs),
            geoids=self.geoids,
            variables=self.variables,
            start_date=self.start_date,
            end_date=self.end_date,
        )

    def build(self, geoids, variables, start_date, end_date):
        return OutputAggregator(
            self._def,
            geoids=geoids,
            variables=variables,
            start_date=start_date,
            end_date=end_date,
        )

    def filter_i(self, return_all=False, **filters):

        defn = [
            i
            for i, d in enumerate(self._def)
            if sum(d[k] == v for k, v in filters.items()) == len(filters)
        ]

        if return_all:
            return defn

        if len(defn) == 1:
            return defn[0]
        elif len(defn) == 0:
            return -1
        else:
            raise IUWMOutputAssertionError(
                "Search for matching output aggregation is too vague! {0}".format(
                    filters
                )
            )

    def get_definition(self, **filters):
        i = self.filter_i(**filters)
        if i == -1:
            return None
        else:
            return self._def[i]

    def get_aggregator(self, **filters):
        if "return_all" in filters:
            raise IUWMOutputAssertionError(
                "Cannot call get_aggregator with return_all!"
            )
        i = self.filter_i(**filters)
        if i == -1:
            return None
        else:
            return self._agg[i]

    def aggregators(self, **filters):
        i = self.filter_i(return_all=True, **filters)
        return [self._agg[ii] for ii in i]

    def daily(self, by_geoid):
        return self.get_aggregator(timestep="daily", by_geoid=by_geoid)

    def monthly(self, by_geoid):
        return self.get_aggregator(timestep="monthly", by_geoid=by_geoid)

    def yearly(self, by_geoid):
        return self.get_aggregator(timestep="yearly", by_geoid=by_geoid)

    def observed(self):
        return self.aggregators(has_observed=True)

    def named(self, the_dir=None, **filters):
        aggs = self.aggregators(**filters)
        return {agg.defn.results_file(the_dir=the_dir): agg for agg in aggs}

    def named_observed(self, the_dir=None):
        return self.named(the_dir=the_dir, has_observed=True)

    def named_results(self, starting_dict=None, the_dir=None, **filters):
        named = self.named(the_dir=the_dir, **filters)
        return {
            k: agg.results(starting_dict=starting_dict.copy())
            for k, agg in named.items()
        }

    def new_directory(self, new_dir):
        return OutputDefinitionList(
            a.new_directory(new_dir) for a in self.aggregators()
        )

    def write_headers(self):
        for a in self.aggregators():
            a.write_headers()

    def initiate(self):
        for a in self.aggregators():
            a.initiate()

    def aggregate(self, date, data):
        for a in self.aggregators():
            a.aggregate(date, data)

    def close(self):
        for a in self.aggregators():
            a.close()
