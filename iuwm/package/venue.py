# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from iuwm.alternative_sources import CIISourceWaterRecycler
from iuwm.demands import UrbanDemandCenter, UrbanDemand


class VenueSourceWaterRecycler(CIISourceWaterRecycler):

    end_uses = CIISourceWaterRecycler.end_uses + [
        'venue_flushing',
        'venue_irrigation',
        'venue_cool_irr',  # cooling and irrigation
        'animal_washing',
        'venue_combfi',
        'venue_combwi',
        'venue_combfwi',  # toilet flushing, animal washing, and irrigation
        'venue_combfcwi',  # toilet flushing, car washing, animal washing, and irrigation
        'venue_combcfcwi',  # cooling, toilet flushing, car washing, animal washing, and irrigation
        'venue_process',
        'venue_all',
    ]

    def list_end_use_demands(self):

        cii = self.model.data.cii
        irrigation = self.model.data.outdoor.irrigation
        flushing = [
            cii.venue.attendees.men.toilet,
            cii.venue.attendees.men.urinal,
            cii.venue.attendees.women.toilet,
            cii.venue.employees.men.toilet,
            cii.venue.employees.men.urinal,
            cii.venue.employees.women.toilet,
        ]
        washing = [
            cii.venue.animals.washing
        ]
        process = [
            cii.venue.other.clothes,
            cii.venue.other.powerwash,
            cii.venue.other.waterlab,
            cii.venue.other.cooling,
            cii.venue.other.dust,
            cii.venue.other.demonstration,
            cii.venue.other.carwash,
        ]

        return CIISourceWaterRecycler.list_end_use_demands(self) + [
            flushing,
            irrigation,
            [irrigation, cii.venue.other.cooling],
            washing,
            flushing + [irrigation],
            washing + [irrigation],
            flushing + washing + [irrigation],
            flushing + washing + [cii.venue.other.carwash, irrigation],
            flushing + washing + [cii.venue.other.carwash, irrigation, cii.venue.other.cooling],
            process,
            flushing + washing + [irrigation] + process,
        ]


def venue_water_use(model, person):
    """
    Calculate water use by attendees

    :param model: the model with all of its data
    :param person: the descriptor of the person that is using water
    :param kwargs: additional parameters passed for the event
    """

    # generic gallons per use
    toilet_gpu = model.get_param('toilet_gpu')
    urinal_gpu = model.get_param('urinal_gpu')
    faucet_gpu = model.get_param('faucet_gpu')
    shower_gpu = model.get_param('shower_gpu')

    # number of attendees
    plural = '{0}s'.format(person)
    n_people = model.get_param(plural)
    fraction_men = model.get_param('{0}_percent_men'.format(person)) / 100.0

    # number of uses per attendee per day
    men_toilet_uses = model.get_param('{0}_men_toilet_uses'.format(person))
    men_urinal_uses = model.get_param('{0}_men_urinal_uses'.format(person))
    women_toilet_uses = model.get_param('{0}_women_toilet_uses'.format(person))
    faucet_uses = model.get_param('{0}_faucet_uses'.format(person))
    shower_uses = model.get_param('{0}_shower_uses'.format(person))

    # other parameters
    fc = model.get_param('indoor_percent_consumed') / 100.0

    # number of men and women
    men = n_people * fraction_men
    women = n_people * (1 - fraction_men)

    # total number of toilets
    men_toilet = men * men_toilet_uses * toilet_gpu
    men_urinal = men * men_urinal_uses * urinal_gpu
    women_toilet = women * women_toilet_uses * toilet_gpu
    faucet = n_people * faucet_uses * faucet_gpu
    shower = n_people * shower_uses * shower_gpu

    # calculate total attendee usage per day
    return UrbanDemandCenter(
        plural,
        UrbanDemandCenter(
            'men',
            UrbanDemand('toilet', men_toilet, consumed=fc, black=1 - fc),
            UrbanDemand('urinal', men_urinal, consumed=fc, black=1 - fc),
        ),
        UrbanDemandCenter(
            'women',
            UrbanDemand('toilet', women_toilet, consumed=fc, black=1 - fc),
        ),
        UrbanDemand('faucet', faucet, consumed=fc, black=0, gray=1 - fc),
        UrbanDemand('shower', shower, consumed=fc, black=0, gray=1 - fc),
    )


def animal_consumption(model, animal):

    n_animals = model.get_param(animal)
    gpu = model.get_param('{0}_gpu'.format(animal))
    uses = model.get_param('{0}_uses'.format(animal))
    return n_animals, n_animals * gpu * uses


def animal_water_use(model):

    cattle, cattle_use = animal_consumption(model, 'cattle')
    horse, horse_use = animal_consumption(model, 'horse')
    sheep, sheep_use = animal_consumption(model, 'sheep')
    swine, swine_use = animal_consumption(model, 'swine')

    n_animals = cattle + horse + sheep + swine
    n_washes = model.get_param('wash_uses')
    wash_gpu = model.get_param('wash_gpu')
    wc = model.get_param('percent_wash_consumed') / 100.0
    washing = n_animals * wash_gpu * n_washes

    return UrbanDemandCenter(
        'animals',
        UrbanDemand('cattle', cattle_use, consumed=1.0, black=0.0),
        UrbanDemand('horse', horse_use, consumed=1.0, black=0.0),
        UrbanDemand('sheep', sheep_use, consumed=1.0, black=0.0),
        UrbanDemand('swine', swine_use, consumed=1.0, black=0.0),
        UrbanDemand('washing', washing, consumed=wc, black=1 - wc),
    )


def special_use(model, name, gpu_name, use_name, consumed=0.0, gray=0.0, black=1.0, min_mixing_fraction=0.0):

    vol = model.get_param(gpu_name)
    factor = model.get_param(use_name)

    return UrbanDemand(
        name,
        vol * factor,
        consumed=consumed,
        gray=gray,
        black=black,
        min_mixing_fraction=min_mixing_fraction,
    )


def special_uses(model):

    mmf = model.get_param('venue_min_mixing_percent') / 100.0

    return UrbanDemandCenter(
        'other',
        special_use(model, 'dish', 'dish_gpu', 'dish_use', min_mixing_fraction=mmf, consumed=0.1, black=0.9),
        special_use(model, 'clothes', 'clothes_gpu', 'clothes_use', min_mixing_fraction=0.0, consumed=0.1, black=0.9),
        special_use(model, 'powerwash', 'powerwash_gpu', 'powerwash_use', min_mixing_fraction=0.0, consumed=1.0, black=0.0),
        special_use(model, 'waterlab', 'waterlab_gpm', 'waterlab_minutes', min_mixing_fraction=mmf, consumed=0.1, black=0.9),
        special_use(model, 'cooling', 'cooling_gal_per_area', 'cooling_area', min_mixing_fraction=0.0, consumed=1.0, black=0.0),
        special_use(model, 'dust', 'dust_gal_per_area', 'dust_area', min_mixing_fraction=0.0, consumed=1.0, black=0.0),
        special_use(model, 'demonstration', 'demonstration_gpm', 'demonstration_minutes', min_mixing_fraction=0.0, consumed=0.1, black=0.9),
        special_use(model, 'carwash', 'carwash_gpu', 'carwash_use', min_mixing_fraction=0.0, consumed=0.3, black=0.7),
        special_use(model, 'concessions', 'concessions_gal_per_attendee', 'attendees', min_mixing_fraction=mmf, consumed=0.7, black=0.3),
    )


def calculate_all_uses(model, **kwargs):

    # percent of total use that are leaks
    leak_fraction = model.get_param('venue_leak_percent') / 100.0
    frac_bl = model.get_param('venue_leak_percent_blackwater') / 100.0
    frac_co = 1 - frac_bl

    # get the uses
    uses = [
        venue_water_use(model, 'attendee'),
        venue_water_use(model, 'employee'),
        animal_water_use(model),
        special_uses(model),
    ]

    # update cii demand
    venue = UrbanDemandCenter('venue', *uses).leaky(leak_fraction, consumed=frac_co, gray=0.0, black=frac_bl)
    model.data.cii.add(venue)


def on_output_writing(model, **kwargs):

    cii = model.data.cii
    flushing = [
        cii.venue.attendees.men.toilet,
        cii.venue.attendees.men.urinal,
        cii.venue.attendees.women.toilet,
        cii.venue.employees.men.toilet,
        cii.venue.employees.men.urinal,
        cii.venue.employees.women.toilet,
    ]
    irrigation = model.data.outdoor.irrigation
    animals = cii.venue.animals
    washing = animals.washing
    cleaning = [
        cii.venue.other.dish,
        cii.venue.other.clothes,
        cii.venue.other.powerwash,
    ]
    process = [
        cii.venue.other.waterlab,
        cii.venue.other.cooling,
        cii.venue.other.dust,
        cii.venue.other.demonstration,
        cii.venue.other.carwash,
        cii.venue.other.concessions,
    ]

    cii.venue_flushing_total_use = sum(d.total_use for d in flushing)
    cii.venue_irrigation_total_use = irrigation.total_use
    cii.venue_animal_total_use = animals.total_use
    cii.venue_animal_washing_total_use = washing.total_use
    cii.venue_cleaning_total_use = sum(d.total_use for d in cleaning)
    cii.venue_process_total_use = sum(d.total_use for d in process)

    cii.venue_flushing_demand = sum(d.demand for d in flushing)
    cii.venue_irrigation_demand = irrigation.demand
    cii.venue_animal_demand = animals.demand
    cii.venue_animal_washing_demand = washing.demand
    cii.venue_cleaning_demand = sum(d.demand for d in cleaning)
    cii.venue_process_demand = sum(d.demand for d in process)


def subscribe(model):

    model.registered_alternative_uses['cii'] = VenueSourceWaterRecycler

    model.subscribe(calculate_all_uses, event_name='on_cii_calculated')
    model.subscribe(on_output_writing)
