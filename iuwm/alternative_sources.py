# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from collections import OrderedDict

import numpy as np

import constants
import summing
from components import IUWMObject


class AlternativeWater(IUWMObject):
    def __init__(self, name, supply, cii, res):
        IUWMObject.__init__(self, name)
        self.supply = supply

        self.cii = cii
        self.res = res
        self.recycle_centers = [self.cii, self.res]

        self.other_variables = {'outflow'}

    def __getattr__(self, name):
        if name in summing.REUSE_SUMMING_FUNCTIONS:
            f = summing.REUSE_SUMMING_FUNCTIONS[name]
            return f(self.recycle_centers)
        elif name == 'outflow':
            return self.supply - self.reused - self.change_in_storage
        else:
            raise AttributeError('Attribute {0} does not exist in "{1}"!'.format(name, self.name))

    def items(self):
        return list(self.__dict__.items()) + \
               [(k, self.__getattr__(k)) for k in summing.REUSE_SUMMING_FUNCTIONS] + \
               [(k, self.__getattr__(k)) for k in self.other_variables]

    def cii_use(self, supply):
        return self.cii.use(supply)

    def res_use(self, supply):
        return self.res.use(supply)

    def use(self, model):

        # use for CII purposes
        devoted = self.cii_use(self.supply)
        leftover = self.supply - devoted
        self.check(leftover)

        # use for residential purposes
        devoted += self.res_use(leftover)
        leftover = self.supply - devoted
        self.check(leftover)

    def check(self, leftover):
        if np.any(leftover < -constants.TOLERANCE):
            raise ValueError('Leftover water is less than zero by {0}!'.format(-np.min(leftover)))


class Graywater(AlternativeWater):
    def __init__(self, name, model):

        supply = model.data.cii.gray_produced + model.data.indoor.gray_produced

        storage_of_gray_for_cii = model.data.storage_of_gray_for_cii
        storage_of_gray_for_res = model.data.storage_of_gray_for_res

        AlternativeWater.__init__(self, name, supply, storage_of_gray_for_cii, storage_of_gray_for_res)

    def use(self, model):
        cii_devoted = self.cii_use(model.data.cii.gray_produced)
        res_devoted = self.res_use(model.data.indoor.gray_produced)
        leftover = self.supply - cii_devoted - res_devoted
        self.check(leftover)


class Stormwater(AlternativeWater):
    def __init__(self, name, model):

        supply = model.data.outdoor.stormwater.gallons

        storage_of_storm_for_cii = model.data.storage_of_storm_for_cii
        storage_of_storm_for_res = model.data.storage_of_storm_for_res

        AlternativeWater.__init__(self, name, supply, storage_of_storm_for_cii, storage_of_storm_for_res)


class Roofwater(AlternativeWater):
    def __init__(self, name, model):

        supply = model.data.outdoor.roof.gallons

        storage_of_roof_for_cii = model.data.storage_of_roof_for_cii
        storage_of_roof_for_res = model.data.storage_of_roof_for_res

        AlternativeWater.__init__(self, name, supply, storage_of_roof_for_cii, storage_of_roof_for_res)


class Wastewater(AlternativeWater):
    def __init__(self, name, model):

        indoor = model.data.indoor
        cii = model.data.cii
        graywater = model.data.graywater

        supply = indoor.black_produced + cii.black_produced + graywater.outflow

        storage_of_waste_for_cii = model.data.storage_of_waste_for_cii
        storage_of_waste_for_res = model.data.storage_of_waste_for_res

        AlternativeWater.__init__(self, name, supply, storage_of_waste_for_cii, storage_of_waste_for_res)


class FixedSupplyWaterSource(AlternativeWater):
    def __init__(self, name, model):
        supply = model.get_param('{source}'.format(source=name))

        res_name = 'storage_of_{source}_for_res'.format(source=name)
        cii_name = 'storage_of_{source}_for_cii'.format(source=name)

        res_storage = model.data[res_name]
        cii_storage = model.data[cii_name]

        AlternativeWater.__init__(self, name, supply, cii_storage, res_storage)


class RecycleBin(IUWMObject):
    def __init__(self, name, capacity, adoption, fraction_available, starting_percent=0, max_reuse=np.inf):

        IUWMObject.__init__(self, name)
        self.capacity = capacity * adoption
        self.adoption = adoption
        self.storage = starting_percent * capacity
        self.prev_storage = self.storage
        self.spill = np.zeros(self.storage.shape)
        self.reused = np.zeros(self.storage.shape)
        self.change_in_storage = np.zeros(self.storage.shape)
        self.fraction_available = fraction_available
        self.max_reuse = max_reuse
        self.any_adoption = np.any(self.adoption)

    def advance(self):
        self.prev_storage = self.storage
        self.spill = np.zeros(self.storage.shape)
        self.reused = np.zeros(self.storage.shape)
        self.change_in_storage = np.zeros(self.storage.shape)

    def add(self, produced, purpose):
        """
        Adds produced water while simultaneously removing demand from the storage container ("recycle bin"). If the
        storage container cannot add any more water, spill occurs. If the storage container has no more water available
        to be used, it reduces the demand by the amount that can be extracted from the storage container. This estimates
        the amount of water that was reused, which is then returned.

        Both "produced" and "purpose" are multiplied by the adoption fraction of this particular fit-for-purpose reuse
        system. Also, "inflow" is reduced to the fraction that is available for reuse, since not 100% of water supply
        of a particular water source will always be available for reuse.

        :param produced:  The amount of source water (graywater, blackwater, or stormwater) produced during this timestep.
                        This represents the amount of water that is stored for use either within this timestep or a
                        future timestep.
        :param purpose: The end-use purpose (UrbanDemand or Indoor or Outdoor objects) for this water source either
                        by potable or non-potable (irrigation and flushing) uses.
        :return:        Returns the amount of water that was reused, or applied toward the specified demand. This value
                        should always be within [0, demand].
        """

        # reduce total produced and total water use by the adoption fractions
        devoted_available_water = produced * self.adoption
        inflow = devoted_available_water * self.fraction_available
        if np.any(inflow < 0):
            raise Exception('Internal model error: Inflows should be positive!')
        reused_max = np.minimum(self.adoption * purpose.max_reuse, self.max_reuse)
        if np.any(reused_max < 0):
            raise Exception('Internal model error: Demands should be positive!')

        # water available for reuse
        water_available = np.minimum(self.storage + inflow, reused_max)
        reused = purpose.reuse(water_available)
        if np.any(reused < 0):
            raise Exception('Internal model error: Reused amounts should be positive!')
        self.reused += reused

        # calculate next period's storage without bounds...
        # round because floating point errors will cause storage to be negative sometimes
        next_storage = np.round(self.storage + inflow - reused, 7)

        # upper bound on storage, spill when exceeded
        self.storage = np.minimum(next_storage, self.capacity)
        if np.any(self.storage < 0):
            raise Exception('Internal model error: Storage should be positive!')
        curr_spill = next_storage - self.storage
        self.spill += curr_spill
        devoted_available_water -= curr_spill

        # update the final change in storage
        self.change_in_storage = self.storage - self.prev_storage
        produced -= devoted_available_water
        if np.any(produced < 0):
            raise Exception('Internal model error: Produced should be positive!')
        return devoted_available_water

    @classmethod
    def new(cls, data):
        if isinstance(data, dict):
            return cls(**data)
        elif isinstance(data, cls):
            return data
        else:
            raise ValueError('Cannot create StorageContainer with data of type {0}'.format(type(data)))


class RecyclingCenter(IUWMObject):

    end_uses = []

    def __init__(self, source, model, fraction_available=1.0):

        # set instance variables
        IUWMObject.__init__(self, source)

        self.model = model
        self.source = source
        self.recycle_bins = []
        self.fraction_available = fraction_available
        self.n_end_uses = len(self.end_uses)
        self.zeros = np.zeros(model.n_sites)

        for eu in self.end_uses:
            rb = model.new_recycle_bin(source, eu)
            self.recycle_bins.append(rb)
            if self.n_end_uses > 1:
                self[eu] = rb

        if len(self.recycle_bins) == 0:
            raise ValueError('Must provide non-empty "recycle_bins"!')

        if np.asarray(self.adoption > 1).any():
            raise ValueError('Technology adoption factors must sum to less than or equal to 1!')

    def advance(self):
        for recycle_bin in self.recycle_bins:
            recycle_bin.advance()

    def __getattr__(self, name):
        if name in summing.REUSE_SUMMING_FUNCTIONS:
            f = summing.REUSE_SUMMING_FUNCTIONS[name]
            return f(self.recycle_bins)
        else:
            raise AttributeError('Attribute {0} does not exist in "{1}"!'.format(name, self.name))

    def items(self):
        return list(self.__dict__.items()) + \
               [(k, self.__getattr__(k)) for k in summing.REUSE_SUMMING_FUNCTIONS]

    def end_use(self, water_available, purpose, container):
        if container.any_adoption:
            if hasattr(purpose, '__iter__'):
                return sum([container.add(water_available, p) for p in purpose])
            else:
                return container.add(water_available, purpose)
        return self.zeros

    def list_end_use_demands(self):
        raise NotImplementedError('"list_end_use_demands" is not implemented, but should be!')

    def use(self, reuse_supply):

        # only a fraction of the total supply is available for reuse
        q_avail = reuse_supply * self.fraction_available
        leftover = np.copy(q_avail)

        # check
        end_uses = self.list_end_use_demands()
        if len(end_uses) != len(self.recycle_bins):
            raise ValueError('Parameter "end_uses" must be the same length as "recycle_bins"!')

        # apply available water to each end use and return the total amount of water devoted to be reused
        devoted = sum([self.end_use(leftover, eu, container=rb) for eu, rb in zip(end_uses, self.recycle_bins)])
        if np.any(np.abs(leftover - (q_avail - devoted)) > constants.TOLERANCE):
            raise ValueError('Internal model error: Leftover reused water does not equal supply minus devoted!')
        return devoted


class CIIRecyclingCenter(RecyclingCenter):

    end_uses = ['cii']

    def list_end_use_demands(self):
        return [self.model.data.cii]


class ResidentialRecyclingCenter(RecyclingCenter):

    end_uses = ['flushing', 'potable', 'irrigation', 'combfi', 'combpi']

    def list_end_use_demands(self):
        indoor = self.model.data.indoor
        irrigation = self.model.data.outdoor.irrigation
        return [indoor.toilet, indoor, irrigation, [indoor.toilet, irrigation], [indoor, irrigation]]


SOURCES = OrderedDict((
    ('roof', Roofwater),
    ('storm', Stormwater),
    ('gray', Graywater),
    ('waste', Wastewater),
    ('fixed_supply', FixedSupplyWaterSource),
))
