# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import csv
import matplotlib
if constants.SILENT_PLOTTING:
    matplotlib.use('Agg')  # Force matplotlib to not use any Xwindows backend.
import matplotlib.pyplot as plt
import multiprocessing
import numpy as np
import os
import performance
import results
import sys

np.warnings.simplefilter('ignore')


def get_output_file(output_dir, file_name, sub=None):
    if sub is None:
        out_dir = output_dir
    else:
        out_dir = os.path.join(output_dir, sub)

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    return os.path.join(out_dir, file_name)


def get_cumulative_performance(metric, param_values, perf):

    # sort by parameter value
    i = np.argsort(param_values)
    param_values = param_values[i]
    perf = perf[i]

    # transform
    transformed = performance.get_transformer(metric)(perf)

    # build cumulative graph
    incremental = transformed / transformed.sum()
    cumulative = np.cumsum(incremental)
    return param_values, perf, transformed, incremental, cumulative


class ParameterData:

    def __init__(self, param, metric, header, param_values, perf, output_dir, lazy=True):

        self.header = header
        self.param = param
        self.metric = metric
        self.metric_name = performance.get_metric_name(self.metric)
        self.param_values = param_values
        self.perf = perf
        self.csv_headers = ['param_value', 'value_of_' + self.metric, 'transformed', 'incremental', 'cumulative']
        self.calculated = False
        self.output_dir = output_dir
        self._percentiles = [2.5, 5, 10, 25, 50, 75, 90, 95, 97.5]

        if not lazy:
            self._calc()

    def _calc(self):
        if not self.calculated:
            par, perf, tra, inc, cum = get_cumulative_performance(self.metric, self.param_values, self.perf)

            self.param_values = par
            self.perf = perf
            self.transformed = tra
            self.incremental = inc
            self.cumulative = cum

            self.calculated = True

    def mode(self):
        self._calc()
        i = np.argmax(self.transformed)
        return self.param_values[i], self.transformed[i]

    def percentile(self, q):
        self._calc()
        param_value = np.interp(q / 100.0, self.cumulative, self.param_values)
        perf = np.interp(param_value, self.param_values, self.transformed)
        return param_value, perf

    def summary_headers(self):
        labels = ['mode'] + ['perc_{0}'.format(v) for v in self._percentiles]
        headers = ['header', 'param'] + labels + ['{0}_{1}'.format(l, self.metric) for l in labels]
        return headers

    def percentiles(self):
        perc, perc_metric = zip(*[self.percentile(p) for p in self._percentiles])
        return perc, perc_metric

    def summary(self):
        mode, mode_metric = self.mode()
        perc, perc_metric = self.percentiles()
        return [self.header, self.param, mode] + list(perc) + [mode_metric] + list(perc_metric)

    def items(self):
        self._calc()
        return zip(self.param_values, self.perf, self.transformed, self.incremental, self.cumulative)

    def out_data_file(self):
        return get_output_file(self.output_dir, '{0}_{1}.csv'.format(self.header, self.param), sub='data')

    def out_fig_file(self):
        return get_output_file(self.output_dir, '{0}_{1}.png'.format(self.header, self.param), sub='figs')

    def save(self):
        self._calc()

        # save data
        with open(self.out_data_file(), 'wb') as f:
            cw = csv.writer(f)
            cw.writerow(self.csv_headers)
            for row in self.items():
                cw.writerow(row)

    def save_plot(self):
        self._calc()

        plt.figure()

        plt.plot(self.param_values, self.cumulative)
        plt.xlabel(self.param)
        plt.ylabel('Cumulative ' + self.metric_name)

        plt.savefig(self.out_fig_file())
        plt.close('all')


def get_param_items(pd):
    return pd.items()


def get_param_mode(pd):
    return pd.mode()


def get_param_percentiles(pd):
    return pd.percentiles()


def get_param_summary(pd):
    return pd.summary()


def plot_cum_likelihood(pd):
    pd.save_plot()

    
class PerformancePlotter:

    def __init__(self, results_file, output_dir=None, metric=constants.DEFAULT_CALIBRATION_METRIC, verbose=False):

        self.results_file = results_file
        self.metric = metric
        self.metric_name = performance.get_metric_name(metric)
        self.results = results.ResultsCsvFile(self.results_file)
        self.verbose = verbose

        if output_dir is None:
            output_dir = os.path.splitext(results_file)[0]
            assert output_dir != results_file
        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        self.parameters = self.results.parameters()
        self.param_data = list(self._param_data())

    def console(self, s):
        if self.verbose:
            print(s)

    def _param_data(self):
        # loop through geoids
        for header, perf in self.results.items(metric=self.metric):
            # for each parameter
            for param, param_values in self.parameters.items():
                # yield data and optimal values
                yield ParameterData(param, self.metric, header, param_values, perf, self.output_dir)

    def loop_param_data(self):
        n = float(len(self.param_data))
        for i, d in enumerate(self.param_data):
            if self.verbose:
                sys.stdout.write('  {0:45s} {1:20s} {2:5.1f}%    \r'.format(d.header, d.param, (i + 1) / n * 100))
                sys.stdout.flush()
            yield i, d
        sys.stdout.write('\n')
        sys.stdout.flush()

    def save_data(self, output_dir):

        out_file = get_output_file(output_dir, 'data.csv')
        self.console('Saving metric data in {0}...'.format(out_file))

        pool = multiprocessing.Pool(multiprocessing.cpu_count() - 1)
        rows = pool.map(get_param_items, self.param_data)

        with open(out_file, 'wb') as fh:
            cw = csv.writer(fh)
            cw.writerow(['header', 'param'] + self.param_data[0].csv_headers)
            for i, d in self.loop_param_data():
                for row in rows[i]:
                    cw.writerow([d.header, d.param] + list(row))

                # # write individual file for parameter and geoid
                # of = get_output_file(output_dir, '{0}_{1}.csv'.format(d.header, d.param), sub='data')
                # d.save(of)

    def save_cum_likelihood_plots(self):

        # save each parameter to graph of cumulative likelihood
        self.console('Saving plots of Cumulative {0}...'.format(self.metric_name))

        self.param_data[0].out_fig_file()  # creates the directory before trying to multiprocess
        pool = multiprocessing.Pool(multiprocessing.cpu_count() - 1)
        pool.map(plot_cum_likelihood, self.param_data)

    def save_summary(self, summary_file):

        self.console('Summary of {0}'.format(self.metric_name))
        with open(summary_file, 'wb') as f:

            cw = csv.writer(f)
            cw.writerow(self.param_data[0].summary_headers())

            pool = multiprocessing.Pool(multiprocessing.cpu_count() - 1)
            rows = pool.map(get_param_summary, self.param_data)
            for row in rows:
                cw.writerow(row)

    def save_all(self):

        # turn off warnings when calculating statistics
        self.save_summary(get_output_file(self.output_dir, 'summary.csv'))
        self.save_cum_likelihood_plots()
        # self.save_data(self.output_dir)


def from_sensitivity(results_file, output_dir=None, metric=constants.DEFAULT_CALIBRATION_METRIC, verbose=False):

    # performance indicators
    perf = PerformancePlotter(results_file, output_dir=output_dir, metric=metric, verbose=verbose)

    # if output directory is not provided, save outputs to a directory with the same name as a results file
    perf.save_all()
