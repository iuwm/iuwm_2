# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from aggregators import OutputAggregator
import alternative_sources as alt
from collections import OrderedDict
from components import IUWMObject
import constants
import convert
from costs import CapitalCost, VariableCost
import csv
from data import IUWMModelData
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from demands import UrbanDemandCenter, UrbanDemand, Irrigation
import et
import inputs
import itertools
import manipulator
import numpy as np
import os
import re
import sys
import traceback
import utils
import warnings
import weather


def read_input_file(service_area, update_in_place=False):

    sa = manipulator.update_version(service_area, in_place=update_in_place)
    if sa != service_area:
        print("Now reading {0}...".format(sa))
        service_area = sa

    # get input data from file
    if isinstance(service_area, str):
        fh = open(service_area, "r")
    else:
        fh = service_area
        fh.seek(0)

    cr = csv.reader(fh)
    csv_headers = next(row for row in cr)
    headers, units = zip(
        *[inputs.standard_name(h, return_units=True) for h in csv_headers]
    )
    data = [row for row in cr]
    fh.close()

    # get all input data into an array
    arr_data = np.array(data, dtype=np.string_, ndmin=2)

    # transform data to be dictionary of numpy arrays
    input_data = {}
    for i, (h, u) in enumerate(zip(headers, units)):
        v = utils.input_param(h, arr_data[:, i], n_sites=arr_data.shape[0], units=u)
        if v is not None:
            input_data[h] = v
    return service_area, headers, input_data


def read_param_input(param_file, service_areas):
    with open(param_file, "r") as f:
        cr = csv.reader(f)
        headers = [inputs.standard_name(h) for h in next(row for row in cr)]
        raw_data = [row for row in cr]

    if "geoid" in headers:

        assert (
            service_areas is not None
        ), "Must provide the service areas file when using geoids in the parameter file!"
        assert (
            "scenario" in headers
        ), 'The "scenario" column must be in the parameter file {0} also.'.format(
            param_file
        )

        geoid_col = headers.index("geoid")
        index_col = headers.index("scenario")

        service_area, _, data = read_input_file(service_areas)
        geoids = data["geoid"]

        def prep_data(scenario, curr_param_set):
            def get_geoid_row(gid):
                a = np.where(curr_param_set[:, geoid_col] == gid)
                if len(a) == 0 or len(a[0]) == 0:
                    raise ValueError(
                        'Could not find geoid "{0}" in the parameter file!'.format(gid)
                    )
                return a[0][0]

            order_i = [get_geoid_row(geoid) for geoid in geoids]
            ordered_param_set = curr_param_set[order_i]
            d = {
                inputs.standard_name(n): utils.input_param(n, ordered_param_set[:, p_i])
                for p_i, n in zip(range(len(headers)), headers)
                if n != "scenario"
            }
            d = {n: v for n, v in d.items() if v is not None}
            d["scenario"] = scenario
            return d

        sorted_data = sorted(raw_data, key=lambda r: r[index_col])
        grouped_data = itertools.groupby(sorted_data, key=lambda r: r[index_col])
        data = [
            prep_data(scen, np.array(list(curr_data)))
            for scen, curr_data in grouped_data
        ]
    else:
        data = [
            {inputs.standard_name(h): v for h, v in zip(headers, row)}
            for row in raw_data
        ]

    return data


class IUWM:
    def __init__(
        self,
        service_area,
        start_date=None,
        end_date=None,
        weather_parameters=None,
        timeseries=None,
        registered_inputs=None,
        registered_land_uses=None,
        registered_alternative_uses=None,
        registered_alternative_sources=None,
        output_variables=None,
        output=None,
        write_possible_outputs="",
        parameter_setter=None,
        packages=None,
        verbose=False,
        update_in_place=False,
        suppress_warnings=False,
        check_level=1,
    ):
        """
        The main class that runs the Integrated Urban Water Model (IUWM)

        :param service_area: File path (*.csv) to the service area

        :param start_date: Start date of simulation (as datetime or string in yyyy-mm-dd format)
        :param end_date: End date of simulation (as datetime or string in yyyy-mm-dd format)

        :param weather_parameters: Weather parameters (a dict {
            'source': <possible sources ['NARR', 'PRISM', 'MACA', 'CoAgMet', 'NCWCD', 'GHCND', 'file']>,
            'file': <weather data file path of weather file if provided, used when source == 'file'>,
            'climate_model': <the GCM model if using MACA climate data>,
            'climate_rcp': <the RCP option ('45' or '85') if using MACA climate data>,
            'force_download': <force download of new climate data, do not use cache>,
        })

        :param timeseries: A dict containing timeseries data specifications: {
            'file': <a file containing timeseries data (with columns 'date', 'geoid' [optional], model-var-names...)>,
            'repeat_year': <boolean that specifies whether or not the timeseries repeats every year>,
        }

        CSV file(s) of input variables that change over time. Required columns are "date" and "geoid".
        Data is updated only on the exact date that it is specified. Data after that date remains unchanged until
        updated specifically again. Thus, IUWM interpolates across missing dates using a "step" function.

        :param registered_inputs: A list of input variable names to register (so that the model does not produce
            warnings when those values appear in the input file)
        :param registered_land_uses: A list of land uses that can be used in the model
        :param registered_alternative_uses: A list of the alternative end uses that can be used in the model
        :param registered_alternative_sources: A list of the alternative water sources that can be used in the model

        :param output_variables: A list of output variables
        :param output: A list of output aggregators (of type OutputAggregator or dict {
            'file': <output file path>,
            'timestep': <output timestep>,
            'by_geoid': <specifies whether output is by geoid or not>,
            'flatten': <specifies whether output flattens geoid information into columns>,
            'in_memory': <specifies whether output will be aggregated within memory>,
        }
        :param write_possible_outputs: Specifies to write an output file that lists all possible model outputs (can
            change between model runs depending on input data)

        :param parameter_setter: A function or dictionary to set parameters

        :param packages: External (or builtin) packages to utilize when running this model

        :param verbose: Specifies whether or not output will be verbose
        :param update_in_place: If True, version updates will happen in the original file, default False
        :param suppress_warnings: Specifies whether or not to suppress warnings
        :param check_level: The level of mass balance to check
        """

        # defaults
        self.initiated = False
        self._defaults()

        # input parameters
        self.service_area_file = service_area

        self.start_date = start_date
        self.end_date = end_date

        self.weather_parameters = weather_parameters

        self.timeseries = timeseries

        self.registered_inputs = set(registered_inputs) if registered_inputs else set()
        self.registered_land_uses = registered_land_uses
        self.registered_alternative_uses = registered_alternative_uses
        self.registered_alternative_sources = registered_alternative_sources

        self.output_variables = output_variables
        self.output = output
        self.write_possible_outputs = write_possible_outputs

        self.parameter_setter = parameter_setter

        self.packages = packages

        self.verbose = verbose
        self.update_in_place = update_in_place
        self.suppress_warnings = suppress_warnings
        self.check_level = check_level

    def input_dict(self):
        return dict(
            service_area=self.service_area,
            start_date=self.start_date,
            end_date=self.end_date,
            weather_parameters=self.weather_parameters,
            timeseries=self.timeseries,
            registered_inputs=self.registered_inputs,
            registered_land_uses=self.registered_land_uses,
            registered_alternative_uses=self.registered_alternative_uses,
            registered_alternative_sources=self.registered_alternative_sources,
            output_variables=self.output_variables,
            output=self.output,
            write_possible_outputs=self.write_possible_outputs,
            parameter_setter=self.parameter_setter,
            packages=self.packages,
            verbose=self.verbose,
            suppress_warnings=self.suppress_warnings,
            check_level=self.check_level,
        )

    def copy(self):
        return IUWM(**self.input_dict())

    def new_directory(self, new_dir):
        m = self.copy()
        if m.output:
            m.output = m.output.new_directory(new_dir)
        return m

    def _defaults(self):
        self.warnings = set()
        self.weather_data = None
        self.inputs = IUWMObject("inputs")
        self.inputs.date = None
        self.events = utils.local_events()
        self.modules = {}
        self.headers = None
        self.dates = None
        self.data = None
        self.user_output = {}
        self.service_area = None
        self.timers = {}

    def _init(self):

        # defaults
        self._defaults()

        # run information
        if self.start_date is not None and not isinstance(self.start_date, datetime):
            self.start_date = datetime.strptime(str(self.start_date), "%Y-%m-%d")
        if self.end_date is not None and not isinstance(self.end_date, datetime):
            self.end_date = datetime.strptime(str(self.end_date), "%Y-%m-%d")

        # alternative sources
        ras = self.registered_alternative_sources
        if ras is None:
            ras = alt.SOURCES
        elif hasattr(ras, "items"):
            ras = OrderedDict([(k, v) for k, v in ras.items()])
        elif hasattr(ras, "__getitem__"):
            ras = OrderedDict([(k, ras[k]) for k in ras])
        else:
            ras = OrderedDict([(k, alt.SOURCES[k]) for k in ras])
        self.registered_alternative_sources = ras

        # alternative uses
        self.registered_alternative_uses = {
            "res": alt.ResidentialRecyclingCenter,
            "cii": alt.CIIRecyclingCenter,
        }
        if self.registered_alternative_uses is not None:
            self.registered_alternative_uses.update(self.registered_alternative_uses)

        # registered land use
        rls = self.registered_land_uses
        if rls is not None:
            if isinstance(rls, str):
                rls = utils.read_csv_key_values(
                    rls, key="land_use", value="percent_irr"
                )
            elif not isinstance(rls, dict):
                raise ValueError(
                    "Expected a dictionary for registered_land_uses containing land use codes paired "
                    "with percent irrigated area"
                )
        self.registered_land_uses = rls

        # import package
        if self.packages:
            # package should be a list of paired event names with a function that takes **kwargs
            for pkg in self.packages:
                self.subscribe(pkg)

        # input data & parameters
        self.registered_inputs.update(
            utils.init_output_variables(self.registered_inputs, remove_fxn=True)
        )
        self.registered_inputs.update(inputs.PARAMETER_NAMES)
        self.fire("on_inputs_registered")

        # end uses
        self.registered_end_uses = set(
            eu for u in self.registered_alternative_uses.values() for eu in u.end_uses
        )
        self.registered_end_uses.update(self.registered_alternative_sources.keys())

        # store input data in model object
        self._put_input_data(self.service_area_file)

        # weather information
        self.put_weather(self.weather_parameters)

        # timeseries input
        self.timeseries_input = []
        self.add_timeseries_input(self.weather_data)
        if self.timeseries:
            for ts in self.timeseries:
                self.add_timeseries_input(ts.file, repeat_yearly=ts.repeat_yearly)

        # output
        self.output_variables = utils.init_output_variables(self.output_variables)

        if isinstance(self.output, (list, tuple)):
            self.output = OutputAggregator(self.output)

        if isinstance(self.output, OutputAggregator):
            self.output = self.output.build(
                self.geoids, self.output_variables, self.start_date, self.end_date
            )
        else:
            raise ValueError(
                "Output aggregators must be a list of dictionaries or aggregators.OutputAggregator"
            )

        # set input parameters when provided
        self.set_parameters(self.parameter_setter)

        # reset to initial information
        self.output.initiate()

        self.initiated = True

    def initiate(self):
        self._init()

    def register_alternative_source(self, source_name, source_class, order=None):
        if order is None:
            self.registered_alternative_sources[source_name] = source_class
        else:
            new_order = self.registered_alternative_sources.keys()
            new_order.insert(order, source_name)
            self.registered_alternative_sources = OrderedDict(
                [
                    (k, source_class)
                    if k == source_name
                    else (k, self.registered_alternative_sources[k])
                    for k in new_order
                ]
            )

    def set_parameters(self, parameter_setter):
        if parameter_setter is not None:
            for k, v in parameter_setter.items():

                if isinstance(v, str):
                    if v.strip() != "":
                        self.inputs[k] = utils.input_param(k, v, n_sites=self.n_sites)

                elif isinstance(v, np.ndarray) and np.any(v != ""):
                    assert np.all(
                        v != ""
                    ), "All values must be specified by geoid in scenario file!"
                    self.inputs[k] = utils.input_param(k, v, n_sites=self.n_sites)

                elif isinstance(v, tuple):
                    # set values by indices
                    indices, values = v
                    self.inputs[k][indices] = values

                else:
                    self.inputs[k] = utils.input_param(k, v, n_sites=self.n_sites)

    def build_data(self):
        leak_percent = self.get_param("distribution_leak_percent") / 100.0
        leak_black = self.get_param("distribution_leak_percent_blackwater") / 100.0
        leak_consumed = 1.0 - leak_black
        return IUWMModelData(
            "data",
            self.inputs,
            self.user_output,
            leak_percent,
            leak_consumed,
            0.0,
            leak_black,
        )

    def is_registered(self, param_name):

        # direct parameter name
        if param_name in self.registered_inputs:
            return True

        # percent offset of
        if param_name.startswith("percent_offset_of_"):
            param_name = param_name[len("percent_offset_of_") :]
            if param_name in self.registered_inputs:
                return True

        # absolute offset of
        if param_name.startswith("absolute_offset_of_"):
            param_name = param_name[len("absolute_offset_of_") :]
            if param_name in self.registered_inputs:
                return True

        # match source/use variables
        starting = "|".join(
            [
                "percent_adoption_of",
                "percent_available_of",
                "storage_of",
                "percent_starting_storage_of",
                "capital_cost_of",
                "maximum_reuse_of",
            ]
        )
        match = re.search(r"({0})_(\w+)_for_(\w+)".format(starting), param_name)
        if match:
            source = match.group(2)
            purpose = match.group(3)
            if (
                source in self.registered_alternative_sources
                and purpose in self.registered_end_uses
            ):
                return True

        # match land uses
        match = re.search(r"(area|percent_irr)_(.+)\b", param_name)
        if match:
            land_use = match.group(2)
            if land_use in self.registered_land_uses:
                return True

        return False

    def update_registered_land_uses(self, input_data):
        # register land uses
        rls = self.registered_land_uses

        if "land_use" in input_data:
            if rls is None:
                rls = dict()

            # collect land use codes from input file
            lu_codes = self.full_array(input_data["land_use"])

            # collect percent irrigated area by land use from input file if available
            percent_irr_area = self.full_array(
                input_data["percent_irr"] if "percent_irr" in input_data else 0.0
            )

            # link land use with percent irrigated area
            land_uses = OrderedDict(
                [(lu, p) for lu, p in zip(lu_codes, percent_irr_area)]
            )

            # update registered land uses
            rls.update(land_uses)

        elif rls is None:

            # default to NLCD categories
            rls = OrderedDict(
                [
                    (c, inputs.PARAMETERS["area_{0}".format(c)].default)
                    for c in constants.NLCD_CLASSES
                ]
            )

        self.registered_land_uses = OrderedDict(
            [(k, self.full_array(v)) for k, v in rls.items()]
        )

    def _put_input_data(self, service_area_file):

        # open the service area file that describes the service areas
        self.service_area_file, headers, input_data = read_input_file(
            service_area_file, update_in_place=self.update_in_place
        )

        # detect no data
        if headers is None:
            raise ValueError("Service area file requires data!")

        # save headers
        self.headers = [str(h).lower() for h in headers]

        # sites information
        if "geoid" not in input_data:
            raise ValueError('Parameter "geoid" is required but not provided!')
        self.geoids = input_data["geoid"]
        self.geoid_indices = {g: i for i, g in enumerate(self.geoids)}
        self.n_sites = len(self.geoids)
        self.ones = np.ones(self.n_sites)
        self.zeros = self.full_array(0.0)

        # update registered land uses
        self.update_registered_land_uses(input_data)

        # run warnings unless suppressing
        if not self.suppress_warnings:

            unknown = [h for h in self.headers if not self.is_registered(h)]
            if unknown:

                reg_input_file = os.path.abspath("registered_inputs.txt")
                with open(reg_input_file, "w") as f:
                    f.write("\n".join(sorted(list(self.registered_inputs))))

                msg = (
                    "\n"
                    "WARNING: Parameter(s) found that are not inputs to IUWM!\n"
                    "Registered IUWM parameters can be found here:\n    {0}\n"
                    'In addition to model parameters in that file: any parameter can have "percent_offset_of_" or '
                    '"absolute_offset_of_" added to the beginning of its name.\n'
                    "\nWARNING: The following parameters are not inputs to IUWM!\n  {1}"
                )
                print(msg.format(reg_input_file, "\n  ".join(unknown)))
                self.warnings.add(msg)

        self.inputs.update(input_data)
        self.inputs["date"] = None
        return self

    def add_timeseries_input(self, timeseries_input, repeat_yearly=False, i=-1):

        if isinstance(timeseries_input, str):
            timeseries_input = utils.read_timeseries_input(
                timeseries_input, self.geoid_indices
            )

        if repeat_yearly:
            timeseries_input = utils.change_year_of_timeseries(timeseries_input, 2000)

        if i == -1:
            self.timeseries_input.append((timeseries_input, repeat_yearly))
            return len(self.timeseries_input) - 1
        else:
            self.timeseries_input[i] = (timeseries_input, repeat_yearly)
            return i

    def advance_timeseries_single(self, ti, repeat_year=False, date=None):
        """
        Inserts values from a date within a timeseries dict into the model at the current timestep. If the date is not
        found, no data is inserted into the model

        :param ti: Timeseries input, a dictionary of dates to dicts (of variable names to values or arrays of values)
            ti = {date1: {v1: val1, v2: val2, ...}, date2: {v1: val1, v2: val2, ...}, ...}
        :param repeat_year: Timeseries should be repeated year to year (True or False, default is False)
        :param date: Date within timeseries input to add to this
        """

        if date is None:
            date = self.inputs["date"]

        if repeat_year:
            lookup_date = utils.change_year(date, 2000)
        else:
            lookup_date = date

        if lookup_date in ti:
            for h, v in ti[lookup_date].items():

                # copy zeros array
                if h not in self.inputs:
                    self.inputs[h] = np.copy(self.zeros)

                # set values
                if isinstance(v, tuple):
                    # set values by indices
                    indices, values = v
                    self.inputs[h][indices] = values
                else:
                    # set all values (v should be a single float...)
                    self.inputs[h] = v

            self.inputs["date"] = date

    def advance_timeseries(self, date=None):
        for ti, repeat_year in self.timeseries_input:
            self.advance_timeseries_single(ti, repeat_year=repeat_year, date=date)

    def param_provided(self, param_name):
        param_name = param_name.lower()
        return param_name in self.headers

    def get_param(
        self,
        param_name,
        default_value=None,
        bounds=None,
        required=False,
        array=False,
        single=False,
    ):
        """
        Get parameters from the model by name while being able to override defaults and bounds

        :param param_name: Name of parameter
        :param default_value: Default value for the parameter. If not specified, default from `input_variables.csv`.
        :param bounds: Bounds of the parameter.
        :param required: Specifies whether the parameter is required. If not required and not provided, returns None
        :param array: Ensure that the output value is an array with `n_sites` elements
        :param single: Ensure that the output value is a single value and that all inputs match each other across space
        :return: Returns the parameter value that is specified in model, or default value.
        """

        # case insensitive
        param_name = param_name.lower().strip()

        # defaults
        if param_name in inputs.PARAMETERS:
            p = inputs.PARAMETERS[param_name]

            if default_value is None:
                default_value = p.default

            if bounds is None:
                bounds = p.bounds

            if not required:
                required = p.required

        # get the parameter directly from the input data (use default if not found)
        if param_name in self.inputs:
            param_value = self.inputs[param_name]
        else:
            param_value = default_value

        # if not provided, determine what to do based on whether the parameter is required
        if param_value is None:
            if required:
                raise ValueError(
                    "Required parameter {0} not provided!".format(param_name)
                )
            else:
                return None

        # bounds
        if self.check_level >= 0 and bounds is not None:
            if len(bounds) != 2:
                raise ValueError("Bounds must be a list or array of 2 elements exactly")

            if not np.isnan(bounds[0]):
                if np.any(param_value < bounds[0]):
                    raise ValueError(
                        'Parameter "{0}" must be >= {1}!'.format(param_name, bounds[0])
                    )
            if not np.isnan(bounds[1]):
                if np.any(param_value > bounds[1]):
                    raise ValueError(
                        'Parameter "{0}" must be <= {1}!'.format(param_name, bounds[1])
                    )

        # apply absolute offset if applicable
        param_offset_name = "absolute_offset_of_{0}".format(param_name)
        if self.param_provided(param_offset_name):
            param_offset = self.get_param(param_offset_name)
            if param_offset.any():
                # NOTE: do not do "param_value += param_offset"! it will change data that should not be changed!
                param_value = param_value + param_offset
                if self.check_level >= 0 and bounds is not None:
                    param_value = np.clip(param_value, *bounds)

        # apply percent offset if applicable
        param_offset_name = "percent_offset_of_{0}".format(param_name)
        if self.param_provided(param_offset_name):
            param_offset = self.get_param(param_offset_name) / 100.0
            if np.any(param_offset):
                # NOTE: do not do "param_value *= (1.0 + param_offset)"! it will change data that should not be changed!
                param_value = param_value * (1.0 + param_offset)
                if self.check_level >= 0 and bounds is not None:
                    param_value = np.clip(param_value, *bounds)

        # return the parameter value
        if array and (
            not isinstance(param_value, np.ndarray) or len(param_value) != self.n_sites
        ):
            return self.full_array(param_value)

        if single and isinstance(param_value, np.ndarray) and len(param_value) > 0:
            if not np.all(param_value == param_value[0]):
                raise ValueError(
                    'Parameter "{0}" must have one value across all spatial subunits'.format(
                        param_name
                    )
                )
            return param_value[0]

        return param_value

    def full_array(self, v):
        if not isinstance(v, np.ndarray):
            return np.full(self.n_sites, v)
        return v

    def get_option(self, param_name, **kwargs):
        v = self.get_param(param_name, array=True, **kwargs)
        bounds = utils.get_param_bounds(param_name)
        opt = {i: v == i for i in range(int(bounds[0]), int(bounds[1]) + 1)}
        if not np.all(sum(opt.values())):
            raise ValueError(
                'Option/method "{0}" was not specified for all spatial subunits!'.format(
                    param_name
                )
            )
        return opt

    def put_param(self, param_name, param_value):

        # case insensitive
        param_name = param_name.lower()

        # add data to the input
        self.inputs[param_name] = param_value

        return self

    def get_global_coordinates(self):
        longitudes = np.float64(self.get_param("longitude"), ndmin=1)
        latitudes = np.float64(self.get_param("latitude"), ndmin=1)
        sites = [
            {"id": str(geoid), "name": str(geoid), "longitude": x, "latitude": y}
            for geoid, x, y in zip(self.geoids, longitudes, latitudes)
        ]
        return sites

    def get_station_ids(self):
        ids = self.get_param("station_id")
        names = self.get_param("station_name", np.array([""] * len(ids)))
        sites = [
            {"id": station_id, "name": station_name}
            for station_id, station_name in zip(ids, names)
        ]
        return sites

    def reset_weather(self):
        if np.any(self.weather_parameters["source"] != "file"):
            if not isinstance(self.weather_data, weather.WeatherGetter):
                raise AssertionError("Weather data should be of WeatherGetter class!")
            self.weather_data.close()
            self.put_weather(self.weather_parameters)

    def put_weather(self, weather_parameters):

        # set default
        if weather_parameters is None:
            weather_parameters = {
                # possible sources ['NARR', 'PRISM', 'MACA', 'CoAgMet', 'NCWCD', 'GHCND', 'file']
                "source": self.get_param("weather_source", self.full_array("prism")),
                "file": "",  # the file path of weather data if a weather file is provided
                "climate_model": "",  # the GCM model if using MACA
                "climate_rcp": "",  # the rcp option ('45' or '85') if using MACA
            }

        # convert source to array
        if not isinstance(weather_parameters["source"], np.ndarray):
            weather_parameters["source"] = self.full_array(weather_parameters["source"])

        # lower
        weather_parameters["source"] = np.array(
            [str(s).lower() for s in weather_parameters["source"]]
        )

        # checks
        unique_sources = np.unique(weather_parameters["source"])
        first_source = unique_sources[0]
        if (
            first_source != "file"
            and weather.is_gridded(first_source)
            and len(unique_sources) != 1
        ):
            raise ValueError(
                "Cannot retrieve more than one different gridded datasets!"
            )

        # get model and rcp scenario to run
        climate_model = None
        climate_rcp = None
        if first_source == "maca":
            climate_model = self.get_param(
                "climate_model"
            )  # Climate model. Required if weatherSource is "maca".
            climate_rcp = self.get_param(
                "climate_rcp"
            )  # RCP scenario. Required if weatherSource is "maca".
            if isinstance(climate_model, np.ndarray):
                climate_model = climate_model[0]
            if isinstance(climate_rcp, np.ndarray):
                climate_rcp = climate_rcp[0]

        # get new weather data at the current service area
        is_file = np.all(weather_parameters["source"] == "file")
        if is_file:

            weather_parameters["download"] = False

            # check if file is specified
            if "file" not in weather_parameters or not weather_parameters["file"]:
                raise ValueError(
                    "If using weather data from a file, must specify a file path."
                )
            data = utils.get_weather_from_file(
                weather_parameters["file"], self.geoid_indices
            )
            self.dates = sorted(data.keys())
            if self.start_date is None:
                self.start_date = self.dates[0]
            if self.end_date is None:
                self.end_date = self.dates[-1]
            self.dates = [
                d for d in self.dates if (self.start_date <= d <= self.end_date)
            ]
            self.weather_data = data

        else:

            if np.any(is_file):
                raise ValueError(
                    'Cannot get weather from "file" when getting data from other sources as well!'
                )

            weather_parameters["download"] = True

            # get dates from start and end dates
            if self.start_date is None or self.end_date is None:
                raise ValueError(
                    "Must provide a start and end date when extracting weather data from CSIP!"
                )
            n_days = int(
                (self.end_date - self.start_date + timedelta(days=1)).total_seconds()
                / 86400
            )
            self.dates = [
                self.start_date + timedelta(days=day) for day in range(n_days)
            ]

            # check possible sources
            source = weather_parameters["source"]
            first_source = next(src for src in source)
            if [src for src in source if src not in weather.SOURCE_NAMES]:
                msg = "Source {0} is not found in possible sources ({1})"
                raise ValueError(msg.format(source, ", ".join(weather.SOURCE_NAMES)))

            # get location information
            if [src for src in source if weather.is_gridded(src)]:
                assert (
                    source == first_source
                ).all(), (
                    "For gridded datasets, the weather source must all be the same!"
                )
                sites = self.get_global_coordinates()
            else:
                sites = self.get_station_ids()
            for site, src in zip(sites, source):
                site["source"] = src

            # get the weather converter
            self.weather_data = weather.WeatherGetter(
                sites,
                range(self.start_date.year, self.end_date.year + 1),
                climate_model=climate_model,
                climate_rcp=climate_rcp,
                build_converter=utils.build_weather_units_converter,
                verbose=self.verbose,
                use_cache=not self.weather_parameters.get("force_download", False),
            )

        self.weather_parameters = weather_parameters

    def new_recycle_bin(self, src, use, n_adopters="households"):

        src = src.lower()
        use = use.lower()

        src = src.lower().strip()
        if src not in self.registered_alternative_sources:
            msg = "Source must be: {0}. Got {1}!"
            raise ValueError(
                msg.format(
                    ", ".join(k for k in self.registered_alternative_sources), src
                )
            )

        if isinstance(n_adopters, str):
            adopters = self.get_param(n_adopters)
        else:
            adopters = n_adopters

        adoption = (
            self.get_param("percent_adoption_of_{0}_for_{1}".format(src, use), 0.0)
            / 100.0
        )
        f_avail = (
            self.get_param("percent_available_of_{0}_for_{1}".format(src, use), 100.0)
            / 100.0
        )
        capacity_per_adopter = self.get_param(
            "storage_of_{0}_for_{1}".format(src, use), 0.0
        )
        start = self.get_param(
            "percent_starting_storage_of_{0}_for_{1}".format(src, use), 0.0
        )
        max_reuse = self.get_param(
            "maximum_reuse_of_{0}_for_{1}".format(src, use), np.inf
        )

        capacity = capacity_per_adopter * adopters * adoption

        return alt.RecycleBin(
            use,
            capacity,
            adoption,
            f_avail,
            starting_percent=start,
            max_reuse=max_reuse,
        )

    def new_recycle_centers(self, src):

        src = src.lower().strip()

        # total fraction available
        res_avail = (
            self.get_param("percent_available_of_{source}_for_res".format(source=src))
            / 100.0
        )
        cii_avail = (
            self.get_param("percent_available_of_{source}_for_cii".format(source=src))
            / 100.0
        )

        # split between residential and CII
        return (
            self.registered_alternative_uses["res"](
                src, self, fraction_available=res_avail
            ),
            self.registered_alternative_uses["cii"](
                src, self, fraction_available=cii_avail
            ),
        )

    def init_reuse(self):
        self.fire("on_initializing")
        for source in self.registered_alternative_sources:
            res, cii = self.new_recycle_centers(source)
            self.data["storage_of_{source}_for_res".format(source=source)] = res
            self.data["storage_of_{source}_for_cii".format(source=source)] = cii
        self.fire("on_initialized")

    def curr_weather(self):

        self.fire("on_weather_calculating")

        # get parameters
        wth_params = {
            "date": self.inputs["date"],
            "et_method": self.get_param("et_method"),
            "latitude": self.get_param("latitude"),
            "longitude": self.get_param("longitude"),
            "temp_min": self.get_param("temp_min"),
            "temp_max": self.get_param("temp_max"),
            "temp_avg": self.get_param("temp_avg"),
            "rel_hum_min": self.get_param("rel_hum_min"),
            "rel_hum_max": self.get_param("rel_hum_max"),
            "rel_hum_avg": self.get_param("rel_hum_avg"),
            "wind_speed": self.get_param("wind_speed"),
            "east_wind": self.get_param("east_wind"),
            "north_wind": self.get_param("north_wind"),
            "solar_rad": self.get_param("solar_rad"),
            "precipitation": self.get_param("precipitation"),
            "elevation": self.get_param("elevation"),
            "albedo": self.get_param("albedo"),
            "n_sunshine": self.get_param("n_sunshine"),
            "solar_rad_reg_a": self.get_param("solar_rad_reg_a"),
            "solar_rad_reg_b": self.get_param("solar_rad_reg_b"),
            "k_t": self.get_param("k_t"),
        }

        # update current input data with weather information
        wth = et.Weather("weather", **wth_params)
        self.inputs["weather"] = wth

        self.fire("on_weather_calculated", wth=wth)

        return wth

    def get_demand_profiles(self):

        # variables for splitting indoor use amongst fixtures (e.g., toilet, bath, etc.)
        demand_profiles = constants.DEMAND_PROFILES.copy()
        end_uses = constants.INDOOR_END_USES

        # add user-defined profile to all profiles
        fraction_user_def = self.get_param("percent_user_def") / 100.0
        if np.any(fraction_user_def > 0.0) and "profile_a" in self.inputs:
            fmt = "profile_{0}_fraction"
            dp = demand_profiles["reu2016"]
            up = {k: self.get_param(fmt.format(k), self.ones * dp[k]) for k in end_uses}

            user_def_profile = {
                "a": self.get_param("profile_a"),
                "b": self.get_param("profile_b"),
            }
            user_def_profile.update(up)

            demand_profiles["user_def"] = user_def_profile

        return demand_profiles, end_uses

    def curr_indoor(self):

        self.fire("on_indoor_calculating")

        # number of households and population
        households = self.get_param(
            "households"
        )  # number of households in each spatial unit
        population = self.get_param(
            "population"
        )  # number of people in each spatial unit

        # percent of faucet demand returning to graywater
        # kitchen faucet water is black
        faucet_gray = self.get_param("faucet_percent_graywater") / 100.0
        # percent leaks that are black
        fraction_leak_black = self.get_param("leak_percent_blackwater") / 100.0
        fraction_leak_consumed = 1 - fraction_leak_black

        # percent of indoor demand that is consumed equals leaks (inverse of that which becomes wastewater)
        #
        # 10-14% in Great Lakes region
        # Shaffer and Runkle (2007). Consumptive Water-Use Coefficients for the Great Lakes Basin and Climatically
        #     Similar Areas. Available at https://pubs.usgs.gov/sir/2007/5197/pdf/SIR2007-5197_low-res_all.pdf
        #
        # Less than 1% evaporative or drinking consumption
        # Yao Li, Zhenghong Tang, Can Liu, Ayse Kilic, Estimation and investigation of consumptive water use in
        #     residential area-Case cities in Nebraska, U.S.A. Sustainable Cities and Society, Volume 35, November
        #     2017, Pages 637-644, ISSN 2210-6707, https://doi.org/10.1016/j.scs.2017.09.012.
        #
        # Because of these estimates, assume that consumed = % of leaks for residential indoor
        # This line is no longer needed:
        fraction_consumed = self.get_param("indoor_percent_consumed") / 100.0

        # fraction required for mixing (require a default of 60% original source water)
        min_mixing_fraction = self.get_param("indoor_min_mixing_percent") / 100.0
        z = self.full_array(0.0)

        # cache the household density
        household_density = self.full_array(0.0)
        non_zero = households > 0
        household_density[non_zero] = (
            population[non_zero] / households[non_zero]
        )  # people/household

        demand_profiles, end_uses = self.get_demand_profiles()

        # otherwise,
        #   use demand profiles from input parameters
        #   demand profile (default: standard home)
        def arr(name):
            v = self.full_array(self.get_param("percent_" + name) / 100.0)
            return v

        profile_fractions = {
            profile_name: arr(profile_name) for profile_name in demand_profiles
        }

        # check total of fractions
        total_profile_fractions = np.array([v for v in profile_fractions.values()])
        if (np.array(sum(total_profile_fractions)) != 1).any():
            raise ValueError(
                "All profile percents (percent_userdef, percent_reu2016, percent_reu1999, percent_he) "
                "must add up to exactly 100%! Be careful not to leave percent_reu2016 out because it "
                "defaults to 100%"
            )
        for profile_fraction in total_profile_fractions:
            if np.logical_or(profile_fraction < 0, 1 < profile_fraction).any():
                raise ValueError("Percents must be between 0 and 100%")

        # calculate total indoor demand and wastewater
        ind = {k: self.full_array(0.0) for k in end_uses}
        ind["demand"] = self.full_array(0.0)  # total demand (gallons per day, gal/day)
        for demand_type in end_uses:
            ind[demand_type] = self.full_array(0.0)

        # demand is weighted average of estimated demand for each profile type
        for profile_name, profile in demand_profiles.items():

            # demand calculations
            d_per = (
                profile_fractions[profile_name]
                * profile["a"]
                * (household_density ** profile["b"])
            )
            d_tot = d_per * households

            # breakdown of indoor end uses
            demand_total_fraction = sum(
                profile[demand_type] for demand_type in end_uses
            )
            total_indoor_demand = self.full_array(0.0)
            for demand_type in end_uses:
                k_saved = self.get_param(
                    "fraction_saved_{0}".format(demand_type), self.zeros
                )

                # automatic warning when greater than a certain value
                if (
                    not self.suppress_warnings
                    and demand_type not in self.warnings
                    and demand_type in constants.REASONABLE_SAVINGS
                    and np.any(k_saved > constants.REASONABLE_SAVINGS[demand_type])
                ):

                    msg = '\nWARNING: Savings for end use "{0}" seems pretty high, expecting a value less than {1}.'
                    warnings.warn(
                        msg.format(
                            demand_type, constants.REASONABLE_SAVINGS[demand_type]
                        )
                    )
                    self.warnings.add(msg)

                end_use_demand = (
                    d_tot
                    * profile[demand_type]
                    / demand_total_fraction
                    * (1.0 - k_saved)
                )
                ind[demand_type] += end_use_demand
                total_indoor_demand += end_use_demand

            # recalculate total demand
            ind["demand"] += total_indoor_demand

        # calculate leak fraction
        non_zero = ind["demand"] != 0
        leak_fraction = self.full_array(0.0)
        leak_fraction[non_zero] = ind["leak"][non_zero] / ind["demand"][non_zero]

        # recalculate consumed fraction
        # this was a result of solving:
        #     consumptive use as a fraction of total demand = consumptive use of each appliance plus consumptive leaks
        frac_co = (fraction_consumed - fraction_leak_consumed * leak_fraction) / (
            1.0 - leak_fraction
        )
        if np.any(frac_co < 0):
            raise ValueError(
                'Parameter "indoor_percent_consumed" cannot be less than (1-leak_percent_black)'
                "*(leak as percent of indoor use)"
            )
        frac_nco = 1.0 - frac_co

        # faucet black
        fg = faucet_gray
        fb = frac_nco - fg
        mmf = min_mixing_fraction

        # build output
        indoor = UrbanDemandCenter(
            "indoor",
            UrbanDemand(
                "bath",
                ind["bath"],
                consumed=frac_co,
                gray=frac_nco,
                black=0.0,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "clothes",
                ind["clothes"],
                consumed=frac_co,
                gray=frac_nco,
                black=0.0,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "dish",
                ind["dish"],
                consumed=frac_co,
                gray=0.0,
                black=frac_nco,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "faucet",
                ind["faucet"],
                consumed=frac_co,
                gray=fg,
                black=fb,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "other",
                ind["other"],
                consumed=frac_co,
                gray=0.0,
                black=frac_nco,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "shower",
                ind["shower"],
                consumed=frac_co,
                gray=frac_nco,
                black=0.0,
                min_mixing_fraction=mmf,
            ),
            UrbanDemand(
                "toilet",
                ind["toilet"],
                consumed=frac_co,
                gray=0.0,
                black=frac_nco,
                min_mixing_fraction=z,
            ),
        ).leaky(
            leak_fraction,
            consumed=fraction_leak_consumed,
            gray=0.0,
            black=fraction_leak_black,
        )

        # checks
        if np.any(
            np.abs(indoor.consumed - fraction_consumed * ind["demand"]) > 1e-6
        ) or np.any(
            np.abs(indoor.nonconsumed - (1 - fraction_consumed) * ind["demand"]) > 1e-6
        ):
            raise FloatingPointError(
                "Consumed and nonconsumed fractions are not summing properly!"
            )

        self.data.indoor = indoor
        self.fire("on_indoor_calculated", indoor=indoor)

        return indoor

    def curr_cii(self):
        """
        Calculates indoor CII demand based on residential indoor demand or the number of households.

        :param date: Date in the simulation
        :param indoor: Residential indoor object
        :return: Returns the indoor CII object
        """

        indoor = self.data.indoor

        self.fire("on_cii_calculating", indoor=indoor)

        # indoor CII demand
        z = self.full_array(0.0)
        cii_indoor_per_household = self.get_param("cii_indoor_per_household", z)
        cii_indoor_per_res_indoor = self.get_param("cii_indoor_per_res_indoor", z)
        cii_indoor_gpd = self.get_param("cii_indoor_gpd", z)
        cii_option = self.get_param("cii_indoor_calc_option")
        households = self.get_param("households")

        # percent of indoor CII demand that produces blackwater and graywater
        frac_black = self.get_param("cii_percent_blackwater") / 100.0
        frac_gray = self.get_param("cii_percent_graywater") / 100.0

        # check options
        if np.logical_or(cii_option < 1, 3 < cii_option).any():
            raise ValueError(
                "There are only  three options for CII indoor calculations (cii_indoor_calc_option)! "
                "Option 1 is to use cii_indoor_per_household. "
                "Option 2 is to use cii_indoor_per_res_indoor. "
                "Option 3 is to use ci_indoor_gpd."
            )

        # calculate the total CII demand (can mix the calculation options by subunit)
        cii_indoor = (
            (cii_option == 1) * cii_indoor_per_household * households
            + (cii_option == 2) * cii_indoor_per_res_indoor * indoor.demand
            + (cii_option == 3) * cii_indoor_gpd
        )
        k_c = 1 - frac_black - frac_gray
        cii = UrbanDemandCenter(
            "cii",
            UrbanDemand(
                "indoor", cii_indoor, consumed=k_c, gray=frac_gray, black=frac_black
            ),
        )

        self.data.cii = cii
        self.fire("on_cii_calculated", cii=cii)

        return cii

    def irrigating(self):

        date = self.inputs["date"]
        yday = date.timetuple().tm_yday

        # get current weather data
        wth = self.get_param("weather")

        # irrigation season method
        opt = self.get_option("irr_season_method")

        irr_on = self.full_array(False)

        # the temperature in degrees Celsius above which people will irrigate their lawns
        # defaults to the calibrated value from Fort Collins, CO
        if np.any(opt[1]):
            warm = wth.temp_avg >= self.get_param("irr_avg_temp_cutoff")
            irr_on[opt[1]] = warm[opt[1]]

        # todo: later inform by this: https://usda.mannlib.cornell.edu/usda/nass/planting/uph97.pdf
        if np.any(opt[2]):
            irr_season_start = self.get_param("irr_season_start", self.full_array(0))
            irr_season_end = self.get_param("irr_season_end", self.full_array(366))
            in_season = np.logical_and(irr_season_start <= yday, yday <= irr_season_end)
            irr_on[opt[2]] = in_season[opt[2]]

        # irrigation is always on
        if np.any(opt[3]):
            irr_on[opt[3]] = True

        return irr_on

    def default_date_key(
        self, ts=None, lag_years=None, lag_months=None, lag_days=None, no_lag=False
    ):

        # default timeseries is annual
        if ts is None:
            ts = "annual"

        # collect lagged information if provided
        if lag_years is None:
            lag_years = self.get_param("irr_depth_lag_years", single=True)
        if lag_months is None:
            lag_months = self.get_param("irr_depth_lag_months", single=True)
        if lag_days is None:
            lag_days = self.get_param("irr_depth_lag_days", single=True)
        rd = relativedelta(years=lag_years, months=lag_months, days=lag_days)

        # default date_key
        def daily_freeze(d):
            if not no_lag:
                d = d + rd
            return datetime(d.year, d.month, d.day)

        def monthly_freeze(d):
            if not no_lag:
                d = d + rd
            return datetime(d.year, d.month, 1)

        def annual_freeze(d):
            if not no_lag:
                d = d + rd
            return datetime(d.year, 1, 1)

        date_key = {
            "daily": daily_freeze,
            "monthly": monthly_freeze,
            "annual": annual_freeze,
            "avg_annual": lambda d: datetime(2000, 1, 1),
        }[ts]

        return date_key

    def averaged_irr_depth(
        self,
        param_name,
        ts=None,
        lag_years=None,
        lag_months=None,
        lag_days=None,
        mean=False,
    ):

        # default date key
        date_key = self.default_date_key(
            ts=ts, lag_years=lag_years, lag_months=lag_months, lag_days=lag_days
        )
        no_lag = self.default_date_key(ts=ts, no_lag=True)

        # update method for daily ET calculation then revert back
        opt = self.get_option("irr_depth_method")
        v = self.get_param("irr_depth_method")
        v[opt[3]] = 1
        v[opt[4]] = 1
        v[opt[5]] = 1

        # calculate annual average ET based on daily ET
        daily = {date: self.irr_depth()[-1] for date in self.iter_time(reset=True)}
        aggregated = {
            ref_date: {param_name: np.mean([daily[date] for date in dates], axis=0)}
            for ref_date, dates in itertools.groupby(daily, key=date_key)
        }

        # add missing dates because of lagging
        aggregated.update(
            {
                ref_date: {param_name: np.mean([daily[date] for date in dates], axis=0)}
                for ref_date, dates in itertools.groupby(daily, key=no_lag)
                if ref_date not in aggregated
            }
        )

        # revert methods back
        v[opt[3]] = 3
        v[opt[4]] = 4
        v[opt[5]] = 5

        # return only the mean if desired
        if mean:
            return np.mean([v[param_name] for v in aggregated.values()], axis=0)
        else:
            return aggregated

    def avg_annual_irr_depth(self, param_name, **kwargs):
        return self.averaged_irr_depth(param_name, ts="avg_annual", **kwargs)

    def annual_irr_depth(self, param_name, **kwargs):
        return self.averaged_irr_depth(param_name, ts="annual", **kwargs)

    def monthly_irr_depth(self, param_name, **kwargs):
        return self.averaged_irr_depth(param_name, ts="monthly", **kwargs)

    def irr_depth(self):

        # get current weather data
        wth = self.get_param("weather")

        # get plant factors
        # use SLIDE model for irrigation demands
        #   http://ucanr.edu/sites/UrbanHort/Water_Use_of_Turfgrass_and_Landscape_Plant_Materials/SLIDE__Simplified_Irrigation_Demand_Estimation/
        pfs = self.get_param("plant_factor")  # 0.8 for cool season grass

        # percent of ET demand met
        fraction_et_met = self.get_param("percent_et_met") / 100.0
        fraction_nir_met = self.get_param("percent_nir_met") / 100.0

        # percent decrease in  irrigation
        fraction_irr_decrease = self.get_param("percent_irr_decrease") / 100.0

        # irr percent efficient
        fraction_irr_efficiency = self.get_param("percent_irr_efficiency") / 100.0

        # percent pcp to help meet ET
        fraction_precip_accounted = self.get_param("percent_precip_accounted") / 100.0

        # boolean for turning off irrigation
        irr_on = self.irrigating()

        # evapotranspiration (inches)
        pet = wth.pet_in  # inches
        aet = pet * pfs
        pet[~irr_on] = 0.0  # potential/reference ET
        aet[~irr_on] = 0.0  # actual ET in inches

        # theoretical irrigation requirement in inches
        nir = np.maximum(aet - wth.precipitation_in, 0.0)
        eff_pcp = np.maximum(aet - nir, 0.0)
        gir = nir / fraction_irr_efficiency

        # actual irrigation amount
        aet_met = aet * fraction_et_met
        imperfect_nir = (
            aet_met - wth.precipitation_in * fraction_precip_accounted
        )  # inches
        irr_depth = np.maximum(fraction_nir_met * imperfect_nir, 0.0)  # inches
        theoretical_depth = (
            irr_depth / fraction_irr_efficiency * (1.0 - fraction_irr_decrease)
        )  # in inches

        # irrigation depth calculation
        opt = self.get_option("irr_depth_method")
        applied_depth = self.full_array(0.0)

        if np.any(opt[1]):
            # calculate irrigation depth from daily ET estimates
            applied_depth[opt[1]] = theoretical_depth[opt[1]]

        if np.any(opt[2]):
            # directly specify an irrigation depth
            applied_depth[opt[2]] = self.get_param(
                "irr_depth", required=True, array=True
            )  # inches/day

        # todo: add calculation of irrigation depth based on average annual ET
        if np.any(opt[3]):

            # calculate irrigation depth from average annual ET estimates
            if "avg_annual_irr_depth" not in self.inputs:
                # calculate average annual irrigation depth
                aad = self.avg_annual_irr_depth("avg_annual_irr_depth", mean=True)
                self.inputs["avg_annual_irr_depth"] = aad

            applied_depth[opt[3]] = self.get_param(
                "avg_annual_irr_depth", required=True, array=True
            )

        if np.any(opt[4]):

            # calculate irrigation depth from annual ET estimates
            if "annual_irr_depth" not in self.inputs:
                ad = self.annual_irr_depth("annual_irr_depth")

                # set irrigation depth (only used when lagged parameters are used)
                self.inputs["annual_irr_depth"] = np.mean(
                    [v["annual_irr_depth"] for v in ad.values()], axis=0
                )

                # set data from a new timeseries input
                self.add_timeseries_input(ad)
                self.advance_timeseries_single(ad)

            applied_depth[opt[4]] = self.get_param(
                "annual_irr_depth", required=True, array=True
            )

        if np.any(opt[5]):

            # calculate irrigation depth from monthly ET estimates (include monthly and daily lags)
            if "monthly_irr_depth" not in self.inputs:

                md = self.monthly_irr_depth("monthly_irr_depth")

                # set irrigation depth (only used when lagged parameters are used)
                self.inputs["monthly_irr_depth"] = np.mean(
                    [v["monthly_irr_depth"] for v in md.values()], axis=0
                )

                # set data from a new timeseries input
                self.add_timeseries_input(md)
                self.advance_timeseries_single(md)

            applied_depth[opt[5]] = self.get_param(
                "monthly_irr_depth", required=True, array=True
            )

        return (
            irr_on,
            pet,
            aet,
            eff_pcp,
            nir,
            gir,
            aet_met,
            irr_depth,
            theoretical_depth,
            applied_depth,
        )

    def curr_irr(self):

        # irrigation depth to volume given fractions of irrigated land
        # get irrigated area
        opt = self.get_option("irr_area_method")  # option for irrigated area calc
        # zones = self.get_param('zones')  # option

        # irrigation requirements
        (
            irr_on,
            pet,
            aet,
            eff_pcp,
            nir,
            gir,
            aet_met,
            irr_d,
            theory_d,
            app_d,
        ) = self.irr_depth()
        irr_gpa = convert.IN2GPA * app_d  # gal/acre

        irr_area = self.full_array(0.0)

        if np.any(opt[1]):  # irrigated area by land use type category

            # irrigated area of cells within land use categories
            irr_area_acres = self.full_array(0.0)
            for lu in self.registered_land_uses:

                # area in acres
                area = self.get_param("area_{0}".format(lu), bounds=[0, np.nan])

                # percent of area that is irrigated
                k_irr = (
                    self.get_param("percent_irr_{0}".format(lu), bounds=[0.0, 100.0])
                    / 100.0
                )

                # calculate irrigated area for all spatial subunits that have the land use type
                ia = area * k_irr
                irr_area_acres += ia

            irr_area[opt[1]] = irr_area_acres[opt[1]]

        if np.any(opt[2]):  # irrigated area directly specified

            # irrigated area
            irr_area_acres = self.get_param("irr_area", array=True)
            irr_area[opt[2]] = irr_area_acres[opt[2]]

        if np.any(opt[3]):  # irrigated area as percent of shape_area

            # irrigated area of cells within land use categories
            irr_area_acres = self.full_array(0.0)

            area = self.get_param("shape_area", required=True)
            k_irr = (
                self.get_param("percent_irr", bounds=[0.0, 100.0], required=True)
                / 100.0
            )
            irr_area_acres = area * k_irr

            irr_area[opt[3]] = irr_area_acres[opt[3]]

        if np.any(
            opt[4]
        ):  # irrigated area specified for land use in parameter `land_use`

            # irrigated area of cells within land use categories
            irr_area_acres = self.full_array(0.0)
            land_uses = self.get_param("land_use", required=True)
            land_use_opt = {}
            for lu in self.registered_land_uses:

                area = self.get_param("shape_area", required=True)

                # percent of area that is irrigated
                k_irr = self.registered_land_uses[lu]

                # calculate irrigated area for all spatial subunits that have the land use type
                ia = area * k_irr
                i = land_uses == lu
                irr_area_acres[i] += ia[i]
                land_use_opt[lu] = i

            if land_use_opt and not np.all(sum(land_use_opt.values()[opt[4]])):
                raise ValueError("Land use values not set for some spatial subunits!")

            irr_area[opt[4]] = irr_area_acres[opt[4]]

        irr = irr_area * irr_gpa

        return Irrigation(
            "irrigation",
            pet,
            aet,
            eff_pcp,
            nir,
            gir,
            aet_met,
            irr_d,
            theory_d,
            app_d,
            irr_area,
            irr,
        )

    def curr_outdoor(self):

        self.start("outdoor")
        self.fire("on_outdoor_calculating")

        # get current weather data
        wth = self.get_param("weather")

        # total area that stormwater can be generated from
        area_total = self.get_param("shape_area")  # total acreage of the sub_unit
        roof_area = self.get_param("roof_area")

        # stormwater parameters
        # from NLCD layers (percent impervious)
        fraction_impervious = self.get_param("percent_impervious") / 100.0

        # percent of precipitation that produces runoff
        # default 90% is suggested by:
        #    Schueler, T. 1987. Controlling urban runoff: a practical manual for planning and designing urban BMPs.
        #         Metropolitan Washington Council of Governments. Washington, DC
        fraction_precip_runoff = self.get_param("percent_precipitation_runoff") / 100.0

        # checks
        if np.logical_or(fraction_impervious < 0, 1 < fraction_impervious).any():
            raise ValueError(
                "Cannot provide a percentage less than 0% or greater than 100%!"
            )

        # calculation irrigation needs based on evapotranspiration
        irr = self.curr_irr()

        # stormwater calculation from:
        #     Schueler, T. 1987. Controlling urban runoff: a practical manual for planning and designing urban BMPs.
        #         Metropolitan Washington Council of Governments. Washington, DC
        runoff_coefficient = 0.05 + 0.9 * fraction_impervious
        stormwater = IUWMObject("stormwater")
        stormwater.inches = (
            wth.precipitation_in * fraction_precip_runoff * runoff_coefficient
        )  # inches
        stormwater.gallons = stormwater.inches * convert.IN2GPA * area_total  # gallons
        stormwater.area = area_total
        stormwater.weather = wth

        # roof runoff calculations
        roof_interception = self.get_param("roof_interception")
        roof = IUWMObject("roof")
        roof.inches = np.maximum(wth.precipitation_in - roof_interception, 0.0)
        roof.gallons = roof.inches * convert.IN2GPA * roof_area
        roof.area = roof_area

        # leakage information
        leak_fraction = self.get_param("outdoor_leak_percent") / 100.0
        leak_black = self.get_param("outdoor_leak_percent_blackwater") / 100.0
        leak_consumed = 1 - leak_black

        # build outdoor output
        outdoor = UrbanDemandCenter(
            "outdoor",
            irr,
            stormwater,
            roof,
        ).leaky(leak_fraction, consumed=leak_consumed, gray=0.0, black=leak_black)

        self.data.outdoor = outdoor
        self.fire("on_outdoor_calculated", outdoor=outdoor)
        self.stop("outdoor")

        return outdoor

    def curr_cost(self):
        # todo: make generic for any amount of sources/purposes
        self.start("cost")
        self.fire("on_cost_calculating")

        # get parameters
        water_cost = self.get_param("water_cost")
        wastewater_effluent_cost = self.get_param("wastewater_effluent_cost")
        stormwater_use_cost = self.get_param("cost_to_use_storm")
        graywater_use_cost = self.get_param("cost_to_use_gray")
        wastewater_use_cost = self.get_param("cost_to_use_waste")
        c_fixed_supply = self.get_param("cost_to_use_fixed_supply")

        # get intermediate model data
        indoor = self.data.indoor
        outdoor = self.data.outdoor
        stormwater = self.data.stormwater
        graywater = self.data.graywater
        wastewater = self.data.wastewater
        fixed_supply = self.data.fixed_supply

        # demand
        total_demand = indoor.demand + outdoor.demand

        # calculate total costs
        c_treated_use = total_demand / 1000.0 * water_cost
        c_waste_effluent = wastewater.supply / 1000.0 * wastewater_effluent_cost
        c_storm_use = stormwater.reused / 1000.0 * stormwater_use_cost
        c_gray_use = graywater.reused / 1000.0 * graywater_use_cost
        c_waste_use = wastewater.reused / 1000.0 * wastewater_use_cost
        c_fixed_supply_use = wastewater.reused / 1000.0 * c_fixed_supply
        cost = VariableCost(
            "cost",
            c_treated_use,
            c_waste_effluent,
            c_storm_use,
            c_gray_use,
            c_waste_use,
        )

        self.data.cost = cost
        self.fire("on_cost_calculated", cost=cost)
        self.stop("cost")

        return cost

    def capital_cost(self):

        # get parameters
        indoor_upgrade_cost = self.full_array(0.0)
        for demand_type in constants.INDOOR_END_USES:
            k_saved = self.get_param(
                "fraction_saved_{0}".format(demand_type), self.zeros
            )
            eff_cost = self.get_param(
                "efficiency_cost_for_{0}".format(demand_type), self.zeros
            )
            indoor_upgrade_cost += k_saved * eff_cost

        # alternative source water costs
        households = self.get_param("households")
        costs = {
            source_type: self.full_array(0.0)
            for source_type in self.registered_alternative_sources
        }

        for source_type in self.registered_alternative_sources:
            for recycle_bin in self.registered_end_uses:
                var_name = "percent_adoption_of_{0}_for_{1}".format(
                    source_type, recycle_bin
                )
                k_adopted = self.get_param(var_name) / 100.0

                var_name = "storage_of_{0}_for_{1}".format(source_type, recycle_bin)
                capacity = self.get_param(var_name)

                var_name = "capital_cost_of_{0}_for_{1}".format(
                    source_type, recycle_bin
                )
                cost_per_kgals = self.get_param(var_name)

                cost = cost_per_kgals * capacity / 1000.0 * households * k_adopted

                src_name = (
                    constants.SOURCE_TYPES[source_type]
                    if source_type in constants.SOURCE_TYPES
                    else source_type
                )
                costs[src_name] += cost

        costs["indoor"] = indoor_upgrade_cost

        return CapitalCost("capital", **costs)

    def advance_recycling_centers(self):

        self.fire("on_timestep_initializing")
        for source in self.registered_alternative_sources:
            self.data["storage_of_{source}_for_res".format(source=source)].advance()
            self.data["storage_of_{source}_for_cii".format(source=source)].advance()
        self.fire("on_timestep_initialized")

    def use_alternative_water(self, source):

        source = source.lower().strip()
        name = constants.SOURCE_TYPES[source]

        self.start(name)

        self.fire("on_{source}_calculating".format(source=name))

        alt_source = self.registered_alternative_sources[source]
        v = alt_source(name, self)
        self.data[name] = v

        self.fire("on_{source}_calculated".format(source=name), **{name: v})

        v.use(self)

        self.fire("on_{source}_used".format(source=name), **{name: v})

        self.stop(name)
        return v

    def write_possible_outputs_to_file(self, po_file):
        outputs = set(self.data.keys())
        if os.path.exists(po_file):
            with open(po_file, "r") as fh:
                outputs.update(
                    [s.strip() for s in re.split(r"\r?\n", fh.read()) if s.strip()]
                )
        with open(po_file, "w") as fh:
            fh.write("\n".join(sorted(list(outputs))))

    def add_output(self, var_name, data):
        self.user_output[var_name] = data

    def write_output_headers(self):

        self.output.write_headers()

    def write_output(self):

        # retrieve output data and build into new structure
        self.start("output_retr")
        self.fire("on_output_writing", output=self.data)
        stacked_data = np.zeros((self.n_sites, len(self.output_variables)))
        for var_i, var in enumerate(self.output_variables):
            var_name, _ = utils.get_fxn(var)
            stacked_data[:, var_i] = self.data.get(var_name)
        self.stop("output_retr")

        # write outputs and possible outputs
        self.start("output_write")
        if self.write_possible_outputs and self.inputs["first_date"]:
            self.write_possible_outputs_to_file(self.write_possible_outputs)
        date = self.inputs["date"]
        self.output.aggregate(date, stacked_data)
        self.fire("on_output_written", output=self.data)
        self.stop("output_write")

    def close_output(self):

        self.output.close()

    def init_timers(self):
        self.timers = OrderedDict()

    def start(self, curr_timer):
        if curr_timer not in self.timers:
            self.timers[curr_timer] = utils.Timer()
        self.timers[curr_timer].start()

    def stop(self, name):
        self.timers[name].record()

    def print_times(self):
        print("Time Summary:")
        for name, timer in self.timers.items():
            if name != "total":
                print(
                    "  {0:<13s}: {1}".format(
                        name.title().replace("_", " "), timer.summary()
                    )
                )
            else:
                print(
                    "  {0:<13s}: {1}".format(
                        name.title().replace("_", " "), timer.time_str()
                    )
                )

    def fire(self, event_name, **kwargs):
        if event_name not in self.events:
            self.events[event_name] = utils.Event()
        if "model" in kwargs:
            raise ValueError('Cannot supply an argument named "model"!')
        self.events[event_name](self, date=self.inputs.date, **kwargs)

    def subscribe(self, subscriber, event_name=None):

        if hasattr(subscriber, "__call__"):

            s = subscriber

        elif isinstance(subscriber, str):

            # get file path and name space
            subscriber = subscriber.strip()
            py_file, sub_name = os.path.splitext(subscriber)

            if re.match(r"^packages?(\.|\\|/).*", py_file):
                py_file = re.sub(
                    r"^packages?(\.|\\|/)", constants.THIS_DIR + "/package/", py_file
                )

            name_space = os.path.basename(py_file)
            py_file = os.path.abspath(py_file) + ".py"

            # remove .py
            if sub_name.lower().strip() == ".py":
                sub_name = ""

            # import the module (only once)
            if py_file in self.modules:
                m = self.modules[py_file]
            else:
                import imp

                try:
                    m = imp.load_source(name_space, py_file)
                except IOError as e:
                    msg = "{1}\nCould not find package at {0}!\nCheck path to package."
                    raise IOError(msg.format(py_file, e))

                self.modules[py_file] = m

            # error message
            err_msg = (
                'The custom module {0} must contain a function "def subscribe(model):", OR you must '
                "subscribe a specified function by putting the function name after the module name "
                "in the path to the custom module. For example, path/to/module_name.function_name."
            )

            if len(sub_name) > 1:

                # if the subroutine name is provided, use that
                sub_name = sub_name[1:]
                s = getattr(m, sub_name)

            elif hasattr(m, "subscribe"):

                # use a function called subscribe which should have just the model object as an argument
                try:
                    sub = getattr(m, "subscribe")
                    sub(self)
                    return
                except:
                    raise ValueError(err_msg)

            else:
                raise ValueError(err_msg)

        else:
            raise ValueError(
                "A custom subscriber must either be a string or a callable function!"
            )

        if event_name is None:
            event_name = s.__name__
        self.subscribe_function(event_name, s)

    def subscribe_function(self, event_name, subscriber):
        if event_name not in self.events:
            msg = 'There is no event named "{0}". These are the events that can be subscribed to:\n  {1}'
            raise ValueError(msg.format(event_name, "\n  ".join(constants.EVENTS)))
        self.events[event_name].append(subscriber)

    def check_output(self):
        self.start("checks")
        if self.check_level >= 1:
            self.fire("on_output_checking", output=self.data)
            self.data.checks()
        self.stop("checks")

    def iter_time(self, reset=False):

        saved = {}
        if reset:
            saved = self.inputs.copy()

        first_date = True
        for date in self.dates:

            # date information
            self.start("startup")
            self.inputs["date"] = date

            yday = date.timetuple().tm_yday

            self.inputs["yday"] = yday
            self.inputs["first_date"] = first_date
            self.inputs["year_top"] = yday == 1 or first_date

            first_date = False

            self.fire("on_timestep_top")

            # add timeseries input
            self.advance_timeseries()

            # update weather (the primary driver for changes in time)
            self.curr_weather()
            self.stop("startup")

            yield date

        if reset:
            self.inputs = saved

    def retrieve_all_weather(self):
        return [_ for _ in self.iter_time(reset=True)]

    def run(self):

        try:

            # initiate
            if not self.initiated:
                self._init()

            # starting run
            if self.verbose:
                print("\nStarting run...")

            # event for beginning of run
            self.fire("on_begin")

            # timers
            self.init_timers()
            self.start("total")
            self.start("startup")

            # start output
            self.write_output_headers()

            # initialize recycling centers
            self.data = self.build_data()
            self.init_reuse()
            self.stop("startup")

            # start timestep loop
            for date in self.iter_time():

                # verbosity
                self.start("startup")
                if self.verbose and self.inputs["year_top"]:
                    print("  {0}".format(date.year))
                    self.fire("on_timestep_year_top")

                # initialize objects for a new timestep
                self.advance_recycling_centers()

                # get weather outputs
                self.stop("startup")

                # calculate indoor (subscribers to model events can make this change over time)
                self.start("res_indoor")
                self.curr_indoor()
                self.stop("res_indoor")

                # calculate CII (subscribers to model events can make this change over time)
                self.start("cii_indoor")
                self.curr_cii()
                self.stop("cii_indoor")

                # outdoor demand
                outdoor = self.curr_outdoor()
                self.data.outdoor = outdoor

                # stormwater, graywater, and wastewater reuse (along with other registered alternative sources)
                for src in self.registered_alternative_sources:
                    self.use_alternative_water(src)

                # costs
                self.curr_cost()

                # write output
                self.write_output()

                # check mass balance
                self.check_output()

                # bottom of timestep
                self.fire("on_timestep_bottom")

            # print runtime information
            self.stop("total")
            if self.verbose:
                self.print_times()

            # finalize
            self.fire("on_finalize")

        except Exception as e:

            sys.stderr.write("{0}\n".format(e))
            sys.stderr.flush()
            traceback.print_exc()

        finally:

            self.close_output()


def run_dict(input_dict):

    if "service_area" not in input_dict:
        raise ValueError(
            'Service area input file path is a required parameter (use "service_area" as the key).'
        )
    service_area = input_dict.pop("service_area")

    try:
        m = IUWM(service_area, **input_dict)
        m.run()
    except Exception as e:
        return "\n\nError:\n{0}\n\nStack:\n{1}\n\n".format(e, traceback.format_exc())
    return ""
