# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from datetime import datetime, timedelta
import numpy as np
import re

BASE_DATE = datetime(1900, 1, 1)

IN2GPA = 43560.0 * 7.48 / 12.0  # inches of depth of fluid to gallon per acre


def to_text(v, default=None):
    return v if v != '' and v is not None else default


def to_real(v, default=None):
    return float(v) if v != '' and v is not None else default


def to_bool(v):
    v = v.strip().lower()
    if v in {'yes', 'true', 't', 'y', '1'}:
        return True
    elif v in {'no', 'false', 'f', 'n', '0', '', None}:
        return False
    else:
        raise ValueError('Expected boolean type value, did not get it!')


def to_int(v, default=None):
    return int(v) if v != '' and v is not None else default


def to_text_array(v):
    return np.array(v, dtype=np.string_, ndmin=1)


def to_real_array(v):
    return np.float64(v, ndmin=1)


def to_bool_array(v):
    return np.array(v, dtype=np.bool_, ndmin=1)


def to_int_array(v):
    return np.array(v, dtype=np.int32, ndmin=1)
    # return np.int32(v, ndmin=1)


def to_date(date_str, default=None, fmt='American'):
    if isinstance(date_str, datetime):
        return date_str
    if fmt.lower() == 'american':
        if '/' in date_str:
            return datetime.strptime(date_str, '%m/%d/%Y')
        elif '-' in date_str:
            return datetime.strptime(date_str, '%Y-%m-%d')
        elif default is not None:
            return default
        else:
            raise ValueError('Date "{0}" is not a recognized format.'.format(date_str))
    return datetime.strptime(date_str, fmt)


def to_datenum(date_str, fmt='American'):
    """
    Convert a string date to the number of seconds from Jan. 1, 1900

    :param date_str: String containing a date
    :param fmt: The format of the date. If "American", then it tests %m/%d/%Y and %Y-%m-%d
    :return: Returns the number of seconds from Jan. 1, 1900
    """
    return date2num(to_date(date_str, fmt=fmt))


def to_list(s, default=None, pattern=r'[,\s]+'):
    if s:
        if isinstance(s, str):
            s = s.strip()
            if s:
                return [s.strip() for s in re.split(pattern, s)]
            else:
                return default
        elif isinstance(s, list):
            return s
        elif hasattr(s, '__iter__'):
            return list(s)
        else:
            return [s]
    else:
        return []


def to_text_list(v, default=None):
    return [to_text(d, default=default) for d in to_list(v)]


def to_real_list(v, default=None):
    return [to_real(d, default=default) for d in to_list(v)]


def to_bool_list(v):
    return [to_bool(d) for d in to_list(v)]


def to_int_list(v, default=None):
    return [to_int(d, default=default) for d in to_list(v)]


def to_date_list(v, default=None, fmt='American'):
    return [to_date(d, default=default, fmt=fmt) for d in to_list(v)]


def format_date(s, default=None):
    if s:
        try:
            return datetime.strptime(s, '%Y-%m-%d' if '-' in s else '%m/%d/%Y').strftime('%Y-%m-%d')
        except ValueError:
            return default
    else:
         return default


def date2num(d):
    """Convert a date to the number of seconds from Jan. 1, 1900"""
    return (d - BASE_DATE).total_seconds()


def num2date(n):
    """Convert the number of seconds from Jan. 1, 1900 to a date"""
    return BASE_DATE + timedelta(seconds=n)


def f2c(f):
    """Convert degrees Fahrenheit to degrees Celsius"""
    return (f - 32) / 1.8


def c2f(c):
    """Convert degrees Celsius to degrees Fahrenheit"""
    return c * 1.8 + 32


def k2c(k):
    """Convert degrees Kelvin to Celsius"""
    return k - 273.15


def c2k(c):
    """Convert degrees Celsium to Kelvin"""
    return c + 273.15


def in2mm(inches):
    return inches * 25.4


def ft2mm(ft):
    return ft * 12 * 25.4


def cm2mm(cm):
    return cm * 10


def m2mm(m):
    return m * 1000


def cm2in(cm):
    """Convert centimeters (cm) to inches"""
    return cm / 2.54


def mm2in(mm):
    """Convert millimeters (mm) to inches"""
    return mm / 25.4


def mm2ft(mm):
    return mm / 25.4 / 12.0


def mm2cm(mm):
    return mm / 10.0


def mm2m(mm):
    return mm / 1000.0


def wm2ld(wm):
    """Convert Watts per square meter (W/m^2) to langleys/day"""
    return wm / 0.484583


def mjm2mm(mjm):
    """
    Convert MJ/m^2/day to mm/day (equivalent evaporation in mm/day, by multiplying by the inverse of the latent heat
    of vaporization 1/lambda=0.408)
    """
    return mjm * 0.408


def mm2mjm(mm):
    """Convert mm/day to MJ/m^2/day"""
    return mm / 0.408


def in2gpa(inches):
    return inches * IN2GPA  # inches to gallon per acre


def mph2ms(mph):
    """Convert miles per hour to meters per second"""
    return mph / 2.23694


def fs2ms(feet_per_second):
    """Convert feet/second to meters per second"""
    return feet_per_second / 3.28084


def ms2mph(meters_per_second):
    """Convert meters per second to miles per hour"""
    return meters_per_second * 2.23694


def ms2fs(meters_per_second):
    """Convert feet/second to meters per second"""
    return meters_per_second * 3.28084


def id2ms(inches_per_day):
    """Convert inches/day to meters per second"""
    return fs2ms(inches_per_day / 12.0 / 86400.0)


def ms2id(meters_per_second):
    """Convert meters/second to inches per day"""
    return ms2fs(meters_per_second) * 12.0 * 86400.0


def iy2ms(inches_per_year):
    """Convert inches/year to meters per second"""
    return fs2ms(inches_per_year / 365.25 / 12.0 / 86400.0)


def ms2iy(meters_per_second):
    """Convert meters/second to inches per day"""
    return ms2fs(meters_per_second) * 365.25 * 12.0 * 86400.0


def fd2ms(feet_per_day):
    """Convert feet/day to meters per second"""
    return fs2ms(feet_per_day / 86400.0)


def ms2fd(meters_per_second):
    """Convert meters per second to feet per day"""
    return ms2fs(meters_per_second) * 86400.0


def fy2ms(feet_per_year):
    """Convert feet/year to meters per second"""
    return fs2ms(feet_per_year / 365.25 / 86400.0)


def ms2fy(meters_per_second):
    """Convert meters per second to feet per year"""
    return ms2fs(meters_per_second) * 365.25 * 86400.0


def mmd2ms(mm_per_day):
    """Convert mm/day to meters per second"""
    return mm_per_day / 1000.0 / 86400.0


def ms2mmd(meters_per_second):
    """Convert meters/second to mm per day"""
    return meters_per_second * 1000.0 * 86400.0


def mmy2ms(mm_per_year):
    """Convert mm/year to meters per second"""
    return mm_per_year / 365.25 / 1000.0 / 86400.0


def ms2mmy(meters_per_second):
    """Convert meters/second to mm per day"""
    return meters_per_second * 365.25 * 1000.0 * 86400.0


def cmd2ms(cm_per_day):
    """Convert cm/day to meters per second"""
    return cm_per_day / 100.0 / 86400.0


def ms2cmd(meters_per_second):
    """Convert meters/second to cm per day"""
    return meters_per_second * 100.0 * 86400.0


def cmy2ms(cm_per_year):
    """Convert cm/year to meters per second"""
    return cm_per_year / 365.25 / 100.0 / 86400.0


def ms2cmy(meters_per_second):
    """Convert meters/second to cm per day"""
    return meters_per_second * 365.25 * 100.0 * 86400.0


def md2ms(m_per_day):
    """Convert m/day to meters per second"""
    return m_per_day / 86400.0


def ms2md(meters_per_second):
    """Convert meters/second to m per day"""
    return meters_per_second * 86400.0


def my2ms(m_per_year):
    """Convert m/year to meters per second"""
    return m_per_year / 365.25 / 86400.0


def ms2my(meters_per_second):
    """Convert meters/second to m per day"""
    return meters_per_second * 365.25 * 86400.0


def ld2mjm(langley_per_day):
    return langley_per_day * 0.484583 * 0.0864


def wm2mjm(wm):
    """Convert W/m^2 to MJ/m^2/day"""
    return wm * 0.0864


def mjm2ld(mjm):
    return mjm / (0.484583 * 0.0864)


def mjm2wm(mjm):
    return mjm / 0.0864


def frac2perc(frac):
    return frac * 100.0


def perc2frac(perc):
    return perc / 100.0


def h2a(hectares):
    return hectares * 2.47105


def sm2a(sq_meters):
    return sq_meters * 247.105e-6


def smi2a(sq_miles):
    return sq_miles * 640


def skm2a(sq_kilometers):
    return sq_kilometers * 247.105


def a2h(hectares):
    return hectares / 2.47105


def a2sm(sq_meters):
    return sq_meters / 247.105e-6


def a2smi(sq_miles):
    return sq_miles / 640


def a2skm(sq_kilometers):
    return sq_kilometers / 247.105


def identity(x):
    return x


TO_STD = {
    # temperature to deg. C
    'temperature': {
        'f': f2c,
        'c': identity,
        'k': lambda k: k2c,
        'degf': f2c,
        'degc': identity,
        'degk': lambda k: k2c,
        'deg f': f2c,
        'deg c': identity,
        'deg k': lambda k: k2c,
    },

    # length to mm (e.g., precipitation)
    'length': {
        'in': in2mm,
        'ft': ft2mm,
        'mm': identity,
        'cm': cm2mm,
        'm': m2mm,
    },

    # area to acres
    'area': {
        'acres': identity,
        'hectares': h2a,
        'm2': sm2a,
        'm^2': sm2a,
        'km2': skm2a,
        'km^2': skm2a,
        'mi2': smi2a,
        'mi^2': smi2a,
    },

    # radiation to MJ/m^2/day (e.g., daily solar radiation)
    'radiation': {
        'langley': ld2mjm,
        'langleys': ld2mjm,
        'langleys/day': ld2mjm,
        'mj/m^2': identity,
        'mj/m^2/day': identity,
        'mj/m2': identity,
        'mj/m2/day': identity,
        'w/m^2': wm2mjm,
        'w/m2': wm2mjm,
    },

    # factor/percentage to % (e.g., relative humidity)
    'factor': {
        '%': identity,
        '': frac2perc,
        'fraction': frac2perc,
        'dimensionless': frac2perc,
    },

    # velocity to m/s (e.g., wind speed)
    'velocity': {
        'm/s': identity,
        'mph': mph2ms,
        'mi/hr': mph2ms,
        'ft/s': fs2ms,
        'in/day': id2ms,
        'in/yr': id2ms,
        'in/year': iy2ms,
        'ft/day': fd2ms,
        'ft/yr': fd2ms,
        'ft/year': fy2ms,
        'mm/day': mmd2ms,
        'mm/yr': mmd2ms,
        'mm/year': mmy2ms,
        'cm/day': cmd2ms,
        'cm/yr': cmd2ms,
        'cm/year': cmy2ms,
        'm/day': md2ms,
        'm/yr': md2ms,
        'm/year': my2ms,
    },
}


FROM_STD = {
    # temperature from deg. C
    'temperature': {
        'f': c2f,
        'c': identity,
        'k': c2k,
        'degf': c2f,
        'degc': identity,
        'degk': c2k,
        'deg f': c2f,
        'deg c': identity,
        'deg k': c2k,
    },

    # length from mm
    'length': {
        'in': mm2in,
        'ft': mm2ft,
        'mm': identity,
        'cm': mm2cm,
        'm': mm2m,
    },

    # area from acres
    'area': {
        'acres': identity,
        'hectares': h2a,
        'm2': sm2a,
        'm^2': sm2a,
        'km2': skm2a,
        'km^2': skm2a,
        'mi2': smi2a,
        'mi^2': smi2a,
    },

    # radiation from MJ/m^2/day (e.g., daily solar radiation)
    'radiation': {
        'langley': mjm2ld,
        'langleys': mjm2ld,
        'langleys/day': mjm2ld,
        'mj/m^2': identity,
        'mj/m^2/day': identity,
        'mj/m2': identity,
        'mj/m2/day': identity,
        'w/m^2': mjm2wm,
        'w/m2': mjm2wm,
    },

    # factor/percentage from % (e.g., relative humidity)
    'factor': {
        '%': identity,
        '': perc2frac,
        'fraction': perc2frac,
        'dimensionless': perc2frac,
    },

    # velocity from m/s (e.g., wind speed)
    'velocity': {
        'm/s': identity,
        'mph': ms2mph,
        'mi/hr': ms2mph,
        'ft/s': ms2fs,
        'in/day': ms2id,
        'in/yr': ms2id,
        'in/year': ms2iy,
        'ft/day': ms2fd,
        'ft/yr': ms2fd,
        'ft/year': ms2fy,
        'mm/day': ms2mmd,
        'mm/yr': ms2mmd,
        'mm/year': ms2mmy,
        'cm/day': ms2cmd,
        'cm/yr': ms2cmd,
        'cm/year': ms2cmy,
        'm/day': ms2md,
        'm/yr': ms2md,
        'm/year': ms2my,
    },
}


def unit_converter(units_type, from_units, to_units):

    # prepare strings
    units_type = units_type.lower().strip()
    from_units = from_units.lower().strip()
    to_units = to_units.lower().strip()

    # check values
    if units_type not in TO_STD:
        raise ValueError('Unit type "{0}" not found!'.format(units_type))
    if from_units not in TO_STD[units_type]:
        raise ValueError('Units "{0}" not available for unit type "{1}"'.format(from_units, units_type))
    if to_units not in TO_STD[units_type]:
        raise ValueError('Units "{0}" not available for unit type "{1}"'.format(to_units, units_type))

    # build converter function
    to_si = TO_STD[units_type][from_units.lower()]
    from_si = FROM_STD[units_type][to_units.lower()]
    return lambda v: from_si(to_si(v))
