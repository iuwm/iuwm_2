# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import os
import csv
import sys
import numpy as np
from collections import OrderedDict

PARAMETERS = [
    "profile_a",
    "profile_b",
    "irr_cutoff",
    "percent_irr_open",
    "percent_irr_low",
    "percent_irr_med",
    "percent_irr_high",
    "plant_factor",
    "percent_precip_accounted",
]

TARGET_FUNCTION = {
    "min": lambda x: np.argmin(np.abs(x)),
    "max": np.argmax,
}

INDOOR_TARGETS = OrderedDict(
    [
        ("nsce", {"dir": "max"}),
        ("mre", {"value": 0.05, "dir": "min", "cvt": np.abs}),
        ("bias_fraction", {"value": 0.05, "dir": "min", "cvt": np.abs}),
    ]
)

OUTDOOR_TARGETS = OrderedDict(
    [
        ("nsce", {"value": 0.9, "dir": "max"}),
        ("mre", {"value": 0.05, "dir": "min", "cvt": np.abs}),
        ("bias_fraction", {"value": 0.5, "dir": "min", "cvt": np.abs}),
    ]
)


class ResultParser:
    def __init__(self, location, variable, performance):

        self.location = location
        self.variable = variable
        self.performance = performance

    @staticmethod
    def parse(header):

        variables = header.split("_")
        if len(variables) < 3:
            return None
        location = variables[0]
        if len(location) < 12:
            return None
        variable = variables[1]
        performance = "_".join(variables[2:])

        return ResultParser(location, variable, performance)


def nondominated(bias_fraction, mre, nsce=None):
    bias_fraction = np.abs(bias_fraction)
    mre = np.abs(mre)
    if nsce is not None:
        return np.array(
            [
                not np.logical_and(
                    np.logical_and(a < nsce, b > bias_fraction), c > mre
                ).any()
                for a, b, c in zip(nsce, bias_fraction, mre)
            ]
        )
    else:
        return np.array(
            [
                not np.logical_and(b > bias_fraction, c > mre).any()
                for b, c in zip(bias_fraction, mre)
            ]
        )


def bias_equals_mre(indices, bias_fraction, mre, nsce=None):
    return indices[np.argmin((np.abs(bias_fraction) - np.abs(mre)) ** 2)]


def get_locations(csv_file):
    with open(csv_file, "r") as f:
        cr = csv.reader(f)
        headers = next(row for row in cr)
        results = [r for r in [ResultParser.parse(h) for h in headers] if r is not None]
        locs_unique = sorted(list(set(r.location for r in results)))
    return locs_unique


def collect_results(
    csv_file,
    output_var,
    targets,
    parameter_names=PARAMETERS,
    tie_breaker=bias_equals_mre,
):

    # read data and define variables
    with open(csv_file, "r") as f:
        cr = csv.reader(f)
        headers = next(row for row in cr)

        target_names = targets.keys()
        total_cols = ["sum_total_{0}_{1}".format(output_var, t) for t in target_names]

        params = [p for p in parameter_names if p in headers]
        col_names = ["index"] + total_cols + params
        index_col = 0

        cols = [headers.index(c) for c in col_names]
        data = np.float32([[row[c] for c in cols] for row in cr])

    # define helper functions
    def get_performances(perf_name):
        i = target_names.index(perf_name)
        d = data[:, i + 1]
        t = targets[perf_name]
        target_cvt = (
            t["cvt"] if "cvt" in t else None
        )  # None or function of one variable
        if target_cvt:
            d = target_cvt(d)
        return d

    # return whether each parameter set is satisfactory
    def is_satisfactory(perf_name):
        d = get_performances(perf_name)
        t = targets[perf_name]
        if "dir" not in t or "value" not in t:
            return np.ones(d.shape, dtype=bool)
        else:
            target_dir = t["dir"]
            return {
                "min": lambda x: x <= t["value"],
                "max": lambda x: x >= t["value"],
            }[target_dir](d)

    # get satisfactory indices
    i = np.sum(np.vstack([is_satisfactory(t) for t in target_names]), axis=0) == len(
        target_names
    )

    # checks
    if i.sum() == 0:
        raise ValueError("No parameter set meeting all targets was found!")

    # tie breakers
    if i.sum() > 1:

        # nondominated solutions
        perfs = {n: get_performances(n) for n in target_names}
        nond = nondominated(**perfs)
        i = np.logical_and(i, nond)
        i = np.where(i)[0]

        # tie breaker when there are multiple nondominated sets
        if len(i) != 1:
            if tie_breaker is None:
                raise ValueError(
                    "Must provide a tie_breaker when a nondominated set of parameters are returned!"
                )
            i = tie_breaker(i, **{n: v[i] for n, v in perfs.items()})

    # return data
    d = data[i].flatten()
    return {
        "parameters": {
            p: (d[col_names.index(p)] if p in col_names else np.nan) for p in PARAMETERS
        },
        "performance": {
            t: (d[col_names.index(p)] if p in col_names else np.nan)
            for t, p in zip(target_names, total_cols)
        },
        "index": d[index_col],
    }


def combine(name, indoor, outdoor):
    ips = indoor["parameters"]
    ops = outdoor["parameters"]
    return {
        "name": name,
        "parameters": {
            p: (ips[p] if np.isnan(ops[p]) else ops[p])
            for p in PARAMETERS
            if not np.isnan(ops[p]) or not np.isnan(ips[p])
        },
        "indoor_performance": indoor["performance"],
        "outdoor_performance": outdoor["performance"],
        "indoor_index": indoor["index"],
        "outdoor_index": outdoor["index"],
    }


class CsvFile:
    def __init__(self, csv_file):

        with open(csv_file, "r") as f:
            cr = csv.reader(f)
            self.headers = next(row for row in cr)
            self.data = np.float32([row for row in cr])

        self.result_variables = [
            r for r in [ResultParser.parse(h) for h in self.headers] if r is not None
        ]
        self.locations = sorted(list(set(r.location for r in self.result_variables)))
        self.indices = np.int32(self.data[:, self.headers.index("index")])

    def __getitem__(self, column):
        if isinstance(column, str):
            return self.data[:, self.headers.index(column)]
        else:
            return self.data[:, column]

    def get_row_by_index(self, scenario_index):
        return np.where(self.indices == scenario_index)[0][0]

    def get_value(self, row, column):
        if isinstance(column, str):
            return self.data[row, self.headers.index(column)]
        else:
            return self.data[row, column]

    def best_row(self, column, direction):
        # direction should be 'min' or 'max'
        return TARGET_FUNCTION[direction](self[column])

    def best_value(self, column, direction):
        # direction should be 'min' or 'max'
        return self.get_value(self.best_row(column, direction), column)


def results_by_geoid(csv_file, output_var, targets, sum_total_results, out_file=None):

    # output_var should be "indoor.demand" or "outdoor.demand"

    # read files
    data = CsvFile(csv_file)

    # get best data from service area overall
    index = sum_total_results["index"]
    overall_row = data.get_row_by_index(index)
    params = [p for p in PARAMETERS if p in data.headers]

    # prepare output variables
    out_headers = (
        ["geoid", "overall_index"]
        + ["overall_" + p for p in targets]
        + [p + "_at_overall" for p in targets]
        + ["best_" + p for p in targets]
        + ["best_" + p + "_index" for p in targets]
        + ["best_" + p + "_" + param for p in targets for param in params]
    )
    output = []

    # loop through each location
    col = "{0}_{1}_{2}"
    for loc in data.locations:
        best_rows = [
            data.best_row(col.format(loc, output_var, p), targets[p]["dir"])
            for p in targets
        ]

        out_row = [loc, index]
        out_row.extend(sum_total_results["performance"][p] for p in targets)
        out_row.extend(
            data.get_value(overall_row, col.format(loc, output_var, p)) for p in targets
        )
        out_row.extend(
            data.get_value(row, col.format(loc, output_var, p))
            for row, p in zip(best_rows, targets)
        )
        out_row.extend(data.get_value(row, "index") for row in best_rows)
        out_row.extend(data.get_value(row, p) for row in best_rows for p in params)
        output.append(out_row)

    # write output file if desired
    if out_file is not None:
        with open(out_file, "wb") as f:
            cw = csv.writer(f)
            cw.writerow(out_headers)
            for out_row in output:
                cw.writerow(out_row)

    return out_headers, output


def defaults(model_file):

    # read only the first row
    with open(model_file, "r") as f:
        cr = csv.DictReader(f)
        row = next(row for row in cr)

    # collect the default parameter values
    return {p: float(row[p]) for p in PARAMETERS}


def save_to_csv(model_file, scenarios):

    # get default values
    def_values = defaults(model_file)

    # get headers
    headers = (
        ["scenario", "indoor_index", "outdoor_index"]
        + ["indoor_{0}".format(t) for t in INDOOR_TARGETS]
        + ["outdoor_{0}".format(t) for t in OUTDOOR_TARGETS]
        + PARAMETERS
    )

    # write output in csv file
    csv_file = "collected_results.csv"
    with open(csv_file, "wb") as f:
        cw = csv.writer(f)
        cw.writerow(headers)
        for output in scenarios:
            r = [output["name"], output["indoor_index"], output["outdoor_index"]]
            r.extend([output["indoor_performance"][t] for t in INDOOR_TARGETS])
            r.extend([output["outdoor_performance"][t] for t in OUTDOOR_TARGETS])
            r.extend([output["parameters"].get(p, def_values[p]) for p in PARAMETERS])
            cw.writerow(r)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Collect results from IUWM calibration."
    )
    parser.add_argument("dir", type=str, help="Directory of IUWM calibration.")
    parser.add_argument("model_file", type=str, help="IUWM model file.")
    args = parser.parse_args()

    # all data
    all_headers = []
    all_data = []

    print("Collecting calibration data!")

    # outdoor early calibration period
    cf = "{0}/outdoor/results.csv".format(args.dir)
    cfo = "{0}/outdoor/performance_by_geoid.csv".format(args.dir)
    outdoor_early = collect_results(
        cf, "outdoor.demand", OUTDOOR_TARGETS, tie_breaker=bias_equals_mre
    )
    results_by_geoid(cf, "outdoor.demand", OUTDOOR_TARGETS, outdoor_early, out_file=cfo)

    # outdoor late calibration period
    cf = "{0}_2010/outdoor/results.csv".format(args.dir)
    cfo = "{0}_2010/outdoor/performance_by_geoid.csv".format(args.dir)
    outdoor_late = collect_results(
        cf, "outdoor.demand", OUTDOOR_TARGETS, tie_breaker=bias_equals_mre
    )
    results_by_geoid(cf, "outdoor.demand", OUTDOOR_TARGETS, outdoor_late, out_file=cfo)

    # indoor early calibration period
    cf = "{0}/indoor/results.csv".format(args.dir)
    cfo = "{0}/indoor/performance_by_geoid.csv".format(args.dir)
    indoor_early = collect_results(
        cf, "indoor.demand", INDOOR_TARGETS, tie_breaker=bias_equals_mre
    )
    results_by_geoid(cf, "indoor.demand", INDOOR_TARGETS, indoor_early, out_file=cfo)

    # indoor late calibration period
    cf = "{0}_2010/indoor/results.csv".format(args.dir)
    cfo = "{0}_2010/indoor/performance_by_geoid.csv".format(args.dir)
    indoor_late = collect_results(
        cf, "indoor.demand", INDOOR_TARGETS, tie_breaker=bias_equals_mre
    )
    results_by_geoid(cf, "indoor.demand", INDOOR_TARGETS, indoor_late, out_file=cfo)

    # combine
    combined_early = combine("2000-2009 Calibration", indoor_early, outdoor_early)
    combined_late = combine("2010-2014 Calibration", indoor_late, outdoor_late)

    # save output to csv
    save_to_csv(args.model_file, [combined_early, combined_late])

    # results by geoid
    print("Done!")
