# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from aggregators import IUWMOutputAssertionError, OutputCsvFile
from collections import OrderedDict
import constants
from constants import IUWM_PARAM_TAG
import itertools
from model import IUWM
import multiprocessing as mp
import numpy as np
import os
import sys
import time


class ModelScenario:

    def __init__(self, model_inputs, output_dir, named_parameters, index, n_params, total_start, verbose=False):

        # set instance variables
        self.model = IUWM(**model_inputs)
        self.output_dir = output_dir
        self.index = index
        self.scenario = str(index)
        self.named_parameters = named_parameters
        self.n_params = n_params
        self.total_start = total_start
        self.verbose = verbose

        # get the scenario name from named parameters if provided
        if 'scenario' in named_parameters:
            self.scenario = named_parameters.pop('scenario')

        self.direct_output_dir = os.path.join(self.output_dir, self.scenario)
        self.model.output = self.model.output.copy(in_memory=True).new_directory(self.direct_output_dir)

    def run(self):

        # calculate runtime for this model run
        curr_time = time.time()

        p = self.index / float(self.n_params) * 100.0
        t = (curr_time - self.total_start) / 60.0
        sys.stdout.write('Working on run {0:>6d} ({1:>5.1f}%, {2:>6.1f} min.)         \r'.format(self.index, p, t))
        sys.stdout.flush()

        # set parameters in model
        self.model.initiate()
        self.model.set_parameters(self.named_parameters)

        # run the model
        self.model.run()

        # calculate results
        return self.model.output.named_results(starting_dict=OrderedDict([
            ('index', self.index),
            ('scenario', self.scenario),
            (IUWM_PARAM_TAG, self.named_parameters),
            ('runtime', time.time() - curr_time),
        ]), the_dir=self.output_dir)


def run_model_scenario(ms):
    """Runs a model scenario defined by a ModelScenario object"""
    return ms.run()


def run_model_scenario_dict(ms_dict):
    """Runs a model scenario defined by dictionary input to a ModelScenario object"""
    return ModelScenario(**ms_dict).run()


class BatchRunner:
    def __init__(self, inputs, parameters, output, parallel=None, verbose=False, rank=0, n_processes=1):

        # perform parallel rerouting as necessary
        self.parallel = parallel.lower().strip() if parallel else ''
        self.n_processes = n_processes if n_processes else 1
        self.rank = rank
        self.comm = None
        self.verbose = verbose

        if self.parallel in {'mpi', 'mpi_scatter_gather'}:
            from mpi4py import MPI
            self.comm = MPI.COMM_WORLD
            self.n_processes = self.comm.Get_size()
            self.rank = self.comm.Get_rank()

        # outputs
        self.inputs = inputs
        self.model_inputs = self.inputs.copy()
        self.service_area = self.inputs.pop('service_area')
        self.parameters = parameters
        self.n_params = len(parameters)

        # timing
        self.total_start = time.time()

        # output
        self.defn = output
        assert self.defn.directory is not None, 'Need to provide an output directory'
        if self.rank == 0:
            if not os.path.exists(self.defn.directory):
                os.makedirs(self.defn.directory)
        self.model_inputs['output'] = self.defn

        # model
        self.model = IUWM(**self.model_inputs)
        self.model.initiate()  # update cached data before parallel runs
        self.model.retrieve_all_weather()  # update cached data before parallel runs
        self.model_inputs['service_area'] = self.model.service_area_file

        # print number of workers
        if self.parallel:
            self.console('Running on {0} workers!'.format(self.n_processes))

    def console(self, s):
        if self.verbose and self.rank == 0:
            print(s)
            
    def new_output_dir(self, scenario):
        return os.path.join(self.defn.directory, str(scenario))

    def indexed_parameters(self):
        return enumerate(self.parameters)

    def create_model_scenario_dict(self, index, named_parameters, verbose=False):

        # set directory for scenario output
        d = dict(
            model_inputs=self.model_inputs,
            output_dir=self.defn.directory,
            named_parameters=named_parameters,
            index=index,
            n_params=self.n_params,
            total_start=self.total_start,
            verbose=verbose,
        )
        return d

    def create_model_scenario(self, index, named_parameters):
        d = self.create_model_scenario_dict(index, named_parameters)
        return ModelScenario(**d)

    def run_model_scenarios(self, index, named_parameters):
        ms = self.create_model_scenario(index, named_parameters)
        return run_model_scenario(ms)

    def seq_run(self):
        for i, p in self.indexed_parameters():
            yield self.run_model_scenarios(i, p)

    def multi_run(self):
        scenarios = [self.create_model_scenario_dict(i, p, verbose=True) for i, p in self.indexed_parameters()]
        pool = mp.Pool(self.n_processes)
        return pool.map(run_model_scenario_dict, scenarios)

    def scatter_gather_main(self):
        for param_chunk in chunks(self.indexed_parameters(), size=self.n_processes):

            # data to send to each worker
            data = [v for v in param_chunk]

            # fill ends of data
            if len(data) < self.n_processes:
                data = data + [None] * (self.n_processes - len(data))

            # scatter data
            data = self.comm.scatter(data, root=0)
            if data:
                i, p = data
                result = self.run_model_scenarios(i, p)
            else:
                result = None
            res = self.comm.gather(result, root=0)
            if res:
                for result in res:
                    if result is not None:
                        yield result

    def scatter_gather_worker(self):

        for _ in chunks(self.indexed_parameters(), size=self.n_processes):
            data = self.comm.scatter(None, root=0)
            if data:
                i, p = data
                result = self.run_model_scenarios(i, p)
            else:
                result = None
            self.comm.gather(result, root=0)

    def job_scheduler(self):

        # self.console('\nJob scheduler: rank {0}'.format(self.rank))
        w = 1
        job_count = 0
        job_runtimes = np.zeros(self.n_params)
        for i, p in self.indexed_parameters():
            looking = True
            while looking:

                # listen for ready message
                if self.comm.iprobe(source=w, tag=constants.MSG_TAG):
                    data = self.comm.recv(source=w, tag=constants.MSG_TAG)
                    assert data == 1, 'Data is not one!'

                    # worker is ready, send him parameters
                    self.comm.isend((i, p), dest=w, tag=constants.DATA_TAG)
                    looking = False

                # listen for done message
                elif self.comm.iprobe(source=w, tag=constants.DATA_TAG):
                    data = -999
                    attempts = 0
                    while data == -999:
                        try:
                            data = self.comm.recv(source=w, tag=constants.DATA_TAG)
                            job_runtimes[data['index']] = data['runtime']
                        except:
                            attempts += 1
                            time.sleep(0.3)
                            if attempts >= 10:
                                raise ValueError('Attempted to receive data, but could not! Reduce number of nodes!')
                    job_count += 1
                    yield data

                time.sleep(0.05)
                w += 1
                if w >= self.n_processes:
                    w = 1

        self.console('\nWaiting for all workers to finish at {0:>.1f} min...\n'.format((time.time() - self.total_start) / 60.0))
        while job_count != self.n_params:

            if self.comm.iprobe(source=w, tag=constants.DATA_TAG):
                data = self.comm.recv(source=w, tag=constants.DATA_TAG)
                job_runtimes[data['index']] = data['runtime']
                job_count += 1
                yield data

            w += 1
            if w >= self.n_processes:
                w = 1

        self.console('\nAverage runtime: {0:.1f} (std. dev. {1:.2f}) seconds'.format(job_runtimes.mean(), job_runtimes.std()))
        self.console("\nTelling the workers we're done at {0:>.1f} min...\n".format((time.time() - self.total_start) / 60.0))
        for w in range(1, self.n_processes):
            self.comm.isend(0, dest=w, tag=constants.MSG_TAG)

        time.sleep(5)
        print('\n\nShutting down the scheduler at {0:>.1f} min.!\n'.format((time.time() - self.total_start) / 60.0))

    def job_worker(self):

        # self.console('\nJob worker: rank {0:<3d}'.format(self.rank))
        run_more = True
        while run_more:

            # send ready message
            self.comm.isend(1, dest=0, tag=constants.MSG_TAG)
            # self.console('Job worker: rank {0:<3d} sent ready message'.format(self.rank))

            # listen for instructions
            completed = False
            while not completed:

                # listen for data
                if self.comm.iprobe(source=0, tag=constants.DATA_TAG):
                    # self.console('Job worker: rank {0:<3d} getting data'.format(self.rank))

                    # got parameters
                    i, p = self.comm.recv(source=0, tag=constants.DATA_TAG)
                    # self.console('Job worker: rank {0:<3d} got data'.format(self.rank))

                    # run parameters
                    result = self.run_model_scenarios(i, p)
                    # self.console('Job worker: rank {0:<3d} ran model'.format(self.rank))

                    # send result
                    self.comm.send(result, dest=0, tag=constants.DATA_TAG)
                    # self.console('Job worker: rank {0:<3d} sent data'.format(self.rank))
                    completed = True

                # listen for finish message
                elif self.comm.iprobe(source=0, tag=constants.MSG_TAG):
                    data = self.comm.recv(source=0, tag=constants.MSG_TAG)
                    assert data == 0, 'Data is not zero!'
                    # self.console('Job worker: rank {0:<3d} finished'.format(self.rank))
                    sys.stdout.write('Rank {0:>3d} is exiting...   \r'.format(self.rank))
                    sys.stdout.flush()
                    run_more = False
                    break

                # sleep
                else:
                    time.sleep(0.3)
        return tuple()

    def run_iter(self):

        # run the model
        self.total_start = time.time()

        if self.parallel == 'mpi':
            if self.rank == 0:
                return self.job_scheduler()
            else:
                return self.job_worker()

        elif self.parallel == 'mpi_scatter_gather':
            if self.rank == 0:
                return self.scatter_gather_main()
            else:
                return self.scatter_gather_worker()

        elif self.parallel == 'multiprocessing':
            # parallel runs with the built-in library
            if self.n_processes > 1:
                return self.multi_run()
            else:
                return self.seq_run()

        elif self.parallel:
            raise AttributeError('Unrecognized argument for "parallel": {0}!'.format(self.parallel))

        else:
            # run sequentially
            return self.seq_run()

    def run(self, in_memory=False, include_parameters=True):

        if self.rank != 0:
            self.run_iter()
            return OrderedDict()

        # run model, writing output as we go
        files = {}
        for named_results in self.run_iter():

            # not supposed to be here without rank being zero
            if self.rank != 0:
                raise IUWMOutputAssertionError('Somehow we entered this loop without rank being zero!')

            for file_path, result in named_results.items():

                if file_path not in files:
                    files[file_path] = OutputCsvFile(
                        file_path,
                        in_memory=in_memory,
                        include_parameters=include_parameters,
                    )

                files[file_path].writerow(result)

        for c_file in files.values():
            c_file.close()

        return files


def chunks(iterable, size=10000):
    iterator = iter(iterable)
    for first in iterator:
        yield itertools.chain([first], itertools.islice(iterator, size - 1))

