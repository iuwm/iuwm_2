# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from batch import BatchRunner
from collections import OrderedDict
import csv
import math
from multiprocessing import Pool
import numpy as np
import os
import results
from SALib.analyze import sobol
from SALib.sample import saltelli
import sys
import time
import utils
import warnings


def calc_n_required(n_params):
    """
    Calculates the number of required samples based on

        Burhenne, S., Jacob, D., and Henze, G. P. Sampling based on Sobol' sequences for
        Monte Carlo Techniques applied to building simulations. 12th Conference of International
        Building Performance Simulation Association, Sydney, Australia, Nov. 14-16, 2011.
        http://www.ibpsa.org/?page_id=105

    :param n_params: The number of parameters being analyzed
    :return: Returns the total number of required samples
    """

    # The real number of samples that will be run is:
    # 2 ** n_params * (2 * n_params + 2)
    # SALib automatically adds the (2 * n_params + 2) at the end.
    return 2 ** n_params


def get_n_runs(nparams, n_runs=None, calc_second_order=False, rank=0):

    if calc_second_order:
        fac = 2 * nparams + 2
        eqn = '2 * nparams + 2'
    else:
        fac = nparams + 2
        eqn = 'nparams + 2'

    r_runs = calc_n_required(nparams)
    if n_runs is None:
        # use recommended number of runs
        n_runs = r_runs
    else:
        # user-provided number of runs reduced by multiplication factor to set n_runs exactly at what user provided
        new_runs = int(math.ceil(n_runs / float(fac)))
        if n_runs % fac != 0 and rank == 0:
            msg = 'WARNING: NUMBER OF RUNS IS NOT A MULTIPLE OF {0} (= {3}). ' \
                  'NUMBER OF RUNS IS CHANGED FROM {1} TO {2}.'
            print(msg.format(fac, n_runs, new_runs * fac, eqn))
        n_runs = new_runs

    if rank == 0:
        print('Running {0} parameter sets (at least {1} recommended)...'.format(n_runs * fac, r_runs * fac))
        if n_runs < r_runs:
            print('WARNING: RUNNING LESS SETS THAN RECOMMENDED!')

    return n_runs


def save_parameters(param_file, parameter_names, values):
    with open(param_file, 'wb') as f: 
        cw = csv.writer(f)
        cw.writerow(['index', 'scenario'] + parameter_names)
        for i, r in enumerate(values):
            cw.writerow([i, 'scen_{0}'.format(i)] + list(r))
            
            
def run(inputs, param_info, output, n_runs=None, parallel=None, n_processes=1,
        param_file_name='params.csv', calc_second_order=False, verbose=False):

    start_time = time.time()

    # parallel parameters
    rank = 0
    if parallel in {'mpi', 'mpi_scatter_gather'}:
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()

    # ensure output directory exists
    output_dir = output.directory
    assert output_dir is not None, 'Need to provide an output directory!'
    if rank == 0:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    param_file = os.path.join(output_dir, param_file_name)

    # get param_info if string
    if isinstance(param_info, (str, unicode)):
        if utils.same_file(param_info, param_file):
            raise ValueError('Parameter inputs cannot be the same as outputs {0}'.format(param_info))
        param_info = utils.read_param_file(param_info, delimiter=',')

    # calculate number of runs
    n_runs = get_n_runs(param_info['num_vars'], n_runs=n_runs, calc_second_order=calc_second_order, rank=rank)

    # generate parameter list
    parameters = saltelli.sample(param_info, n_runs, calc_second_order=calc_second_order)

    # save parameters for good keeping
    save_parameters(param_file, param_info['names'], parameters)

    # run model, writing output as we go
    params = [OrderedDict([(n, p) for n, p in zip(param_info['names'], cp)]) for cp in parameters]

    bv = BatchRunner(inputs, params, output, parallel=parallel, n_processes=n_processes, verbose=verbose)
    files = bv.run(in_memory=True)
    if rank == 0:
        for file_path in files:
            analyze(file_path, param_info, calc_second_order=calc_second_order, n_processes=n_processes)
    print('\nDone in {0:.1f} minutes!\n'.format((time.time() - start_time) / 60.0))


class SensitivityAnalysisData:

    def __init__(self, output_dir, result_name, param_info, result, calc_second_order=False):
        self.output_dir = output_dir
        self.result_name = result_name
        self.result = result
        self.param_info = param_info
        self.calc_second_order = calc_second_order

    def run(self):

        # skip metrics with nans in them...
        if len(self.result) == 0 or np.isnan(self.result).any():
            return False

        # ignore calculation errors because there are usually many...
        err = np.geterr()
        stdout = sys.stdout
        try:
            np.seterr('ignore')

            with open(os.path.join(self.output_dir, self.result_name + '.txt'), 'w') as sys.stdout:
                sobol.analyze(
                    self.param_info,
                    self.result,
                    calc_second_order=self.calc_second_order,
                    print_to_console=True,
                )

            return True

        except Exception as e:
            msg = 'Error calculating sensitivity of {0}:\n{1}\n{2}\n...'.format(self.result_name, self.result, e)
            sys.stderr.write(msg)
            return False

        finally:
            sys.stdout = stdout
            np.seterr(**err)


def analyze_single(sad):
    return sad.run()


def analyze(results_file, param_info, calc_second_order=False, n_processes=1):

    # get param_info if string
    if isinstance(param_info, (str, unicode)):
        param_info = utils.read_param_file(param_info, delimiter=',')

    nm = 'metrics_' + os.path.splitext(os.path.basename(results_file))[0]
    output_dir = os.path.join(os.path.dirname(results_file), nm)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    warnings.simplefilter('ignore')

    # calculate sensitivity indices
    print('Calculating sensitivity indices on metrics for {0}...'.format(nm))
    res_file = results.ResultsCsvFile(results_file)

    sad_list = [
        SensitivityAnalysisData(output_dir, result_name, param_info, result, calc_second_order=calc_second_order)
        for result_name, result in res_file.items()
    ]

    if n_processes > 1:
        p = Pool(n_processes)
        p.map(analyze_single, sad_list)
    else:
        for sad in sad_list:
            analyze_single(sad)

    warnings.simplefilter('default')
