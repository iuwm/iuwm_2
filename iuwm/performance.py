# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import csv
import numpy as np
import os
import matplotlib
if constants.SILENT_PLOTTING:
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as sp


def residuals(observed, modeled, **extra):
    return (observed - modeled).flatten()


def bias(observed, modeled, **extra):
    # bias (sum of errors)
    return np.sum(observed - modeled)


def bias_fraction(observed, modeled, **extra):
    # percent bias sum(errors) / sum(observed)
    return bias(observed, modeled) / np.sum(observed)


def sse(observed, modeled, **extra):
    # sum of squared errors
    return np.sum((observed - modeled) ** 2)


def sae(observed, modeled, **extra):
    # sum of absolute errors
    return np.sum(np.abs(observed - modeled))


def dev(observed, modeled, **extra):
    # model deviation sum(modeled) / sum(observed)
    return np.sum(modeled) / np.sum(observed)


def rel_errors(observed, modeled):
    re = np.zeros(observed.shape)

    # when observed data is not zero
    oi = np.logical_and(~np.isnan(observed), observed != 0)
    if oi.sum() != 0:
        m = modeled[oi]
        o = observed[oi]
        re[oi] = (o - m) / o

    # when modeled data is not zero
    mi = np.logical_and(~oi, np.abs(modeled) > 0.0001 * modeled.std())
    if mi.sum() != 0:
        m = modeled[mi]
        re[mi] = -m / np.abs(m)
    return re


def mre(observed, modeled, **extra):
    # mean relative error
    return np.mean(rel_errors(observed, modeled))


def mae(observed, modeled, **extra):
    # mean absolute error
    return np.mean(np.abs(observed - modeled))


def rmse(observed, modeled, **extra):
    # root mean squared error
    return np.sqrt(np.mean((observed - modeled) ** 2))


def pwrmse(observed, modeled, **extra):
    # peak-weighted root mean squared error
    avg = np.mean(observed)
    return np.sqrt(np.mean(((observed - modeled) ** 2) * (observed + avg) / (2 * avg)))


def nsce(observed, modeled, **extra):
    # Nash-Sutcliffe coefficient of efficiency
    return 1 - sse(observed, modeled) / np.sum((observed - np.mean(observed)) ** 2)


def mce(observed, modeled, **extra):
    # modified coefficient of efficiency
    return 1 - sae(observed, modeled) / np.sum(np.abs(observed - np.mean(observed)))


def correl(observed, modeled, **extra):
    o = observed - observed.mean()
    m = modeled - modeled.mean()
    return np.sum(o * m) / np.sqrt(np.sum(o ** 2) * np.sum(m ** 2))


def error_auto_correl(observed, modeled, **extra):
    e = residuals(observed, modeled)
    return correl(e[1:], e[:-1])


def error_ar1_random_comp(observed, modeled, rho=None, **extra):
    e = residuals(observed, modeled)
    if rho is None:
        rho = error_auto_correl(observed, modeled)
    return e[1:] - rho * e[:-1]


def error_ar1_random_comp_mean(observed, modeled, rho=None, **extra):
    v = error_ar1_random_comp(observed, modeled, rho=rho, **extra)
    return v.mean()


def error_ar1_random_comp_std(observed, modeled, rho=None, ddof=1, **extra):
    v = error_ar1_random_comp(observed, modeled, rho=rho, **extra)
    return v.std(ddof=ddof)


def log_likelihood_ar1(observed, modeled, rho=None, sv=None, ddof=1, **extra):
    n = len(observed)
    e = residuals(observed, modeled)
    if rho is None:
        rho = error_auto_correl(observed, modeled)
    v = error_ar1_random_comp(observed, modeled, rho=rho)
    if sv is None:
        sv = v.std(ddof=ddof)

    rho2 = 1 - rho ** 2
    sv2 = sv ** 2

    t1 = - 0.5 * n * np.log(2 * np.pi)
    t2 = - 0.5 * np.log(sv2 / rho2)
    t3 = - 0.5 / sv2 * (rho2 * e[0] ** 2 + np.sum(v ** 2))

    return t1 + t2 + t3


def rsq(observed, modeled, **extra):
    return correl(observed, modeled) ** 2


def error_mean(observed, modeled, **extra):
    return np.mean(observed - modeled)


def error_std(observed, modeled, **extra):
    return np.std(observed - modeled)


def error_skew(observed, modeled, **extra):
    return sp.skew(observed - modeled)


def error_kurtosis(observed, modeled, **extra):
    return sp.kurtosis(observed - modeled)


def adj_rsq(observed, modeled, n_parameters, has_intercept, **extra):
    n = float(len(observed))
    p = float(n_parameters if has_intercept else n_parameters + 1)
    return 1 - (n - 1) / (n - p) * (1 - rsq(observed, modeled))


def lillie_test(observed, modeled, **extra):
    import statsmodels.stats as sm
    # Lilliefors test for normality
    # returns (K-S statistic, p-value)
    r = residuals(observed, modeled)
    return sm.diagnostic.kstest_normal(r)


def shapiro(observed, modeled, **extra):
    # Shapiro-Wilk test for normality
    # returns (test statistic, p-value)
    r = residuals(observed, modeled)
    return sp.shapiro(r)


def anderson(observed, modeled, dist='norm', **extra):
    # Anderson-Darling test for data coming from a particular distribution
    # returns (test statistic, critical values (corresponding to significance levels), significance levels)
    r = residuals(observed, modeled)
    return sp.anderson(r, dist=dist)


def brown_forsythe(observed, modeled, center='median', **extra):
    # Performs the Brown-Forsythe test (using median) and the Levene test (using mean)
    # returns (test statistic, p-value)
    r = residuals(observed, modeled)
    c = len(observed) / 2
    return sp.levene(r[:c], r[c:], center=center)


def durbin_watson(observed, modeled, **extra):
    import statsmodels.stats as sm
    # Performs the Durbin-Watson test
    # returns the test statistic (between 0 and 4)
    #   when 2, no serial correlation
    #   when 0, positive serial correlation
    #   when 4, negative serial correlation
    r = residuals(observed, modeled)
    return sm.stattools.durbin_watson(r)


def aic(observed, modeled, n_parameters, has_intercept, **extra):
    # Calculates Akaike's information criterion (AIC)
    r = residuals(observed, modeled)
    n = len(observed)
    p = n_parameters if has_intercept else n_parameters + 1
    return n * np.log(np.sum(r ** 2)) - n * np.log(n) + 2 * p


def sbc(observed, modeled, n_parameters, has_intercept, **extra):
    # Calculates Schewarz' Bayesian Criterion (SBC)
    r = residuals(observed, modeled)
    n = len(observed)
    p = n_parameters if has_intercept else n_parameters + 1
    return n * np.log(np.sum(r ** 2)) - n * np.log(n) + np.log(n) * p


def f_stat(observed, modeled, n_parameters, has_intercept, **extra):
    # Calculates the f-statistic for the regression
    r = residuals(observed, modeled)
    n = len(observed)
    dfm = n_parameters - 1 if has_intercept else n_parameters
    dfe = n - n_parameters

    msm = np.sum((modeled - modeled.mean())**2) / float(dfm)
    mse = np.sum(r**2) / float(dfe)
    f = msm / mse

    rv = sp.f(dfm, dfe)
    p_value = 1-rv.cdf(f)
    return f, p_value


def scale_0_1(v):
    min_v = np.min(v)
    max_v = np.max(v)
    if max_v == min_v:
        return np.ones(v.shape)
    return (v - min_v) / (max_v - min_v)


def identity(v):
    return np.copy(v)


METRICS = [
    nsce,
    mre,
    bias_fraction,
    rmse,
    pwrmse,
    rsq,
    error_mean,
    error_std,
    error_skew,
    error_auto_correl,
    error_ar1_random_comp_mean,
    error_ar1_random_comp_std,
    log_likelihood_ar1,
]

METRIC_NAMES = [v.__name__ for v in METRICS]

STATISTICS = METRICS + [
    adj_rsq,
    lillie_test,
    shapiro,
    anderson,
    brown_forsythe,
    durbin_watson,
    aic,
    sbc,
    f_stat
]

STATISTIC_NAMES = [v.__name__ for v in STATISTICS]

TRANSFORMERS = {
    'nsce': scale_0_1,
    'mre': scale_0_1,
    'bias_fraction': scale_0_1,
    'rmse': scale_0_1,
    'pwrmse': scale_0_1,
    'rsq': scale_0_1,
    'error_mean': scale_0_1,
    'error_std': scale_0_1,
    'error_skew': scale_0_1,
    'error_auto_correl': scale_0_1,
    'error_ar1_random_comp_mean': scale_0_1,
    'error_ar1_random_comp_std': scale_0_1,
    'log_likelihood_ar1': np.exp,
}

NAMERS = {
    'nsce': 'Nash-Sutcliffe Coefficient of Efficiency',
    'mre': 'Mean Relative Error',
    'bias_fraction': 'Bias Fraction',
    'rmse': 'Root Mean Squared Error',
    'pwrmse': 'Peak-Weighted Root Mean Squared Error',
    'rsq': 'Square of the pearson correlation coefficient (r^2)',
    'error_mean': 'Mean of residual errors',
    'error_std': 'Standard deviation of residual errors',
    'error_skew': 'Skewness of residual errors',
    'error_auto_correl': 'Auto-correlation of residual errors',
    'error_ar1_random_comp_mean': 'Mean of random error component of fitted AR1 model',
    'error_ar1_random_comp_std': 'Standard deviation of random error component of fitted AR1 model',
    'log_likelihood_ar1': 'Log-likelihood value assuming an AR1 model',
}


def get_metrics(observed, modeled, metrics=METRICS, **extra):

    o = observed
    m = modeled
    g = globals()

    def name_and_func(c):
        if isinstance(c, (str, unicode)):
            name = c
            f = g[c]
        else:
            name = c.__name__
            f = c
        return name, f(o, m)

    return [name_and_func(metric) for metric in metrics]


def get_metric_functions(metrics):
    out_val = []
    d = globals()
    for m in metrics:
        if isinstance(m, str):
            if m not in d:
                raise ValueError('Metric "{0}" not found!'.format(m))
            else:
                out_val.append(d[m])
        elif hasattr(m, '__call__') or hasattr(m, '__func__'):
            out_val.append(m)
        else:
            raise ValueError('Metric "{0}" of type "{1}" is not of right type!'.format(m, type(m)))
    return out_val


def get_statistics(observed, modeled, n_parameters, has_intercept, dist, statistics=STATISTICS, **extra):

    o = observed
    m = modeled
    n = n_parameters
    h = has_intercept
    d = dist

    g = globals()

    def name_and_func(c):
        if isinstance(c, (str, unicode)):
            name = c
            f = g[c]
        else:
            name = c.__name__
            f = c
        return name, f(o, m, n_parameters=n, has_intercept=h, dist=d, **extra)

    return [name_and_func(metric) for metric in statistics]


def get_transformer(metric):
    return TRANSFORMERS[metric] if metric in TRANSFORMERS else identity


def get_metric_name(metric):
    return NAMERS[metric] if metric in NAMERS else metric


def print_metrics(observed, modeled, **extra):
    for n, v in get_metrics(observed, modeled, **extra):
        print('{0:>15s}: {1}'.format(n, v))


def print_statistics(observed, modeled, n_parameters, has_intercept, dist, **extra):
    for n, v in get_statistics(observed, modeled, n_parameters, has_intercept, dist, **extra):
        print('{0:>15s}: {1}'.format(n, v))


def save_metrics(out_file, observed, modeled, **extra):
    with open(out_file, 'wb') as f:
        cw = csv.writer(f)
        cw.writerow(['metric', 'value'])
        for n, v in get_metrics(observed, modeled, **extra):
            cw.writerow([n, v])


def save_statistics(out_file, observed, modeled, n_parameters, has_intercept, dist, **extra):
    with open(out_file, 'wb') as f:
        cw = csv.writer(f)
        cw.writerow(['metric', 'value'])
        for n, v in get_statistics(observed, modeled, n_parameters, has_intercept, dist, **extra):
            cw.writerow([n, v])


def plot(var_name, observed, modeled, fig_args=None, out_dir=None):

    if fig_args is None:
        fig_args = {}
    fig = plt.figure(**fig_args)

    plt.plot(modeled, observed, 'ko')

    plt.xlabel('Modeled {0}'.format(var_name), fontsize=12)
    plt.ylabel('Observed {0}'.format(var_name), fontsize=12)

    if out_dir is None:
        out_dir = '.'

    fig.tight_layout()
    plt.savefig(os.path.join(out_dir, 'obs_vs_mod_{0}.png'.format(var_name)))
