# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from collections import OrderedDict
import csv
import numpy as np
import re


def np_and(*args):
    if len(args) == 0:
        return False
    elif len(args) == 1:
        return np.bool_(args[0])
    elif len(args) == 2:
        return np.logical_and(args[0], args[1])
    else:
        return np_and(args[0], np_and(args[1], args[2:]))


class ColumnFormatter:
    col_type = None
    sep = None
    fmt_str = None

    def __init__(self, flags=re.IGNORECASE):

        self.sep_list = self.sep if isinstance(self.sep, (tuple, list)) else [self.sep]

        if self.sep_list:
            self.fmt = self.fmt_str % tuple(self.sep_list)
        else:
            self.fmt = self.fmt_str
        self.vars = re.findall(r"\{(\w+)\}", self.fmt_str)

        self.re_str = re.sub(r"\{(%s)\}" % "|".join(self.vars), r"(.*)", self.fmt)
        self.re = re.compile(self.re_str, flags=flags)

    def format(self, **kwargs):
        return self.fmt.format(**kwargs)

    def search(self, string):
        return self.re.search(string)

    def _parts(self, match, *groups):
        return {
            "col_type": self.col_type,
            "geoid": "",
            "var": match,
            "metric": "",
        }

    def parts(self, string):
        match = self.search(string)
        if match:
            return self._parts(match.group(0), *match.groups())
        else:
            return None


class SpecialFormatter(ColumnFormatter):
    col_type = "special"
    sep = []
    fmt_str = "(index|scenario|runtime)"


class VarByGeoidFormatter(ColumnFormatter):
    col_type = "var_by_geoid"
    sep = ["__v__"]
    fmt_str = "{geoid}%s{var}"

    def _parts(self, match, *groups):
        d = ColumnFormatter._parts(self, match, *groups)
        d["geoid"] = groups[0]
        d["var"] = groups[1]
        return d


class MetricByGeoidFormatter(VarByGeoidFormatter):
    col_type = "metric_by_geoid"
    sep = VarByGeoidFormatter.sep + ["__m__"]
    fmt_str = VarByGeoidFormatter.fmt_str + "%s{metric}"

    def _parts(self, match, *groups):
        d = VarByGeoidFormatter._parts(self, match, *groups)
        d["metric"] = groups[2]
        return d


class ObservedByGeoidFormatter(VarByGeoidFormatter):
    col_type = "observed_by_geoid"
    sep = ["__o__"] + VarByGeoidFormatter.sep
    fmt_str = "observed%s" + VarByGeoidFormatter.fmt_str


class ErrorByGeoidFormatter(VarByGeoidFormatter):
    col_type = "error_by_geoid"
    sep = ["__e__"] + VarByGeoidFormatter.sep
    fmt_str = "error%s" + VarByGeoidFormatter.fmt_str


class VarFormatter(ColumnFormatter):
    col_type = "var"
    sep = []
    fmt_str = "{var}"


class MetricFormatter(VarFormatter):
    col_type = "metric"
    sep = VarFormatter.sep + ["__m__"]
    fmt_str = VarFormatter.fmt_str + "%s{metric}"

    def _parts(self, match, *groups):
        d = VarFormatter._parts(self, match, *groups)
        d["var"] = groups[0]
        d["metric"] = groups[1]
        return d


class ObservedFormatter(VarFormatter):
    col_type = "observed"
    sep = ["__o__"] + VarFormatter.sep
    fmt_str = "observed%s" + VarFormatter.fmt_str


class ErrorFormatter(VarFormatter):
    col_type = "error"
    sep = ["__e__"] + VarFormatter.sep
    fmt_str = "error%s" + VarFormatter.fmt_str


FORMATTERS = [  # ordered by most specific to least specific
    SpecialFormatter(),
    MetricByGeoidFormatter(),
    ObservedByGeoidFormatter(),
    ErrorByGeoidFormatter(),
    VarByGeoidFormatter(),
    MetricFormatter(),
    ObservedFormatter(),
    ErrorFormatter(),
    VarFormatter(),
]
FORMATTERS_DICT = OrderedDict([(f.col_type, f) for f in FORMATTERS])


def header(col_type, geoid=None, var=None, metric=None):
    return FORMATTERS_DICT[col_type].format(geoid=geoid, var=var, metric=metric)


def parse_header(header):

    if isinstance(header, (np.ndarray, list)):
        return zip(*[parse_header(h) for h in header])

    for formatter in FORMATTERS:
        p = formatter.parts(header)
        if p:
            return tuple(p[k] for k in ("col_type", "geoid", "var", "metric"))

    # anything that does not match everything else is labeled as a parameter
    return "parameter", "", header, ""


def save(file_path, res):
    """
    Save a dictionary of batch run results to file
    """
    with open(file_path, "wb") as f:
        keys = res.keys()
        cw = csv.DictWriter(f, fieldnames=keys)
        cw.writeheader()
        for i in range(len(res[keys[0]])):
            cw.writerow({k: res[k][i] for k in keys})


class ResultsCsvFile:
    def __init__(self, csv_file):
        """
        Retrieve results from a batch results file

        :param csv_file: The file containing batch run results
        """

        self.csv_file = csv_file

        with open(self.csv_file, "r") as f:
            cr = csv.reader(f)

            # read headers
            self.header_list = list(next(row for row in cr))

            # parse header information
            hp = {h: parse_header(h) for h in self.header_list}
            self.header_info = {
                h: {
                    "col_type": p[0],
                    "geoid": p[1],
                    "var": p[2],
                    "metric": p[3],
                }
                for h, p in hp.items()
            }

            # collect and sort data
            i = self.header_list.index("index")  # use to sort
            self.data_list = sorted([row for row in cr], key=lambda r: r[i])

        # remove special columns for querying rows
        self.indices = self._pop_col("index", np.int32)
        self.scenarios = self._pop_col("scenario", np.string_)
        self.runtimes = self._pop_col("runtime", np.float64)
        self.rows = {
            "index": self.indices,
            "scenario": self.scenarios,
            "runtime": self.runtimes,
        }

        # convert to array after removing special columns
        self.headers = np.array(self.header_list, dtype=np.string_, ndmin=1)
        self.data = np.array(self.data_list, dtype=np.float64, ndmin=2)

        self.n_rows = self.data.shape[0]
        self.n_cols = self.data.shape[1]

        # column information
        self.col_types = self._col_values("col_type")
        self.geoids = self._col_values("geoid")
        self.variables = self._col_values("var")
        self.metrics = self._col_values("metric")
        self.cols = {
            "col_type": self.col_types,
            "geoid": self.geoids,
            "var": self.variables,
            "metric": self.metrics,
        }

    def save(self, file_path):
        """
        Save data in this instance to file

        :param file_path: The path within which to save data
        """
        with open(file_path, "wb") as f:
            cw = csv.writer(f)
            cw.writerow(["index", "scenario", "runtime"] + self.header_list)
            for i, s, rt, r in zip(
                self.indices, self.scenarios, self.runtimes, self.data_list
            ):
                cw.writerow([i, s, rt] + r)

    def _pop_col(self, header, dtype):
        i = self.header_list.index(header)
        assert self.header_list.pop(i) == header, "Columns do not line up!"
        return np.array([row.pop(i) for row in self.data_list], dtype=dtype, ndmin=1)

    def _col_values(self, name):
        info = [self.header_info[h][name] for h in self.headers]
        return np.array(info, dtype=np.string_, ndmin=1)

    def filter_i(self, **filters):
        """
        Filter the dataset by row and column information

        :param filters: Values or functions to match row indices ("index", "scenario")
            or column indices ("col_type", "geoid", "var", "metric") to search for.
            Functions should return True or False for single values passed to it.
        :return: Returns row, col indices of data filtered by arguments
        """

        def select(data, the_filter):
            if callable(the_filter):
                f = np.vectorize(the_filter)
                return f(data)
            else:
                return data == the_filter

        rows = [select(self.rows[k], v) for k, v in filters.items() if k in self.rows]
        if rows:
            i = np.where(np_and(*rows))[0]
        else:
            i = np.full(self.n_rows, True)

        cols = [select(self.cols[k], v) for k, v in filters.items() if k in self.cols]
        if cols:
            j = np.where(np_and(*cols))[0]
        else:
            j = np.arange(self.n_cols)

        return i, j

    def filter(self, i=None, j=None, **filters):
        """
        Filter the dataset by row and column information

        :param filters: Values or functions to match row indices ("index", "scenario")
            or column indices ("col_type", "geoid", "var", "metric") to search for.
            Functions should return True or False for single values passed to it.
        :param i: Row indices to filter data by
        :param j: Column indices to filter data by
        :return: Returns a 2D array of data filtered by arguments
        """
        if i is None or j is None:
            i, j = self.filter_i(**filters)
        headers = [self.headers[k] for k in j]
        return headers, self.data[i][:, j]

    def items(self, i=None, j=None, **filters):
        """
        Retrieves paired headers with associated data arrays

        :param filters: Values or functions to match row indices ("index", "scenario")
            or column indices ("col_type", "geoid", "var", "metric") to search for.
            Functions should return True or False for single values passed to it.
        :param i: Row indices to filter data by
        :param j: Column indices to filter data by
        :return: Returns an iterator with paired headers and a 1D array of data filtered by arguments
        """
        if i is None or j is None:
            i, j = self.filter_i(**filters)
        d = self.data[i]
        return ((self.headers[k], d[:, k]) for k in j)

    def dict(self, i=None, j=None, **filters):
        """
        Retrieves a dict with paired headers with associated data arrays

        :param filters: Values or functions to match row indices ("index", "scenario")
            or column indices ("col_type", "geoid", "var", "metric") to search for.
            Functions should return True or False for single values passed to it.
        :param i: Row indices to filter data by
        :param j: Column indices to filter data by
        :return: Returns a dict of paired headers and a 1D array of data filtered by arguments
        """
        return OrderedDict(self.items(i=i, j=j, **filters))

    def parameters(self):
        """
        Retrieves parameters from the results file - should always be the last few columns labeled 'var'

        :return: Returns a dictionary of parameters
        """
        i, j = self.filter_i(col_type="var")
        end_consecutive = np.where(np.diff(j) != 1)[0]
        if end_consecutive:
            j = j[end_consecutive[-1] + 1 :]
        return self.dict(i=i, j=j)


class ModelDataArray:
    def __init__(self, dims, headers, data, flat):
        self.dims = dims
        self.data = data
        self.flat = flat
        self.headers = headers
        self.headers_col = {h: i for i, h in enumerate(headers)}

        self._check()

    def _check(self):
        if len(self.dims) != 2:
            raise ValueError('Must provide data that has two named dimensions "dims"')

        if not self.flat:
            d1 = self.dims[0]
            if d1 not in self.headers_col:
                raise AssertionError(
                    '1st dimension "{0}" must be in headers when not flat!'.format(d1)
                )

    def flatten_headers(self):
        ci = self.headers_col[self.dims[0]]
        headers = [h for i, h in enumerate(self.headers) if i != ci]
        axis_data = self.data[:, ci]
        return []

    def flatten(self):
        return ModelDataArray(self.dims, self.data.flatten(), True)
