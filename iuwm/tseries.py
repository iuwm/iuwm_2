from ast import literal_eval
import csv
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import inputs
import itertools
import numpy as np
from operator import itemgetter
import re
import time
import utils

DATE_20CENTURY = datetime(1900, 1, 1)
DATE_EPOCH = datetime(1970, 1, 1)


class Timestep:
    custom = -1
    daily = 0
    weekly = 1
    monthly = 2
    annual = 3

    def __init__(
        self,
        enum_val=-1,
        years=0,
        months=0,
        days=0,
        leapdays=0,
        weeks=0,
        hours=0,
        minutes=0,
        seconds=0,
        microseconds=0,
    ):

        self.preset = {
            "daily": Timestep.daily,
            "weekly": Timestep.weekly,
            "monthly": Timestep.monthly,
            "yearly": Timestep.annual,
            "annual": Timestep.annual,
        }
        self.preset_inv = {
            Timestep.daily: "daily",
            Timestep.weekly: "weekly",
            Timestep.monthly: "monthly",
            Timestep.annual: "annual",
        }

        if isinstance(enum_val, Timestep):
            self.__dict__.update(enum_val.__dict__.copy())

        elif isinstance(enum_val, str):
            if enum_val in self.preset:
                self.enum_val = self.preset[enum_val]
                self.delta = {
                    0: relativedelta(days=1),
                    1: relativedelta(weeks=1),
                    2: relativedelta(months=1),
                    3: relativedelta(years=1),
                }[self.enum_val]
                self.ndays = 1 if self.enum_val == Timestep.daily else 0
            else:
                self.enum_val = -1
                args = literal_eval(
                    enum_val
                )  # enumval is assumed to be a python dictionary
                self.delta = relativedelta(**args)
                self.ndays = (
                    self.days(DATE_20CENTURY)
                    if years == 0 and months == 0 and leapdays == 0
                    else 0
                )

        elif isinstance(enum_val, int) and enum_val >= 0:
            self.enum_val = enum_val
            self.delta = {
                0: relativedelta(days=1),
                1: relativedelta(weeks=1),
                2: relativedelta(months=1),
                3: relativedelta(years=1),
            }[enum_val]
            self.ndays = 1 if self.enum_val == Timestep.daily else 0

        else:
            self.enum_val = enum_val
            self.ndays = 0
            self.delta = relativedelta(
                years=years,
                months=months,
                days=days,
                leapdays=leapdays,
                weeks=weeks,
                hours=hours,
                minutes=minutes,
                seconds=seconds,
                microseconds=microseconds,
            )

    def __cmp__(self, other):
        ts = Timestep(other)
        return int(self.enum_val).__cmp__(ts.enum_val)

    def is_finer_than(self, timestep):
        timestep = Timestep(timestep)
        return self.days(DATE_EPOCH) <= timestep.days(DATE_EPOCH)

    def increment(self, the_date):
        return the_date + self.delta

    def days(self, the_date):
        return date2num(the_date + self.delta) - date2num(the_date)

    def is_preset(self):
        return self.enum_val in self.preset_inv

    def preset_string(self):
        return self.preset_inv[self.enum_val] if self.is_preset() else None

    def __repr__(self):
        if self.is_preset():
            return self.preset_string()

        else:
            d = {
                "years": self.delta.years,
                "months": self.delta.months,
                "days": self.delta.days,
                "leapdays": self.delta.leapdays,
                "hours": self.delta.hours,
                "minutes": self.delta.minutes,
                "seconds": self.delta.seconds,
                "microseconds": self.delta.microseconds,
            }
            return str(d)


def aggregate_in_bins(
    input_dates, input_data, output_dates, fxn="sum", num_allowed_nans=0, fill_fxn="nan"
):

    # instantiate variables
    output_len = len(output_dates)

    if input_data is None:
        raise ValueError("Input data cannot be NoneType!")

    if input_data.ndim > 1:

        if not isinstance(fxn, (list, tuple)):
            fxn = [fxn] * input_data.shape[1]

        if len(fxn) != input_data.shape[1]:
            raise ValueError(
                'Parameter "fxn" must have the same number of elements as the columns of data'
            )

        out_data = np.zeros((output_len, input_data.shape[1]))
        nan_i = set()
        for i in range(input_data.shape[1]):
            out_data[:, i], nan_i_curr = aggregate_in_bins(
                input_dates,
                input_data[:, i],
                output_dates,
                fxn=fxn[i],
                num_allowed_nans=num_allowed_nans,
                fill_fxn=fill_fxn,
            )
            nan_i.update({i for i in nan_i_curr})
        nan_i = np.array(sorted(nan_i), dtype=np.int64)

    else:

        # checks
        out_data = np.ones(output_len) * np.nan
        if isinstance(fxn, (list, tuple)):
            if len(fxn) != 1:
                raise ValueError(
                    'Parameter "fxn" must only have one element when aggregating 1D data!'
                )
            fxn = fxn[0]
        if len(input_data) == 0:
            return out_data, np.arange(output_len)

        # instantiate variables
        bins = np.digitize(input_dates, output_dates, right=False) - 1
        cnt = np.bincount(bins, minlength=output_len)
        i = slice(None)

        # allow nans in input dataset up to a certain number
        if num_allowed_nans > 0:
            nonnans = ~np.isnan(input_data)
            bins = bins[nonnans]
            input_data = input_data[nonnans]
            cnt_wo_nans = np.bincount(bins, minlength=len(cnt))
            i = cnt - cnt_wo_nans <= num_allowed_nans
            cnt = cnt_wo_nans[i]

        # get weighted sums
        col_data = np.bincount(bins, weights=input_data, minlength=output_len)[i]
        nonzero = cnt > 0
        f = matching_aggregation(fxn)
        f.data_count = cnt[nonzero]  # for average
        f.saved_data = col_data[nonzero]  # for median and percentile
        out_data[i][nonzero] = f.temporal_reduce(f.saved_data)

        nans = np.isnan(out_data)
        nan_i = np.where(nans)[0]

        # fill missing data if possible
        if nans.any():
            if fill_fxn == "step":
                # fill with the previous available data point

                # get locations of values just before nans
                nonnans = ~nans
                fill_val_i = np.where(np.logical_and(nonnans[:-1], nans[1:]))[0]

                # get fill values
                fill_vals = out_data[fill_val_i]

                # get fill value for each nan location
                bin_i = np.digitize(nan_i, fill_val_i, right=False) - 1
                if len(bin_i):
                    middle_vals_i = np.logical_and(bin_i != -1, bin_i != bin_i[-1])
                else:
                    middle_vals_i = bin_i != -1
                bin_i = bin_i[middle_vals_i]
                fill_vals = fill_vals[bin_i]

                # set nan locations with fill values
                out_data[nan_i[middle_vals_i]] = fill_vals

            elif fill_fxn.startswith("val_"):

                fill_val = float(fill_fxn[4:])
                out_data[nan_i] = fill_val

            elif fill_fxn != "nan":
                raise ValueError(
                    'fill_fxn can only be "step", or "nan"! Got {0}'.format(fill_fxn)
                )

    return out_data, nan_i


def truncate_date(d, timestep):
    ts = Timestep(timestep)
    if ts == Timestep.daily:
        return datetime(d.year, d.month, d.day)
    elif ts == Timestep.monthly:
        return datetime(d.year, d.month, 1)
    elif ts == Timestep.annual:
        return datetime(d.year, 1, 1)
    else:
        raise ValueError('Unrecognized timestep "{0}"!'.format(ts))


def date_compare(d1, d2, timestep):
    d1 = truncate_date(d1, timestep)
    d2 = truncate_date(d2, timestep)
    return d1 == d2


def dates2nums(dates):
    d2n = np.vectorize(date2num)
    return d2n(dates)


def date2num(date):
    return (date - DATE_20CENTURY).total_seconds() / 86400.0


def num2date(num_in_days):
    return DATE_20CENTURY + timedelta(days=num_in_days)


def nums2dates(nums_in_days):
    n2d = np.vectorize(num2date)
    return n2d(nums_in_days)


def struct2date(struct):
    return datetime(*struct[:6])


def structs2dates(structs):
    return [datetime(*t[:6]) for t in structs]


def date2struct(date):
    return date.timetuple()


def dates2structs(dates):
    return [d.timetuple() for d in dates]


def get_month(date):
    return date.month


def get_months(dates):
    gm = np.vectorize(get_month)
    return gm(dates)


def get_month_index(date, wateryearformat):
    if wateryearformat:
        return (date.month + 2) % 10
    else:
        return date.month - 1


def get_month_indices(dates, water_year_format):
    gm = np.vectorize(get_month_index)
    return gm(dates, water_year_format)


def fill_dates(start_date, end_date, timestep, date_objects=False):
    out_dates = []
    curr_date = start_date
    while curr_date < end_date:
        out_dates.append(curr_date)
        curr_date = timestep.increment(curr_date)
    if date_objects:
        return start_date, end_date, out_dates
    else:
        start_date = date2num(start_date)
        end_date = date2num(end_date)
        out_dates = dates2nums(out_dates)
        return start_date, end_date, out_dates


class TimeSeries:
    def __init__(self, dates=None, data=None, headers=None):
        if dates is None:
            dates = []
        self.dates = dates
        if data is None:
            data = []
        self.data = np.array(data, dtype=np.float64)
        self.headers = headers
        self._checks()
        self.nobs = None
        self.flags = None
        self.metadata = {}

    def copy(self):
        dates = self.dates[:] if isinstance(self.dates, list) else np.array(self.dates)
        data = self.data[:] if isinstance(self.data, list) else np.array(self.data)
        headers = [h for h in self.headers] if self.headers else None
        return TimeSeries(dates, data, headers)

    def _checks(self):
        assert len(self.dates) == len(self.data), "length of dates and data must equal"

    def size(self):
        return len(self.dates)

    def len(self):
        return len(self.dates)

    def __len__(self):
        return len(self.dates)

    def truncate(self, start_date, end_date):
        dates = []
        data = []
        for d, v in zip(self.dates, self.data):
            if start_date <= d <= end_date:
                dates.append(d)
                data.append(v)
        return self.__class__(dates, data, headers=self.headers)

    def get_start_date(self):
        return self.dates[0] if len(self.dates) > 0 else None

    def get_end_date(self):
        return self.dates[-1] if len(self.dates) > 0 else None

    def get_time_structs(self):
        return [d.timetuple() for d in self.dates]

    def get_dates(self):
        return self.dates

    def get_year_day(self):
        return [(t.tm_year, t.tm_yday) for t in self.get_time_structs()]

    def get_days_since_1900(self):
        return dates2nums(self.dates)

    def get_timestamps(self):
        return np.int64([time.mktime(d.timetuple()) for d in self.dates])

    def get_year_month_day(self):
        return np.int32([d.timetuple()[:3] for d in self.dates])

    def set_dates_from_time_structs(self, times):
        self.dates = structs2dates(times)

    def set_dates_from_time_stamps(self, times):
        self.dates = nums2dates(times)

    def fill_dates(self, timestep, start_date=None, end_date=None, date_objects=False):

        timestep = Timestep(timestep)

        # use starting and ending dates for the timeseries
        if start_date is None:
            start_date = self.get_start_date()
        if end_date is None:
            end_date = self.get_end_date() + timedelta(microseconds=1)

        # get dates as floating-point numbers
        start_date, end_date, out_dates = fill_dates(
            start_date, end_date, timestep, date_objects=date_objects
        )
        return timestep, start_date, end_date, out_dates

    def aggregate(
        self,
        to_timestep,
        start_date=None,
        end_date=None,
        fxn="sum",
        num_allowed_nans=0,
        fill_fxn="nan",
    ):

        # the timestep we're summing to
        to_timestep = Timestep(to_timestep)

        # dates and timestep variables
        timestep, start_date, end_date, out_dates = self.fill_dates(
            to_timestep, start_date, end_date
        )

        # truncate to time period
        self.dates = dates2nums(self.dates)
        time_i = np.logical_and(start_date <= self.dates, self.dates < end_date)
        self.dates = self.dates[time_i]

        if not isinstance(self.data, np.ndarray):
            self.data = np.array(self.data)
        self.data = self.data[time_i]

        # sum the output data
        input_dates = self.dates
        out_data, nan_i = aggregate_in_bins(
            input_dates,
            self.data,
            out_dates,
            fxn=fxn,
            num_allowed_nans=num_allowed_nans,
            fill_fxn=fill_fxn,
        )

        # set output dates and nobs
        self.dates = nums2dates(out_dates)
        self.data = out_data

        if self.nobs is not None:
            if not isinstance(self.nobs, np.ndarray):
                self.nobs = np.array(self.nobs)
            self.nobs = self.nobs[time_i]
            out_nobs, _ = aggregate_in_bins(
                input_dates,
                self.nobs,
                out_dates,
                fxn="sum",
                num_allowed_nans=num_allowed_nans,
                fill_fxn="val_0.0",
            )
            self.nobs = np.array(out_nobs, dtype=np.int32)

        if self.flags is not None:
            if not isinstance(self.flags, np.ndarray):
                self.flags = np.array(self.flags)
            self.flags = list(self.flags[time_i])
            for row_i in nan_i:
                self.flags.insert(row_i, "None")

    def indices_at_matching_dates(self, other):
        # returns (indices for this, indices for other)

        # get dates as numbers
        t = dates2nums(self.dates)
        ts = date2num(self.get_start_date())
        te = date2num(self.get_end_date())

        o = dates2nums(other.dates)
        os = date2num(other.get_start_date())
        oe = date2num(other.get_end_date())

        (ti,) = np.where(np.logical_and(os <= t, t <= oe))
        (oi,) = np.where(np.logical_and(ts <= o, o <= te))

        assert len(ti) == len(
            oi
        ), "Lengths of overlapping data do not match (data not sorted? different timesteps?)!"

        return ti, oi

    def data_at_matching_dates(self, other):
        # returns (data for this, data for other)

        ti, oi = self.indices_at_matching_dates(other)
        return self.data[ti], other.data[oi]


def date_from_values(values):
    values = list(int(v) for v in values)
    return datetime(*(values + [1] * (3 - len(values))))


def try_num(v):
    try:
        return int(v)
    except ValueError:
        pass

    try:
        return float(v)
    except ValueError:
        pass

    return v


class CategorizedTimeseries:
    """
    Categorizes timeseries into bins or categories

    Attributes:
        date_cols       list of columns by which to sort and return (e.g., ['year', 'month']
        sort_by         list of columns by which to sort in addition to date_cols (e.g., ['geoid'])
        timestep        timestep of output timeseries
        categories      list of columns by which to categorize (e.g., ['geoid', 'category']
        variables       list of variables that to be extracted from the data
    """

    categories = None
    variables = None
    date_cols = None
    sort_by = []
    timestep = None

    def __init__(
        self,
        categories=None,
        variables=None,
        date_cols=None,
        sort_by=None,
        timestep=None,
        grouper_fxn=None,
        timeseries_fxn="sum",
        start_date=None,
        end_date=None,
    ):

        if categories is not None:
            self.categories = categories
        if variables is not None:
            self.variables = variables

        if date_cols is not None:
            self.date_cols = date_cols
        if sort_by is not None:
            self.sort_by = sort_by
            for s in self.sort_by:
                assert (
                    s not in self.categories
                ), "Sort by columns should not be in categories!"

        if timestep is not None:
            self.timestep = timestep

        self._keys = []
        self.key_i = -1
        self.data = {}
        self.aggregate = grouper_fxn is not None
        self.grouper_fxn = grouper_fxn
        if not isinstance(timeseries_fxn, (list, tuple)):
            timeseries_fxn = [timeseries_fxn] * len(self.variables)
        self.timeseries_fxn = timeseries_fxn

        self.start_date = start_date
        self.end_date = end_date

    def add(self, key, child_data):
        if key in self.data:
            raise ValueError("Data contains key {0} already!".format(key))
        self._keys.append(key)
        self.data[key] = child_data

    def keys(self):
        return self._keys

    def __getitem__(self, key):
        if isinstance(key, str):
            return self.data[key]
        else:
            d = self.data
            for k in key:
                d = d[k]
            return d

    def __iter__(self):
        return self

    def __contains__(self, key):
        if isinstance(key, str):
            return key in self.data
        else:
            d = self.data
            for k in key:
                if k not in d:
                    return False
                d = d[k]
            return True

    def next(self):
        self.key_i += 1
        if self.key_i >= len(self._keys):
            self.key_i = -1  # reset
            raise StopIteration
        else:
            return self._keys[self.key_i]

    def reduce(self, grouped_data):
        return self.grouper_fxn(grouped_data)

    def create_child(self, categories):
        return CategorizedTimeseries(
            categories=categories,
            variables=self.variables,
            date_cols=self.date_cols,
            timestep=self.timestep,
            sort_by=self.sort_by,
            grouper_fxn=self.grouper_fxn,
            timeseries_fxn=self.timeseries_fxn,
            start_date=self.start_date,
            end_date=self.end_date,
        )

    def date_from_row(self, row):
        date_values = [int(row[d]) for d in self.date_cols]
        return date_from_values(date_values)

    def read_data(self, data):
        # build data
        if self.categories:

            sorted_data = sorted(data, key=lambda r: r[self.categories[0]])
            for key, child_data in itertools.groupby(
                sorted_data, key=lambda r: r[self.categories[0]]
            ):
                child = self.create_child(self.categories[1:])
                child_data = list(child_data)
                child.read_data(child_data)
                if key in self._keys:
                    raise Exception(
                        "key {0} is somehow in the child keys {1}!".format(
                            key, ", ".join(self._keys)
                        )
                    )
                self.add(key, child)

        else:

            def loop_dict():
                dates = [
                    date_from_values(date_values)
                    for date_values in zip(*[data[dc] for dc in self.date_cols])
                ]
                for output_variable, fxn in zip(self.variables, self.timeseries_fxn):
                    values = data[output_variable]
                    assert isinstance(values, np.ndarray)
                    yield output_variable, dates, values, fxn

            def loop_list():

                sort_by = self.date_cols + [
                    c for c in self.sort_by if c not in self.date_cols
                ]
                ts_sorted = sorted(
                    data, key=lambda cr: tuple(try_num(cr[sc]) for sc in sort_by)
                )
                all_dates = [self.date_from_row(row) for row in ts_sorted]
                dates = sorted(set(all_dates))

                for output_variable, fxn in zip(self.variables, self.timeseries_fxn):

                    if self.aggregate:
                        values = np.zeros(len(dates))
                    else:
                        values = None

                    grouper = itertools.groupby(
                        ts_sorted, key=itemgetter(*self.date_cols)
                    )
                    for i, (date, grouped_data) in enumerate(grouper):

                        val_list = list(grouped_data)
                        if values is None:
                            values = np.zeros((len(dates), len(val_list)))

                        if len(val_list) == 0:
                            raise AssertionError(
                                "Should zero-length within timeseries!"
                            )
                        elif len(val_list) == 1:
                            values[i] = val_list[0][output_variable]
                        elif self.aggregate:
                            values[i] = self.reduce(
                                np.float64([row[output_variable] for row in val_list])
                            )
                        else:
                            values[i, :] = np.float64(
                                [row[output_variable] for row in val_list]
                            )

                    if values.ndim == 2 and values.shape[1] == 1:
                        values = values.flatten()

                    yield output_variable, dates, values, fxn

            gen = loop_dict() if isinstance(data, dict) else loop_list()
            for var, d, v, f in gen:
                ts = TimeSeries(d, v, headers=[var])

                if self.start_date and self.end_date:
                    ts = ts.truncate(self.start_date, self.end_date)

                if ts.dates:
                    ts.aggregate(self.timestep, fxn=f)
                    self.add(var, ts)

        return self

    def read_file(self, csv_file):

        # read csv file
        data = []
        with open(csv_file, "r") as f:
            cr = csv.DictReader(f)

            # if no output variables specified, return all
            if self.variables is None:
                self.variables = [
                    h
                    for h in cr.fieldnames
                    if h not in self.categories + self.date_cols + self.sort_by
                ]

            # required columns
            all_cols = self.categories + self.date_cols + self.variables

            # check for categories and output variables in file
            for h in all_cols:
                if h not in cr.fieldnames:
                    msg = "Header {0} not found in csv file {1}! Headers found: {2}"
                    raise ValueError(msg.format(h, csv_file, ", ".join(cr.fieldnames)))

            # fill list with data
            data = [row for row in cr]
        return self.read_data(data)

    @classmethod
    def from_file(
        cls,
        csv_file,
        categories=None,
        variables=None,
        date_cols=None,
        sort_by=None,
        timestep=None,
        grouper_fxn=np.sum,
        timeseries_fxn="sum",
        start_date=None,
        end_date=None,
    ):

        self = cls(
            categories=categories,
            variables=variables,
            date_cols=date_cols,
            sort_by=sort_by,
            timestep=timestep,
            grouper_fxn=grouper_fxn,
            timeseries_fxn=timeseries_fxn,
            start_date=start_date,
            end_date=end_date,
        )
        return self.read_file(csv_file)


class BaseDataAgg:
    """
    Aggregates outputs that should be averaged both spatially and temporally

    Attributes:
        name            Name of the function; if not provided, this will be lowercase prefix before "DataAgg" in the
                        class name
        regex           Regular expression to match the entire function string, this is wrapped with `^` and `$`
        default         Default value that starts the data aggregation
    """

    name = ""
    regex = None
    default = 0.0
    save = False

    def __init__(self, variables=None, var_i=None, var_matches=None):

        self.variables = variables
        self.var_i = var_i
        self.var_matches = var_matches

        if not self.regex.startswith(r"^"):
            self.regex = r"^" + self.regex

        if not self.regex.endswith(r"$"):
            self.regex = self.regex + r"$"

        self.re = re.compile(self.regex, re.I)

        if not self.name:
            self.name = self.__class__.__name__.lower().replace("dataagg", "")

        self.data_count = 0
        self.saved_data = []

    def copy(self):
        return self.__class__(
            variables=self.variables, var_i=self.var_i, var_matches=self.var_matches
        )

    def _check_lengths(self):
        if self.variables is not None:
            if self.var_i is None:
                raise ValueError(
                    "variables and var_i need to be assigned at the same time!"
                )
            if len(self.variables) != len(self.var_i):
                raise ValueError(
                    "Lengths of variables and var_i lists must be the same!"
                )
            if self.var_matches and len(self.variables) != len(self.var_matches):
                raise ValueError(
                    "Lengths of variables and var_matches lists must be the same!"
                )

    def fxn_match(self, fxn):
        if fxn:
            m = self.re.search(fxn)
            if m:
                return m
        return None

    def match(self, var_name):
        var, fxn = utils.get_fxn(var_name)
        return (
            self.fxn_match(fxn)
            or self.fxn_match(inputs.get_aggregation(var_name))
            or None
        )

    def matches(self, var_name):
        return self.match(var_name) is not None

    def match_variables(self, variables):
        """
        Gets indices of variables that match this function

        :param variables: list of variables that match this function
        :return: returns indices of variables that match this function
        """
        vs = []
        vi = []
        vm = []
        for i, v in enumerate(variables):
            m = self.match(v)
            if m:
                vs.append(v)
                vi.append(i)
                vm.append(m)
        return self.__class__(variables=vs, var_i=vi, var_matches=vm)

    def spatial_reduce(self, v):
        return v

    def advance(self, prev_v, v):
        if self.save:
            self.saved_data.append(v)
        self.data_count += 1
        return v

    def temporal_reduce(self, v):
        if self.save:
            self.saved_data = []
        self.data_count = 0
        return v

    def get_default(self, var=None):
        return self.default


class SpatialAverageDataAgg(BaseDataAgg):
    """
    Aggregates outputs to average across space summed temporally
    """

    regex = "(average|mean)_spatial"
    name = "average_spatial"

    def __init__(self, **kwargs):
        BaseDataAgg.__init__(self, **kwargs)

    def spatial_reduce(self, v):
        n = len(v)
        if not isinstance(v, np.ndarray) or v.ndim == 1:
            s = np.sum(v)
        elif v.ndim == 2:
            s = np.sum(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )
        return s / n


class TemporalAverageDataAgg(BaseDataAgg):
    """
    Aggregates outputs to average daily summed across space
    """

    regex = "(average|mean)_temporal"
    name = "average_temporal"

    def __init__(self, **kwargs):
        BaseDataAgg.__init__(self, **kwargs)

    def advance(self, prev_v, v):
        BaseDataAgg.advance(self, prev_v, v)
        return prev_v + v

    def temporal_reduce(self, v):
        avg = v / self.data_count
        return BaseDataAgg.temporal_reduce(self, avg)


class AverageDataAgg(BaseDataAgg):
    """
    Aggregates outputs to average daily
    """

    regex = "(average|mean)"

    def __init__(self, **kwargs):
        BaseDataAgg.__init__(self, **kwargs)

    def spatial_reduce(self, v):
        n = len(v)
        if not isinstance(v, np.ndarray) or v.ndim == 1:
            s = np.sum(v)
        elif v.ndim == 2:
            s = np.sum(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )
        return s / n

    def advance(self, prev_v, v):
        BaseDataAgg.advance(self, prev_v, v)
        return prev_v + v

    def temporal_reduce(self, v):
        avg = v / self.data_count
        return BaseDataAgg.temporal_reduce(self, avg)


class MinDataAgg(BaseDataAgg):

    regex = "min"
    default = float("inf")

    def spatial_reduce(self, v):
        if v.ndim == 1:
            return np.min(v)
        elif v.ndim == 2:
            return np.min(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )

    def advance(self, prev_v, v):
        BaseDataAgg.advance(self, prev_v, v)
        return np.minimum(prev_v, v)


class MaxDataAgg(BaseDataAgg):

    regex = "max"
    default = float("-inf")

    def spatial_reduce(self, v):
        if v.ndim == 1:
            return np.max(v)
        elif v.ndim == 2:
            return np.max(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )

    def advance(self, prev_v, v):
        BaseDataAgg.advance(self, prev_v, v)
        return np.maximum(prev_v, v)


class MedianDataAgg(BaseDataAgg):
    """
    Aggregates output to median daily
    """

    regex = "median"
    save = True

    def __init__(self, **kwargs):
        BaseDataAgg.__init__(self, **kwargs)

    def spatial_reduce(self, v):
        if v.ndim == 1:
            return np.median(v)
        elif v.ndim == 2:
            return np.median(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )

    def temporal_reduce(self, v):
        d = self.spatial_reduce(np.array(self.saved_data))
        return BaseDataAgg.temporal_reduce(self, d)


class PercentileDataAgg(BaseDataAgg):

    regex = r"percentile(\s*\(\s*(.*?)\s*\))?"
    save = True

    def __init__(self, **kwargs):
        BaseDataAgg.__init__(self, **kwargs)
        if self.var_matches:

            def get_percentile(match):
                if match is None or match.group(1) is None or match.group(2) is None:
                    raise ValueError(
                        'Must provide a percentile in parentheses after "percentile"!'
                    )
                try:
                    v = float(match.group(2))
                    if v < 0 or 100 < v:
                        raise ValueError()
                except ValueError:
                    raise ValueError(
                        'Must provide a real number [0.0, 100.0] for "percentile(<number>)" aggregation!'
                    )

            self.percentiles = np.array([get_percentile(m) for m in self.var_matches])

            if len(self.percentiles) != self.var_matches:
                raise AssertionError(
                    "Percentiles need to be the same length as variables!"
                )

    def spatial_reduce(self, v):
        if v.ndim == 1:
            return np.percentile(v, self.percentiles[0])
        elif v.ndim == 2:
            return np.array(
                [[np.percentile(v[:, i], p) for i, p in enumerate(self.percentiles)]]
            )
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )

    def temporal_reduce(self, v):
        d = self.spatial_reduce(np.array(self.saved_data))
        return BaseDataAgg.temporal_reduce(self, d)


class SumDataAgg(BaseDataAgg):

    regex = "sum"

    def spatial_reduce(self, v):
        if v.ndim == 1:
            return np.sum(v)
        elif v.ndim == 2:
            return np.sum(v, axis=0, keepdims=True)
        else:
            raise NotImplementedError(
                "Data for aggregation must be an array with 1 or 2 dimensions!"
            )

    def advance(self, prev_v, v):
        BaseDataAgg.advance(self, prev_v, v)
        return prev_v + v


def get_aggregations():
    return {
        di.name: di
        for di in [
            da()
            for da in [
                MedianDataAgg,
                SpatialAverageDataAgg,
                TemporalAverageDataAgg,
                AverageDataAgg,
                MinDataAgg,
                MaxDataAgg,
                SumDataAgg,
            ]
        ]
    }


AGGREGATIONS = get_aggregations()


def matching_aggregation(fxn):

    if issubclass(fxn.__class__, BaseDataAgg):
        return fxn

    for agg in AGGREGATIONS.values():
        m = agg.fxn_match(fxn)
        if m:
            return agg.copy()

    raise ValueError('No aggregation matches function "{0}"'.format(fxn))


class DataAggs:
    def __init__(self, variables):

        # save instance variables
        self.variables = variables
        self.n_vars = len(self.variables)
        self.data = None

        # add the rest as those variables that should be summed
        self.aggregations = {
            n: d.match_variables(variables)
            for n, d in AGGREGATIONS.items()
            if n != "sum"
        }

        # remove those that are not used
        self.aggregations = {n: d for n, d in self.aggregations.items() if d.variables}

        # add any remaining to sum
        other_vars = set(v for agg in self.aggregations.values() for v in agg.variables)
        sum_var_i, sum_vars = zip(
            *[(i, v) for i, v in enumerate(variables) if v not in other_vars]
        )
        self.aggregations["sum"] = SumDataAgg(variables=sum_vars, var_i=sum_var_i)

        # check length of aggregations to make sure there is no overlap in aggregations for a variable
        # should have one aggregation per variable (sum is default)
        all_var_i = [i for agg in self.aggregations.values() for i in agg.var_i]
        if len(all_var_i) != len(variables) or len(set(all_var_i)) != len(all_var_i):
            raise AssertionError(
                "Internal Error: Aggregations are not as long as output variable list!"
            )

    def spatial_reduce(self, data):
        d = np.zeros((1, self.n_vars))
        for agg in self.aggregations.values():
            i = agg.var_i
            d[:, i] = agg.spatial_reduce(data[:, i])
        return d

    def advance(self, data):
        d = np.full(data.shape, 0.0)
        for agg in self.aggregations.values():
            i = agg.var_i
            if self.data is None:
                v = np.full(data[:, i].shape, agg.default)
            else:
                v = self.data[:, i]
            d[:, i] = agg.advance(data[:, i], v)
        self.data = d
        return d

    def temporal_reduce(self):
        d = np.copy(self.data)
        for agg in self.aggregations.values():
            i = agg.var_i
            d[:, i] = agg.temporal_reduce(d[:, i])
        self.data = None
        return d
