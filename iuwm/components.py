# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import inspect
import numpy as np

WARNED = False


class IUWMObject:

    def __init__(self, name, **kwargs):
        self.name = name
        if kwargs:
            self.__dict__.update(kwargs)

    def __contains__(self, param_name):
        return param_name in self.__dict__

    def update(self, o):
        return self.__dict__.update(o)

    def __getitem__(self, param_name):
        return getattr(self, param_name)

    def __setitem__(self, param_name, item):
        self.__dict__[param_name] = item

    def items(self):
        return self.__dict__.items()

    def copy(self):
        return IUWMObject(**self.__dict__.copy())

    def get(self, param_name):

        param_names = param_name.split('.')
        obj_names = param_names[:-1]
        final_param_name = param_names[-1]
        obj = self

        try:
            for param in obj_names:
                obj = obj[param]
                if not isinstance(obj, IUWMObject):
                    if not hasattr(obj, '__getitem__'):
                        msg = 'Object {0} in parameter {1} is of type {2}, not IUWMObject!'
                        raise ValueError(msg.format(param, param_name, type(obj)))

            # get the parameter value and return it
            if isinstance(obj, np.ndarray):
                global WARNED
                if not WARNED:
                    print('WARNING: Data {0} does not include "{1}".'.format(
                        param_name.replace('.' + final_param_name, ''),
                        final_param_name,
                    ))
                    WARNED = True
                return obj

            v = obj[final_param_name]
            if hasattr(v, '__call__'):
                if inspect.getargspec(v).args == ['self']:
                    v = v()
                else:
                    raise ValueError('Cannot access a function that requires arguments other than "self"!')
            return v

        except Exception as e:

            # parameter does not exist, or is not an np.ndarray!
            # print(type(obj))
            # print(param_name)
            import traceback
            tback = traceback.format_exc()

            if hasattr(obj, 'keys'):
                possible_params = obj.keys()
                if final_param_name not in possible_params:
                    pp = '\n\n{1}\n\nPossible parameters include:\n  {0}'.format('\n  '.join(possible_params), tback)
                    if hasattr(obj, final_param_name):
                        msg = 'Output parameter "{0}" exists in object "{1}", but is of the wrong type "{2}"!'
                        msg = msg.format(final_param_name, obj.name, type(getattr(obj, final_param_name)))
                        raise ValueError(msg + pp)
                    else:
                        msg = 'Output parameter "{0}" does not exist in object "{1}"!'
                        raise ValueError(msg.format(param_name, self.__class__.__name__) + pp)

            raise ValueError('Could not find parameter "{0}":\n\n{1}\n\n{2}'.format(param_name, tback, e))

    def keys(self, prefix=''):
        params = []
        for k, v in self.items():
            if isinstance(v, np.ndarray):
                params.append(prefix + k)
            elif issubclass(v.__class__, IUWMObject):
                params.extend(v.keys(prefix=prefix + k + '.'))
            elif isinstance(v, dict):
                params.extend(prefix + k + '.' + ki for ki in v.keys())
            elif hasattr(v, '__call__') and inspect.getargspec(v).args == ['self']:
                params.extend(prefix + k)
        params.sort()
        return params
