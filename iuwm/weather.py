import csv
from collections import OrderedDict
from datetime import datetime
import fnmatch
import gc
import itertools
import json
from multiprocessing import Process, Pipe, freeze_support
import numpy as np
import os
import re
import requests
import sys
from tseries import TimeSeries, Timestep

# directory locations (remain constant)
HOST = r"http://csip.engr.colostate.edu:8083"
CSIP_SOURCE_NAMES = ["NARR", "PRISM", "MACA", "CoAgMet", "NCWCD"]  # , 'GHCND']
SOURCE_NAMES = [source_name.lower() for source_name in CSIP_SOURCE_NAMES]
SOURCE_GRIDDED = [True, True, True, False, False, False]
SITE_COLUMNS = ["Source", "StationID", "Name", "Latitude", "Longitude"]
MODEL_NUMBERS = [str(m_num) for m_num in range(20)]
MODEL_NAMES = [
    "bcc-csm1-1",
    "bcc-csm1-1-m",
    "BNU-ESM",
    "CanESM2",
    "CCSM4",
    "CNRM-CM5",
    "CSIRO-Mk3-6-0",
    "GFDL-ESM2G",
    "GFDL-ESM2M",
    "HadGEM2-CC365",
    "HadGEM2-ES365",
    "inmcm4",
    "IPSL-CM5A-LR",
    "IPSL-CM5A-MR",
    "IPSL-CM5B-LR",
    "MIROC5",
    "MIROC-ESM",
    "MIROC-ESM-CHEM",
    "MRI-CGCM3",
    "NorESM1-M",
]
MODEL_RCPS = ["rcp45", "rcp85"]
MODEL_VARIABLES = ["tmin", "tmax", "pp", "sr", "rhave", "windspeed_avg"]
MODEL_VARIABLE_AGGREGATION = ["mean", "mean", "sum", "mean", "mean", "mean"]
VARIABLE_MAPPING = {
    "temp_min": "temp_min",
    "tmpmin": "temp_min",
    "tmin": "temp_min",
    "temp_max": "temp_max",
    "tmpmax": "temp_max",
    "tmax": "temp_max",
    "temp_dew_avg": "temp_dew_avg",
    "tdmean": "temp_dew_avg",
    "precipitation": "precipitation",
    "apcp": "precipitation",
    "pp": "precipitation",
    "ppt": "precipitation",
    "solar_rad": "solar_rad",
    "dswrf": "solar_rad",
    "sr": "solar_rad",
    "rel_hum_avg": "rel_hum_avg",
    "rel_hum": "rel_hum_avg",
    "rh": "rel_hum_avg",
    "rhave": "rel_hum_avg",
    "rel_hum_min": "rel_hum_min",
    "rhsmin": "rel_hum_min",
    "rel_hum_max": "rel_hum_max",
    "rhsmax": "rel_hum_max",
    "specific_humidity": "specific_humidity",
    "huss": "specific_humidity",
    "sh": "specific_humidity",
    "wind_speed": "wind_speed",
    "windspeed": "wind_speed",
    "windspeed_avg": "wind_speed",
    "windgust": "wind_speed",
}

CACHE_DAYS_TO_REPLACE = 1
try:
    THIS_DIR = os.path.abspath(os.path.dirname(__file__))
except NameError:  # we are the main py2exe script, not a module
    THIS_DIR = os.path.abspath(os.path.dirname(sys.argv[0]))

if "library.zip" == os.path.basename(THIS_DIR):
    THIS_DIR = os.path.dirname(THIS_DIR)

WEATHER_CACHE = os.path.expanduser(os.path.join("~", ".iuwm_cache", "weather"))
UNITS_REGEX = re.compile(r"\((.*?)\)")
SPLIT_STRING = ","
CHUNK_SIZE = 20

VERSIONS = {
    "maca": "2.0",
}


def iter_files(the_dir, the_pattern):
    for root, _, file_names in os.walk(the_dir):
        for filename in fnmatch.filter(file_names, the_pattern):
            yield os.path.join(root, filename)


def map_variable(var_name):
    v_name = var_name if " (" not in var_name else var_name[: var_name.find(" (")]
    v_name = v_name.strip()
    # Check for extra variables
    return VARIABLE_MAPPING[v_name] if v_name in VARIABLE_MAPPING else var_name


def header_variables(headers):
    return [map_variable(h) for h in headers]


def get_units(var_name):
    s = UNITS_REGEX.search(var_name)
    if s is not None:
        return s.group(1)
    else:
        return ""


def header_units(headers):
    return [get_units(h) for h in headers]


def variable_column(var_name, headers):
    return header_variables(headers).index(map_variable(var_name))


def get_maca_model_names(verbose=False):
    model_names = []
    for climate_model in MODEL_NUMBERS:
        climate_rcp = "rcp45"
        extra = to_point_features([{"longitude": -105.077, "latitude": 40.322}])
        extra["forecast_model"] = str(climate_model)
        extra["forecast_option"] = str(climate_rcp)
        resp = get_csip_data(
            "maca",
            "metric",
            "2010-1-01",
            "2010-1-02",
            extra_data=extra,
            return_raw=True,
        )
        if "result" not in resp:
            with open("saved_csip_response.json", "w") as f:
                f.write(json.dumps(resp))
        name = [
            result_item["value"]["model"]
            for result_item in resp["result"]
            if result_item["name"] == "results"
        ][0]
        if verbose:
            print("Model {0:>2s}: {1}".format(climate_model, name))
        model_names.append(name)

    return model_names


def get_header_key(source, units):
    header_key = units + "_" + source
    return header_key.lower()


def get_csip_param(params, param, default=None):
    for p in params:
        if p["name"] == param:
            return p["value"]
    return default


def headers_from_response(response):
    if "parameter" in response:
        return get_csip_param(response["parameter"], "climate_data")
    return None


def get_csip_headers(source, save=False):

    url = "{0}/csip-climate/m/{1}/{2}".format(
        HOST, source, VERSIONS[source] if source in VERSIONS else "1.0"
    )
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    resp = requests.get(url, headers=headers)

    def save_response(path="saved_csip_response.json"):
        with open(path, "w") as f:
            f.write(resp.text)

    if save:
        save_response()

    try:
        resp = json.loads(resp.text)
        headers = headers_from_response(resp)
        return headers
    except Exception as e:
        save_response()
        raise e


def get_csip_data(
    source,
    units,
    start_date_str,
    end_date_str,
    extra_data=None,
    save=False,
    return_raw=False,
):
    """
    Download data from CSIP at HOST.

    source          : 'maca', 'narr', 'prism', 'coagmet', 'ncwcd', OR 'ghcnd'.
    units           : 'metric' or 'english'
    start_date_str  : start date in %Y-%m-%d format
    end_date_str    : end date in %Y-%m-%d format
    extra_data      : extra data is a dictionary whose keys are appended as new parameters, values as parameter values
    save            : save the exact CSIP output JSON to working directory as 'saved_csip_response.json'?

    returns data    : on success, list that contains rows of data. if not successful, dictionary or string containing
                      direct JSON output
    """

    source = source.lower()
    url = "{0}/csip-climate/m/{1}/{2}".format(
        HOST, source, VERSIONS[source] if source in VERSIONS else "1.0"
    )
    data = {
        "parameter": [
            {
                "name": "climate_type",
                "value": CSIP_SOURCE_NAMES[SOURCE_NAMES.index(source)],
            },
            {
                "name": "units",
                "value": units,
            },
            {
                "name": "start_date",
                "value": start_date_str,
            },
            {
                "name": "end_date",
                "value": end_date_str,
            },
        ]
    }

    if source == "coagmet":
        data["parameter"].append({"name": "time", "value": "daily"})

    if not is_gridded(source):
        data["parameter"].append({"name": "climate_data", "value": MODEL_VARIABLES})

    if extra_data is not None:
        for key, value in extra_data.items():
            data["parameter"].append({"name": key, "value": value})

    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    req = requests.post(url, data=json.dumps(data), headers=headers)

    def save_response(path="saved_csip_response.json"):
        with open(path, "w") as f:
            f.write(req.text)

    if save:
        save_response()

    try:
        resp = json.loads(req.text)
    except Exception as e:
        save_response()
        raise e

    if return_raw:
        return resp

    def get_feature_data(feature):
        name = "name" if "name" in feature else "id"
        hdr = feature["data"][0]
        feature_data = [["id"] + hdr]
        feature_data.extend([[str(feature[name])] + r for r in feature["data"][1:]])
        return feature_data

    # collect data
    data = None
    if "result" in resp:
        features = get_csip_param(resp["result"], "output")
        if features:
            # include headers from first feature
            data = get_feature_data(features.pop(0))
            for feature in features:
                # skip headers for all features after the first
                data.extend(get_feature_data(feature)[1:])

    if data is None:
        save_response()

    return data


def to_station_features(sites):
    return {"station_list": [(s["id"], s["name"]) for s in sites]}


def to_point_features(sites):
    # Build feature collection

    features = []
    names = set()

    # sites are a list of coordinates
    for i in range(len(sites)):
        site = sites[i]
        x = float(site["longitude"])
        y = float(site["latitude"])
        name = "{0}{1}{2}".format(x, SPLIT_STRING, y)
        if name not in names:
            features.append(
                {
                    "type": "Feature",
                    "properties": {"name": name, "gid": i + 1},
                    "geometry": {
                        "type": "Point",
                        "coordinates": [x, y],
                        # Patterson: the service is not parsing the crs line. Since this is 4326,
                        #  it's optional anyway.
                        # "crs": {"type": "name", "properties": {"name": "EPSG:4326"}},
                    },
                }
            )
            names.add(name)

    input_zone_features = {"type": "FeatureCollection", "features": features}

    return {"input_zone_features": input_zone_features}


def save_sites_file(sites_file, sites, overwrite=False):
    assert not os.path.exists(sites_file) or overwrite, "Sites file already exists!"

    if not sites:
        return

    all_keys = next(s for s in sites).keys()
    special_keys = ["id", "name", "longitude", "latitude", "source"]
    extra_keys = [k for k in all_keys if k not in special_keys]
    with open(sites_file, "wb") as f:
        cw = csv.writer(f)
        cw.writerow(
            ["StationID", "Name", "Longitude", "Latitude", "Source"] + extra_keys
        )
        for row in sites:
            cw.writerow([row[k] for k in special_keys] + [row[k] for k in extra_keys])


def read_sites_file(sites_file, source=None, climate_model=None, climate_rcp=None):
    with open(sites_file, "r") as f:
        cr = csv.reader(f)

        # get headers
        headers = []
        for row in cr:
            headers = [s.lower() for s in row]
            break

        # check
        if "source" not in headers and source is None:
            raise Exception(
                'Must provide a source either as a field named "Source" in the sites file, '
                "or as a command line option --source!"
            )

        # columns of interest
        if source is not None:
            source = source.lower()
        source_col = headers.index("source") if "source" in headers else None
        sid_col = headers.index("stationid")
        name_col = headers.index("name")
        lon_col = headers.index("longitude")
        lat_col = headers.index("latitude")
        output_col = headers.index("outputname") if "outputname" in headers else None

        # get other data
        output = []
        for row in cr:
            curr_source = row[source_col].lower() if source_col is not None else source

            if curr_source.lower() == "ghcnd":
                station_id = row[sid_col]
            else:
                try:
                    station_id = int(row[sid_col])
                except ValueError:
                    station_id = row[sid_col]

            d = {
                "id": station_id,
                "name": row[name_col],
                "latitude": float(row[lat_col]),
                "longitude": float(row[lon_col]),
                "source": curr_source,
            }

            if output_col is not None:
                output_name = row[output_col]
                d["output"] = output_name

            output.append(d)

    return new_sites(
        output, source=source, climate_model=climate_model, climate_rcp=climate_rcp
    )


def new_site(site, source=None, climate_model=None, climate_rcp=None):
    if source == "maca":
        if climate_model is None or climate_rcp is None:
            raise Exception(
                'The "climate_model" and "climate_rcp" parameters must be provided when source is "maca".'
            )

        if isinstance(climate_model, int) or climate_model in MODEL_NUMBERS:
            climate_model = MODEL_NAMES[int(climate_model)]

        if climate_model not in MODEL_NAMES:
            msg = "Model {0} not found in possible models ({1})!"
            raise Exception(msg.format(climate_model, ", ".join(MODEL_NAMES)))

        if climate_rcp not in MODEL_RCPS:
            msg = "Option {0} not found in possible options ({1})!"
            raise Exception(msg.format(climate_rcp, ", ".join(MODEL_RCPS)))

    else:

        climate_model = None
        climate_rcp = None

    d = site.copy()
    if source is not None and is_gridded(source):
        d["source"] = source
    if climate_model is not None:
        d["model"] = climate_model
    if climate_rcp is not None:
        d["rcp"] = climate_rcp
    d["is_grid"] = is_gridded(d["source"])
    return d


def new_sites(sites, source=None, climate_model=None, climate_rcp=None):
    ns = []
    for site in sites:
        d = new_site(
            site, source=source, climate_model=climate_model, climate_rcp=climate_rcp
        )
        ns.append(d)
    return ns


def get_key_from_site(site, force_grid=False, force_station=False):
    if isinstance(site, str) or isinstance(site, unicode):
        return str(site).lower()
    elif (
        force_grid
        or ("is_grid" in site and site["is_grid"])
        or ("source" in site and is_gridded(site["source"]))
    ) and not force_station:
        return SPLIT_STRING.join(
            [str(s) for s in [site["longitude"], site["latitude"]]]
        ).lower()
    else:
        return SPLIT_STRING.join([str(s) for s in [site["id"], site["name"]]]).lower()


def cache_path(site, start_date, end_date):
    key = get_key_from_site(site)
    s = start_date.strftime("%Y-%m-%d")
    e = end_date.strftime("%Y-%m-%d")
    if "rcp" in site and "model" in site:
        loc_key = os.path.join(
            site["source"], site["model"], site["rcp"], s, e, key + ".npy"
        )
    else:
        loc_key = os.path.join(site["source"], s, e, key + ".npy")
    return os.path.join(WEATHER_CACHE, loc_key)


def cache_paths(sites, start_date, end_date):
    return [cache_path(site, start_date, end_date) for site in sites]


def analyze_cache(sites, start_date, end_date):
    paths = cache_paths(sites, start_date, end_date)
    existing = []
    non_existing = []
    for s, p in zip(sites, paths):
        if os.path.exists(p) and os.path.exists(p + ".hdr"):
            s["cache"] = p
            s["cache_exists"] = True
            existing.append(s)
        else:
            s["cache"] = p
            s["cache_exists"] = False
            non_existing.append(s)
    return existing, non_existing


def load_cache(sites, start_date, end_date, aggregate_timestep=None):
    existing, non_existing = analyze_cache(sites, start_date, end_date)
    data = {}
    for site in existing:
        key = get_key_from_site(site)
        d = np.load(site["cache"])
        dates = [datetime(int(di[2]), int(di[1]), int(di[0])) for di in d]
        ts = TimeSeries(dates, d[:, 4:])
        wo = WeatherOutput(
            site,
            start_date,
            end_date,
            use_cache=True,
            aggregate_timestep=aggregate_timestep,
            ts=ts,
        )
        data[key] = wo
    return data, non_existing


def is_gridded(source):
    if source is None or source.lower() == "snotel":
        return False
    source = source.lower()
    if source not in SOURCE_NAMES:
        raise Exception("Source '{0}' is not in source names!".format(source))
    source_i = SOURCE_NAMES.index(source)
    return SOURCE_GRIDDED[source_i]


def check_data(data):
    """
    Check the output data.
    Raise an Exception if failed

    :param data: The output of the function "get_csip_data"
    """

    if isinstance(data, dict):
        print("Dictionary: ")
        d = json.dumps(data, sort_keys=True, indent=4, separators=(",", ": "))
        with open("weather_error.json", "w") as f:
            f.write(d)
        print(d)
        raise Exception("Data could not be found!")

    if not isinstance(data, list):
        print("data")
        print(data)
        raise Exception("Data could not be found!")


def chunks(l, n):
    """
    Yield successive n-sized chunks from l.

    :param l: the original list
    :param n: the size of each chunk
    """
    for i in range(0, len(l), n):
        yield l[i : i + n]


def update_site_id(id_from_csip):
    if id_from_csip.count(",") == 1:
        lon, lat = id_from_csip.split(",")
        try:
            lat = float(lat)
            lon = float(lon)
            return SPLIT_STRING.join([str(f) for f in [lon, lat]])
        except ValueError:
            return id_from_csip
    return id_from_csip


def get_data(
    sites,
    start_date,
    end_date,
    climate_model=None,
    climate_rcp=None,
    merge_with_narr=False,
    use_cache=True,
    output_dir=None,
    verbose=False,
    chunk_size=CHUNK_SIZE,
    units="metric",
    aggregate_timestep=None,
):
    """
    Retrieves weather data from CSIP given locations or station IDs.

    Output Data Format:
      A dictionary with key as location string and value as 2D array with dimensions time (1), variable (2) as follows:

      PRISM:
            cell, date, tmpmin (deg. C), tmpmax (deg. C), precipitation (cm)
      NARR, MACA, and PRISM while merge_with_NARR == True:
            cell, date, tmpmin (deg. C), tmpmax (deg. C), precipitation (cm), Solar Radiation (W/m^2),
            Relative Humidity (%), Wind speed (m/s)
    """

    if not sites:
        raise Exception("There are no sites to retrieve data for!")

    # station id and locations
    s = start_date.strftime("%Y-%m-%d")
    e = end_date.strftime("%Y-%m-%d")

    # determine what source is
    source = sites[0]["source"].lower()
    is_grid = is_gridded(source)

    # source unable to download
    unrecognized = set(
        [
            site["source"].lower()
            for site in sites
            if site["source"].lower() not in SOURCE_NAMES
        ]
    )
    if unrecognized:
        sites = [site for site in sites if site["source"].lower() in SOURCE_NAMES]
        print("WARNING! DOWNLOADER NOT DEFINED FOR THE FOLLOWING SOURCES:")
        for src in unrecognized:
            print("     {0}".format(src))

    # update rcp scenario in sites
    if source == "maca":
        sites = new_sites(
            sites, source=source, climate_model=climate_model, climate_rcp=climate_rcp
        )

    # check cache
    out_data = {}
    if use_cache:
        out_data, non_existing = load_cache(
            sites, start_date, end_date, aggregate_timestep=aggregate_timestep
        )
    else:
        non_existing = sites

    # save data to output_dir
    if output_dir:
        if verbose and out_data:
            print("Saving output from cache...")
        for wo in out_data.values():
            wo.save()

    # get new non-existing data
    if non_existing:

        chunk_i = 0
        for non_existing_chunk in chunks(non_existing, chunk_size):

            if verbose and len(non_existing) > chunk_size:
                chunk_i += 1
                print("Downloading Chunk {0}...".format(chunk_i))

            # gridded vs station data
            headers = None
            if is_grid:
                # gridded data need lat-lon
                extra = to_point_features(non_existing_chunk)

                # special data for MACA
                if source == "maca":
                    extra["climate_model"] = str(climate_model)
                    extra["climate_rcp"] = str(climate_rcp)

                data_some = get_csip_data(source, units, s, e, extra)
                curr_sites = non_existing_chunk

                # check data
                check_data(data_some)
                headers = data_some.pop(0)[2:]  # skip id and date columns

            else:

                sources = [
                    site["source"]
                    for site in non_existing_chunk
                    if site["source"].strip()
                ]
                sources_set = set(sources)

                data_some = []
                curr_sites = []
                for source_curr in sources_set:
                    # station data need station identifiers
                    curr_non_existing_chunk = [
                        site
                        for site in non_existing_chunk
                        if site["source"] == source_curr
                    ]
                    extra = to_station_features(curr_non_existing_chunk)
                    stdata = get_csip_data(source_curr, units, s, e, extra)
                    check_data(stdata)
                    headers = stdata.pop(0)[2:]  # skip id and date columns

                    data_some.extend(stdata)
                    curr_sites.extend(curr_non_existing_chunk)

            # get dates
            if data_some:

                # build map to sites info
                sites_info = {}
                for site in curr_sites:
                    site_id = str(site["id"]).lower()
                    sites_info[site_id] = site
                    sites_info[site["name"].lower()] = site
                    sites_info[get_key_from_site(site)] = site

                # loop through the output data
                new_data = {}
                for rows in itertools.groupby(data_some, key=lambda x: x[0]):

                    # get the dates
                    site_id = str(rows[0]).lower()
                    site_id = update_site_id(site_id)
                    if site_id not in sites_info:
                        msg = (
                            'Looking for "{0}" but could not find... Keys include: {1}'
                        )
                        raise Exception(msg.format(rows[0], sites_info.keys()))
                    site = sites_info[site_id]
                    rows = [r for r in rows[1]]
                    dates = [datetime.strptime(d[1], "%Y-%m-%d") for d in rows]
                    num_data = np.array(
                        [d[2:] for d in rows]
                    )  # skip id and date columns

                    # fill the data to the specified timestep
                    ts = TimeSeries(dates, num_data, headers=headers)
                    wo = WeatherOutput(
                        site,
                        start_date,
                        end_date,
                        output_dir=output_dir,
                        use_cache=use_cache,
                        aggregate_timestep=aggregate_timestep,
                        ts=ts,
                        headers=headers,
                    )

                    # add the data to the dictionary
                    new_data[wo.key] = wo

                # save returned
                if output_dir or use_cache:
                    if verbose:
                        print("Saving the output...")
                    for wo in new_data.values():
                        wo.save()

                # update the output data dictionary
                out_data.update(new_data)

    ordered_data = OrderedDict()
    for site in sites:
        key = get_key_from_site(site)
        ordered_data[key] = out_data[key]

    # merge PRISM data with NARR
    if source == "prism" and merge_with_narr:
        narr_sites = new_sites(sites, "narr")
        narr_data = get_data(
            narr_sites,
            start_date,
            end_date,
            use_cache=use_cache,
            verbose=verbose,
            aggregate_timestep=aggregate_timestep,
            chunk_size=chunk_size,
            units=units,
        )

        for wo_prism, wo_narr in zip(ordered_data.values(), narr_data.values()):
            wo_prism.ts.data = np.hstack((wo_prism.ts.data, wo_narr.ts.data[:, 3:]))
            wo_prism.headers = wo_prism.headers + wo_narr.headers[3:]
            wo_prism.variables = wo_prism.variables + wo_narr.variables[3:]
            wo_prism.site["header"] = wo_prism.headers

    return ordered_data


def convert_output_data_to_matrices_by_variable(out_data, headers, ordered_sites=None):
    data_by_variable = {}

    if ordered_sites is None:
        ordered_sites = [out_data[o]["metadata"] for o in out_data]

    assert len(ordered_sites) > 0, "Data must have something in it!"

    sites = []
    for curr_site in ordered_sites:

        site_key = get_key_from_site(curr_site, force_grid=True)
        if site_key not in headers or site_key not in out_data:
            site_key = get_key_from_site(curr_site, force_station=True)
        if site_key not in headers or site_key not in out_data:
            raise Exception(
                'The site_key "{0}" was not found in the data'.format(site_key)
            )

        var_names = [map_variable(v) for v in headers[site_key][4:]]
        site = out_data[site_key]["metadata"]
        data = out_data[site_key]["data"]

        sites.append(site)

        if data.shape[1] - 4 < len(var_names):
            msg = "Data and variable names from headers do not have the same length (data={0}, headers={1})!"
            raise ValueError(msg.format(data.shape[1] - 4, len(var_names)))

        for j, var_name in zip(range(len(var_names)), var_names):
            o = data[:, 4 + j].reshape((-1, 1))

            if var_name not in data_by_variable:
                data_by_variable[var_name] = o

            else:
                try:
                    data_by_variable[var_name] = np.hstack(
                        (data_by_variable[var_name], o)
                    )
                except ValueError as e:
                    msg = "Array dimensions do not match! Got {0} and {1}"
                    raise Exception(
                        msg.format(data_by_variable[var_name].shape, o.shape)
                    )

    if sorted(var_names) != sorted(data_by_variable.keys()):
        msg = "Variable names {0} does not equal keys in output dictionary {1}!"
        raise ValueError(msg.format(sorted(var_names), sorted(data_by_variable.keys())))

    return sites, data_by_variable


def get_data_by_year(
    sites,
    year,
    climate_model=None,
    climate_rcp=None,
    merge_with_narr=False,
    use_cache=True,
    aggregate_timestep=Timestep.daily,
    pipe=None,
):
    data = get_data(
        sites,
        datetime(year, 1, 1),
        datetime(year + 1, 1, 1),
        climate_model=climate_model,
        climate_rcp=climate_rcp,
        merge_with_narr=merge_with_narr,
        use_cache=use_cache,
        aggregate_timestep=aggregate_timestep,
    )
    if pipe is not None:
        pipe.send(data)
    return data


def get_data_by_year_async(
    sites,
    year,
    climate_model=None,
    climate_rcp=None,
    merge_with_narr=False,
    use_cache=True,
    aggregate_timestep=Timestep.daily,
    verbose=False,
):
    parent, child = Pipe()
    if verbose:
        print("Downloading weather data for year {0}...".format(year))

    # Patterson: added redundancy check
    current_sites = []
    unique = []
    for s in sites:
        check_site = [s["latitude"], s["longitude"]]
        if check_site not in current_sites:
            current_sites.append(check_site)
            unique.append(s)

    p = Process(
        target=get_data_by_year,
        args=(
            unique,
            year,
            climate_model,
            climate_rcp,
            merge_with_narr,
            use_cache,
            aggregate_timestep,
            child,
        ),
    )
    # p.daemon = True
    p.start()
    return parent, p


def organize_by_date(data, sites):
    """
    Converts downloaded data format to the format used by IUWM

    :param dates: A list of dates for the output weather data to use
    :param data: A dictionary of locations with with 2D arrays with dimensions: time (0), variable (1).
    :param sites: A list of site information for the weather data
    :return: Returns a dictionary of climate variables mapped to 2D arrays with dimensions: time (0), location (1)
    """

    # exit if no data
    keys = data.keys()
    if not keys:
        return {}

    # get size of output data
    key = keys[0]
    first_data = data[key]
    dates = first_data.ts.dates
    variables = first_data.variables
    n_time = len(dates)

    # get all site data in order
    data3d = np.dstack([data[get_key_from_site(site)].ts.data for site in sites])

    def transpose(row_data, date, var_list):
        d = {v: row_data[var_i, :] for var_i, v in enumerate(var_list)}
        d["date"] = date
        return d

    # create dictionary that depends on date
    assert (
        n_time == data3d.shape[0]
    ), "Number of timesteps needs to match the first axis length in the data!"
    out_data = {
        date: transpose(data3d[date_i], date, variables)
        for date_i, date in enumerate(dates)
    }
    return out_data


class WeatherError(AssertionError):
    pass


class WeatherGetter:
    def __init__(
        self,
        sites,
        years,
        climate_model=None,
        climate_rcp=None,
        merge_with_narr=False,
        use_cache=True,
        aggregate_timestep=Timestep.daily,
        buffer_size=2,
        sleep_seconds=0.5,
        converter=None,
        build_converter=None,
        verbose=False,
        use_async=False,
    ):

        self.sites = sites
        self.years = years
        self.climate_model = climate_model
        self.climate_rcp = climate_rcp
        self.merge_with_narr = merge_with_narr
        self.use_cache = use_cache
        self.aggregate_timestep = aggregate_timestep
        self.buffer_size = buffer_size
        self.verbose = verbose
        self.use_async = use_async
        if len(years) <= 0:
            raise Exception("The number of years must be at least 1!")

        self.processes = []
        self.pipes = []
        self.curr_years = []

        self.sleep_seconds = sleep_seconds

        self.build_converter = build_converter
        self.converter = converter
        self.curr_data = {}

    def __iter__(self):
        return self

    def keys(self):
        return self.curr_data.keys()

    def __getitem__(self, date):
        """
        Get data based on key. Default key is based on site location. Use converter to change key-value pairing.
        """

        if date not in self:
            raise KeyError(
                'Attempting to get "{0}" from weather data, but failed.'.format(date)
            )
        return self.curr_data[date]

    def __contains__(self, date):
        if date not in self.curr_data:
            try:
                self.next(date.year)  # updates self.curr_data, so check again
            except StopIteration:
                raise WeatherError(
                    "Attempted to get weather beyond period of record provided in data"
                )
        return date in self.curr_data

    def add(self, year):
        if year not in self.curr_years and year in self.years:
            pipe, p = get_data_by_year_async(
                self.sites,
                year,
                climate_model=self.climate_model,
                climate_rcp=self.climate_rcp,
                merge_with_narr=self.merge_with_narr,
                use_cache=self.use_cache,
                aggregate_timestep=self.aggregate_timestep,
                verbose=self.verbose,
            )
            self.processes.append(p)
            self.pipes.append(pipe)
            self.curr_years.append(year)

    def pop(self, i=0):
        if i < len(self.pipes):
            pipe = self.pipes.pop(i)
            process = self.processes.pop(i)
            self.curr_years.pop(i)

            data = pipe.recv()
            process.join()
            return data
        return None

    def next(self, year):

        if year not in self.years:
            raise AssertionError("Requested a year not available in weather data")

        if self.use_async:
            if year not in self.curr_years:
                while len(self.pipes) > 0:
                    self.pop(0)
                for i in range(self.buffer_size):
                    self.add(year + i)

            while len(self.pipes) > self.buffer_size:
                self.pop(0)

            year_i = self.curr_years.index(year)
            data = self.pop(year_i)
            self.add(year + self.buffer_size)

        else:

            data = get_data_by_year(
                self.sites,
                year,
                climate_model=self.climate_model,
                climate_rcp=self.climate_rcp,
                merge_with_narr=self.merge_with_narr,
                use_cache=self.use_cache,
                aggregate_timestep=self.aggregate_timestep,
            )

        # organize data by date
        data_by_date = organize_by_date(data, self.sites)

        # build converter if desired
        if self.converter is None and self.build_converter is not None:
            keys = data.keys()
            if keys:
                d = data[keys[0]]
                self.converter = self.build_converter(
                    {v: d.get_units(v) for v in d.variables}
                )

        # convert data units
        if self.converter is not None:
            for date in data_by_date.keys():
                for v in data_by_date[date].keys():
                    if v in self.converter:
                        data_by_date[date][v] = self.converter[v](data_by_date[date][v])

        # set current data by date
        self.curr_data = data_by_date
        return data_by_date

    def close(self):
        n = len(self.pipes)
        if n > 0:
            for i in range(n):
                self.pop(0)


def retrieve_data_sub(
    sites_file,
    source,
    start_date,
    end_date,
    output_dir=None,
    verbose=False,
    climate_model=None,
    climate_rcp=None,
    use_cache=False,
    chunk_size=CHUNK_SIZE,
    aggregate_timestep=None,
):
    # get list of sites
    if verbose:
        print("Reading sites...")
    sites = read_sites_file(
        sites_file, source=source, climate_model=climate_model, climate_rcp=climate_rcp
    )

    # retrieve data
    if verbose:
        print("Retrieving data from CSIP...")
    data = get_data(
        sites,
        start_date,
        end_date,
        climate_model=climate_model,
        climate_rcp=climate_rcp,
        merge_with_narr=False,
        use_cache=use_cache,
        output_dir=output_dir,
        verbose=verbose,
        chunk_size=chunk_size,
        aggregate_timestep=aggregate_timestep,
    )

    # done
    if verbose:
        print("Done!")

    return data


def retrieve_data(
    sites_file,
    source,
    start_date,
    end_date,
    output_dir=None,
    verbose=False,
    use_cache=False,
    climate_models=None,
    climate_rcps=None,
    chunk_size=CHUNK_SIZE,
    aggregate_timestep=None,
):
    outputs = {}

    if source is not None:
        source = source.lower()
    if source == "maca":
        models = MODEL_NAMES if climate_models is None else climate_models
        rcps = MODEL_RCPS if climate_rcps is None else climate_rcps
        for model, rcp in itertools.product(models, rcps):
            print("Working on Model {0} Scenario {1}".format(model, rcp))
            outputs["{0}/{1}/{2}".format(source, model, rcp)] = retrieve_data_sub(
                sites_file,
                source,
                start_date,
                end_date,
                output_dir=output_dir,
                verbose=verbose,
                climate_model=model,
                climate_rcp=rcp,
                use_cache=use_cache,
                chunk_size=chunk_size,
                aggregate_timestep=aggregate_timestep,
            )
            gc.collect()
    else:
        if source is None:
            sites = read_sites_file(sites_file, source=source)
            source = sites[0]["source"].lower()
        outputs[source] = retrieve_data_sub(
            sites_file,
            source,
            start_date,
            end_date,
            output_dir=output_dir,
            verbose=verbose,
            use_cache=use_cache,
            chunk_size=chunk_size,
            aggregate_timestep=aggregate_timestep,
        )

    return outputs


class WeatherOutput:
    def __init__(
        self,
        site,
        start_date,
        end_date,
        output_dir=None,
        use_cache=False,
        aggregate_timestep=None,
        ts=None,
        headers=None,
    ):

        self.site = site
        self.source = self.site["source"].lower()
        self.site["source"] = self.source
        self.key = get_key_from_site(self.site)
        self.start_date = start_date
        self.end_date = end_date

        # data description
        self.climate_model = self.site["model"] if "model" in self.site else None
        self.climate_rcp = self.site["rcp"] if "rcp" in self.site else None
        self.output_dir = output_dir
        self.use_cache = use_cache
        self.aggregate_timestep = aggregate_timestep

        # getting data
        self._set_ts(ts)
        self.aggregate()

        # store headers and variables
        self._load_headers(headers or ts.headers)

    def _set_ts(self, ts=None):
        if ts is not None:
            self.ts = ts
        else:
            self.ts = self.read_data()

    def cache(self):
        return cache_path(self.site, self.start_date, self.end_date)

    def output_name(self):
        return self.site["output"] if "output" in self.site else self.key

    def output_file(self):

        if self.output_dir is None:
            return None

        output_name = self.output_name()
        if self.site["source"] == "maca":
            return os.path.join(
                self.output_dir,
                self.site["model"],
                self.site["rcp"],
                output_name + ".csv",
            )
        else:
            return os.path.join(self.output_dir, output_name + ".csv")

    def get_output_file(self):

        if self.output_dir is None and not self.use_cache:
            raise Exception(
                "Output directory must be provided, or the cache must be used!"
            )

        if self.use_cache:
            return self.cache()
        else:
            return self.output_file()

    def get_header_file(self, ext=".hdr"):
        return self.get_output_file() + (ext if self.use_cache else "")

    def get_output_dir(self):
        return os.path.dirname(self.get_output_file())

    def output_exists(self):
        return os.path.exists(self.get_output_file())

    def aggregate(self):

        if self.aggregate_timestep is not None:
            self.ts.aggregate(
                self.aggregate_timestep,
                self.start_date,
                self.end_date,
                fxn=MODEL_VARIABLE_AGGREGATION[: self.ts.data.shape[1]],
            )

    def has_var(self, var_name):
        return map_variable(var_name) in self.variables

    def get_units(self, var_name):
        if not self.has_var(var_name):
            raise ValueError(
                'Cannot retrieve units for variable "{0}", it does not exist in data!'.format(
                    var_name
                )
            )
        return self.units[self.variables.index(map_variable(var_name))]

    def get_data(self, var_name=None):

        if var_name is None:
            return self.ts.data
        else:
            return self.ts.data[:, self.variables.index(map_variable(var_name))]

    def label(self):
        if self.site["source"] == "maca":
            return "{0}-{1}-{2}".format(
                self.site["source"], self.site["model"], self.site["rcp"]
            )
        return self.site["source"]

    def output_array(self):
        dates_array = np.array(
            [
                (date.day, date.month, date.year, date.timetuple().tm_yday)
                for date in self.ts.dates
            ]
        )
        return np.hstack((dates_array, self.ts.data))

    def get_headers(self):
        return ["day", "month", "year", "julian day"] + self.headers

    def _save_headers(self, path=None):
        if path is None:
            path = self.get_header_file()
        with open(path, "wb") as f:
            cw = csv.DictWriter(f, self.headers)
            cw.writeheader()

    def _load_headers(self, headers=None, path=None):

        if headers is None:
            if path is None:
                path = self.get_header_file()
            if os.path.exists(path):
                with open(path, "r") as f:
                    cr = csv.DictReader(f)
                    headers = cr.fieldnames
            else:
                headers = [
                    h
                    for h in get_csip_headers(self.source)
                    if h.lower().strip() != "date"
                ]

        self.headers = headers
        self.variables = header_variables(self.headers)
        self.units = header_units(self.headers)
        self.site["header"] = self.headers
        return self.headers

    def read_data(self):

        if not self.output_exists():
            raise Exception("Output {0} does not exist!".format(self.get_output_file()))

        headers, d = self._load(self.get_output_file(), binary=self.use_cache)

        dates = [datetime(int(di[2]), int(di[1]), int(di[0])) for di in d]
        return TimeSeries(dates, d[:, 4:], headers)

    def _load(self, file_path, binary=False, just_headers=False):

        data = None

        if binary:

            headers = self._load_headers(path=file_path + ".hdr")
            if not just_headers:
                data = np.load(file_path)

        else:

            with open(file_path, "r") as f:
                cr = csv.reader(f)
                headers = next(r for r in cr)
                if not just_headers:
                    data = np.array([r for r in cr])

        return headers, data

    def _save(self, file_path, binary=False, just_headers=False):

        out_dir = os.path.dirname(file_path)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        if binary:

            if not just_headers:
                np.save(self.cache(), self.output_array())
            self._save_headers(file_path + ".hdr")

        else:
            with open(file_path, "wb") as f:
                cw = csv.writer(f)
                cw.writerow(self.get_headers())
                if not just_headers:
                    for row in self.output_array():
                        cw.writerow(row)

    def save(self):

        # save cache
        if self.use_cache:
            self._save(self.cache(), binary=True)

        # save output
        if self.output_dir:
            self._save(self.output_file())


def split_sites(sites_file, sources=None, climate_models=None, climate_rcps=None):
    # require sources to be described in the sites file
    if not sources:
        return [], read_sites_file(sites_file)

    # read_sites_file(sites_file, source=None, climate_model=None, climate_rcp=None)
    gridded_sources = [source for source in sources if is_gridded(source)]
    gridded_sites = []
    for source in gridded_sources:
        if source == "maca":
            for model, option in itertools.product(climate_models, climate_rcps):
                gridded_sites.extend(
                    read_sites_file(
                        sites_file,
                        source=source,
                        climate_model=model,
                        climate_rcp=option,
                    )
                )
        else:
            gridded_sites.extend(read_sites_file(sites_file, source=source))

    # if there is any station data, read sites file with those sources in mind...
    nongridded_sites = []
    nongridded_sources = [source for source in sources if not is_gridded(source)]
    if nongridded_sources or [source for source in sources if source is None]:
        if len(nongridded_sources) != 1:
            # require the sites file to specify sources
            nongridded_sites = read_sites_file(sites_file)
        else:
            # set the only source as the default
            nongridded_sites = read_sites_file(
                sites_file, next(source for source in nongridded_sources)
            )

    return gridded_sites, nongridded_sites


def create_source_key(site):
    if site["source"] == "maca":
        return "{0}/{1}/{2}".format(site["source"], site["model"], site["rcp"])
    else:
        return site["source"]


class WeatherOutputs:
    def __init__(
        self,
        sites_file,
        start_date,
        end_date,
        output_dir=None,
        use_cache=False,
        sources=None,
        climate_models=None,
        climate_rcps=None,
        aggregate_timestep=None,
    ):

        self.gridded, self.nongridded = split_sites(
            sites_file,
            sources=sources,
            climate_models=climate_models,
            climate_rcps=climate_rcps,
        )

        def create_weather_output(site):
            return WeatherOutput(
                site,
                start_date,
                end_date,
                output_dir=output_dir,
                use_cache=use_cache,
                aggregate_timestep=aggregate_timestep,
            )

        self.sites = self.gridded + self.nongridded
        self.site_data = [create_weather_output(site) for site in self.sites]
        self.site_dict = {}
        for wo in self.site_data:
            src_key = create_source_key(wo.site)
            site_key = get_key_from_site(wo.site)
            if src_key in self.site_data:
                self.site_dict[src_key][site_key] = wo
            else:
                self.site_dict[src_key] = {site_key: wo}

    def get_data(self, site=None):

        if site is not None:
            src_key = create_source_key(site)
            site_key = get_key_from_site(site)
            return self.site_dict[src_key][site_key]

        return self.site_data

    def size(self):

        return len(self.site_data)


def get_data_headers(units="metric", verbose=False):
    headers = []
    for source in SOURCE_NAMES:
        header = get_csip_headers(source)
        if verbose:
            print("{0}:\n {1}".format(source, ", ".join([str(d) for d in header])))
        headers.append(header)
    return headers


if __name__ == "__main__":

    # works when freezing with py2exe
    freeze_support()

    # command-line arguments
    import argparse

    parser = argparse.ArgumentParser(
        description="Retrieve weather data from different sources at various locations."
    )
    parser.add_argument(
        "--sites_file",
        type=str,
        default=None,
        help="Path of CSV file containing sites with columns: ({0}).".format(
            ",".join(SITE_COLUMNS)
        ),
    )
    parser.add_argument(
        "--source",
        type=str,
        default=None,
        help="Data source name ({0}).".format(", ".join(CSIP_SOURCE_NAMES)),
    )
    parser.add_argument(
        "--output_dir", type=str, default=None, help="Path to the output directory."
    )
    parser.add_argument(
        "--cache", action="store_true", help="Store downloads in cache."
    )
    parser.add_argument(
        "--start_date",
        type=str,
        default="1981-01-01",
        help='Start date in yyyy-mm-dd format. Default is "1981-01-01".',
    )
    parser.add_argument(
        "--end_date",
        type=str,
        default="2014-12-31",
        help='Ending date in yyyy-mm-dd format. Default is "2014-12-31".',
    )
    parser.add_argument(
        "--chunk_size",
        type=int,
        default=CHUNK_SIZE,
        help="Number of location downloaded at one time.",
    )
    parser.add_argument(
        "--aggregate_timestep",
        type=str,
        default=None,
        help="Timestep at which to aggregate the data",
    )
    parser.add_argument(
        "--list_names",
        action="store_true",
        default=False,
        help="List the GCM model names",
    )
    parser.add_argument(
        "--list_units",
        action="store_true",
        default=False,
        help="List the units for each gridded dataset",
    )
    parser.add_argument(
        "--climate_models",
        type=str,
        nargs="+",
        default=None,
        help="The MACA model number(s) or name(s)",
    )
    parser.add_argument(
        "--climate_rcps",
        type=str,
        nargs="+",
        default=None,
        help="The MACA RCP option(s) (rcp45 or rcp85)",
    )
    args = parser.parse_args()

    if args.list_names:
        print("List of GCM models in MACA data in CSIP at CSU:")
        get_maca_model_names(verbose=True)

    if args.list_units:
        print("\nMetric units")
        get_data_headers("metric", verbose=True)
        print("\nEnglish units")
        get_data_headers("english", verbose=True)

    if not args.list_names and not args.list_units and args.sites_file is None:

        print(
            "Must provide one of --sites_file or --list_names or --list_units options!"
        )
        parser.print_help()

    else:

        # retrieve data
        data_start = datetime.strptime(args.start_date, "%Y-%m-%d")
        data_end = datetime.strptime(args.end_date, "%Y-%m-%d")
        if args.output_dir is None and not args.cache:
            print(
                "Without any output directory or cache, the weather downloader is useless... "
                'Changed the output_dir to "output" by default'
            )
            args.output_dir = "output"

        retrieve_data(
            args.sites_file,
            args.source,
            data_start,
            data_end,
            output_dir=args.output_dir,
            verbose=True,
            use_cache=args.cache,
            climate_models=args.climate_models,
            climate_rcps=args.climate_rcps,
            chunk_size=args.chunk_size,
            aggregate_timestep=args.aggregate_timestep,
        )
