# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from collections import OrderedDict
import constants
import convert
import csv
from datetime import datetime
import fnmatch
import inputs
import itertools
import numpy as np
import os
import re
import sys
import time
import warnings


RE_FUNCTION = re.compile(r"^(.*)\.__(.*)__$")


def merge_dicts(*dicts):
    if not dicts:
        return {}
    d = dicts[0].copy()
    for i in range(1, len(dicts)):
        d.update(dicts[i])
    return d


class InsensitiveDictReader(csv.DictReader):
    """
    This class overrides the csv.fieldnames property, which converts all fieldnames without leading and trailing
    spaces and to lower case.
    """

    @property
    def fieldnames(self):
        return [field.strip().lower() for field in csv.DictReader.fieldnames.fget(self)]

    def next(self):
        return InsensitiveDict(csv.DictReader.next(self))


class InsensitiveDict(dict):
    # This class overrides the __getitem__ method to automatically strip() and lower() the input key

    def __getitem__(self, key):
        return dict.__getitem__(self, key.strip().lower())


def ensure_fraction(v):
    v = np.array(v)
    if (v < -constants.TOLERANCE).any() or (v > 1 + constants.TOLERANCE).any():
        raise ValueError("Fraction must be between 0 and 1!")

    below_zero = v < 0.0
    v[below_zero] = 0.0

    above_one = v > 1.0
    v[above_one] = 1.0

    return v


def read_csv_variables(csv_file, field="variable_name"):
    with open(csv_file, "r") as f:
        cr = csv.DictReader(f)
        if field not in cr.fieldnames:
            raise ValueError(
                'Could not find required field "{0}" in {1}!'.format(field, csv_file)
            )
        return [row[field] for row in cr]


def read_csv_key_values(csv_file, key="category", value=None, default=None):
    with open(csv_file, "r") as f:
        cr = csv.DictReader(f)
        if key not in cr.fieldnames:
            raise ValueError(
                'Could not find required field "{0}" in {1}!'.format(key, csv_file)
            )
        if value is not None:
            if value not in cr.fieldnames and default is None:
                raise ValueError(
                    'Could not find required value field "{0}" in {1}!'.format(
                        value, csv_file
                    )
                )
            return OrderedDict(
                [
                    (r[key], r[value] if value in r and r[value].strip() else default)
                    for r in cr
                ]
            )
        return OrderedDict([(r[key], default) for r in cr])


def parameter_list(param_names=None, defaults=None, csv_field="variable_name"):
    # get default if none provided
    defaults = [inputs.standard_name(p) for p in defaults]
    if param_names is None:
        if defaults is None:
            return None
        param_names = defaults

    # add param_names from csv file
    csv_variables = set(
        p for p in param_names if inputs.standard_name(p).endswith(".csv")
    )
    for v in csv_variables:
        i = param_names.index(v)
        param_names[i : i + 1] = read_csv_variables(v, field=csv_field)

    # normalize the variable names (lower case and no spaces) after csv files are extracted
    param_names = [inputs.standard_name(p) for p in param_names]

    # add defaults to the beginning
    if "defaults" in param_names or "default" in param_names and defaults is not None:
        if "defaults" in param_names:
            param_names.remove("defaults")
        if "default" in param_names:
            param_names.remove("default")
        param_names = [p for p in param_names if p not in defaults]
        param_names = defaults + param_names

    return param_names


def init_output_variables(output_variables=None, remove_fxn=False):
    output_variables = parameter_list(
        output_variables, defaults=constants.DEFAULT_OUTPUT_VARIABLES
    )
    if output_variables is None:
        output_variables = constants.DEFAULT_OUTPUT_VARIABLES

    if remove_fxn:
        output_variables = [get_fxn(v)[0] for v in output_variables]

    return output_variables


def get_fxn(var_name):
    match = RE_FUNCTION.search(var_name)
    if match:
        var_name = match.group(1)
        fxn = match.group(2)
        return var_name, fxn
    return var_name, None


def user_friendly_time(
    t, units="seconds", threshold_factor=1.5, fmt="%5.1f", abbreviate=False
):
    """
    Produces a date/time that is friendly to the eyes

    :param t: amount of time
    :param units: units of time
    :param threshold_factor: threshold above which
    :param fmt:
    :param abbreviate:
    :return:
    """
    # search for the units in a list of acceptable ones
    all_units = ["seconds", "minutes", "hours", "days", "years"]
    all_conversions = [60.0, 60.0, 24.0, 365.25]
    i = all_units.index(units)

    # advance in the sequence
    if i < len(all_conversions) and t > all_conversions[i] * threshold_factor:
        return user_friendly_time(
            t / all_conversions[i],
            units=all_units[i + 1],
            threshold_factor=threshold_factor,
            fmt=fmt,
            abbreviate=abbreviate,
        )
    else:
        if abbreviate:
            abbreviations = ["sec", "min", "hr ", "dy ", "yr "]
            units = abbreviations[i]
        return "{0} {1}".format(fmt % t, units)


def build_weather_units_converter(var_units):
    cvt = {}
    for v in var_units:
        if v in inputs.WEATHER:
            info = inputs.WEATHER[v]
            cvt[v] = convert.unit_converter(info.units_type, var_units[v], info.units)
    return cvt


def read_headers(csv_file, case_insensitive=True):
    with open(csv_file, "r") as f:
        if case_insensitive:
            cr = InsensitiveDictReader(f)
        else:
            cr = csv.DictReader(f)
        fields = cr.fieldnames
    return fields


def read_timeseries_input(
    input_file,
    geoid_indices,
    field_definitions=None,
    geoid_col="geoid",
    sum_space=False,
    return_headers=False,
):
    # read data
    with open(input_file, "r") as f:
        cr = InsensitiveDictReader(f)
        headers = cr.fieldnames
        fields = set(headers)
        csv_data = [row for row in cr]

    # dates conversion
    special = {"date", "year", "month", "day", geoid_col}
    date_field = inputs.get(
        "date", "date", convert.BASE_DATE, description="Date", required=False
    )

    if "date" in fields:

        def date_grabber(r):
            return r["date"]

    else:

        def date_grabber(r):
            ymd = [int(v) for v in (r["year"], r.get("month", 1), r.get("day", 1))]
            return "{0:04d}-{1:02d}-{2:02d}".format(*ymd)

    def date_conv(r):
        return date_field.convert(date_grabber(r))

    # get field _def
    if field_definitions is None:
        field_definitions = {
            h: inputs.get(h, default_type="real", default_value=0.0)
            for h in fields
            if h not in special
        }

    # remove fields that do not exist in the data
    field_definitions = field_definitions.copy()
    field_keys = list(field_definitions.keys())
    for h in field_keys:
        info = field_definitions[h]
        if not info.check_fieldnames(fields):
            field_definitions.pop(h)
        elif h in special:
            field_definitions.pop(h)

    # sort by date first
    sorted_data = sorted(csv_data, key=date_conv)

    # collect output data in a dictionary
    n_sites = len(geoid_indices)
    out_data = {}
    if geoid_col in fields:

        # sort data by date and collect variables as arrays of sorted spatial information (by geoid)
        for date, same_date_rows in itertools.groupby(sorted_data, key=date_conv):

            out_row = {"date": date}

            same_date_rows = [
                row for row in same_date_rows if row[geoid_col] in geoid_indices
            ]
            indices = np.int32(
                [geoid_indices[row[geoid_col]] for row in same_date_rows]
            )

            if len(set(indices)) != len(indices):
                raise ValueError(
                    "Multiple geoids for the same date! File {0}. Date {1}.".format(
                        input_file, date
                    )
                )

            if len(indices) == n_sites:
                for h, info in field_definitions.items():
                    values = info.convert_array(
                        [info.convert_from_row(row) for row in same_date_rows]
                    )
                    new_values = np.copy(values)
                    new_values[indices] = values
                    if sum_space:
                        new_values = np.sum(new_values)
                    out_row[h] = new_values
            else:
                for h, info in field_definitions.items():
                    values = info.convert_array(
                        [info.convert_from_row(row) for row in same_date_rows]
                    )
                    if sum_space:
                        msg = "Summing values across space when not all geoids were provided in {0}!".format(
                            input_file
                        )
                        warnings.warn(msg)
                        values = np.sum(values)
                    out_row[h] = (indices, values)

            out_data[date] = out_row

    else:

        # collect data by date
        for row in sorted_data:
            date = date_conv(row)
            out_row = {
                h: info.convert_from_row(row) for h, info in field_definitions.items()
            }
            out_data[date] = out_row

    if return_headers:
        return headers, out_data
    else:
        return out_data


def change_year(date, to_year):
    if date.month == 2 and date.day == 29:
        if to_year % 4 == 0 and (to_year % 400 == 0 or to_year % 100 != 0):
            date = datetime(to_year, date.month, date.day)
        else:
            date = datetime(to_year, date.month, 28)
    else:
        date = datetime(to_year, date.month, date.day)
    return date


def change_year_of_timeseries(timeseries_input, to_year):
    new_dict = {}
    for date, val in timeseries_input.items():

        date = change_year(date, to_year)
        if date in new_dict:
            msg = "A repeating timeseries includes the same day/month combination twice ({0}/{1})!"
            raise ValueError(msg.format(date.day, date.month))
        new_dict[date] = val

    return new_dict


def get_weather_from_file(weather_file, geoid_indices):
    return read_timeseries_input(
        weather_file, geoid_indices, field_definitions=inputs.WEATHER
    )


def input_param(param_name, param_value, n_sites=None, units=None):
    param_name, param_units = inputs.standard_name(param_name, return_units=True)
    if param_units is None:
        param_units = units
    existing_param = param_name in inputs.PARAMETERS

    try:
        a = inputs.get(
            param_name, default_type="real", default_value=0.0
        ).convert_array(param_value, units=param_units)
        if n_sites is not None:
            if a.ndim == 0 or len(a) == 1:
                return np.full(n_sites, a)
            else:
                return a
        else:
            return a
    except ValueError as e:
        if param_name in {"profile_a", "profile_b"}:
            return None
        else:

            # throw error when we know the parameter
            msg = (
                "Could not read input data parameter {0}! Is it in the wrong format? "
                "Here is the error that occurred:\n\n{1}\n\n"
            )
            if existing_param:
                raise ValueError(msg.format(param_name, e))

            # just get the string if we don't know the parameter
            try:
                inputs.PARAMETERS.pop(param_name)
                return inputs.get(
                    param_name, default_type="text", default_value=""
                ).convert_array(param_value)
            except ValueError as e:
                raise ValueError(msg.format(param_name, e))


def get_param_bounds(param_name):
    if param_name in inputs.PARAMETERS:
        return inputs.PARAMETERS[param_name].bounds
    raise ValueError('Parameter "{0}" not found in inputs.'.format(param_name))


def wait_until_file_is_closed(file_path, max_tries=3600):
    for i in range(max_tries):
        try:
            with open(file_path, "w"):
                pass
            if i != 0:
                print("\n")
            return
        except IOError:
            if i == 0:
                print("\nCould not open file {0}!".format(file_path))
            sys.stdout.write("\rAttempt {0} out of {1}...".format(i + 1, max_tries))
            time.sleep(1)


class Timer:
    def __init__(self):
        self.start_time = time.time()
        self.recorded = []
        self.start_time_lap = self.start_time

    def start(self):
        self.start_time = time.time()
        self.start_time_lap = self.start_time

    def record(self):
        elapsed = self.lap()
        self.recorded.append(elapsed)
        return elapsed

    def lap(self):
        curr_time = time.time()
        elapsed = curr_time - self.start_time_lap
        self.start_time_lap = curr_time
        return elapsed

    def total(self):
        return time.time() - self.start_time

    def record_average(self):
        return np.mean(self.recorded)

    def record_sum(self):
        return sum(self.recorded)

    def record_std(self):
        return np.std(self.recorded)

    def time_str(self):
        return user_friendly_time(self.record_sum(), abbreviate=True)

    def summary(self):
        return "Avg lap: {0} ({1:.3f}). Total: {2}".format(
            user_friendly_time(self.record_average(), abbreviate=True, fmt="%6.2f"),
            self.record_std(),
            user_friendly_time(self.record_sum(), abbreviate=True),
        )


class Event(list):
    """Event subscription.

    A list of callable objects. Calling an instance of this will cause a
    call to each item in the list in ascending order by index.

    Example Usage:
    >>> def f(x):
    ...     print('f(%s)' % x)
    >>> def g(x):
    ...     print('g(%s)' % x)
    >>> e = Event()
    >>> e()
    >>> e.append(f)
    >>> e(123)
    f(123)
    >>> e.remove(f)
    >>> e()
    >>> e += (f, g)
    >>> e(10)
    f(10)
    g(10)
    >>> del e[0]
    >>> e(2)
    g(2)

    """

    def __call__(self, *args, **kwargs):
        for f in self:
            f(*args, **kwargs)

    def __repr__(self):
        return "Event(%s)" % list.__repr__(self)


def local_events():
    return {e: Event() for e in constants.EVENTS}


def get_buried_dict_value(d, keys):
    if isinstance(keys, str):
        return d[keys]
    else:
        for key in keys:
            d = d[key]
        return d


def buried_dict_value_exists(d, keys):
    if isinstance(keys, str):
        return keys in d
    else:
        for key in keys:
            if key not in d:
                return False
            d = d[key]
        return True


def same_file(path1, path2):
    return os.path.abspath(os.path.normpath(path1)) == os.path.abspath(
        os.path.normpath(path2)
    )


def default_output_dir(service_area_file):
    return os.path.splitext(service_area_file)[0]


def read_param_file(*args, **kwargs):
    from SALib.util import read_param_file as rpf

    return rpf(*args, **kwargs)


class TimeseriesInput:
    def __init__(self, file, repeat_yearly=False):
        self.file = file
        self.repeat_yearly = repeat_yearly


def same_dir(d1, d2):
    d1 = os.path.normpath(os.path.abspath(d1))
    d2 = os.path.normpath(os.path.abspath(d2))
    return d1 == d2


def search_files(the_dir, pattern, skip=None):
    for root, dirnames, filenames in os.walk(the_dir):
        for filename in fnmatch.filter(filenames, pattern):
            if skip and [s for s in skip if re.search(s, filename)]:
                continue
            yield os.path.join(root, filename)


def search_directories(the_dir, pattern):
    for root, dirnames, filenames in os.walk(the_dir):
        for dirname in fnmatch.filter(dirnames, pattern):
            yield os.path.join(root, dirname)


def list_files(the_dir, pattern, skip=None):
    return list(search_files(the_dir, pattern, skip=skip))


def re_search_files(the_dir, patterns, agg=any):

    if not isinstance(patterns, (list, tuple)):
        patterns = [patterns]

    for root, dir_names, file_names in os.walk(the_dir):
        for file_name in file_names:
            file_path = os.path.join(root, file_name)
            if agg(re.search(pattern, file_path) for pattern in patterns):
                yield file_path


def re_search_directories(the_dir, patterns, agg=any):

    if not isinstance(patterns, (list, tuple)):
        patterns = [patterns]

    for root, dir_names, file_names in os.walk(the_dir):
        for dir_name in dir_names:
            dir_path = os.path.join(root, dir_name)
            if agg(re.search(pattern, dir_path) for pattern in patterns):
                yield dir_path


def re_list_files(the_dir, patterns, agg=any):
    return list(re_search_files(the_dir, patterns, agg=agg))
