# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import aggregators
import argparse
import batch
import calibration
import constants
import convert
import inputs
import model
from multiprocessing import freeze_support, cpu_count
import os
import sensitivity
import sys
from utils import TimeseriesInput
import weather

THIS_DIR = os.path.abspath(os.path.dirname(__file__))


def prepare_output(args):
    if hasattr(args, 'obs_timestep') and args.obs_timestep:
        args.output = args.output.compare_observed(args.obs_timestep, args.obs_file,
                                                   args.obs_var, args.iuwm_var, args.metrics, args.compare_file)
    if args.output_dir is not None:
        args.output = args.output.new_directory(args.output_dir)
    return args


def get_kwargs(args):

    # prepare output object
    args = prepare_output(args)

    # weather source
    weather_source = args.source if args.source is not None else ('file' if args.weather_file is not None else 'prism')
    weather_source = weather_source.lower()

    return {
        # inputs
        'service_area': args.service_areas,

        # timeseries inputs
        'timeseries': args.timeseries,

        # simulation time period
        'start_date': args.start_date,
        'end_date': args.end_date,

        # climate
        'weather_parameters': {
            'source': weather_source,
            'file': args.weather_file,
            'climate_model': args.climate_model,
            'climate_rcp': args.climate_rcp,
            'force_download': args.force_download,
        },

        # output
        'output': args.output,
        'output_variables': args.output_variables,

        # program options
        'verbose': args.verbose,
        'update_in_place': args.update_in_place,
        'suppress_warnings': args.suppress_warnings,
        'write_possible_outputs': args.write_possible_outputs,
        'check_level': args.check_level,

        # special inputs
        'registered_inputs': args.registered_inputs,
        'registered_land_uses': args.registered_land_uses,
        'packages': args.packages,
    }


class IUWMParsingError(AttributeError):
    pass


class ErrorRaisingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise IUWMParsingError(message)


def output_sub_parser(parents=None):
    """
    Builds a parser to specify output variables

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for parsing output parameters
    """
    parser = ErrorRaisingArgumentParser(parents=parents if parents else [], add_help=False)
    parser.add_argument('file', type=str, help='Path to a CSV file that will contain model outputs')
    parser.add_argument('timestep', type=str, choices=aggregators.OutputAggregator.classes.keys(),
                        help='Output timestep of data to save in output file')
    parser.add_argument('by_geoid', type=convert.to_bool, nargs='?', default=False,
                        help='Save output for each spatial subunit')
    parser.add_argument('flatten', type=convert.to_bool, nargs='?', default=False,
                        help='Save spatial output across the columns instead of in rows with a "geoid" column')
    parser.add_argument('in_memory', type=convert.to_bool, nargs='?', default=False,
                        help='Hold output data in memory after model run (primarily for aggregating annual average)')
    return parser


def parse_for_output(argv=None, default=None):
    """
    Builds a parser to specify output variables

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for parsing output parameters
    """
    parser = argparse.ArgumentParser('Parser for just --output')
    parser.add_argument('--output', action=OutputAction, type=str, default=None, nargs='+',
                        help='Output file parameter(s) in this order: file,timestep[,by_geoid][,flatten][,in_memory]')
    args, _ = parser.parse_known_args(argv)  # ignore unknown arguments
    return args.output or default


def timeseries_parser(parents=None):
    """
    Builds a parser to specify timeseries input files

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for parsing timeseries
    """
    parser = ErrorRaisingArgumentParser(parents=parents if parents else [], add_help=False)
    parser.add_argument('file', type=str, help='Input timeseries CSV file')
    parser.add_argument('repeat_yearly', type=convert.to_bool, nargs='?', default=False,
                        help='Timeseries input repeats yearly')
    return parser


class BuildClassAction(argparse.Action):

    cls = None

    def get_parser(self):
        raise NotImplementedError('Parser not implemented!')

    def _get_instance(self, values, option_string=None):

        p = self.get_parser()
        try:
            # parse args with its own positional parser
            args = p.parse_args(values)
        except IUWMParsingError as e:
            if option_string:
                self._prog = option_string.strip('-')
            p.print_help(sys.stderr)
            raise e

        if self.cls is None:
            raise IUWMParsingError('No class specified!')

        instance = self.cls(**vars(args))
        return instance

    def __call__(self, parser, args, values, option_string=None):
        instance = self._get_instance(values, option_string=option_string)
        setattr(args, self.dest, instance)


class ListClassAction(BuildClassAction):

    list_cls = None

    def get_parser(self):
        raise NotImplementedError('Parser not implemented!')

    def __call__(self, parser, args, values, option_string=None):
        instance = self._get_instance(values, option_string=option_string)

        items = [c for c in (getattr(args, self.dest, []) or [])]
        items.append(instance)

        if self.list_cls is None:
            setattr(args, self.dest, items)
        else:
            setattr(args, self.dest, self.list_cls(items))


class OutputAction(ListClassAction):

    cls = aggregators.OutputDefinition
    list_cls = aggregators.OutputDefinitionList

    def get_parser(self):
        return output_sub_parser()


class TimeseriesAction(ListClassAction):

    cls = TimeseriesInput

    def get_parser(self):
        return timeseries_parser()


def model_parser(parents=None):
    """
    Builds the shared command line parser arguments for running the model

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for running the model
    """

    parser = argparse.ArgumentParser(parents=parents if parents else [], add_help=False)

    # service area input file (primary input file for IUWM)
    parser.add_argument('service_areas', type=str, help='Path of the CSV file containing service areas.')

    # simulation time period
    parser.add_argument('--start_date', type=convert.to_date, help='Simulation starting date [yyyy-mm-dd]')
    parser.add_argument('--end_date', type=convert.to_date, help='Simulation ending date (inclusive) [yyyy-mm-dd]')

    # climate data input
    parser.add_argument('--source', type=str, default=None,
                        help='The weather source ({0}). Default is "prism".'.format(
                            ', '.join(weather.SOURCE_NAMES)))
    parser.add_argument('--weather_file', type=str, default=None,
                        help='Path to user-specified weather in CSV format.')
    parser.add_argument('--climate_model', type=str, default=None,
                        help='The GCM model ({0}).'.format(', '.join(weather.MODEL_NAMES)))
    parser.add_argument('--climate_rcp', type=str, default=None, help='The RCP scenario ("45" or "85").')
    parser.add_argument('--force_download', action='store_true', default=False, help='Do not use cached weather data.')

    # timeseries input
    parser.add_argument('--timeseries', action=TimeseriesAction, type=str, default=None, nargs='+',
                        help='Timeseries input file parameters in this order: file[,repeat_yearly]. '
                             'Required columns in the timeseries input files are "date" and "geoid". '
                             'Other columns should be names of input variables to the model. '
                             'Data is updated only on the exact date that it is specified. '
                             'Data after that date remains unchanged until updated specifically again. Thus, '
                             'IUWM interpolates missing data as a "step" function. '
                             'When repeat_yearly is true ("t", "1", "true", "yes"), timseries inputs repeat yearly.')

    # special inputs
    parser.add_argument('--registered_inputs', type=str, nargs='+',
                        help='Additional input parameters to register (to avoid printed warning).')
    parser.add_argument('--registered_land_uses', type=str, nargs='+',
                        help='Land uses to register for calculating irrigated area')
    parser.add_argument('--registered_alternative_sources', type=str, nargs='+', default=constants.SOURCE_TYPES,
                        help='Alternative sources to register (order matters). '
                             'Default is {0}'.format('->'.join(constants.SOURCE_TYPES)))

    # custom modules for extending IUWM
    parser.add_argument('--packages', type=str, default=[], nargs='+',
                        help='Path to other python modules with custom routines and functionality for IUWM. '
                             'This path does not include the ".py" at the end. When just specifying the path to '
                             'the module, a function defined as "def subscribe(model):" must be found in the '
                             'module. If you want to subscribe a specific function, put ".function_name" at the '
                             'end of the module path. That function must be defined like this: '
                             '"def function_name(model, **kwargs):". '
                             'Replace "function_name" with any function name of your choice.')

    # output selection
    parser.add_argument('--output', action=OutputAction, type=str, default=None, nargs='+',
                        help='Output file parameter(s) in this order: file,timestep[,by_geoid][,flatten][,in_memory]')
    parser.add_argument('--output_variables', type=str, nargs='+', default=None, help='Output variables to record.')
    parser.add_argument('--output_dir', type=str, default=None, help='Output directory.')

    # program run options
    parser.add_argument('-v', '--verbose', action='store_true', help='Flag for verbosity')
    parser.add_argument('--update_in_place', action='store_true',
                        help='Update the service area file to the newest version without copying first. It is '
                             'highly recommended to BACKUP your original file.')
    parser.add_argument('--suppress_warnings', action='store_true', help='Suppress all warnings from model runs.')
    parser.add_argument('--write_possible_outputs', type=str, default='', help='Write list of output parameters')
    parser.add_argument('--check_level', type=int, default=2,
                        help='Run various intensities of checks (0=none, 1=just mass bal., 2=all checks)')

    return parser


def validation_parser(parents=None):
    """
    Builds parser of validation parameters to compare modeled and observed

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for validation operations
    """

    parser = argparse.ArgumentParser(parents=parents if parents else [], add_help=False)

    # input files
    parser.add_argument('--obs_file', type=str, default=None, help='Observation file for performing validation')

    # comparison options
    parser.add_argument('--obs_var', type=str, nargs='+', default=None, help='The observed variable(s) for comparison')
    parser.add_argument('--iuwm_var', type=str, nargs='+', default=None, help='The modeled variable(s) for comparison')
    parser.add_argument('--obs_timestep', type=str, default=None,
                        help='The timestep of the observed data to compare with IUWM output '
                             '("daily", "monthly", "annual", or "avg_annual").')
    parser.add_argument('--metrics', type=str, nargs='+', default=None,
                        help='Performance metrics to calculate when comparing observed to modeled.')

    # output options
    parser.add_argument('--compare_file', type=str, help='Path to a file that saves error statistics of a run')

    return parser


def batch_parser(parents=None):
    """
    Builds the shared command line parser arguments for batch operations such as batch, sensitivity and calibration

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for batch operations
    """
    parser = argparse.ArgumentParser(parents=parents if parents else [], add_help=False)

    # options for both batch runs and sensitivity analysis
    parser.add_argument('--n_runs', type=int, default=None, help='Number of model runs to perform')

    # program options
    parser.add_argument('--parallel', type=str, default=None, help='Run multiple models in parallel')
    parser.add_argument('--n_processes', type=int, default=cpu_count(), help='Number of processes to run on.')
    parser.add_argument('--scatter_gather', action='store_true',
                        help='When running parallel, scatter all scenarios to each worker, run IUWM individually for '
                             'each worker, and then gather IUWM results. Default parallelism is more like a job queue, '
                             'which is usually faster, especially when variance in scenario runtime is large.')
    return parser


def sensitivity_parser(parents=None):
    """
    Builds the shared command line parser arguments for batch operations such as batch, sensitivity and calibration

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for batch operations
    """
    parser = argparse.ArgumentParser(parents=parents if parents else [], add_help=False)
    parser.add_argument('param_bounds_file', type=str, help='Output file from sensitivity analysis')
    parser.add_argument('--calc_second_order', action='store_true',
                        help='When running sensitivity analysis, calculate second order sensitivity indices.')
    return parser


def results_parser(parents=None):
    """
    Builds the command line parser arguments for reading batch results files

    :param parents: List of parent parsers that share command line arguments with this
    :return: Returns the command line argument parser for reading batch results files
    """

    # todo: build into this more
    parser = argparse.ArgumentParser(parents=parents if parents else [], add_help=False)
    parser.add_argument('results_file', type=str, help='Output file from sensitivity analysis')
    return parser


MODEL_PARSER = model_parser()
VALIDATION_PARSER = validation_parser()
BATCH_PARSER = batch_parser(parents=[MODEL_PARSER, VALIDATION_PARSER])
SENSITIVITY_PARSER = sensitivity_parser(parents=[BATCH_PARSER])
RESULTS_PARSER = results_parser()


class IUWMCommandError(ValueError):
    pass


class IUWMCommand:
    """
    Defines how to both parse and handle a command from the command line
    """

    command = ''
    help = ''
    parents = []

    def __init__(self):
        self.parser = None

    def add_parser(self, sub_parser):
        self.parser = sub_parser.add_parser(self.command, help=self.help, parents=self.parents)
        self.parser.set_defaults(run=self.run)
        return self.parser

    def add_arguments(self):
        pass

    def run(self, args):
        raise NotImplementedError('Command parser {0} has not been implemented!'.format(self.__class__.__name__))


class IUWMInputListCommand(IUWMCommand):
    """
    Defines how to list inputs from the command line
    """

    command = 'list_inputs'
    help = 'Lists inputs and descriptions'

    def add_arguments(self):
        self.parser.add_argument('--desc', action='store_true', help='List inputs with verbose descriptions')

    def run(self, args):

        # get input parameters
        sorted_keys = inputs.PARAMETERS.keys()

        # list header and variable names/descriptions
        input_desc = [inputs.DataInput.desc_header]
        if args.desc:
            input_desc.extend([str(k) for k in sorted_keys])
        else:
            input_desc.extend([str(inputs.PARAMETERS[k]) for k in sorted_keys])

        # add headers
        print('\nInputs\n  {0}'.format('\n  '.join(input_desc)))


class IUWMEventListCommand(IUWMCommand):
    """
    Defines how to list events from the command line
    """

    command = 'list_events'
    help = 'Lists events in the model that allow customization throughout the model simulation'

    def run(self, args):
        print('\nEvents\n  {0}'.format('\n  '.join(constants.EVENTS)))


class IUWMRunCommand(IUWMCommand):
    """
    Defines how to run IUWM from the command line
    """

    command = 'run'
    help = 'Simulates a single IUWM model scenario'
    parents = [MODEL_PARSER]

    def run(self, args):

        # arguments to build IUWM
        kwargs = get_kwargs(args)

        # run IUWM, checking for errors
        if not args.write_possible_outputs and not args.output:

            msg = 'ERROR: Must specify at least one output (--output OR --output_dir OR --write_possible_outputs)!\n'
            sys.stderr.write(msg)

        else:

            error = model.run_dict(kwargs)
            if error:
                sys.stderr.write(error)


class IUWMBatchCommand(IUWMCommand):
    """
    Defines how to perform batch runs with IUWM from the command line
    """

    command = 'batch'
    help = 'Performs batch runs of IUWM model scenarios'
    parents = [BATCH_PARSER]

    def add_arguments(self):
        # batch runs of IUWM
        self.parser.add_argument('param_file', type=str,
                                 help='Parameter file containing sets of parameters to run. If "geoid" column is '
                                      'present, a column named "scenario" must be present to specify which sets of '
                                      'parameters to run together.')

    def run(self, args):

        kwargs = get_kwargs(args)

        # run a bunch of different sets of parameters
        params = model.read_param_input(args.param_file, service_areas=args.service_areas)

        br = batch.BatchRunner(
            kwargs,
            params,
            args.output,
            parallel=args.parallel,
            n_processes=args.n_processes,
            verbose=args.verbose,
        )
        br.run()


class IUWMSensitivityCommand(IUWMCommand):
    """
    Defines how to perform sensitivity analysis on IUWM parameters from the command line
    """

    command = 'sensitivity'
    help = 'Assesses sensitivity on IUWM model parameters'
    parents = [SENSITIVITY_PARSER]

    def run(self, args):

        # arguments to build IUWM
        kwargs = get_kwargs(args)

        sensitivity.run(
            kwargs,
            args.param_bounds_file,
            args.output,
            n_runs=args.n_runs,
            parallel=args.parallel,
            n_processes=args.n_processes,
            calc_second_order=args.calc_second_order,
            verbose=args.verbose,
        )


class IUWMSensitivityIndicesCommand(IUWMCommand):
    """
    Defines how to calculate sensitivity indices from a results file created from an IUWM sensitivity analysis
    """

    command = 'sensitivity_indices'
    help = 'Calculates sensitivity indices on IUWM model parameters'
    parents = [RESULTS_PARSER, SENSITIVITY_PARSER]

    def run(self, args):
        sensitivity.analyze(
            args.results_file,
            args.param_bounds_file,
            calc_second_order=args.calc_second_order,
            n_processes=args.n_processes,
        )


class IUWMCalibrationCommand(IUWMCommand):
    """
    Defines how to perform automated calibration on IUWM parameters from the command line
    """

    command = 'calibrate'
    help = 'Performs automated calibration on IUWM parameters'
    parents = [RESULTS_PARSER]

    def add_arguments(self):
        self.parser.add_argument('--metric', type=str, default=constants.DEFAULT_CALIBRATION_METRIC,
                                 help='Metric to use in calculation')
        self.parser.add_argument('-v', '--verbose', action='store_true', help='Print out information when running')

    def run(self, args):
        calibration.from_sensitivity(
            args.results_file,
            metric=args.metric,
            verbose=args.verbose
        )


COMMAND_CLASSES = [
    IUWMInputListCommand,
    IUWMEventListCommand,
    IUWMRunCommand,
    IUWMBatchCommand,
    IUWMSensitivityCommand,
    IUWMCalibrationCommand,
]


def main():

    # works when freezing with py2exe
    freeze_support()

    # arguments for simulating
    parser = argparse.ArgumentParser(description='Run the Integrated Urban Water Management Model (IUWM).')

    # add sub parsers for commands
    sp = parser.add_subparsers(title='Commands')

    # list of commands to use parsers from
    for cls in COMMAND_CLASSES:
        cmd = cls()
        cmd.add_parser(sp)
        cmd.add_arguments()

    # arguments
    args = parser.parse_args()
    args.run(args)
