# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import numpy as np

import constants
import summing
from components import IUWMObject


class IUWMModelData(IUWMObject):
    def __init__(self, name, inputs, user_output, leak_percent, leak_consumed, leak_gray, leak_black):
        IUWMObject.__init__(self, name)
        self.inputs = inputs
        self.user = user_output

        self.leak_percent = leak_percent
        self.leak_consumed = leak_consumed
        self.leak_gray = leak_gray
        self.leak_black = leak_black

    def __getattr__(self, name):

        if name in summing.SUMMING_FUNCTIONS:
            f = summing.SUMMING_FUNCTIONS[name]
            return f([self.indoor, self.outdoor, self.cii], self.leak_percent, self.leak_consumed, self.leak_gray,
                     self.leak_black)

        elif name in summing.RESIDENTIAL_SUMMING_FUNCTIONS:
            f = summing.RESIDENTIAL_SUMMING_FUNCTIONS[name]
            return f([self.indoor, self.outdoor], self.leak_percent, self.leak_consumed, self.leak_gray,
                     self.leak_black)

        elif name in summing.TOTAL_REUSE_SUMMING_FUNCTIONS:
            f = summing.TOTAL_REUSE_SUMMING_FUNCTIONS[name]
            return f([self[name] for name in constants.SOURCE_TYPES.values()])

        else:
            raise AttributeError('Attribute {0} does not exist in "{1}"!'.format(name, self.name))

    def items(self):
        return list(self.__dict__.items()) + \
               [(k, self.__getattr__(k)) for k in summing.SUMMING_FUNCTIONS] + \
               [(k, self.__getattr__(k)) for k in summing.RESIDENTIAL_SUMMING_FUNCTIONS] + \
               [(k, self.__getattr__(k)) for k in summing.TOTAL_REUSE_SUMMING_FUNCTIONS]

    def check_mass_balance(self):

        total_use = self.total_use
        co = self.consumed
        nco = self.nonconsumed
        bp = self.black_produced
        gp = self.gray_produced
        dem = self.demand
        reu = self.reused

        checks = [
            total_use - (co + nco),
            total_use - (co + bp + gp),
            total_use - (dem + reu),
        ]

        for i, curr_check in enumerate(checks):
            check_val = np.abs(curr_check)
            pos_i = total_use > 0
            check_val[pos_i] /= total_use[pos_i]
            if np.array(check_val > constants.TOLERANCE).any():
                msg = 'Failed mass balance check {0} by {1} in {2}!'.format(i, np.abs(curr_check).max(), self.name)
                raise AssertionError(msg.format(i, np.abs(curr_check).max(), self.name))

    def checks(self):

        # check nans
        demand = self.demand
        nans = np.isnan(demand)
        if nans.any():
            rows = [str(r) for r in np.where(nans)[0]]
            raise ValueError('NaNs found in the following rows!\n  {0}'.format('\n  '.join(rows)))

        # check reuse above available water
        reused_storm = self.stormwater.reused
        if np.any(reused_storm > self.outdoor.stormwater.gallons + self.stormwater.capacity + 0.1):
            msg = 'Reused more stormwater than is available!\nReused: {0}\nStormwater: {1}\nCapacity: {2}'
            raise Exception(msg.format(reused_storm, self.outdoor.stormwater, self.stormwater.capacity))

        # check mass balance
        self.check_mass_balance()
