# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
from collections import OrderedDict
import performance as perf
import constants
import csv
import matplotlib

if constants.SILENT_PLOTTING:
    matplotlib.use("Agg")  # Force matplotlib to not use any Xwindows backend.
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import scipy.stats as sp
import sys
import time


def optional_bool(v, default=None):
    if v is None or v == "":
        return default
    return v.lower() == "true"


def wait_until_file_is_closed(file_path, max_tries=3600):

    for i in range(max_tries):
        try:
            with open(file_path, "w"):
                pass
            if i != 0:
                print("\n")
            return
        except IOError:
            if i == 0:
                print("\nCould not open file {0}!".format(file_path))
            sys.stdout.write("\rAttempt {0} out of {1}...".format(i + 1, max_tries))
            time.sleep(1)


def boxcox_inv(y, lamb):
    if lamb == 0:
        return np.exp(y)
    else:
        return np.exp(np.log(lamb * y + 1) / lamb)


def get_time_from_variable(var_name):
    t = re.findall("t-(\d+)$", var_name.lower().strip())
    if t:
        return int(t[0])
    return 0


def get_bootstrapped_coefficients(bootstrapped_models):
    return np.array([m.w for m in bootstrapped_models])


def get_bootstrapped_y_percentile(bootstrapped_models, q, orig_x):
    a = np.array([m.predict(orig_x).data[:, 0] for m in bootstrapped_models])
    return np.percentile(a, q, axis=0)


def save_summary_info(out_file, info):
    if not info:
        return

    info_list = info
    if not isinstance(info, list) and not isinstance(info, tuple):
        info_list = [info]

    headers = []
    headers_set = set()
    for i in info_list:
        for k in i.keys():
            if k not in headers_set:
                headers_set.add(k)
                headers.append(k)

    with open(out_file, "wb") as f:
        cw = csv.DictWriter(f, headers)
        cw.writeheader()
        for i in info_list:
            cw.writerow(i)


class RegressionData:
    def __init__(
        self,
        variables,
        data,
        mean=None,
        std=None,
        transformations=None,
        transformations_applied=False,
        transform_params=None,
    ):

        self.n_samples = data.shape[0]
        self.n_vars = len(variables)

        if self.n_vars != data.shape[1]:
            raise ValueError(
                "Data needs to have the same number of columns as variables."
            )
        if self.n_vars != len(np.unique(variables)):
            raise ValueError("Found duplicates. Variable names must be unique!")
        if transformations is None:
            transformations = [None for _ in variables]
        if self.n_vars != len(transformations):
            raise ValueError(
                "Must have the same number of transformations as variables!"
            )
        if transform_params is None:
            transform_params = {}

        self.transformations = transformations
        self.transformations_applied = transformations_applied
        self.transform_params = transform_params
        self.variables = variables
        self.data = data

        self.shape = data.shape

        self.var_cols = {v: i for v, i in zip(variables, range(self.n_vars))}

        if mean is None:
            mean = data.mean(axis=0)
        if std is None:
            std = data.std(axis=0)
        self.mean = mean
        self.std = std

    def __getitem__(self, var_name):
        i = self.var_cols[var_name]
        return self.data[:, i]

    def __setitem__(self, var_name, data):
        i = self.var_cols[var_name]
        self.data[:, i] = data

    def __contains__(self, var_name):
        return var_name in self.var_cols

    def copy(self):

        v = self.variables
        d = np.copy(self.data)
        t = self.transformations
        m = self.mean
        s = self.std
        ta = self.transformations_applied
        tp = self.transform_params
        return RegressionData(
            v,
            d,
            mean=m,
            std=s,
            transformations=t,
            transformations_applied=ta,
            transform_params=tp,
        )

    def append(self, new_variables=None, new_data=None, transformations=None):

        v = self.variables
        d = np.copy(self.data)
        t = self.transformations
        m = self.mean
        s = self.std
        ta = self.transformations_applied
        tp = self.transform_params

        if new_variables is not None:

            for new_var in new_variables:
                if new_var in v:
                    v[v.index(new_var)] = new_var + "_orig"
            v += new_variables
            d = np.hstack((d, new_data))
            if transformations is None:
                transformations = [None for _ in new_variables]
            t += transformations
            m = np.hstack((m, new_data.mean(axis=0)))
            s = np.hstack((s, new_data.std(axis=0)))

        return RegressionData(
            v,
            d,
            mean=m,
            std=s,
            transformations=t,
            transformations_applied=ta,
            transform_params=tp,
        )

    def organize(self, time_shift=0, reverse=False):

        time_shift = abs(time_shift)
        if "t" in self.var_cols and time_shift > 0:

            if "id" not in self.var_cols:
                print(
                    'Warning: A field named "id" should be in the regression dataset to be able to organize data '
                    "by time without using the given ordering!"
                )
                i = np.argsort(self["t"])
            else:
                # sort by id
                i = np.lexsort((self["id"], self["t"]))

            # always make sure that data is sorted
            self.data = self.data[i]

            # copy data for reversing if need be
            data = np.copy(self.data)
            t_data = self["t"]
            t_suffix = "_t-{0}"

            # reverse if wanted
            if reverse:
                t_dir = -1
                t_data = t_data[::-1]
                data = data[::-1]
            else:
                t_dir = 1

            t_unique = sorted(list(set(t_data)), reverse=reverse)

            out_data = [data[t_dir * t_data >= t_dir * t_unique[time_shift]]]
            variables = [v for v in self.variables]
            transformations = [t for t in self.transformations]
            transform_params = self.transform_params.copy()
            for i in range(time_shift):
                variables.extend(v + t_suffix.format(i + 1) for v in self.variables)
                transformations.extend([t for t in self.transformations])
                transform_params.update(
                    {
                        k + t_suffix.format(i + 1): v
                        for k, v in self.transform_params.items()
                    }
                )
                out_data.append(
                    data[
                        np.logical_and(
                            t_dir * t_unique[i] <= t_dir * t_data,
                            t_dir * t_data < t_dir * t_unique[i - time_shift],
                        )
                    ]
                )
            out_data = np.concatenate(out_data, axis=1)

            return RegressionData(
                variables,
                out_data,
                transformations=transformations,
                transformations_applied=self.transformations_applied,
                transform_params=self.transform_params,
            )
        return self

    def filter(self, i=None, keep_stats=False):
        if i is None:
            return self
        return self.new_data(self.data[i], keep_stats=keep_stats)

    def nonnan_i(self):
        return ~np.isnan(self.data).any(axis=1)

    def filter_nans(self, keep_stats=False):
        i = self.nonnan_i()
        return i, self.filter(i, keep_stats=keep_stats)

    def new(self, variables, transformations=None):

        new_data = np.concatenate(
            [
                self.data[:, i : i + 1] if i >= 0 else np.ones((self.n_samples, 1))
                for i in [
                    self.var_cols[v] if v != "intercept" else -1 for v in variables
                ]
            ],
            axis=1,
        )
        return RegressionData(variables, new_data, transformations=transformations)

    def new_data(self, data, keep_stats=False):
        return RegressionData(
            self.variables,
            data,
            mean=self.mean if keep_stats else None,
            std=self.std if keep_stats else None,
            transformations=self.transformations,
            transformations_applied=self.transformations_applied,
            transform_params=self.transform_params,
        )

    def transform(self):
        if self.transformations_applied:
            return self

        out_data = np.zeros((self.n_samples, self.n_vars))
        transform_params = {}
        for i, v, f in zip(range(self.n_vars), self.variables, self.transformations):
            if f:
                if f == "boxcox":
                    new_x, lamb = sp.boxcox(self[v])
                    transform_params[v] = {"lambda": lamb}
                    out_data[:, i] = new_x
                elif f == "log":
                    d = self[v]
                    loc = -1.0 if (d == 0).any() else 0.0
                    transform_params[v] = {"loc": loc}
                    out_data[:, i] = np.log(d - loc)
                elif f == "logit":
                    d = self[v]
                    d[d == 0] = 0.00001
                    d[d == 1] = 0.99999
                    out_data[:, i] = np.log(d / (1 - d))
                else:
                    raise ValueError(
                        "We do not have a transformation called {0}!".format(f)
                    )
            else:
                out_data[:, i] = self[v]

        return RegressionData(
            self.variables,
            out_data,
            transformations=self.transformations,
            transformations_applied=True,
            transform_params=transform_params,
        )

    def untransform(self):
        if not self.transformations_applied:
            return self

        out_data = np.zeros((self.n_samples, self.n_vars))
        for i, v, f in zip(range(self.n_vars), self.variables, self.transformations):
            if f:
                if f == "boxcox":
                    lamb = self.transform_params[v]["lambda"]
                    out_data[:, i] = boxcox_inv(self[v], lamb)
                elif f == "log":
                    loc = self.transform_params[v]["loc"]
                    out_data[:, i] = np.exp(self[v]) + loc
                elif f == "logit":
                    d = np.exp(self[v])
                    out_data[:, i] = d / (d + 1)
                else:
                    raise ValueError(
                        "We do not have a transformation called {0}!".format(f)
                    )
            else:
                out_data[:, i] = self[v]

        return RegressionData(
            self.variables,
            out_data,
            transformations=self.transformations,
            transformations_applied=False,
        )

    def standardize(self):
        i = self.std != 0.0
        sd = np.empty(self.data.shape)
        sd[:, ~i] = self.data[:, ~i]
        sd[:, i] = (self.data[:, i] - self.mean[i]) / self.std[i]
        return self.new_data(sd, keep_stats=True)

    def unstandardize(self):
        i = self.std != 0
        usd = np.empty(self.data.shape)
        usd[:, ~i] = self.data[:, ~i]
        usd[:, i] = self.data[:, i] * self.std[i] + self.mean[i]
        return self.new_data(usd, keep_stats=True)

    def fill(self, y_var, t_data, x, model, predict_historical=False, reverse=False):

        t_data = t_data.data[:, 0]
        t_unique = sorted(list(set(t_data)), reverse=reverse)
        for t_i, t in zip(range(len(t_unique)), t_unique):

            # get the current independent variables and predict with the model
            i = t == t_data
            curr_x = x.filter(i, keep_stats=True)
            y_predict = model.predict(curr_x)

            # place predicted response in this instance
            yf = self[y_var]
            y_pred_data = y_predict.data[:, 0]
            ypd = y_pred_data[::-1] if reverse else y_pred_data
            if predict_historical:
                yf[t == self["t"]] = ypd
            else:
                yft = t == self["t"]
                yfi = np.logical_and(yft, np.isnan(yf))
                yf[yfi] = ypd[np.isnan(yf[yft])]

            # place data in x too if existent
            var_name = y_var + "_t-1"
            if var_name in x and t_i < len(t_unique) - 1:
                xf = x[var_name]
                if predict_historical:
                    xf[t_unique[t_i + 1] == t_data] = y_pred_data
                else:
                    xft = t_unique[t_i + 1] == t_data
                    xfi = np.logical_and(xft, np.isnan(xf))
                    xf[xfi] = y_pred_data[np.isnan(xf[xft])]

    def save(self, out_file, converters=None):
        with open(out_file, "wb") as f:
            cw = csv.writer(f)
            cols = self.variables
            cw.writerow(cols)
            cvt_cols = {}
            if converters:
                cvt_cols = {
                    cols.index(name): converters[name]
                    for name in converters
                    if name in cols
                }
            for row in self.data.tolist():
                for i, cvt in cvt_cols.items():
                    row[i] = cvt(row[i])
                cw.writerow(row)

    @staticmethod
    def from_file(csv_data_file):
        with open(csv_data_file, "r") as f:
            cr = csv.reader(f)
            headers = next(row for row in cr)
            data = np.float64([[v if v else np.nan for v in row] for row in cr])
        return RegressionData(headers, data)


class LinearModel:

    metrics = [
        perf.nsce,
        perf.bias_fraction,
        perf.rmse,
        perf.pwrmse,
        perf.rsq,
        perf.error_mean,
        perf.error_std,
        perf.error_skew,
        perf.mre,
        perf.adj_rsq,
        perf.lillie_test,
        perf.shapiro,
        perf.anderson,
        perf.brown_forsythe,
        perf.durbin_watson,
        perf.aic,
        perf.sbc,
        perf.f_stat,
    ]

    def __init__(self, train_x, train_y, lamb=None, standardize=True):

        self.standardize = standardize
        self.x = train_x
        self.tx = self.x.transform()
        self.sx = self.tx.standardize() if standardize else self.tx
        self.sxd = self.sx.data
        self.sxt = self.sxd.T

        self.y = train_y
        self.ty = self.y.transform()
        self.sy = self.ty.standardize() if standardize else self.ty
        self.syd = self.sy.data

        self.n_samples = self.x.n_samples
        self.n_vars = self.x.n_vars
        self.dfm = self.n_vars - 1 if "intercept" in self.x.variables else self.n_vars
        self.dfe = self.n_samples - self.n_vars

        self.lamb = lamb
        if self.lamb is None:
            self.w, _, self.rank, self.s = np.linalg.lstsq(
                np.dot(self.sxt, self.sxd), np.dot(self.sxt, self.syd)
            )

        else:
            lambda_matrix = lamb * np.eye(self.sxd.shape[1])
            if "intercept" in self.x.variables:
                i = self.x.variables.index("intercept")
                lambda_matrix[i, i] = 0
            self.w, _, self.rank, self.s = np.linalg.lstsq(
                np.dot(self.sxt, self.sxd) + lambda_matrix, np.dot(self.sxt, self.syd)
            )

        self.y_pred = self.predict(self.x)

        modeled = self.y_pred.data[:, 0]
        observed = self.y.data[:, 0]
        self.residuals = perf.residuals(observed, modeled)
        extra = {
            "n_parameters": self.n_vars,
            "has_intercept": "intercept" in self.x.variables,
            "dist": "norm",
        }
        self.perf = OrderedDict(
            [(c.__name__, c(observed, modeled, **extra)) for c in self.metrics]
        )
        self.perf["vif"] = self.vif()
        self.perf["t_stats"] = self.t_stats()

    def vif(self):
        import statsmodels.api as sm

        # calculates variable inflation factors
        if self.n_vars < 2:
            return OrderedDict([])

        def vif_model(x_i):
            x = np.delete(self.x.data, x_i, axis=1)
            y = self.x.data[:, x_i]
            var_name = self.x.variables[x_i]
            x_vars = [v for v in self.x.variables if v != var_name]
            if "intercept" not in x_vars:
                ones = np.ones((x.shape[0], 1))
                x = np.hstack((ones, x))
            m = sm.OLS(y, x)
            results = m.fit()
            return 1 / (1 - results.rsquared)

        return OrderedDict(
            [(self.x.variables[i], vif_model(i)) for i in range(self.n_vars)]
        )

    def t_stats(self):
        r = self.residuals
        s_e = np.sqrt(np.sum(r ** 2) / self.dfe)

        def t_x(x_i):
            x = self.x.data[:, x_i]
            s_xx = np.sum((x - x.mean()) ** 2)
            t = float(abs(self.w[x_i]) * np.sqrt(s_xx) / s_e)
            rv = sp.t(self.n_samples - 2)
            p_value = 1 - rv.cdf(t)
            return t, p_value

        return OrderedDict([(self.x.variables[i], t_x(i)) for i in range(self.n_vars)])

    def predict(self, new_x):
        sx = new_x.transform()
        if self.standardize:
            sx = sx.standardize()
        y_pred = np.dot(sx.data, self.w)
        y = self.ty.new_data(y_pred, keep_stats=True)
        if self.standardize:
            y = y.unstandardize()
        return y.untransform()

    def bootstrap_model(self, new_x=None, n_samples=None):
        if new_x is None:
            new_x = self.tx
        if n_samples is None:
            n_samples = new_x.n_samples

        # sample n_samples from ranks of new_x with replacement (can see repeating numbers)
        ranks = range(new_x.n_samples)
        indices = np.random.choice(ranks, n_samples, True)

        # build new x regression data instance
        x = new_x.filter(indices)

        # build new y regression data instance
        y = self.ty.filter(indices)

        # build new linear model
        return LinearModel(x, y, lamb=self.lamb, standardize=self.standardize)

    def bootstrap(self, n_models, new_x=None, n_samples=None):
        return [
            self.bootstrap_model(new_x=new_x, n_samples=n_samples)
            for _ in xrange(n_models)
        ]

    def bootstrapped_coefficients(self, n_models, new_x=None, n_samples=None):
        return get_bootstrapped_coefficients(
            self.bootstrap(n_models, new_x=new_x, n_samples=n_samples)
        )

    def summary_info(self, bootstrapped_coefficients=None):
        d = OrderedDict()
        d.update(self.perf)
        d["y"] = self.y.variables[0]
        for y_var, mean_val, std_val in zip(self.y.variables, self.y.mean, self.y.std):
            d[y_var + "_mean"] = mean_val
            d[y_var + "_std"] = std_val
        for x_var, weight, mean_val, std_val in zip(
            self.x.variables, self.w.flatten(), self.x.mean, self.x.std
        ):
            d[x_var] = weight
            d[x_var + "_mean"] = mean_val
            d[x_var + "_std"] = std_val
        if self.lamb is not None:
            d["lamb"] = self.lamb
        if bootstrapped_coefficients is not None:
            for x_var, i in zip(self.x.variables, range(self.x.n_vars)):
                weights = bootstrapped_coefficients[:, i]
                d["w_" + x_var + "_mean"] = weights.mean()
                d["w_" + x_var + "_std"] = weights.std()
                d["w_" + x_var + "_min"] = weights.min()
                d["w_" + x_var + "_perc_2.5"] = np.percentile(weights, 2.5)
                d["w_" + x_var + "_perc_5"] = np.percentile(weights, 5)
                d["w_" + x_var + "_perc_25"] = np.percentile(weights, 25)
                d["w_" + x_var + "_median"] = np.percentile(weights, 50)
                d["w_" + x_var + "_perc_75"] = np.percentile(weights, 75)
                d["w_" + x_var + "_perc_95"] = np.percentile(weights, 95)
                d["w_" + x_var + "_perc_97.5"] = np.percentile(weights, 97.5)
                d["w_" + x_var + "_max"] = weights.max()

        return d

    def save_metrics(self, metric_file, bootstrapped_coefficients=None):
        info = self.summary_info(bootstrapped_coefficients=bootstrapped_coefficients)
        wait_until_file_is_closed(metric_file)
        save_summary_info(metric_file, info)
        return info

    def save_residual_plot(self, residual_plot_file):
        plt.figure()
        plt.title(self.y.variables[0])
        plt.xlabel("Observed")
        plt.ylabel("Residuals = Predicted - Observed")
        plt.plot(self.y.data, self.residuals, "ko")
        plt.savefig(residual_plot_file)

    def save_residual_hist_plot(self, residual_plot_file):
        plt.figure()
        plt.title(self.y.variables[0])
        plt.xlabel("Residual")
        plt.ylabel("Count")
        plt.hist(self.residuals)
        plt.savefig(residual_plot_file)

    def save_observed_vs_modeled_plot(self, obs_vs_mod_file, bootstrapped_models=None):
        plt.figure(figsize=(7, 7))
        plt.title(self.y.variables[0])
        plt.xlabel("Observed")
        plt.ylabel("Predicted")

        # plot ranges of output
        if bootstrapped_models:
            low = get_bootstrapped_y_percentile(bootstrapped_models, 2.5, self.x)
            high = get_bootstrapped_y_percentile(bootstrapped_models, 97.5, self.x)
            for i in range(self.y.n_samples):
                plt.plot([self.y.data[i, 0]] * 2, [low[i], high[i]], "r-")

        # plot observed vs predicted
        plt.plot(self.y.data, self.y_pred.data, "ko")

        # plot one-to-one
        min_val = min(self.y.data.min(), self.y_pred.data.min())
        max_val = max(self.y.data.max(), self.y_pred.data.max())
        interval = [min_val, max_val]
        plt.plot(interval, interval, "k-")

        # set bounds
        xlim = plt.xlim()
        ylim = plt.ylim()
        interval = [min(xlim[0], ylim[0]), max(xlim[1], ylim[1])]
        plt.xlim(interval)
        plt.ylim(interval)

        # save
        plt.tight_layout()
        plt.savefig(obs_vs_mod_file)

    def save_coefficient_plot(self, coefficient_plot_file, bootstrapped_coefficients):

        # plotting values
        w_names = self.x.variables
        w_low = np.percentile(bootstrapped_coefficients, 2.5, axis=0)
        w_median = np.percentile(bootstrapped_coefficients, 50, axis=0)
        w_high = np.percentile(bootstrapped_coefficients, 97.5, axis=0)

        # confidence interval
        n = bootstrapped_coefficients.shape[0]
        n_vars = bootstrapped_coefficients.shape[1]
        assert n_vars == len(
            w_names
        ), "Number of coefficients need to match number of variables!"

        # start figure
        plt.figure()
        plt.title(self.y.variables[0])
        plt.xlabel("Weights")
        plt.ylabel("Weight Values")

        # plot coefficient boxes
        for i in range(n_vars):
            x_pos = i + 1
            plt.plot([x_pos] * n, bootstrapped_coefficients[:, i], "ko", alpha=0.3)
            plt.plot([x_pos - 0.2, x_pos + 0.2], [w_low[i]] * 2, "b-", linewidth=1)
            plt.plot([x_pos - 0.3, x_pos + 0.3], [w_median[i]] * 2, "r-", linewidth=1)
            plt.plot([x_pos - 0.2, x_pos + 0.2], [w_high[i]] * 2, "b-", linewidth=1)

        # plot weight == 0
        xlim = [0, n_vars + 1]
        plt.plot(xlim, [0, 0], "k--")
        plt.xlim(xlim)
        plt.xticks(range(1, n_vars + 1), w_names, size="small", rotation=90)

        plt.tight_layout()
        plt.savefig(coefficient_plot_file)

    def save_results(self, output_dir, bootstrapped_models=None):
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # get coefficients
        bc = None
        if bootstrapped_models is not None:
            bc = get_bootstrapped_coefficients(bootstrapped_models)

        metrics_file = os.path.join(output_dir, "metrics.csv")
        info = self.save_metrics(metrics_file, bootstrapped_coefficients=bc)

        if bc is not None:
            self.save_coefficient_plot(os.path.join(output_dir, "coefficients.png"), bc)

        self.save_residual_plot(os.path.join(output_dir, "residuals.png"))
        self.save_residual_hist_plot(os.path.join(output_dir, "residuals_hist.png"))

        self.save_observed_vs_modeled_plot(
            os.path.join(output_dir, "observed_vs_modeled.png"),
            bootstrapped_models=bootstrapped_models,
        )
        plt.close("all")
        return info


class RegressionConfiguration:
    def __init__(
        self,
        scenario,
        y_var,
        x_vars,
        y_transform=None,
        x_transformations=None,
        lamb=None,
        standardize=True,
    ):

        # number of variables s
        self.n_vars = len(x_vars)

        # check
        if x_transformations:
            if self.n_vars != len(x_transformations):
                msg = "Number of variables ({0}) must be the same as the number of functions ({1})!"
                raise ValueError(msg.format(self.n_vars, len(x_transformations)))
        else:
            x_transformations = [None for _ in x_vars]

        # set variables
        self.scenario = scenario
        self.y_var = y_var
        self.y_transform = y_transform
        self.x_vars = x_vars
        self.x_transformations = x_transformations
        self.lamb = lamb
        self.standardize = standardize

    def all_x(self, data):
        return data.new(self.x_vars, self.x_transformations)

    def all_y(self, data):
        return data.new([self.y_var], [self.y_transform])

    def time_shift(self):
        return max([get_time_from_variable(v) for v in self.x_vars])

    def organize_data(
        self,
        data,
        filter_nans=False,
        return_time=False,
        first_nonnan=False,
        reverse=False,
    ):
        t_shift = self.time_shift()
        data = data.organize(t_shift, reverse=reverse)

        x = self.all_x(data)
        y = self.all_y(data)

        if filter_nans or first_nonnan:
            # filter nans from both x and y data
            x_i = x.nonnan_i()
            y_i = y.nonnan_i()

            if first_nonnan:
                i = slice(np.where(np.logical_and(x_i, y_i))[0][0], x.data.shape[0])
            else:
                i = np.logical_and(x_i, y_i)

            x = x.filter(i)
            y = y.filter(i)

        else:
            i = slice(None)

        if return_time:
            t = data.new(["t"])
            t = t.filter(i)
            return x, y, t

        return x, y

    def model(self, data, reverse=False):
        x, y = self.organize_data(data, filter_nans=True, reverse=reverse)
        return LinearModel(x, y, lamb=self.lamb, standardize=self.standardize)

    @staticmethod
    def from_file(config_file):

        with open(config_file, "r") as f:
            cr = csv.DictReader(f)
            headers = cr.fieldnames
            data = [row for row in cr]

        x_all_vars = [
            h
            for h in headers
            if h not in ["scenario", "y", "lambda", "y_transform", "standardize", "run"]
        ]

        configs = []
        for row in data:

            # get variables from csv row
            scenario = row["scenario"]
            y_var = row["y"]
            y_transform = row.get("y_transform", None)
            lamb = row.get("lambda", None)
            standardize = optional_bool(row.get("standardize", None), True)
            run_model = optional_bool(row.get("run", None), True)
            if not run_model:
                continue
            x_vars = [x_var for x_var in x_all_vars if row[x_var]]
            x_transformations = [row[x_var] for x_var in x_vars]
            x_transformations = map(
                lambda x: None if x.lower() == "x" else x, x_transformations
            )

            # build configuration instance
            config = RegressionConfiguration(
                scenario,
                y_var,
                x_vars,
                y_transform=y_transform,
                x_transformations=x_transformations,
                lamb=lamb,
                standardize=standardize,
            )
            configs.append(config)

        return configs


def run_regression(
    output_dir,
    data_file,
    config_file=None,
    y_var=None,
    x_vars=None,
    transformations=None,
    lamb=None,
    n_bootstrap_runs=0,
    predict=None,
    predict_out_file=None,
    predict_historical=False,
    keep_all_columns=False,
    parameters_to_iuwm_inputs=False,
    verbose=False,
):

    if verbose:
        print("Reading regression data...")
    r_data = RegressionData.from_file(data_file)

    if verbose:
        print("Building regression configuration...")
    if config_file is not None:
        configs = RegressionConfiguration.from_file(config_file)

    else:
        if y_var is None or x_vars is None:
            raise ValueError(
                "Must specify either a list of variables (--y_var and --x_vars) at the command line "
                "or a configuration file to perform the regression."
            )
        configs = [
            RegressionConfiguration(
                "", y_var, x_vars, x_transformations=transformations, lamb=lamb
            )
        ]

    info_fs = []
    info_bs = []
    filled_data = r_data.copy()
    for config in configs:

        # build the linear model
        mf = config.model(r_data, reverse=False)
        mb = config.model(r_data, reverse=True)
        if config.scenario:
            if verbose:
                print("Save results for scenario {0}...".format(config.scenario))
            o_dir = os.path.join(output_dir, config.scenario)
            o_dir_b = os.path.join(output_dir, config.scenario + "_backwards")
        else:
            if verbose:
                print("Save results...")
            o_dir = output_dir
            o_dir_b = output_dir + "_backwards"

        # bootstrapping
        bsf = None
        bsb = None
        if n_bootstrap_runs:
            if verbose:
                print("  (estimating uncertainty in coefficients)...")
            bsf = mf.bootstrap(n_bootstrap_runs)
            bsb = mb.bootstrap(n_bootstrap_runs)

        # future predictions
        if predict:

            # organize data for both forward and backward models
            xf, yf, tf_data = config.organize_data(
                filled_data, return_time=True, first_nonnan=True, reverse=False
            )
            xb, yb, tb_data = config.organize_data(
                filled_data, return_time=True, first_nonnan=True, reverse=True
            )

            # go forward in time
            filled_data.fill(
                yf.variables[0],
                tf_data,
                xf,
                mf,
                predict_historical=predict_historical,
                reverse=False,
            )

            # go backward in time
            filled_data.fill(
                yb.variables[0],
                tb_data,
                xb,
                mb,
                predict_historical=predict_historical,
                reverse=True,
            )

        # save results
        info_f = mf.save_results(o_dir, bootstrapped_models=bsf)
        info_b = mb.save_results(o_dir_b, bootstrapped_models=bsb)
        if config.scenario:
            new_info = [("scenario", config.scenario)] + list(info_f.items())
            info_f = OrderedDict(new_info)
            new_info = [("scenario", config.scenario)] + list(info_b.items())
            info_b = OrderedDict(new_info)
        info_fs.append(info_f)
        info_bs.append(info_b)

    if verbose:
        print("Saving summary information...")
    summary_file = os.path.join(output_dir, "summary.csv")
    wait_until_file_is_closed(summary_file)
    save_summary_info(summary_file, info_fs)

    if verbose:
        print("Saving summary information for backward model...")
    summary_file = os.path.join(output_dir, "summary_backward.csv")
    wait_until_file_is_closed(summary_file)
    save_summary_info(summary_file, info_bs)

    if predict:
        print("Saving predicted data...")
        out_file = os.path.join(output_dir, "predicted.csv")

        # get input and output variables of models selected for prediction
        out_vars = []
        if "t" in filled_data and "id" in filled_data:
            out_vars += ["t", "id"]
        for config in configs:
            for x_var in config.x_vars:
                if x_var not in out_vars and get_time_from_variable(x_var) == 0:
                    out_vars.append(x_var)
            if config.y_var not in out_vars:
                out_vars.append(config.y_var)

        # if we want to convert output data to IUWM, convert just special variables
        iuwm_vars = []
        if parameters_to_iuwm_inputs:
            iuwm_data = []

            if "impervious_area" in filled_data:
                percent_impervious = (
                    filled_data["impervious_area"] / filled_data["shape_area"]
                )
                iuwm_vars.append("percent_impervious")
                iuwm_data.append(percent_impervious.reshape((-1, 1)))

            if "area_open" not in out_vars:
                area_open = (
                    filled_data["urban_area"]
                    - filled_data["area_low"]
                    - filled_data["area_med"]
                    - filled_data["area_high"]
                )
                iuwm_vars.append("area_open")
                iuwm_data.append(area_open.reshape((-1, 1)))

            filled_data = filled_data.append(
                iuwm_vars, np.concatenate(iuwm_data, axis=1)
            )

            var_cols = filled_data.var_cols
            if "t" in var_cols and "id" in var_cols:
                filled_data.variables[var_cols["t"]] = "scenario"
                filled_data.variables[var_cols["id"]] = "geoid"
                out_vars[var_cols["t"]] = "scenario"
                out_vars[var_cols["id"]] = "geoid"
                filled_data.var_cols["scenario"] = var_cols.pop("t")
                filled_data.var_cols["geoid"] = var_cols.pop("id")

        # if we do not want to keep all the columns, remove the unimportant ones
        if not keep_all_columns:
            filled_data = filled_data.new(out_vars + iuwm_vars)

        wait_until_file_is_closed(out_file)
        filled_data.save(
            out_file, converters={"t": int, "id": int, "scenario": int, "geoid": int}
        )

        if predict_out_file is not None:
            wait_until_file_is_closed(predict_out_file)
            filled_data.save(
                predict_out_file,
                converters={"t": int, "id": int, "scenario": int, "geoid": int},
            )

    if verbose:
        print("Done!")
    return info_fs


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(
        description="Build and test different regression models."
    )
    parser.add_argument(
        "output_dir", type=str, help="Output directory for regression results."
    )
    parser.add_argument(
        "data_file",
        type=str,
        help="Path to the file containing data to perform regression on. This "
        'data can have a field named "t" to be able to automatically '
        "regress against the previous timestep. Data within timesteps "
        'must either be organized in the same order, or an "id" column '
        "must be provided to match the same data over time.",
    )
    parser.add_argument(
        "-c",
        "--config_file",
        type=str,
        default=None,
        help="Path to csv file containing the configuration of the regression. "
        'The file should contain columns "scenario", "y", names of the independent variables, '
        'and optionally "lambda", which is the value of a term for regularized regression to '
        'push effects of "bad" variables out. '
        'If an intercept is desired put "intercept" in variable column. '
        'Values under "transform" can be null or 1 (assume no '
        "transformation), or "
        "the name of any numpy function used to calculate the desired "
        "transformation of a variable. For example, if you want to take "
        'the log of a variable, put "log" in the "transform" column.',
    )
    parser.add_argument(
        "--y_var",
        type=str,
        default=None,
        help="The response variable of the regression.",
    )
    parser.add_argument(
        "--x_vars",
        type=str,
        nargs="+",
        default=None,
        help="List of variables to be used in the regression. If an intercept is desired, list a "
        'variable named "intercept". This list must be the same length of "--transform" function '
        "names if existent.",
    )
    parser.add_argument(
        "--transformations",
        type=str,
        nargs="+",
        default=None,
        help="List of numpy functions that can be used to transform the data. Must have the same "
        "length as the list of x-variable names (--x_vars). If no transformation is desired for "
        'a particular variable, just put "" as the transform function.',
    )
    parser.add_argument(
        "--percent_train",
        type=float,
        default=100.0,
        help="Specify the percent of input data "
        "that will be randomly selected for "
        "training the models.",
    )
    parser.add_argument(
        "--percent_valid",
        type=float,
        default=100.0,
        help="Specify the percent of input data "
        "that will be randomly selected for "
        "validating the model.",
    )
    parser.add_argument(
        "--lamb",
        type=float,
        default=None,
        help="Lambda value for generalized (or regularized) "
        "regression to shrink model coefficients to zero.",
    )
    parser.add_argument(
        "-n",
        "--n_bootstrap_runs",
        type=int,
        default=0,
        help="Specify the number of bootstrapped runs "
        "for uncertainty estimation in "
        "the resulting coefficients.",
    )

    parser.add_argument(
        "--predict",
        action="store_true",
        default=False,
        help="Predict with the model all the "
        "timesteps in the input dataset "
        '("t" must be included in dataset).',
    )
    parser.add_argument(
        "--predict_out_file",
        type=str,
        default=None,
        help="Additional output file to which to save "
        "predictions (in addition to the file found "
        "in output_dir/predicted.csv).",
    )
    parser.add_argument(
        "--keep_all_columns",
        action="store_true",
        default=False,
        help="Specifies whether to keep all "
        "columns in the predicted "
        "output dataset.",
    )
    parser.add_argument(
        "--predict_historical",
        action="store_true",
        default=False,
        help="Replace historical values "
        "with predicted ones when "
        "running in prediction "
        "mode.",
    )
    parser.add_argument(
        "--parameters_to_iuwm_inputs",
        action="store_true",
        default=False,
        help="Update output model parameters to IUWM inputs",
    )

    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, help="Be verbose!"
    )
    args = parser.parse_args()

    run_regression(
        args.output_dir,
        args.data_file,
        config_file=args.config_file,
        y_var=args.y_var,
        x_vars=args.x_vars,
        transformations=args.transformations,
        lamb=args.lamb,
        n_bootstrap_runs=args.n_bootstrap_runs,
        predict=args.predict,
        predict_out_file=args.predict_out_file,
        predict_historical=args.predict_historical,
        keep_all_columns=args.keep_all_columns,
        parameters_to_iuwm_inputs=args.parameters_to_iuwm_inputs,
        verbose=args.verbose,
    )
