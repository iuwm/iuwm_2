# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import convert
import csv
import itertools
import numpy as np
import os
import re
import version


VARIABLE_WITH_UNITS = re.compile(r"(\w+)\s*(\(|__)(.*)(\)|__)")


def units_from_label(label):
    m = VARIABLE_WITH_UNITS.search(label)
    if m:
        variable = m.group(1).lower().strip()
        units = m.group(3).lower().strip()
    else:
        variable = label.lower().strip()
        units = None
    return variable, units


def parameter_name(p):
    """Removes location information from a parameter to get the parameter name"""
    if "." in p:
        return p.split(".")[-1]
    return p


def standard_name(p, return_units=False):
    p, u = units_from_label(p)
    if return_units:
        return p, u
    return p


def match_field_with_units(fieldnames, data_name, required=False):
    data_name = standard_name(data_name)
    matches = [f for f in fieldnames if standard_name(f).startswith(data_name)]
    if len(matches) == 1:
        return matches[0]
    elif len(matches) > 1:
        msg = 'Provided more than one value for "{0}" (with different units perhaps?)! Must provide only one.'
        raise ValueError(msg.format(data_name))
    elif required:
        raise ValueError(
            'Value for "{0}" not provided, but it is required!'.format(data_name)
        )
    else:
        return None


class DataInput:

    data_type = None
    converter = None
    array_converter = None

    desc_fmt = "{0:<55} {1:<5} {2:<7} {3}{4}"
    desc_header = desc_fmt.format("name", "type", "default", "description", "")

    def __init__(
        self,
        name,
        default,
        description="",
        required=False,
        units_type=None,
        units=None,
        bounds=None,
        aggregation="sum",
    ):

        if self.data_type is None:
            raise ValueError("Must provide data_type!")
        if self.converter is None:
            raise ValueError("Must provide converter!")
        if self.array_converter is None:
            raise ValueError("Must provide array_converter!")

        self.name = standard_name(name)
        self.default = self.convert(default)
        self.description = description
        self.required = required
        self.units_type = units_type
        self.units = units
        self.bounds = bounds
        self.fieldnames = None
        self.field_label = None
        self.aggregation = aggregation

    def convert(self, v):
        return self.converter.__func__(v)

    def check_fieldnames(self, fieldnames):
        """
        Add fieldnames (that come from the data) to this instance to determine if this variable exists in the data

        :param fieldnames: List, set, or dictionary of field names in the data source
        :return: Returns whether or not this variable exists in the dataset
        """
        self.fieldnames = fieldnames
        self.field_label = match_field_with_units(
            fieldnames, self.name, required=self.required
        )
        exists = self.field_label is not None
        return exists

    def convert_from_row(self, row):

        if self.field_label is None:
            self.fieldnames = row.keys()
            self.field_label = match_field_with_units(
                self.fieldnames, self.name, required=self.required
            )
            if self.field_label is None:
                raise ValueError(
                    'Could not find parameter "{0}" in row!'.format(self.name)
                )

        val = self.convert(row[self.field_label])
        if self.units is not None and self.units_type is not None:
            v, u = units_from_label(self.field_label)
            if u is not None:
                cvt = convert.unit_converter(self.units_type, u, self.units)
                return cvt(val)
        return val

    def convert_array(self, v, units=None):
        v_arr = self.array_converter.__func__(v)
        if units is not None and self.units is not None and self.units_type is not None:
            cvt = convert.unit_converter(self.units_type, units, self.units)
            return cvt(v_arr)
        return v_arr

    def default_array(self, n_sites):
        return np.full(n_sites, fill_value=self.default)

    def __repr__(self):
        opt = " (OPTIONAL)" if not self.required else ""
        return self.desc_fmt.format(
            self.name, self.data_type, self.default, self.description, opt
        )


class TextInput(DataInput):
    data_type = "text"
    converter = convert.to_text
    array_converter = convert.to_text_array


class RealInput(DataInput):
    data_type = "real"
    converter = convert.to_real
    array_converter = convert.to_real_array


class BoolInput(DataInput):
    data_type = "bool"
    converter = convert.to_bool
    array_converter = convert.to_bool_array


class IntInput(DataInput):
    data_type = "int"
    converter = convert.to_int
    array_converter = convert.to_int_array


class DateInput(DataInput):
    data_type = "date"
    converter = convert.to_date
    array_converter = convert.to_list


class TextListInput(DataInput):
    data_type = "text_list"
    converter = convert.to_text_list
    array_converter = convert.to_text_array


class RealListInput(DataInput):
    data_type = "real_list"
    converter = convert.to_real_list
    array_converter = convert.to_real_array


class BoolListInput(DataInput):
    data_type = "bool_list"
    converter = convert.to_bool_list
    array_converter = convert.to_bool_array


class IntListInput(DataInput):
    data_type = "int_list"
    converter = convert.to_int_list
    array_converter = convert.to_int_array


class DateListInput(DataInput):
    data_type = "date_list"
    converter = convert.to_date_list
    array_converter = convert.to_list


SELECTOR = {
    "text": TextInput,
    "real": RealInput,
    "bool": BoolInput,
    "int": IntInput,
    "date": DateInput,
    "text_list": TextListInput,
    "real_list": RealListInput,
    "bool_list": BoolListInput,
    "int_list": IntListInput,
    "date_list": DateListInput,
}

SPECIAL_DEFAULTS = {
    "OLD_VERSION": version.OLD,
}


# read input data attribute descriptions
def new(
    name,
    default_type,
    default_value,
    description="",
    required=False,
    units_type=None,
    units=None,
    bounds=None,
    aggregation="sum",
):

    return SELECTOR[default_type](
        name,
        default_value,
        description=description,
        required=required,
        units_type=units_type,
        units=units,
        bounds=bounds,
        aggregation=aggregation,
    )


def default_inputs():
    names = []
    attributes = {}

    with open(os.path.join(constants.DATA_DIR, "input_variables.csv"), "r") as f:
        cr = csv.DictReader(f)
        for row in cr:

            # get variable information
            name = standard_name(row["name"])
            data_type = standard_name(row["type"])
            default_value = row["default"]
            if default_value in SPECIAL_DEFAULTS:
                default_value = SPECIAL_DEFAULTS[default_value]
            input_desc = row["description"]
            agg = row["aggregation"].strip().lower()
            if not agg:
                agg = "sum"
            lower = convert.to_real(row["lower"], default=np.nan)
            upper = convert.to_real(row["upper"], default=np.nan)
            kwargs = {
                "units_type": row["units_type"],
                "units": row["units"],
                "bounds": [lower, upper],
                "required": convert.to_bool(row["required"]),
                "aggregation": agg,
            }

            names.append(name)

            # replace source, reuse, and end_use with specifics
            all_template_values = []
            template_keys = []
            if "{source}" in name or "{source}" in input_desc:
                template_keys.append("source")
                all_template_values.append(constants.SOURCE_TYPES)
            if "{reuse}" in name or "{reuse}" in input_desc:
                template_keys.append("reuse")
                all_template_values.append(constants.REUSE_CONTAINERS)
            if "{end_use}" in name or "{end_use}" in input_desc:
                template_keys.append("end_use")
                all_template_values.append(constants.INDOOR_END_USES)
            if "{land_use}" in name or "{land_use}" in input_desc:
                template_keys.append("land_use")
                all_template_values.append(constants.NLCD_CLASSES)

            # build list of attributes
            for template_values in itertools.product(*all_template_values):
                specs = {k: v for k, v in zip(template_keys, template_values)}
                nm = name.format(**specs)
                desc = input_desc.format(**specs)
                attributes[nm] = new(
                    nm, data_type, default_value, description=desc, **kwargs
                )

    return names, attributes


PARAMETER_RAW_NAMES, PARAMETERS = default_inputs()
PARAMETER_NAMES = set(PARAMETERS.keys())


def get(name, default_type=None, default_value=None, **kwargs):
    name = standard_name(name)
    if name in PARAMETERS:
        return PARAMETERS[name]
    else:
        if default_type is None or default_value is None:
            raise ValueError(
                "Default type and value need to be provided when input variable is not provided!"
            )
        d = new(name, default_type, default_value, **kwargs)
        PARAMETERS[name] = d
        return d


def get_aggregations():
    return {k: v.aggregation for k, v in PARAMETERS.items()}


AGGREGATIONS = get_aggregations()


def get_aggregation(name):
    name = standard_name(name)
    name = parameter_name(name)
    if name in AGGREGATIONS:
        return AGGREGATIONS[name]
    else:
        return None


def create_weather_field_definitions():
    return {
        "temp_min": get(
            "temp_min",
            "real",
            0.0,
            description="Minimum temperature (deg. C)",
            required=True,
            units_type="temperature",
            units="deg C",
        ),
        "temp_max": get(
            "temp_max",
            "real",
            0.0,
            description="Maximum temperature (deg. C)",
            required=True,
            units_type="temperature",
            units="deg C",
        ),
        "temp_avg": get(
            "temp_avg",
            "real",
            0.0,
            description="Average temperature (deg. C)",
            required=False,
            units_type="temperature",
            units="deg C",
        ),
        "temp_dew_avg": get(
            "temp_avg",
            "real",
            0.0,
            description="Average dew point temperature (deg. C)",
            required=False,
            units_type="temperature",
            units="deg C",
        ),
        "precipitation": get(
            "precipitation",
            "real",
            0.0,
            description="Precipitation (mm)",
            required=True,
            units_type="length",
            units="mm",
        ),
        "solar_rad": get(
            "solar_rad",
            "real",
            0.0,
            description="Solar radiation (MJ/m^2/day)",
            required=False,
            units_type="radiation",
            units="MJ/m^2/day",
        ),
        "rel_hum_max": get(
            "rel_hum_max",
            "real",
            0.0,
            description="Daily maximum relative humidity (%)",
            required=False,
            units_type="factor",
            units="%",
        ),
        "rel_hum_min": get(
            "rel_hum_min",
            "real",
            0.0,
            description="Daily minimum relative humidity (%)",
            required=False,
            units_type="factor",
            units="%",
        ),
        "rel_hum_avg": get(
            "rel_hum_avg",
            "real",
            0.0,
            description="Daily mean relative humidity (%)",
            required=False,
            units_type="factor",
            units="%",
        ),
        "wind_speed": get(
            "wind_speed",
            "real",
            0.0,
            description="Wind speed (m/s)",
            required=False,
            units_type="velocity",
            units="m/s",
        ),
        "east_wind": get(
            "east_wind",
            "real",
            0.0,
            description="Eastward wind speed (m/s)",
            required=False,
            units_type="velocity",
            units="m/s",
        ),
        "north_wind": get(
            "north_wind",
            "real",
            0.0,
            description="Northward wind speed (m/s)",
            required=False,
            units_type="velocity",
            units="m/s",
        ),
    }


WEATHER = create_weather_field_definitions()
