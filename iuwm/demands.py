# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import numpy as np

import constants
import summing
import utils
from components import IUWMObject


class UrbanDemand(IUWMObject):
    def __init__(self, name, demand, consumed=0.0, gray=0.0, black=1.0, min_mixing_fraction=0.0):

        IUWMObject.__init__(self, name)

        # round values to 8 decimal places
        consumed = utils.ensure_fraction(consumed)
        gray = utils.ensure_fraction(gray)
        black = utils.ensure_fraction(black)

        # checks
        if np.any(np.abs(gray + black + consumed - 1) > constants.TOLERANCE):
            raise ValueError('Fractions gray, black, and lost (consumed) must sum to 1!')

        # static
        self.total_use = demand
        self.fraction_consumed = consumed
        self.fraction_gray = gray
        self.fraction_black = black
        self.min_mixing_fraction = min_mixing_fraction
        self.min_demand = self.min_mixing_fraction * self.total_use
        self.max_reuse = self.total_use - self.min_demand

        # consumed portion
        self.consumed = self.fraction_consumed * self.total_use
        self.nonconsumed = self.total_use - self.consumed

        # effluents
        self.gray_produced = self.fraction_gray * self.total_use
        self.black_produced = self.fraction_black * self.total_use

        # dynamic
        self.demand = self.total_use

    def advance(self):

        self.demand = self.total_use

    def reuse(self, available_water):
        prev_demand = self.demand
        self.demand = np.maximum(self.demand - available_water, self.min_demand)
        reused = prev_demand - self.demand
        return reused

    @classmethod
    def new(cls, data):
        if isinstance(data, dict):
            return cls(**data)
        elif isinstance(data, cls):
            return data
        else:
            raise ValueError('Cannot create {1} with data of type {0}'.format(type(data), cls.__name__))


class UrbanDemandCenter(IUWMObject):

    def __init__(self, name, *urban_demands):
        IUWMObject.__init__(self, name)

        # fraction  of total_use and demand that is leakage (consumed)
        self.leak_fraction = 0.0
        self.leak_consumed = 1.0
        self.leak_gray = 0.0
        self.leak_black = 0.0

        # build sub components of this urban demand center
        self.components = []
        for ud in urban_demands:
            self.add(ud)

    def leaky(self, leak_fraction, consumed=1.0, gray=0.0, black=0.0):
        self.leak_fraction = leak_fraction

        summing.check_fractions(consumed, gray, black)
        self.leak_consumed = consumed
        self.leak_gray = gray
        self.leak_black = black

        return self

    def __getattr__(self, name):
        if name in summing.SUMMING_FUNCTIONS:
            f = summing.SUMMING_FUNCTIONS[name]
            return f(self.components, self.leak_fraction, self.leak_consumed, self.leak_gray, self.leak_black)
        else:
            raise AttributeError('Attribute {0} does not exist in "{1}"!'.format(name, self.name))

    def add(self, demand):

        if isinstance(demand, dict):
            demand = UrbanDemand.new(**demand)
            self.components.append(demand)

        elif issubclass(demand.__class__, UrbanDemand):
            self.components.append(demand)

        elif issubclass(demand.__class__, UrbanDemandCenter):
            self.components.append(demand)

        elif demand.__class__.__name__ == 'UrbanDemand':
            self.components.append(demand)

        elif demand.__class__.__name__ == 'UrbanDemandCenter':
            self.components.append(demand)

        if hasattr(demand, 'name'):
            self[demand.name] = demand

    def items(self):
        return list(self.__dict__.items()) + [(k, self.__getattr__(k)) for k in summing.SUMMING_FUNCTIONS]

    def advance(self):
        for u in self.components:
            u.advance()

    def reuse(self, water_available):

        # get the nonzero potable demand
        demand = self.demand
        nonzero = demand > 0

        # check that we don't have more alternative water available than demand
        if (water_available > demand).any():
            raise ValueError('Cannot take reuse more alternative source water than is demanded!')

        # calculate the fraction of available alternative water to potable demand
        frac = np.zeros(demand.shape)
        frac[nonzero] = water_available[nonzero] / demand[nonzero]
        reductions = [frac * u.demand for u in self.components]
        total_reused = np.zeros(demand.shape)
        for u, r in zip(self.components, reductions):
            total_reused += u.reuse(r)
        return total_reused


class Irrigation(UrbanDemand):
    def __init__(self, name, pet, aet, effective_precip, net_irr_req, gross_irr_req, aet_met,  # theoretical
                 irr_depth, theoretical_depth, applied_depth, irr_area, irr):  # actual

        UrbanDemand.__init__(self, name, irr, consumed=1.0, black=0.0, gray=0.0)

        self.potential_et = pet  # potential ET in inches
        self.actual_et = aet  # actual ET in inches
        self.effective_precip = effective_precip  # effective precipitation in inches
        self.actual_et_met = aet_met  # target ET that is met by the residents in inches
        self.net_req_inches = net_irr_req  # net irrigation requirement in inches
        self.gross_req_inches = gross_irr_req  # gross irrigation requirement in inches
        self.effective = irr_depth  # effective irrigation amount in inches
        self.theoretical = theoretical_depth  # theoretical irrigation application
        self.inches = applied_depth  # irrigation amount in inches (actual not theoretical)
        self.gallons = irr  # irrigation amount in gallons
        self.area = irr_area  # total irrigated area in gallons
