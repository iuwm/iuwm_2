# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import constants
import numpy as np
import utils


def check_fractions(consumed, gray, black):
    if np.any(np.abs(consumed + gray + black - 1) > constants.TOLERANCE):
        raise ValueError('Fractions of consumed, gray, and black must equal 1!')


def sum_total_use(demands, leak_fraction, *args):
    return sum(u.total_use for u in demands) / (1.0 - leak_fraction)


def sum_demand(demands, leak_fraction, *args):
    leakage = sum_leakage(demands, leak_fraction)
    return sum(u.demand for u in demands) + leakage


def sum_min_demand(demands, leak_fraction, *args):
    leakage = sum_leakage(demands, leak_fraction)
    return sum(u.min_demand for u in demands) + leakage


def sum_max_reuse(demands, leak_fraction, *args):
    total_use = sum_total_use(demands, leak_fraction)
    min_demand = sum_min_demand(demands, leak_fraction)
    return total_use - min_demand


def sum_gray_produced(demands, leak_fraction, consumed, gray, black):
    leakage = sum_leakage(demands, leak_fraction)
    gray_produced = sum(u.gray_produced for u in demands)
    return gray_produced + gray * leakage


def sum_black_produced(demands, leak_fraction, consumed, gray, black):
    leakage = sum_leakage(demands, leak_fraction)
    black_produced = sum(u.black_produced for u in demands)
    return black_produced + black * leakage


def sum_consumed(demands, leak_fraction, leak_consumed, gray, black):
    leakage = sum_leakage(demands, leak_fraction)
    consumed = sum(u.consumed for u in demands)
    return consumed + leak_consumed * leakage


def sum_nonconsumed(demands, leak_fraction, leak_consumed, gray, black):
    leakage = sum_leakage(demands, leak_fraction)
    nonconsumed = sum(u.nonconsumed for u in demands)
    return nonconsumed + (1 - leak_consumed) * leakage


def sum_leakage(demands, leak_fraction, *args):
    return leak_fraction * sum_total_use(demands, leak_fraction)


# summing functions
SUMMING_FUNCTIONS = {
    'total_use': sum_total_use,
    'demand': sum_demand,
    'gray_produced': sum_gray_produced,
    'black_produced': sum_black_produced,
    'consumed': sum_consumed,
    'nonconsumed': sum_nonconsumed,
    'leakage': sum_leakage,
    'min_demand': sum_min_demand,
    'max_reuse': sum_max_reuse,
}

RESIDENTIAL_SUMMING_FUNCTIONS = {
    'residential_use': sum_total_use,
    'residential_demand': sum_demand,
}

TOTAL_REUSE_SUMMING_FUNCTIONS = {
    'capacity': lambda a: sum(u.capacity for u in a),
    'storage': lambda a: sum(u.storage for u in a),
    'spill': lambda a: sum(u.spill for u in a),
    'reused': lambda a: sum(u.reused for u in a),
    'change_in_storage': lambda a: sum(u.change_in_storage for u in a),
}

REUSE_SUMMING_FUNCTIONS = utils.merge_dicts(TOTAL_REUSE_SUMMING_FUNCTIONS, {
    'adoption': lambda a: sum(u.adoption for u in a),
})

