@echo off 
set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests

if ["%~1"]==[""] ( 
	set n_proc=1
) else ( 
	set n_proc=%~1
) 

REM Remove possible_outputs to reset it before tests...
del ..\iuwm\data\possible_outputs.txt 2>nul

REM Run tests...
python test.py sensitivity --tolerance 6e-8 --n_proc %n_proc%
python test.py calibrate --tolerance 6e-8 --n_proc %n_proc%
python test.py run --tolerance 6e-8 --n_proc %n_proc%
python test.py batch --tolerance 6e-8 --n_proc %n_proc%

pause 
