@echo off 
set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests

REM Remove possible_outputs to reset it before tests...
del ..\iuwm\data\possible_outputs.txt 2>nul

REM Run tests...
python test.py sensitivity --tolerance 6e-8 --verbose
python test.py calibrate --tolerance 6e-8 --verbose
python test.py run --tolerance 6e-8 --verbose
python test.py batch --tolerance 6e-8 --verbose

pause 