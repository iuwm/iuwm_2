setlocal
:PROMPT
SET /P ans=Are you sure you want to reset (y/[n])?
IF /I "%ans%" NEQ "y" GOTO END

set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests
python test.py reset
pause 

:END 
endlocal