@echo off 

REM python iuwm\manipulator.py update tests\files --var_names version --values 3.0.0 --pattern *.csv --columns 0

set PYTHONPATH=%~dp0\..:%PYTHONPATH%
cd ..
python iuwm\manipulator.py update tests\files --var_names et_method --values 2 --skip "(^weather.*|_ts.csv)"

pause 

