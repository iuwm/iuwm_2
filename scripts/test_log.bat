@echo off 
set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests
python test.py run --tolerance 6e-8 > test.log 2>&1
pause 