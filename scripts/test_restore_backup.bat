setlocal
:PROMPT
SET /P ans=Are you sure you want to restore backed up files (y/[n])?
IF /I "%ans%" NEQ "y" GOTO END

set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests
python test.py restore_backup
pause 

:END 
endlocal