@echo off 
set PYTHONPATH=%~dp0\..;%PYTHONPATH%
cd ..\tests
python test.py calibrate --tolerance 6e-8 --verbose --just_print
pause 