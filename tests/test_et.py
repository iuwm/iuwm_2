import unittest
from datetime import datetime
from iuwm import et
import numpy as np


class ETTestCase(unittest.TestCase):

    def test_et(self):

        d = datetime(2015, 7, 6)
        lon = np.float64([4.36], ndmin=1)
        lat = np.float64([50 + 48 / 60.0], ndmin=1)

        pet = et.penman_monteith(d, lon, lat, 12.3, 21.5,
                                 rel_hum_min=63, rel_hum_max=84, wind_speed=2.078, solar_rad=22.07, elevation=100)
        self.assertAlmostEqual(pet, 3.8795, places=3)

        pet = et.penman_monteith(d, lon, lat, 12.3, 21.5,
                                 rel_hum_min=63, rel_hum_max=84, wind_speed=2.078, n_sunshine=9.25, elevation=100)
        self.assertAlmostEqual(pet, 3.8798, places=3)

        pet = et.penman_monteith(d, lon, lat, 12.3, 21.5,
                                 rel_hum_min=63, rel_hum_max=84, wind_speed=2.078, elevation=100)
        self.assertAlmostEqual(pet, 3.7851, places=3)

        pet = et.hargreaves(d, lat, 12.3, 21.5, solar_rad=22.07)
        self.assertAlmostEqual(pet, 4.2182, places=3)

        pet = et.hargreaves(d, lat, 12.3, 21.5)
        self.assertAlmostEqual(pet, 4.0493, places=3)


if __name__ == '__main__':
    unittest.main()
