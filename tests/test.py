# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #
import argparse
from collections import OrderedDict
import csv
from datetime import datetime
from iuwm import aggregators, calibration, constants, convert, manipulator, console_runner, console_parser, performance
from multiprocessing import freeze_support, Pool
import numpy as np
from io import StringIO
import os
import sys
import time

INPUT_CONVERSIONS = {
    'run': convert.to_bool,
    '__raw__': lambda v: convert.to_list(v, pattern=r'\s+'),
    'write_possible_outputs': convert.to_text,
    'force_download': convert.to_bool,
    'check_level': convert.to_int,
    'start_date': convert.format_date,
    'end_date': convert.format_date,
    'more_variables': convert.to_list,
    'packages': convert.to_list,
    'registered_inputs': convert.to_list,
    'timeseries': convert.to_list,
    'batch_by_geoid': convert.to_bool,
    'obs_var': convert.to_list,
    'iuwm_var': convert.to_list,
    'n_runs': convert.to_int,
}

BATCH_COMMANDS = {'sensitivity', 'batch'}


def get_test_info(tests_csv=None):

    # initialize
    test_info = OrderedDict()

    # read file
    if tests_csv and os.path.exists(tests_csv):
        with open(tests_csv, 'rb') as fh:
            cr = csv.DictReader(fh)
            for row in cr:
                name = row.pop('name')

                # booleans
                for variable, conv in INPUT_CONVERSIONS.items():
                    if variable in row:
                        row[variable] = conv(row[variable])
                        if row[variable] is None:
                            row.pop(variable)

                if 'more_variables' in row:
                    row['output_variables'] = ['defaults'] + row.pop('more_variables')

                # set dictionary
                test_info[name] = row

    return test_info


def read_data(file_string):
    cr = csv.reader(StringIO(unicode(file_string)))
    headers = next(row for row in cr)
    d = [row for row in cr]
    try:
        data = np.array([[row[i] for i, h in enumerate(headers)] for row in d], dtype=np.float64)
    except ValueError:
        data = np.array([row for row in d], dtype=np.string_)
    return headers, data


class IUWMTest:

    def __init__(self, test_name, run=True, tolerance=0, out_dir='output', verbose=False, just_print=False,
                 **test_info):

        # parameters
        self.name = test_name
        self.output_dir = os.path.join(out_dir, self.name)
        self.output_tmp_dir = os.path.join(self.output_dir, 'tmp')
        self.do_run = run
        self.tolerance = tolerance
        self.verbose = verbose
        self.just_print = just_print
        if self.just_print:
            self.verbose = True

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        self.run_info = {t: v for t, v in test_info.items() if str(v).strip()}
        self.run_info['weather_file'] = self.prep_weather_file()
        self.run_info['weather_source'] = self.run_info.pop('weather_source', 'prism')
        self.run_info['output_dir'] = self.output_tmp_dir
        self.run_info['just_print'] = self.just_print
        self.run_info['verbose'] = self.verbose

        self.command = self.run_info['command']
        self.sensitivity = self.run_info['command'] == 'sensitivity'
        self.output = console_parser.parse_for_output(
            self.run_info['__raw__'],
            default=aggregators.OutputDefinitionList([aggregators.OutputDefinition(
                timestep='yearly',
                file='yearly.csv',
                by_geoid=False,
            )]),
        )

        self.output_tmp = self.output.new_directory(self.output_tmp_dir)
        self.output = self.output.new_directory(self.output_dir)

        if self.run_info['command'] in BATCH_COMMANDS:
            self.output_tmp_files = self.output_tmp.results_files(self.output_tmp_dir)
            self.output_files = self.output.results_files(self.output_dir)

        elif self.run_info['command'] == 'calibrate':
            self.output_tmp_files = [calibration.get_output_file(self.output_dir, 'summary.csv', sub='tmp')]
            self.output_files = [calibration.get_output_file(self.output_dir, 'summary.csv')]

        else:
            self.output_tmp_files = self.output_tmp.files()
            self.output_files = self.output.files()

    def prep_weather_file(self):
        weather_file = self.run_info.get('weather_file', '').strip()
        return weather_file

    def test_failed(self, old_data, new_data):

        if old_data is not None and old_data != new_data:

            old_headers, old_nums = read_data(old_data)
            new_headers, new_nums = read_data(new_data)

            if old_headers != new_headers:
                sys.stdout.write('  Headers in new output file do not match old!\n')

            if old_nums.shape != new_nums.shape:
                sys.stderr.write('  Size of outputs has changed!\n')

            else:

                # list the columns that differ from old
                unequal = old_nums != new_nums
                unequal_cols = np.where(np.any(unequal, axis=0))[0]
                unequal_col_names = [old_headers[c] for c in unequal_cols]

                if 'runtime' in unequal_col_names:
                    r_i = unequal_col_names.index('runtime')
                    unequal_col_names.pop(r_i)
                    unequal_cols = np.delete(unequal_cols, r_i, axis=0)
                    if len(unequal_col_names) == 0:
                        # exit quietly if runtime is the only column that differs
                        return False

                # check only columns that differ for magnitude of difference
                ovs = np.array(old_nums[:, unequal_cols]).T  # old values that differ from new
                nvs = np.array(new_nums[:, unequal_cols]).T  # new values that differ from old
                excused = True
                for c, o, n in zip(unequal_col_names, ovs, nvs):
                    try:
                        o = np.float64(o, ndmin=1)
                        n = np.float64(n, ndmin=1)

                        err = np.abs(o - n)
                        o_nz = np.copy(o)
                        o_z_i = o == 0
                        o_nz[o_z_i] = 1
                        rel_err = performance.rel_errors(o, n)
                        # rel_err = np.abs((o - n) / o_nz)
                        # rel_err[np.logical_and(o_z_i, err <= self.tolerance)] = 0
                        # rel_err[np.logical_and(o_z_i, err > self.tolerance)] = 1
                        max_err = err.max()
                        max_rel_err = np.nanmax(np.abs(rel_err))

                        if max_err > self.tolerance and max_rel_err > self.tolerance:
                            if excused:
                                sys.stderr.write('  Columns that differ:\n')
                            msg = '    {0} max error {1:.2E} (rel: {2:.2E})!\n'.format(c, max_err, max_rel_err)
                            sys.stderr.write(msg)
                            excused = False

                    except ValueError:
                        if excused:
                            sys.stderr.write('  Columns that differ:\n')
                        sys.stderr.write('    {0} differs (non-numeric)\n'.format(c))
                        excused = False

                if excused:
                    sys.stdout.write('  Excusing errors, they are less than tolerance {0}\n'.format(self.tolerance))
                    sys.stdout.flush()
                    return False
                
                else:
                    sys.stderr.write('  New output data does not match old!\n')

            msg = '  If this is intended, then delete the output directory:\n    {0}\n\n'
            sys.stderr.write(msg.format(self.output_dir))
            sys.stderr.flush()
            return True
        return False

    def retrieve_output(self, out_file):
        if os.path.exists(out_file):
            with open(out_file, 'r') as fh:
                f_str = fh.read()
            return f_str
        return None

    def run(self):

        start_time = time.time()

        # RUN IUWM
        error_str = console_runner.iuwm_console(**self.run_info)
        if self.just_print:
            return False

        end_time = time.time() - start_time
        msg = 'Tested "{0}"...'.format(self.name)
        status = 'FAILED' if error_str else 'SUCCESS'

        sys.stdout.write('{0:<45s} {1:10.2f} sec... {2}\n'.format(msg, end_time, status))
        sys.stdout.flush()

        if error_str:
            sys.stderr.write(error_str)
            sys.stderr.flush()

        failed = False
        for old_file, new_file in zip(self.output_files, self.output_tmp_files):

            # check to see if the model ran
            # print('old: {0}\nnew:{1}'.format(old_file, new_file))
            if not os.path.exists(new_file):
                sys.stderr.write('  Model did not run properly! There is no output file {0}\n'.format(new_file))
                return True

            # retrieve the data for comparison
            old_data = self.retrieve_output(old_file)
            new_data = self.retrieve_output(new_file)

            if self.test_failed(old_data, new_data):
                # test failed
                failed = True
                with open(new_file, 'w') as fh:
                    fh.write(new_data)

            elif os.path.exists(new_file):
                # test succeeded, remove temporary files
                os.remove(new_file)

            if old_data is None:
                # old data doesn't exist, write it
                with open(old_file, 'w') as fh:
                    fh.write(new_data)

        return failed

    def reset(self):

        print('Resetting "{0}"...'.format(self.name))
        for fp in self.output_files:
            if os.path.exists(fp):
                manipulator.backup(fp)

    def restore_backup(self):

        print('Restoring backup "{0}"...'.format(self.name))
        for fp in self.output_files:
            manipulator.restore_backup(fp)


def run_test(test):
    if test.do_run:
        fail = test.run()
        if fail:
            return True
    return False


def run_tests(tests_file, command=None, model=None, tolerance=None, just_print=False, verbose=False, out_dir='output',
              n_proc=1):

    command = command if command else 'all'

    start_time = datetime.now().strftime('%Y-%m-%d %I:%M %p')
    sys.stdout.write('\n{0:*^79}\n'.format(''))
    sys.stdout.write('{0:45s} {1:>20s}\n'.format('Running "{0}" tests'.format(command), start_time))
    sys.stdout.flush()

    # tests file
    test_information = get_test_info(tests_file)

    # special analyses in parallel with specified number of processes instead
    if command in BATCH_COMMANDS:
        if n_proc > 1:
            for test_info in test_information.values():
                if test_info['command'] in BATCH_COMMANDS:
                    test_info['parallel'] = 'multiprocessing'
                    test_info['n_processes'] = n_proc
            n_proc = 1

    # reset each test
    if model is not None:
        test = IUWMTest(model, tolerance=tolerance, out_dir=out_dir, verbose=True, just_print=just_print,
                        **test_information[model])
        failed = test.run()

    else:
        tests = [IUWMTest(n, tolerance=tolerance, out_dir=out_dir, verbose=verbose or model is not None,
                          just_print=just_print, **d) for n, d in test_information.items()]

        if command != 'all':
            tests = [t for t in tests if t.command == command]

        if n_proc == 1:
            failed = any([run_test(t) for t in tests])
        else:
            p = Pool(n_proc)
            failed = any(p.map(run_test, tests))

    end_time = datetime.now().strftime('%Y-%m-%d %I:%M %p')
    if failed:
        sys.stderr.write('{0:45s} {1:>20s}\n'.format('TESTS FAILED!!!!!', end_time))
    else:
        sys.stdout.write('{0:45s} {1:>20s}\n'.format('Finished successfully!', end_time))
    sys.stdout.write('{0:*^79}\n'.format(''))
    sys.stdout.flush()


class IUWMRestoreBackupCommand(console_parser.IUWMCommand):
    """
    Defines how to backup test outputs from the command line
    """

    command = 'restore_backup'
    help = 'Restores backups for tests'

    def add_arguments(self):
        self.parser.add_argument('--tests', type=str, default='tests.csv', help='Path to the file listing tests.')
        self.parser.add_argument('--out_dir', type=str, default='output', help='Path to directory for test output.')
        self.parser.add_argument('--command', type=str, default='all', help='Command to backup/restore.')

    def run_test(self, test):
        test.restore_backup()

    def run(self, args):

        print('Resetting tests at {0}'.format(datetime.now().strftime('%Y-%m-%d %I:%M %p')))

        # tests file
        test_information = get_test_info(args.tests)

        # reset each test
        for test_name, test_info in test_information.items():
            test = IUWMTest(test_name, out_dir=args.out_dir, **test_info)
            if test.do_run and (args.command == 'all' or args.command == test.command):
                self.run_test(test)


class IUWMResetCommand(IUWMRestoreBackupCommand):
    """
    Defines how to reset test outputs from the command line
    """

    command = 'reset'
    help = 'Resets tests'

    def run_test(self, test):
        test.reset()


class IUWMRunTestCommand(console_parser.IUWMCommand):
    """
    Defines how to run tests
    """

    command = 'run'
    help = 'Test model run'

    def add_arguments(self):
        self.parser.add_argument('--tests', type=str, default='tests.csv', help='Path to the file listing tests.')
        self.parser.add_argument('--out_dir', type=str, default='output',
                                 help='Path to the directory containing test output.')
        self.parser.add_argument('--tolerance', type=float, default=None,
                                 help='Ignore differences less than this tolerance.')
        self.parser.add_argument('--model', type=str, default=None, help='Run one test model by name.')
        self.parser.add_argument('--just_print', action='store_true', help='Just print command lines for each model.')
        self.parser.add_argument('--verbose', action='store_true', help='Be verbose with model output during testing.')
        self.parser.add_argument('--n_proc', type=int, default=1, help='Number of processes to run on.')

    def run(self, args):
        run_tests(args.tests, command=self.command, model=args.model, tolerance=args.tolerance,
                  just_print=args.just_print, verbose=args.verbose, out_dir=args.out_dir, n_proc=args.n_proc)


class IUWMSensitivityTestCommand(IUWMRunTestCommand):
    """
    Defines how to test sensitivity analysis from the command line
    """

    command = 'sensitivity'
    help = 'Test sensitivity'


class IUWMCalibrationTestCommand(IUWMRunTestCommand):
    """
    Defines how to test auto-calibration analysis from the command line
    """

    command = 'calibrate'
    help = 'Test calibration'


class IUWMBatchTestCommand(IUWMRunTestCommand):
    """
    Defines how to test sensitivity analysis from the command line
    """

    command = 'batch'
    help = 'Test batch runs'


class IUWMAllTestCommand(IUWMRunTestCommand):
    """
    Defines how to run tests
    """

    command = 'all'
    help = 'Run tests'


def main():

    # works when freezing with py2exe
    freeze_support()

    # arguments for simulating
    parser = argparse.ArgumentParser(description='Run tests on the Integrated Urban Water Management Model (IUWM).')

    # add sub parsers for commands
    sp = parser.add_subparsers(title='Commands')

    # list of commands to use parsers from
    commands = [
        IUWMRunTestCommand,
        IUWMSensitivityTestCommand,
        IUWMCalibrationTestCommand,
        IUWMBatchTestCommand,
        IUWMAllTestCommand,
        IUWMRestoreBackupCommand,
        IUWMResetCommand
    ]
    for cls in commands:
        cmd = cls()
        cmd.add_parser(sp)
        cmd.add_arguments()

    # arguments
    args = parser.parse_args()
    args.run(args)


if __name__ == '__main__':

    main()
