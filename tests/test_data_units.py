import unittest
from iuwm import inputs


class DataUnitsTestCase(unittest.TestCase):
    def test_something(self):
        u = inputs.units_from_label('min_temp (C)')
        self.assertEqual(u, ('min_temp', 'c'))

        u = inputs.units_from_label('min_temp__C__')
        self.assertEqual(u, ('min_temp', 'c'))

        u = inputs.units_from_label('min_temp')
        self.assertEqual(u, ('min_temp', None))

        u = inputs.units_from_label('min_temp(C)')
        self.assertEqual(u, ('min_temp', 'c'))

        u = inputs.units_from_label('min_temp__KELVIN__')
        self.assertEqual(u, ('min_temp', 'kelvin'))


if __name__ == '__main__':
    unittest.main()
