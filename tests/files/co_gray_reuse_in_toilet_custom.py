# --------------------------------------------------------------------------- #
#                                                                             #
# Integrated Urban Water Model (IUWM)                                         #
#     Forecast urban water demands driven by land, climate, and technology    #
#     Defer expensive infrastructure investments                              #
#                                                                             #
# Authors:                                                                    #
#     Andre Dozier (andre.dozier@colostate.edu)                               #
#     Brad Reichel                                                            #
#     Sybil Sharvelle                                                         #
#     Larry Roesner                                                           #
#     Mazdak Arabi                                                            #
#                                                                             #
# The Integrated Urban Water Model has been developed by Colorado State       #
# University and is copyrighted; however, code is open-source so that         #
# users may examine and modify the code to suit their specific application    #
# needs, subject to the conditions below.                                     #
#                                                                             #
# Copyright 2018 Colorado State University                                    #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
#     http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
#                                                                             #
# --------------------------------------------------------------------------- #


def on_graywater_calculated(model, **kwargs):

    # get parameters from the model
    wth = model.get_param('weather')
    custom_percent = model.get_param('custom_graywater_percent', 0.25)

    # get other intermediate data
    indoor = model.data.indoor
    graywater = model.data.graywater

    # calculate new values
    new_graywater = indoor.demand * custom_percent
    # graywater.supply += new_graywater
    # for c in indoor.components:
    #     f = c.gray_produced / indoor.gray_produced
    #     a = new_graywater * f
    #     c.gray_produced += a
    #
    #     # to maintain mass balance... these other things also this new use / supply applied to them
    #     c.total_use += a
    #     c.demand += a
    #     c.nonconsumed += a

    # you can add custom values directly to model.data available for output by name
    if 'print_count' not in model.inputs:
        model.inputs.print_count = 0
    model.inputs.print_count += 1
    if model.inputs.print_count % 50 == 0:
        print('{0:>6.1f} {1:>6.1f}'.format(wth.temp_min.mean(), graywater.outflow.mean()))

    # add custom output to the model this way too... these variables will be found using the output variable name:
    #     user.<variable_name>
    # for example, the variable below will be available at user.graywater_extra
    model.add_output('graywater_extra', new_graywater)


def register_inputs(model, **kwargs):
    model.registered_inputs.add('custom_graywater_percent')


def subscribe(model):
    """
    Subscribes this custom module to the model by calling model.subscribe
    and optionally name the event to subscribe to.

    :param model: The core IUWM model
    """

    # register inputs so that warning is not displayed for custom inputs you use
    model.subscribe(register_inputs, event_name='on_inputs_registered')

    # subscribe custom functions to events within the model to perform custom actions during model solution
    model.subscribe(on_graywater_calculated, event_name='on_graywater_calculated')

    # OR:
    # model.subscribe_function('on_graywater_calculated', on_graywater_calculated)

    # OR if the name of the function matches the event_name:
    # model.subscribe(on_graywater_calculated)
