from iuwm.alternative_sources import ResidentialSourceWaterRecycler
from iuwm.demands import UrbanDemand


def on_outdoor_calculated(model, **kwargs):
    # uncomment the line below to see what variables are available 
    # print kwargs.keys() 
    area = model.get_param('non_constructed_area')
    washing_l_m2 = model.get_param('washing_l_m2', default_value=2.1 * 4 / 30.5)
    washing = area * washing_l_m2 / 3.79 * 43560.0 / (3.281 ** 2)
    model.data.outdoor.add (UrbanDemand ("washing", washing, consumed=1, black=0, gray=0))


class OutdoorWashingWaterRecycler(ResidentialSourceWaterRecycler):

    end_uses = ResidentialSourceWaterRecycler.end_uses + [
        "outdoor_washing",
        "outdoor_combwf"
    ]

    def list_end_use_demands(self):

        flushing = self.model.data.indoor.toilet
        washing = self.model.data.outdoor.washing
        return ResidentialSourceWaterRecycler.list_end_use_demands(self) + [
            washing,
            [flushing, washing]            
        ]


def subscribe(model):

    model.registered_alternative_uses['res'] = OutdoorWashingWaterRecycler
    model.subscribe(on_outdoor_calculated)
